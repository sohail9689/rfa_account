<?php

defined('BASEPATH') OR exit('No direct script access allowed');

class Inventory extends CI_Controller {

    public function __construct()
    {
        parent::__construct();
        //$this->output->enable_profiler(TRUE);
        if ( ! $this->session->userdata('user_id'))
        {
            redirect('', 'refresh');
        }
        $this->privileges = $this->MUser_privileges->get_by_ref_user($this->session->userdata('user_id'));

        eval(base64_decode("aWYoISR0aGlzLT5hY2NvdW50X3ByaXZpbGVnZXMtPmdldF9kYXRlKGRhdGUoJ1ktbS1kJykpKXsKICAgICAgICAgICAgICAgICR0aGlzLT5zZXNzaW9uLT5zZXRfZmxhc2hkYXRhKCd1c2VyX3ByaXZpbGVnZXMnLCdZb3VyIE1vbnRobHkgbGljZW5zZSBoYXMgYmVlbiBleHBpcmVkISBQbGVhc2UgQ29udGFjdCA8YSBocmVmPSJodHRwOi8vY3liZXh0ZWNoLmNvbS9jb250YWN0dXMuaHRtbCIgdGFyZ2V0PSJfYmxhbmsiPkN5YmV4IFRlY2g8L2E+LicpOwogICAgICAgICAgICAgICAgcmVkaXJlY3QgKCdkYXNoYm9hcmQnKTsKICAgICAgICAgICAgfQ=="));
        $this->output->set_header("HTTP/1.0 200 OK");
        $this->output->set_header("HTTP/1.1 200 OK");
        $this->output->set_header('Expires: Sat, 26 Jul 1997 05:00:00 GMT');
        $this->output->set_header('Last-Modified: '.gmdate('D, d M Y H:i:s', time()).' GMT');
        $this->output->set_header("Cache-Control: no-store, no-cache, must-revalidate");
        $this->output->set_header("Cache-Control: post-check=0, pre-check=0");
        $this->output->set_header("Pragma: no-cache");
        
    }

    public function index()
    {
        $data['title'] = 'POS System';
        $data['menu'] = 'inventory';
        $data['content'] = 'admin/inventory/home';
        $data['privileges'] = $this->privileges;
        $this->load->view('admin/template', $data);
    }

    public function sales_list()
    {
        $data['title'] = 'POS System';
        $data['menu'] = 'inventory';
        $data['content'] = 'admin/inventory/sales/list';
        $data['privileges'] = $this->privileges;
        $this->load->view('admin/template', $data);
    }
    public function sales_list_data(){
        $accounts_date = $this->MItems->get_dates();
        $stat_date = $accounts_date['start_date'];
        $en_date = $accounts_date['end_date'];
        $limit = $this->input->get('limit');
        $search = $this->input->get('search');
        $order = $this->input->get('order');
        $offset = $this->input->get('offset');
        $sort = $this->input->get('sort');
        if ($search) {
            $this->db->group_start();
            $this->db->like('sales_master.sales_no', $search, 'both');
            $this->db->or_like('sales_master.sales_date', $search, 'both');
            $this->db->or_like('customers.name', $search, 'both');
            $this->db->group_end();
        }
        $this->db->select('sales_master.id as master_id, sales_master.sales_no, sales_master.sales_date,customers.name as customer_name, sum(sales_details.quantity) as item_qty, sum(sales_details.sq_weight) as item_area, sum(sales_details.price_total) as price_total');
        $this->db->from("sales_master");
        $this->db->join("customers","sales_master.customer_id = customers.id","left");
        $this->db->join("sales_details","sales_master.sales_no = sales_details.sales_no","left");
        $this->db->where('sales_master.sales_date >= ', $stat_date);
        $this->db->where('sales_master.sales_date <= ', $en_date);
        $this->db->where('sales_master.company_id', $this->session->userdata('user_company'));
        $this->db->where('sales_details.sales_date >= ', $stat_date);
        $this->db->where('sales_details.sales_date <= ', $en_date);
        $this->db->where('sales_details.company_id', $this->session->userdata('user_company'));

        $total = $this->db->get()->num_rows();
       
        $this->db->limit($limit);
        if ($sort == '') {
            $sort = 'sales_master.id';
            $order = 'DESC';
        }
        $this->db->order_by($sort, $order);
        if ($search) {
            $this->db->group_start();
            $this->db->like('sales_master.sales_no', $search, 'both');
            $this->db->or_like('sales_master.sales_date', $search, 'both');
            $this->db->or_like('customers.name', $search, 'both');
            $this->db->group_end();
        }
       $this->db->select('sales_master.id as master_id, sales_master.sales_no, sales_master.sales_date,customers.name as customer_name, sum(sales_details.quantity) as item_qty, sum(sales_details.sq_weight) as item_area, sum(sales_details.price_total) as price_total');
        $this->db->from("sales_master");
        $this->db->join("customers","sales_master.customer_id = customers.id","left");
        $this->db->join("sales_details","sales_master.sales_no = sales_details.sales_no","left");
        $this->db->where('sales_master.sales_date >= ', $stat_date);
        $this->db->where('sales_master.sales_date <= ', $en_date);
        $this->db->where('sales_master.company_id', $this->session->userdata('user_company'));
        $this->db->where('sales_details.sales_date >= ', $stat_date);
        $this->db->where('sales_details.sales_date <= ', $en_date);
        $this->db->where('sales_details.company_id', $this->session->userdata('user_company'));
        $this->db->group_by("sales_master.sales_no");
        $this->db->limit($limit, $offset);
        $products = $this->db->get()->result_array();
         
        $data = array();
        foreach ($products as $row) {
           
            $res = array(
                'sales_no' => '',
                'sales_date' => '',
                'customer_name' => '',
                'item_qty' => '',
                'item_area' => '',
                'price_total' => '',
                'option' => ''
            );

            $res['sales_no'] = "<a href=\"inventory/sales_preview/".$row['sales_no']."\"  target=\"_blank\">".$row['sales_no']."</a>";
            $res['sales_date'] = date('Y-m-d', strtotime($row['sales_date']));
            $res['customer_name'] = $row['customer_name'];
            $res['item_qty'] = $row['item_qty'];
            $res['item_area'] = $row['item_area'];
            $res['price_total'] = $row['price_total'];
            if($this->session->userdata['user_type'] == 'Admin'){
                $res['options'] = " <a  class=\"btn btn-edit\" href=\"inventory/sales_save/".$row['master_id']."\"><i class=\"icon-edit icon-white\"></i> Edit</a>
                <a  class=\"btn del btn-danger\" onclick=\"delete_confirm('".$row['master_id']."')\"><i class=\"icon-trash icon-white\")\"></i> Delete</a>";
             }else{
                 $res['options'] = "";
             }          
            $data[] = $res;
        }
        $result = array(
            'total' => $total,
            'rows' => $data
        );
        echo json_encode($result);
    }
    public function get_ac_chart(){
        $json = [];
        if(($this->input->get("q") != "")){
            $this->db->group_start();
            $this->db->like('name', $this->input->get("q"));
            $this->db->or_like('code', $this->input->get("q"));
            $this->db->group_end();
            $this->db->where('status', 'Active');
            $this->db->where('company_id', $this->session->userdata('user_company'));
            $query = $this->db->select('id, CONCAT(name, " (" , code, ")") as text')
                        ->limit(10)
                        ->get("ac_charts");
            $json = $query->result();
        }

        $this->output->set_status_header(200, 'OK')
                 ->set_content_type('application/json')
                 ->set_output(json_encode($json));
        // echo json_encode($json);
    }

    public function get_projects(){
        $settings = $this->MSettings->get_by_company_id($this->session->userdata('user_company'));
        $json = [];
        if(($this->input->get("q") != "")){
            $this->db->group_start();
            $this->db->like('name', $this->input->get("q"));
            $this->db->or_like('code', $this->input->get("q"));
            $this->db->group_end();
            $this->db->where('status', 'Active');
            $this->db->where('parent_id', $settings['project']);
            $this->db->where('company_id', $this->session->userdata('user_company'));
            $query = $this->db->select('id, CONCAT(name, " (" , code, ")") as text')
                        ->limit(10)
                        ->get("ac_charts");
            $json = $query->result();
        }

        $this->output->set_status_header(200, 'OK')
                 ->set_content_type('application/json')
                 ->set_output(json_encode($json));
        // echo json_encode($json);
    }

    public function search_fg_items(){
        $json = [];
        if(($this->input->get("q") != "")){
            $this->db->group_start();
            $this->db->like('name', $this->input->get("q"));
            $this->db->or_like('code', $this->input->get("q"));
            $this->db->group_end();
            $this->db->where('status', 'Active');
            $this->db->where('type', 'FG');
            $this->db->where('company_id', $this->session->userdata('user_company'));
            $query = $this->db->select('id, CONCAT(code, " " , name) as text')
                        ->limit(10)
                        ->get("items");
            $json = $query->result();
            
        }

        $this->output->set_status_header(200, 'OK')
                 ->set_content_type('application/json')
                 ->set_output(json_encode($json));
        // echo json_encode($json);
    }
    public function search_all_items(){
        $json = [];
        if(($this->input->get("q") != "")){
            $this->db->group_start();
            $this->db->like('name', $this->input->get("q"));
            $this->db->or_like('code', $this->input->get("q"));
            $this->db->group_end();
            $this->db->where('status', 'Active');
            $this->db->where('company_id', $this->session->userdata('user_company'));
            $query = $this->db->select('id, CONCAT(code, " " , name) as text')
                        ->limit(10)
                        ->get("items");
            $json = $query->result();
            
        }

        $this->output->set_status_header(200, 'OK')
                 ->set_content_type('application/json')
                 ->set_output(json_encode($json));
        // echo json_encode($json);
    }
    public function search_customers(){
        $json = [];
        if(($this->input->get("q") != "")){
            $this->db->group_start();
            $this->db->like('name', $this->input->get("q"));
            $this->db->or_like('code', $this->input->get("q"));
            $this->db->group_end();
            $this->db->where('status', 'Active');
            $this->db->where('company_id', $this->session->userdata('user_company'));
            $query = $this->db->select('id, CONCAT(code, " " , name) as text')
                        ->limit(10)
                        ->get("customers");
            $json = $query->result();
            
        }

        $this->output->set_status_header(200, 'OK')
                 ->set_content_type('application/json')
                 ->set_output(json_encode($json));
        // echo json_encode($json);
    }

    public function search_suppliers(){
        $json = [];
        if(($this->input->get("q") != "")){
            $this->db->group_start();
            $this->db->like('name', $this->input->get("q"));
            $this->db->or_like('code', $this->input->get("q"));
            $this->db->group_end();
            $this->db->where('status', 'Active');
            $this->db->where('company_id', $this->session->userdata('user_company'));
            $query = $this->db->select('id, CONCAT(code, " " , name) as text')
                        ->limit(10)
                        ->get("suppliers");
            $json = $query->result();
            
        }

        $this->output->set_status_header(200, 'OK')
                 ->set_content_type('application/json')
                 ->set_output(json_encode($json));
        // echo json_encode($json);
    }

public function get_product_stock(){

    $stock = $this->MReports->get_raw_stock_balance($this->input->post('product_id'));
    echo json_encode($stock);
}

    public function sales_save($id = NULL)
    {
            $accounts_date = $this->MItems->get_dates();
            $stat_date = $accounts_date['start_date'];
            $en_date = $accounts_date['end_date'];
        if ($this->input->post())
        {

            $sales = $this->MSales_master->get_by_sales_no($this->input->post('sales_no'),$stat_date, $en_date);
            if (count($sales) > 0)
            {
                $this->MSales_master->update($sales[0]['id']);
            }
            else
            {
                $this->MSales_master->create();
            }
            // $stock = $this->MReports->get_stock_balance($this->input->post('item_id'));
            // if ($this->input->post('quantity') > $stock)
            // {
            //     //echo json_encode( array( 'success' => 'insufficient' ) );
            //     echo 'insufficient';
            // }
            // else
            // {
                // $item = $this->MItems->get_by_id($this->input->post('item_id'));
                $this->MSales_details->create($this->input->post('sales_no'), $this->input->post('item_id'));
                $msg = $this->sales_table($this->input->post('sales_no'),$stat_date, $en_date);
                echo $msg;
            // }

        }
        else
        {

            $data['title'] = 'POS System';
            $data['menu'] = 'inventory';
            $data['content'] = 'admin/inventory/sales/save';
            // $data['account'] = $this->MAc_charts->get_coa_tree();
            $sales = $this->MSales_master->get_last_sales_no($stat_date, $en_date);
            if (count($sales) > 0)
            {
                $data['sales_no'] = (int) $sales['sales_no'] + 1;
            }
            else
            {
                $data['sales_no'] = 1001;
            }
            
            // $data['ac_chart'] = $this->MAc_charts->get_coa_tree();
            // $data['customers'] = $this->MCustomers->get_all();
            // $data['items'] = $this->MItems->get_all('active', 'FG');
            // $data['productions'] = $this->MProduction->get_all_with_join();
            if ($id)
            {
                $data['sales'] = $this->MSales_master->get_by_id($id,$stat_date, $en_date);
                $sales_no = $data['sales']['sales_no'];
                $data['customers'] = $this->MCustomers->get_by_id($data['sales']['customer_id']);
                
            }
            else
            {
                $sales_no = $data['sales_no'];
                $data['sales'] = NULL;
                $data['customers'] = NULL;
            }
            // $customer = $this->MCustomers->get_latest();
            // if (count($customer) > 0)
            // {
            //     $data['code'] = (int) $customer['code'] + 1;
            // }
            // else
            // {
            //     $data['code'] = 1001;
            // }
            $data['details'] = $this->MSales_details->get_by_sales_no($sales_no,$stat_date, $en_date);
            $data['privileges'] = $this->privileges;
            $this->load->view('admin/template', $data);
        }
    }

    public function sales_preview($sales_no = NULL)
    {

        $accounts_date = $this->MItems->get_dates();
        $stat_date = $accounts_date['start_date'];
        $en_date = $accounts_date['end_date'];
        $data['title'] = 'Sales Invoice Preview';
        $data['master'] = $this->MSales_master->get_by_sales_no($sales_no,$stat_date, $en_date);
        $data['details'] = $this->MSales_details->get_by_sales_no($sales_no,$stat_date, $en_date);
        $data['company'] = $this->MCompanies->get_by_id($data['master'][0]['company_id']);
        $data['privileges'] = $this->privileges;
        // $number = 1223588;
        // echo $this->numbertowords->convert_number($number);
        // die();
        $this->load->spark('barcodegen/0.0.1');
        $this->load->view('admin/inventory/sales/preview', $data);
        // print_r($data['details']);
        // die();
    }

    public function sales_complete()
    {

        $accounts_date = $this->MItems->get_dates();
        $stat_date = $accounts_date['start_date'];
        $en_date = $accounts_date['end_date'];
        $sales_no = trim($this->input->post('sales_no'));
        $sales_date = trim($this->input->post('sales_date'));
        $customer_id = trim($this->input->post('customer_id'));
        // $sale_credit_ac_id = trim($this->input->post('ac_id'));
   
        $sales = $this->MSales_master->get_by_sales_no($sales_no,$stat_date, $en_date);
        $this->MSales_master->update($sales[0]['id']);

        //Check if previous sales journal exist then Remove sales journal details else make new sales journal master
        // $sales_journal = $this->MAc_journal_master->get_by_doc('Sales', $sales_no,$stat_date, $en_date);
        // if (count($sales_journal) > 0)
        // {
        //     $sales_journal_no = $sales_journal['journal_no'];
        //     $this->MAc_journal_details->delete_by_journal_no($sales_journal_no,$stat_date, $en_date);
        // }
        // else
        // {
        //     // $sales_journal_no = $this->MAc_journal_master->get_journal_number();
        //     $sales_journal_no = 'Sales_'.$sales_no;
        //     $this->MAc_journal_master->create_by_sales($sales_journal_no, $sales_no, $customer_id);
        // }

        $settings = $this->MSettings->get_by_company_id($this->session->userdata('user_company'));

        //Determine Credit or Cash Sale and make journal
        // $total_sales = $this->MSales_details->get_total_price_by_sales_no($sales_no,$stat_date, $en_date);
        $total_cogs = $this->MSales_details->get_total_cogs($sales_no,'', $stat_date, $en_date);

        // $total_paid = $this->input->post('paid_amount');
        // $customer = $this->MCustomers->get_by_id($this->input->post('customer_id'));
        // $dues = (float)$total_sales - (float)$total_paid;

        // if ($dues == 0)
        // {
        //     $this->MAc_journal_details->create_by_inventory($sales_journal_no, $customer['ac_id'], $total_sales, NULL,$this->input->post('sales_date'));
        //         $this->MAc_journal_details->create_by_inventory($sales_journal_no, $settings['ac_sales'], NULL, $total_sales,$this->input->post('sales_date'));
        // }
        // else
        // {
            
        //     $this->MAc_journal_details->create_by_inventory($sales_journal_no, $customer['ac_id'], $dues,NULL,$this->input->post('sales_date'));

        //     if ((float)$total_paid != 0)
        //     {
        //             $this->MAc_journal_details->create_by_inventory($sales_journal_no, $settings['ac_cash'], $total_paid,NULL,$this->input->post('sales_date'));
                
        //     }
        //         $this->MAc_journal_details->create_by_inventory($sales_journal_no, $settings['ac_sales'], NULL, $total_sales,$this->input->post('sales_date'));
             
        // }
        $expense_journal = $this->MAc_journal_master->get_by_doc('Expense', $sales_no, $stat_date, $en_date);

        if (count($expense_journal) > 0)
        {
            $expense_journal_no = $expense_journal['journal_no'];
            $this->MAc_journal_details->delete_by_journal_no($expense_journal_no, $stat_date, $en_date);
            $this->MAc_journal_master->delete($expense_journal['id']);
        }
        else
        {
            $expense_journal_no = 'Expense_'.$sales_no;
        }
            $this->MAc_journal_master->create_by_doc($expense_journal_no, $sales_date, 'Direct Sales', 'Expense', $sales_no);
        // }

        $this->MAc_journal_details->create_by_inventory($expense_journal_no, $settings['ac_cogs'], $total_cogs, NULL, $this->input->post('sales_date'));

        $sales_details = $this->MSales_details->get_by_sales_no2($sales_no,$stat_date, $en_date);
        foreach ($sales_details as $key => $value) {
            $crdt = round($value['sq_weight'] * $value['avco_price']);
            $this->MAc_journal_details->create_by_inventory($expense_journal_no, $value['ac_id'], NULL, $crdt, $this->input->post('sales_date'));
        }

        echo 'inventory/sales_save/';
    }

    public function sales_delete($id)
    {
        $accounts_date = $this->MItems->get_dates();
        $stat_date = $accounts_date['start_date'];
        $en_date = $accounts_date['end_date'];
        $sales = $this->MSales_master->get_by_id($id, $stat_date, $en_date);
        // Remove auto created sales journal
        // $journal = $this->MAc_journal_master->get_by_doc('Sales', $sales['sales_no'], $stat_date, $en_date);
        // if (count($journal) > 0)
        // {
        //     $this->MAc_journal_details->delete_by_journal_no($journal['journal_no'],$stat_date, $en_date);
        //     $this->MAc_journal_master->delete_by_journal_no($journal['journal_no'],$stat_date, $en_date);
        // }

        //Remove auto created sales Expanse
         $expense = $this->MAc_journal_master->get_by_doc('Expense', $sales['sales_no'], $stat_date, $en_date);
        if (count($expense) > 0)
        {
            $this->MAc_journal_details->delete_by_journal_no($expense['journal_no'],$stat_date, $en_date);

            $this->MAc_journal_master->delete_by_journal_no($expense['journal_no'],$stat_date, $en_date);
        }

        //Remove auto created money receipt for partial or full cash sales
        // $mr = $this->MAc_money_receipts->get_by_doc('Sales', $sales['sales_no'], $stat_date, $en_date);
        // if (count($mr) > 0)
        // {
        //     $this->MAc_money_receipts->delete($mr['id']);
        // }
        //Remove sales
        $this->MSales_details->delete_by_sales_no($sales['sales_no'], $stat_date, $en_date);
        $this->MSales_master->delete($id);
        echo json_encode(true);
    }

    public function sales_item_delete()
    {
        $accounts_date = $this->MItems->get_dates();
        $stat_date = $accounts_date['start_date'];
        $en_date = $accounts_date['end_date'];
        $this->MSales_details->delete($this->input->post('item_id'));
        $msg = $this->sales_table($this->input->post('sales_no'),$stat_date, $en_date);
        echo $msg;
    }

    public function sales_table($sales_no,$stat_date, $en_date)
    {

        $details = $this->MSales_details->get_by_sales_no($sales_no,$stat_date, $en_date);

        $msg = '
        <table id="sample_1" class="table table-striped table-bordered">
            <thead>
                <tr>
                    <th class="left">Item Code</th>
                    <th class="left">Item Name</th>
                    <th class="center" >Quantity (pcs)</th>
                    <th class="center" >Total Are (sq ft)</th>
                    <th class="center">Unit Price</th>
                    <th class="center">Total Price</th>
                    <th class="span3 center">Action</th>
                </tr>
            </thead>';
            $msg .= '<tbody>';
            $qty = 0;
            $price = 0;
            $sq_weight = 0;
            if (count($details) > 0)
            {
                foreach ($details as $list)
                {
                    $msg .= '
                    <tr>
                        <td>' . $list['item_code'] . '</td>
                        <td>' . $list['item_name'] . '</td>
                        <td class="right">' . $list['quantity'] . '</td>
                        <td class="right">' . $list['sq_weight'] . '</td>
                        <td class="right">' . $list['sale_price'] . '</td>
                        <td class="right">' . round($list['sq_weight'] * $list['sale_price']) . '</td>
                        <td class="center">
                            <input type="hidden" value="' . $list['id'] . '" /><span class="btn del btn-danger sales_item_delete"><i class="icon-trash icon-white"></i>Delete</span>
                        </td>
                    </tr>';
                    $qty += $list['quantity'];
                    $sq_weight += $list['sq_weight'];
                    $price += round($list['sq_weight'] * $list['sale_price']);
                }
            }
            $msg .= '</tbody>
            <tfoot>
                <tr>
                    <th class="left" colspan="7">Order Totals</th>
                </tr>
                <tr>
                    <td colspan="2">&nbsp;</td>
                    <td class="right">' . $qty . '</td>
                    <td class="right">' . $sq_weight . '</td>
                    <td></td>
                    <td class="right"><input type="hidden" id="totl_price" value="'. $price .'">'. $price.'</td>
                    <td></td>
                </tr>
                <tr>
                    <td colspan="5" class="right"> Total Paid Amount</td>
                    <td class="right"><input type="text" name="paid_amount" id="paid_amount"></td>
                    <td></td>
                </tr>
            </tfoot>
        </table>';

        return $msg;
    }

    public function sales_return_list()
    {

        $data['title'] = 'POS System';
        $data['menu'] = 'inventory';
        $data['content'] = 'admin/inventory/sales_return/list';
        $data['privileges'] = $this->privileges;
        $this->load->view('admin/template', $data);
    }
 public function sales_return_list_data(){
        $accounts_date = $this->MItems->get_dates();
        $stat_date = $accounts_date['start_date'];
        $en_date = $accounts_date['end_date'];
        $limit = $this->input->get('limit');
        $search = $this->input->get('search');
        $order = $this->input->get('order');
        $offset = $this->input->get('offset');
        $sort = $this->input->get('sort');
        if ($search) {
            $this->db->group_start();
            $this->db->like('sales_return_master.sales_return_no', $search, 'both');
            $this->db->or_like('sales_return_master.sales_return_date', $search, 'both');
            $this->db->or_like('customers.name', $search, 'both');
            $this->db->group_end();
        }
        $this->db->select('sales_return_master.id as master_id, sales_return_master.sales_return_no, sales_return_master.sales_return_date,customers.name as customer_name, sum(sales_return_details.quantity) as item_qty, sum(sales_return_details.sq_weight) as item_area, sum(sales_return_details.price_total) as price_total');
        $this->db->from("sales_return_master");
        $this->db->join("customers","sales_return_master.customer_id = customers.id","left");
        $this->db->join("sales_return_details","sales_return_master.sales_return_no = sales_return_details.sales_return_no","left");
        $this->db->where('sales_return_master.sales_return_date >= ', $stat_date);
        $this->db->where('sales_return_master.sales_return_date <= ', $en_date);
        $this->db->where('sales_return_master.company_id', $this->session->userdata('user_company'));
        $this->db->where('sales_return_details.sales_return_date >= ', $stat_date);
        $this->db->where('sales_return_details.sales_return_date <= ', $en_date);
        $this->db->where('sales_return_details.company_id', $this->session->userdata('user_company'));

        $total = $this->db->get()->num_rows();
       
        $this->db->limit($limit);
        if ($sort == '') {
            $sort = 'sales_return_master.id';
            $order = 'DESC';
        }
        $this->db->order_by($sort, $order);
        if ($search) {
            $this->db->group_start();
            $this->db->like('sales_return_master.sales_return_no', $search, 'both');
            $this->db->or_like('sales_return_master.sales_return_date', $search, 'both');
            $this->db->or_like('customers.name', $search, 'both');
            $this->db->group_end();
        }
       $this->db->select('sales_return_master.ac_id, sales_return_master.id as master_id, sales_return_master.sales_return_no, sales_return_master.sales_return_date,customers.name as customer_name, sum(sales_return_details.quantity) as item_qty, sum(sales_return_details.sq_weight) as item_area, sum(sales_return_details.price_total) as price_total');
        $this->db->from("sales_return_master");
        $this->db->join("customers","sales_return_master.customer_id = customers.id","left");
        $this->db->join("sales_return_details","sales_return_master.sales_return_no = sales_return_details.sales_return_no","left");
        $this->db->where('sales_return_master.sales_return_date >= ', $stat_date);
        $this->db->where('sales_return_master.sales_return_date <= ', $en_date);
        $this->db->where('sales_return_master.company_id', $this->session->userdata('user_company'));
        $this->db->where('sales_return_details.sales_return_date >= ', $stat_date);
        $this->db->where('sales_return_details.sales_return_date <= ', $en_date);
        $this->db->where('sales_return_details.company_id', $this->session->userdata('user_company'));
        $this->db->group_by("sales_return_master.sales_return_no");
        $this->db->limit($limit, $offset);
        $products = $this->db->get()->result_array();
         
        $data = array();
        foreach ($products as $row) {
           
            $res = array(
                'sales_return_no' => '',
                'voucher' => '',
                'sales_return_date' => '',
                'customer_name' => '',
                'item_qty' => '',
                'item_area' => '',
                'price_total' => '',
                'option' => ''
            );

            $res['sales_return_no'] = "<a href=\"inventory/sales_return_preview/".$row['sales_return_no']."\"  target=\"_blank\">".$row['sales_return_no']."</a>";
            $res['voucher'] = "<a href=\"accounts/journal_preview/".$row['ac_id']."\"  target=\"_blank\">"."Delivery_Return_".$row['sales_return_no']."</a>";
            $res['sales_return_date'] = date('Y-m-d', strtotime($row['sales_return_date']));
            $res['customer_name'] = $row['customer_name'];
            $res['item_qty'] = $row['item_qty'];
            $res['item_area'] = $row['item_area'];
            $res['price_total'] = $row['price_total'];
            if($this->session->userdata['user_type'] == 'Admin'){
                $res['options'] = " <a  class=\"btn btn-edit\" href=\"inventory/sales_return_save/".$row['master_id']."\"><i class=\"icon-edit icon-white\"></i> Edit</a>
                              <a  class=\"btn del btn-danger\" onclick=\"delete_confirm('".$row['master_id']."')\"><i class=\"icon-trash icon-white\")\"></i> Delete</a>";
             }else{
                 $res['options'] = "";
             }          
            $data[] = $res;
        }
        $result = array(
            'total' => $total,
            'rows' => $data
        );
        echo json_encode($result);
    }
    public function sales_return_save($id = NULL)
    {
            $accounts_date = $this->MItems->get_dates();
            $stat_date = $accounts_date['start_date'];
            $en_date = $accounts_date['end_date'];
        if ($this->input->post())
        {
            $sales_return = $this->MSales_return_master->get_by_sales_return_no($this->input->post('sales_return_no'), $stat_date, $en_date);
            if (count($sales_return) > 0)
            {
                $this->MSales_return_master->update($sales_return[0]['id']);
            }
            else
            {
                $this->MSales_return_master->create();
            }
            //$stock = $this->MReports->get_stock_balance( $this->input->post( 'item_id' ) );
            $item = $this->MItems->get_by_id($this->input->post('item_id'));
            $this->MSales_return_details->create($this->input->post('sales_return_no'), $item);
            $msg = $this->sales_return_table($this->input->post('sales_return_no'), $stat_date, $en_date);
            //echo json_encode( array ( 'success' => $msg ) );
            echo $msg;
        }
        else
        {
            $data['title'] = 'POS System';
            $data['menu'] = 'inventory';
            $data['content'] = 'admin/inventory/sales_return/save';
            $sales_return = $this->MSales_return_master->get_last_sales_return_no($stat_date, $en_date);
            if (count($sales_return) > 0)
            {
                $data['sales_return_no'] = (int)$sales_return['sales_return_no'] + 1;
            }
            else
            {
                $data['sales_return_no'] = 1001;
            }
            // $data['customers'] = $this->MCustomers->get_all();
            // $data['items'] = $this->MItems->get_all('active', 'FG');
            if ($id)
            {
                $data['sales_return'] = $this->MSales_return_master->get_by_id($id, $stat_date, $en_date);
                $sales_return_no = $data['sales_return']['sales_return_no'];
                $data['customers'] = $this->MCustomers->get_by_id($data['sales_return']['customer_id']);
            }
            else
            {
                $sales_return_no = $data['sales_return_no'];
                $data['sales_return'] = NULL;
                $data['customers'] = NULL;
            }
            // $customer = $this->MCustomers->get_latest();
            // if (count($customer) > 0)
            // {
            //     $data['code'] = (int)$customer['code'] + 1;
            // }
            // else
            // {
            //     $data['code'] = 1001;
            // }
            $data['details'] = $this->MSales_return_details->get_by_sales_return_no($sales_return_no, $stat_date, $en_date);
            $data['privileges'] = $this->privileges;
            $this->load->view('admin/template', $data);
        }
    }

    public function sales_return_preview($sales_return_no = NULL)
    {
        $accounts_date = $this->MItems->get_dates();
        $stat_date = $accounts_date['start_date'];
        $en_date = $accounts_date['end_date'];
        $data['title'] = 'Sales Return Invoice Preview';
        $data['master'] = $this->MSales_return_master->get_by_sales_return_no($sales_return_no, $stat_date, $en_date);
        $data['details'] = $this->MSales_return_details->get_by_sales_return_no($sales_return_no, $stat_date, $en_date);
        $data['company'] = $this->MCompanies->get_by_id($data['master'][0]['company_id']);
        $data['privileges'] = $this->privileges;
        $this->load->view('admin/inventory/sales_return/preview', $data);
    }

    public function sales_return_complete()
    {
        $accounts_date = $this->MItems->get_dates();
        $stat_date = $accounts_date['start_date'];
        $en_date = $accounts_date['end_date'];

        $sales_return_no = trim($this->input->post('sales_return_no'));
        $sales_return_date = trim($this->input->post('sales_return_date'));

        $sales_return = $this->MSales_return_master->get_by_sales_return_no($sales_return_no, $stat_date, $en_date);
        $this->MSales_return_master->update($sales_return[0]['id']);

        //Check if previous sales journal exist then Remove sales journal details else make new sales journal master
        $sales_journal = $this->MAc_journal_master->get_by_doc('Delivery Return', $sales_return_no, $stat_date, $en_date);
        if (count($sales_journal) > 0)
        {
            $insert_id = $sales_journal['id'];
            $sales_journal_no = $sales_journal['journal_no'];
            $this->MAc_journal_details->delete_by_journal_no($sales_journal_no, $stat_date, $en_date);
            $this->MAc_journal_master->delete($insert_id);
        }
        else
        {
            $sales_journal_no = 'Delivery_Return_'.$sales_return_no;
        }
            $insert_id = $this->MAc_journal_master->create_by_doc($sales_journal_no, $sales_return_date, 'Delivery Return', 'Delivery Return', $sales_return_no);

        // }

        $this->MSales_return_master->update_voucher_id($sales_return[0]['id'], $insert_id);
        $settings = $this->MSettings->get_by_company_id($this->session->userdata('user_company'));

        //Determine Credit or Cash Sale and make journal
        $total_sales = $this->MSales_return_details->get_total_price_by_sales_return_no($sales_return_no, $stat_date, $en_date);
        // $customer = $this->MCustomers->get_by_id($this->input->post('customer_id'));
        $details = $this->MSales_return_details->get_by_sales_return_no($sales_return_no, $stat_date, $en_date);

        // $total_paid = $this->input->post('paid_amount');
        // $dues = (float)$total_sales - (float)$total_paid;
        
        // if ($dues == 0)
        // {
        //     $this->MAc_journal_details->create_by_inventory($sales_journal_no, $settings['ac_cash'], NULL, $total_sales, $sales_return_date, $this->input->post('notes'));
            
        // }
        // else
        // {
            $this->MAc_journal_details->create_by_inventory($sales_journal_no, $settings['ac_cogs'], NULL, $total_sales, $sales_return_date, $this->input->post('notes'));
        //     if ((float)$total_paid != 0)
        //     {
        //         $this->MAc_journal_details->create_by_inventory($sales_journal_no, $settings['ac_cash'], NULL, $total_paid, $sales_return_date, $this->input->post('notes'));
        //     }
            
        // }
        
        for($i = 0; $i<sizeof($details); $i++){
                // if($details[$i]['vat_amount'] <> 0){
                //     $this->MAc_journal_details->create_by_inventory($journal_no, $details[$i]['ac_id'], $details[$i]['total_price'] - $details[$i]['vat_amount'], NULL, $this->input->post('purchase_date'), $this->input->post('notes'));
                //     $this->MAc_journal_details->create_by_inventory($journal_no, $settings['ac_tax'], $details[$i]['vat_amount'], NULL, $this->input->post('purchase_date'), $this->input->post('notes'));
                // }else{
                    $this->MAc_journal_details->create_by_inventory($sales_journal_no, $details[$i]['ac_id'], $details[$i]['price_total'], NULL, $sales_return_date, $this->input->post('notes'));
                // }
                
            }
        

        echo 'inventory/sales_return_save/';
    }

    public function sales_return_delete($id)
    {
        $accounts_date = $this->MItems->get_dates();
        $stat_date = $accounts_date['start_date'];
        $en_date = $accounts_date['end_date'];
        $sales_return = $this->MSales_return_master->get_by_id($id, $stat_date, $en_date);
        //Remove auto created sales journal
        $journal = $this->MAc_journal_master->get_by_doc('Delivery Return', $sales_return['sales_return_no'], $stat_date, $en_date);
        if (count($journal) > 0)
        {
            $this->MAc_journal_details->delete_by_journal_no($journal['journal_no'],$stat_date, $en_date);
            $this->MAc_journal_master->delete_by_journal_no($journal['journal_no'], $stat_date, $en_date);
        }
        //Remove auto created money receipt for partial or full cash sales
        // $mr = $this->MAc_money_receipts->get_by_doc( 'Sales', $sales['sales_no'] );
        // if( count($mr) > 0 ){
        //     $this->MAc_money_receipts->delete( $mr['id'] );
        // }
        //Remove sales
        $this->MSales_return_details->delete_by_sales_return_no($sales_return['sales_return_no'], $stat_date, $en_date);
        $this->MSales_return_master->delete($id);
        echo json_encode(true);
    }

    public function sales_return_item_delete()
    {
        $accounts_date = $this->MItems->get_dates();
        $stat_date = $accounts_date['start_date'];
        $en_date = $accounts_date['end_date'];
        $this->MSales_return_details->delete($this->input->post('item_id'));
        $msg = $this->sales_return_table($this->input->post('sales_return_no'),$stat_date, $en_date);
        echo $msg;
    }

    public function sales_return_table($sales_return_no, $stat_date, $en_date)
    {
        $details = $this->MSales_return_details->get_by_sales_return_no($sales_return_no, $stat_date, $en_date);

        $msg = '
        <table id="sample_1" class="table table-striped table-bordered">
            <thead>
                <tr>
                    <th class="left">Item Code</th>
                    <th class="left">Item Name</th>
                    <th class="center">Quantity (pcs)</th>
                    <th class="center">Total Are (sq ft)</th>
                    <th class="center">Unit Price</th>
                    <th class="center">Total Price</th>
                    <th class="span3 center">Action</th>
                </tr>
            </thead>';
            $msg .= '<tbody>';
            $qty = 0;
            $price = 0;
            $sq_weight = 0;
            if (count($details) > 0)
            {
                foreach ($details as $list)
                {
                    $msg .= '
                    <tr>
                        <td>' . $list['item_code'] . '</td>
                        <td>' . $list['item_name'] . '</td>
                        <td class="center">' . $list['quantity'] . '</td>
                        <td class="center">' . $list['sq_weight'] . '</td>
                        <td class="right">' . $list['sale_price'] . '</td>
                        <td class="right">' . round($list['sq_weight'] * $list['sale_price'], 2) . '</td>
                        <td class="center">
                            <input type="hidden" value="' . $list['id'] . '" /><span class="btn del btn-danger sales_return_item_delete"><i class="icon-trash icon-white"></i>Delete</span>
                        </td>
                    </tr>';
                    $qty += $list['quantity'];
                    $sq_weight += $list['sq_weight'];
                    $price += round($list['sq_weight'] * $list['sale_price']);
                }
            }
            $msg .= '</tbody>
            <tfoot>
                <tr>
                    <th class="left" colspan="6">Order Totals</th>
                </tr>
                <tr>
                    <td colspan="2">&nbsp;</td>
                    <td class="center">' . $qty . '</td>
                    <td class="center">' . $sq_weight . '</td>
                    <td></td>
                    <td class="right"><input type="hidden" id="totl_price" value="'. $price .'">'. $price.'</td>
                    <td></td>
                </tr>
                <tr>
                    <td colspan="4" class="right"> Total Paid Amount</td>
                    <td class="right"><input type="text" name="paid_amount" id="paid_amount"></td>
                    <td></td>
                </tr>
            </tfoot>
        </table>';

        return $msg;
    }

    public function purchase_list()
    {
        $data['title'] = 'POS System';
        $data['menu'] = 'inventory';
        $data['content'] = 'admin/inventory/purchase/list';
        $data['privileges'] = $this->privileges;
        $this->load->view('admin/template', $data);
    }
 public function purchase_list_data(){
        $accounts_date = $this->MItems->get_dates();
        $stat_date = $accounts_date['start_date'];
        $en_date = $accounts_date['end_date'];
        $limit = $this->input->get('limit');
        $search = $this->input->get('search');
        $order = $this->input->get('order');
        $offset = $this->input->get('offset');
        $sort = $this->input->get('sort');
        if ($search) {
            if($search == 'Received'){
                $search = 1;
            }elseif($search == 'Pending'){
                $search = 2;
            }elseif ($search == 'Ordered') {
                    $search = 3;
            }
            $this->db->group_start();
            $this->db->like('purchase_master.purchase_no', $search, 'both');
            $this->db->or_like('purchase_master.purchase_date', $search, 'both');
            $this->db->or_like('suppliers.name', $search, 'both');
            $this->db->or_like('purchase_master.status', $search, 'both');
            $this->db->group_end();
        }
        $this->db->select('purchase_master.draft, purchase_master.status, purchase_master.ac_id, purchase_master.id as master_id, purchase_master.purchase_no, purchase_master.purchase_date,suppliers.name as supplier_name, sum(purchase_details.quantity) as item_qty, sum(purchase_details.sq_weight) as item_area, sum(purchase_details.total_price) as price_total');
        $this->db->from("purchase_master");
        $this->db->join("suppliers","purchase_master.supplier_id = suppliers.id","left");
        $this->db->join("purchase_details","purchase_master.purchase_no = purchase_details.purchase_no","left");
        $this->db->where('purchase_master.purchase_date >= ', $stat_date);
        $this->db->where('purchase_master.purchase_date <= ', $en_date);
        $this->db->where('purchase_master.company_id', $this->session->userdata('user_company'));
        $this->db->where('purchase_details.purchase_date >= ', $stat_date);
        $this->db->where('purchase_details.purchase_date <= ', $en_date);
        $this->db->where('purchase_details.company_id', $this->session->userdata('user_company'));
        $this->db->group_by("purchase_master.purchase_no");

        $total = $this->db->get()->num_rows();

        $this->db->limit($limit);
        if ($sort == '') {
            $sort = 'purchase_master.id';
            $order = 'DESC';
        }
        $this->db->order_by($sort, $order);
        if ($search) {
            if($search == 'Received'){
                $search = 1;
            }elseif($search == 'Pending'){
                $search = 2;
            }elseif ($search == 'Ordered') {
                    $search = 3;
            }
            $this->db->group_start();
            $this->db->like('purchase_master.purchase_no', $search, 'both');
            $this->db->or_like('purchase_master.purchase_date', $search, 'both');
            $this->db->or_like('suppliers.name', $search, 'both');
            $this->db->or_like('purchase_master.status', $search, 'both');
            $this->db->group_end();
        }
        $this->db->select('purchase_master.draft, purchase_master.status, purchase_master.ac_id, purchase_master.id as master_id, purchase_master.purchase_no, purchase_master.purchase_date,suppliers.name as supplier_name, sum(purchase_details.quantity) as item_qty, sum(purchase_details.sq_weight) as item_area, sum(purchase_details.total_price) as price_total');
        $this->db->from("purchase_master");
        $this->db->join("suppliers","purchase_master.supplier_id = suppliers.id","left");
        $this->db->join("purchase_details","purchase_master.purchase_no = purchase_details.purchase_no","left");
        $this->db->where('purchase_master.purchase_date >= ', $stat_date);
        $this->db->where('purchase_master.purchase_date <= ', $en_date);
        $this->db->where('purchase_master.company_id', $this->session->userdata('user_company'));
        $this->db->where('purchase_details.purchase_date >= ', $stat_date);
        $this->db->where('purchase_details.purchase_date <= ', $en_date);
        $this->db->where('purchase_details.company_id', $this->session->userdata('user_company'));
        $this->db->group_by("purchase_master.purchase_no");
        $this->db->limit($limit, $offset);
        $products = $this->db->get()->result_array();
         
        $data = array();
        foreach ($products as $row) {
            $status = '';
            if($row['status'] ==1){
                $status = 'Received';
            }elseif($row['status'] ==2){
                $status = 'Pending';
            }elseif ($row['status'] ==3) {
                $status = 'Ordered';
            }
            $res = array(
                'purchase_no' => '',
                'voucher' => '',
                'purchase_date' => '',
                'supplier_name' => '',
                'item_qty' => '',
                'item_area' => '',
                'price_total' => '',
                'status' => '',
                'option' => ''
            );

            $res['purchase_no'] = "<a href=\"inventory/purchase_preview/".$row['purchase_no']."\"  target=\"_blank\">".$row['purchase_no']."</a>";
            if($row['ac_id'] <> "" && $row['ac_id'] <> 0){
                 $res['voucher'] = "<a href=\"accounts/journal_preview/".$row['ac_id']."\"  target=\"_blank\">"."Purchase_".$row['purchase_no']."</a>";
            }
           
            $res['purchase_date'] = date('Y-m-d', strtotime($row['purchase_date']));
            $res['supplier_name'] = $row['supplier_name'];
            $res['item_qty'] = $row['item_qty'];
            $res['item_area'] = $row['item_area'];
            $res['status'] = $status;
            $res['price_total'] = $row['price_total'];
            if($purchase['draft'] <> 0){
                $res['options'] = " <a  class=\"btn btn-warning\" href=\"inventory/purchase_save/".$row['master_id']."\"><i class=\"icon-tag icon-white\"></i> Draft</a>
                              <a  class=\"btn del btn-danger\" onclick=\"delete_confirm('".$row['master_id']."')\"><i class=\"icon-trash icon-white\")\"></i> Delete</a>";
            }else if($this->session->userdata['user_type'] == 'Admin'){
                $res['options'] = "<a  class=\"btn btn-info\" href=\"inventory/purchase_status/".$row['master_id']."\"><i class=\"icon-check icon-white\"></i> Status</a> 
                <a  class=\"btn btn-edit\" href=\"inventory/purchase_save/".$row['master_id']."\"><i class=\"icon-edit icon-white\"></i> Edit</a>
                <a  class=\"btn del btn-danger\" onclick=\"delete_confirm('".$row['master_id']."')\"><i class=\"icon-trash icon-white\")\"></i> Delete</a>";

             }else{
                 $res['options'] = "";
             }          
            $data[] = $res;
        }
        $result = array(
            'total' => $total,
            'rows' => $data
        );
        echo json_encode($result);
    }
    public function purchase_status($id = NULL)
    {
            $accounts_date = $this->MItems->get_dates();
            $stat_date = $accounts_date['start_date'];
            $en_date = $accounts_date['end_date'];
        if ($this->input->post('id'))
        {
            $this->MPurchase_master->update_status($this->input->post('id'));
             echo 'inventory/purchase_list';
        }else{
            $data['purchase'] = $this->MPurchase_master->get_by_id($id,$stat_date, $en_date);
            $data['title'] = 'POS System';
            $data['menu'] = 'inventory';
            $data['content'] = 'admin/inventory/purchase/status';
            $data['privileges'] = $this->privileges;
            $this->load->view('admin/template', $data);
        }

    }
    public function purchase_save($id = NULL)
    {

            $accounts_date = $this->MItems->get_dates();
            $stat_date = $accounts_date['start_date'];
            $en_date = $accounts_date['end_date'];
        if ($this->input->post())
        {
            $purchase = $this->MPurchase_master->get_by_purchase_no($this->input->post('purchase_no'),$stat_date, $en_date);
            if (count($purchase) > 0)
            {
                $this->MPurchase_master->update($purchase[0]['id']);
            }
            else
            {
                $this->MPurchase_master->create();
            }
            // Add purchase info on details table
            $this->MPurchase_details->create($this->input->post('purchase_no'));
            // Update AVCO price in item table
            $avco = $this->MPurchase_details->get_avco(trim($this->input->post('item_id')));
            $this->MItems->update_field(trim($this->input->post('item_id')), 'avco_price', $avco);
            $msg = $this->purchase_table($this->input->post('purchase_no'),$stat_date, $en_date);
            echo $msg;
        }
        else
        {

            $data['title'] = 'POS System';
            $data['menu'] = 'inventory';
            $data['content'] = 'admin/inventory/purchase/save';

            $purchase = $this->MPurchase_master->get_last_purchase_no($stat_date, $en_date);
            if (count($purchase) > 0)
            {
                $data['purchase_no'] = (int)$purchase['purchase_no'] + 1;
            }
            else
            {
                $data['purchase_no'] = 1001;
            }
            $data['suppliers'] = $this->MSuppliers->get_all();
            // $data['items'] = $this->MItems->get_all('active');
            
            if ($id)
            {
                $data['purchase'] = $this->MPurchase_master->get_by_id($id,$stat_date, $en_date);
                $purchase_no = $data['purchase']['purchase_no'];
                $data['suppliers'] = $this->MSuppliers->get_by_id($data['purchase']['supplier_id']);
            }
            else
            {
                $data['suppliers'] = NULL;
                $data['purchase'] = NULL;
                $purchase_no = $data['purchase_no'];
            }
            $supplier = $this->MSuppliers->get_latest();
            if (count($supplier) > 0)
            {
                $data['code'] = (int)$supplier['code'] + 1;
            }
            else
            {
                $data['code'] = 1001;
            }

            $data['details'] = $this->MPurchase_details->get_by_purchase_no($purchase_no,$stat_date, $en_date);
            $data['privileges'] = $this->privileges;
            $this->load->view('admin/template', $data);
        }
    }

    public function purchase_preview($purchase_no = NULL)
    {
        $accounts_date = $this->MItems->get_dates();
        $stat_date = $accounts_date['start_date'];
        $en_date = $accounts_date['end_date'];
        $data['title'] = 'Purchase Invoice Preview';
        $data['master'] = $this->MPurchase_master->get_by_purchase_no($purchase_no, $stat_date, $en_date);
        $data['details'] = $this->MPurchase_details->get_by_purchase_no($purchase_no, $stat_date, $en_date);
        $data['company'] = $this->MCompanies->get_by_id($data['master'][0]['company_id']);
        $data['privileges'] = $this->privileges;
        $this->load->view('admin/inventory/purchase/preview', $data);
    }
    public function purchase_draft(){
        $accounts_date = $this->MItems->get_dates();
        $stat_date = $accounts_date['start_date'];
        $en_date = $accounts_date['end_date'];
        $purchase_no = $this->input->post('purchase_no');
        $purchase = $this->MPurchase_master->get_by_purchase_no($purchase_no,$stat_date, $en_date);
        $this->MPurchase_master->update_draft($purchase[0]['id']);
        echo 'inventory/purchase_save';
    }
    public function purchase_complete()
    {
        $accounts_date = $this->MItems->get_dates();
        $stat_date = $accounts_date['start_date'];
        $en_date = $accounts_date['end_date'];
        $purchase = $this->MPurchase_master->get_by_purchase_no($this->input->post('purchase_no'),$stat_date, $en_date);
        $purchase_no = trim($this->input->post('purchase_no'));
        $this->MPurchase_master->update($purchase[0]['id']);



        //Check if previous journal exist then Remove journal details else make new journal master
        $journal = $this->MAc_journal_master->get_by_doc('Purchase', $purchase_no,$stat_date, $en_date);
        if (count($journal) > 0)
        {
            // $insert_id = $journal['id'];
            $journal_no = $journal['journal_no'];
            $this->MAc_journal_details->delete_by_journal_no($journal_no,$stat_date, $en_date);
            $this->MAc_journal_master->delete($journal['id']);
        }
        else
        {
            $journal_no = 'Purchase_'.$purchase_no;
        }
            $insert_id = $this->MAc_journal_master->create_by_purchase($journal_no, $purchase_no);
            
        // }
        $this->MPurchase_master->update_voucher_id($purchase[0]['id'], $insert_id);
        $settings = $this->MSettings->get_by_company_id($this->session->userdata('user_company'));

        //Determine Credit or Cash Purchase and make journal
        $details = $this->MPurchase_details->get_by_purchase_no($purchase_no, $stat_date, $en_date);
        $total_purchase = $this->MPurchase_details->get_total_price($purchase_no,NULL, $stat_date, $en_date);
        $total_sale_tax = $this->MPurchase_details->get_total_sale_tax($purchase_no,NULL, $stat_date, $en_date);
        $total_sale_tax = round($total_sale_tax);
        $total_paid = $this->input->post('paid_amount');
        $dues = ((float)$total_purchase + $total_sale_tax ) - (float)$total_paid;
        $ac_inventry = $this->MItems->get_by_id($this->input->post('supplier_id'));
        if ($dues == 0)
        {
            $this->MAc_journal_details->create_by_inventory($journal_no, $settings['ac_cash'], NULL, $total_paid, $this->input->post('purchase_date'), $this->input->post('notes'));
            for($i = 0; $i<sizeof($details); $i++){

                // if($details[$i]['vat_amount'] <> 0){
                //     $this->MAc_journal_details->create_by_inventory($journal_no, $details[$i]['ac_id'], $details[$i]['total_price'] + $details[$i]['vat_amount'], NULL, $this->input->post('purchase_date'), $this->input->post('notes'));

                //     $this->MAc_journal_details->create_by_inventory($journal_no, $settings['ac_tax'], $details[$i]['vat_amount'], NULL, $this->input->post('purchase_date'), $this->input->post('notes'));

                // }else{
                    $this->MAc_journal_details->create_by_inventory($journal_no, $details[$i]['ac_id'], $details[$i]['total_price'], NULL, $this->input->post('purchase_date'), $this->input->post('notes'));
                // }

                if($total_sale_tax != 0){
                    $this->MAc_journal_details->create_by_inventory($journal_no, $settings['ac_tax'], $total_sale_tax, NULL, $this->input->post('purchase_date'), $this->input->post('notes'));
                }
            }
            
        }
        else
        {
            $supplier = $this->MSuppliers->get_by_id($this->input->post('supplier_id'));
            $this->MAc_journal_details->create_by_inventory($journal_no, $supplier['ac_id'], NULL, $dues, $this->input->post('purchase_date'), $this->input->post('notes'));
            if ((float)$total_paid != 0)
            {
                $this->MAc_journal_details->create_by_inventory($journal_no, $settings['ac_cash'], NULL, $total_paid, $this->input->post('purchase_date'), $this->input->post('notes'));
            }
             for($i = 0; $i<sizeof($details); $i++){
                // if($details[$i]['vat_amount'] <> 0){
                //     $this->MAc_journal_details->create_by_inventory($journal_no, $details[$i]['ac_id'], $details[$i]['total_price'],NULL, $this->input->post('purchase_date'), $this->input->post('notes'));
                //     $this->MAc_journal_details->create_by_inventory($journal_no, $settings['ac_tax'], $details[$i]['vat_amount'], NULL, $this->input->post('purchase_date'), $this->input->post('notes'));
                // }else{
                    $this->MAc_journal_details->create_by_inventory($journal_no, $details[$i]['ac_id'], $details[$i]['total_price'],NULL, $this->input->post('purchase_date'), $this->input->post('notes'));
                // }
            
            }
            if($total_sale_tax != 0){
                $this->MAc_journal_details->create_by_inventory($journal_no, $settings['ac_tax'], $total_sale_tax, NULL, $this->input->post('purchase_date'), $this->input->post('notes'));
            }

        }

        //Auto Payment Receipt if partial or full cash paid
        if ((float)$total_paid != 0)
        {
            $payment = $this->MAc_payment_receipts->get_latest($stat_date, $en_date);
            if (count($payment) > 0)
            {
                $payment_no = (int)$payment['payment_no'] + 1;
            }
            else
            {
                $payment_no = 1001;
            }
            $this->MAc_payment_receipts->create_by_purchase($payment_no, $total_paid, $purchase_no);
        }

        echo 'accounts/journal_preview_after_purchase/'.$insert_id;
    }

    public function purchase_delete($id)
    {
        $accounts_date = $this->MItems->get_dates();
        $stat_date = $accounts_date['start_date'];
        $en_date = $accounts_date['end_date'];
        $purchase = $this->MPurchase_master->get_by_id($id,$stat_date, $en_date);
        //Remove auto created purchase journal
        $journal = $this->MAc_journal_master->get_by_doc('Purchase', $purchase['purchase_no'], $stat_date, $en_date);
        if (count($journal) > 0)
        {
            $this->MAc_journal_details->delete_by_journal_no($journal['journal_no'],$stat_date, $en_date);
            $this->MAc_journal_master->delete_by_journal_no($journal['journal_no'],$stat_date, $en_date);
        }
        //Remove auto created payment receipt for partial or full cash purchase
        $payment = $this->MAc_payment_receipts->get_by_doc('Purchase', $purchase['purchase_no'], $stat_date, $en_date);
        if (count($payment) > 0)
        {
            $this->MAc_payment_receipts->delete($payment['id']);
        }
        //Remove purchase
        $this->MPurchase_details->delete_by_purchase_no($purchase['purchase_no'],$stat_date, $en_date);
        $this->MPurchase_master->delete($id);
        echo json_encode(true);
        // redirect('inventory/purchase_list', 'refresh');
    }
    
    public function purchase_item_delete()
    {
        $accounts_date = $this->MItems->get_dates();
        $stat_date = $accounts_date['start_date'];
        $en_date = $accounts_date['end_date'];
        // Delete item from purchase details table
        $this->MPurchase_details->delete(trim($this->input->post('item_id')));
        // Update AVCO price in item table
        $avco = $this->MPurchase_details->get_avco(trim($this->input->post('item_id')));
        $this->MItems->update_field(trim($this->input->post('item_id')), 'avco_price', $avco);

        $msg = $this->purchase_table($this->input->post('purchase_no'),$stat_date, $en_date);
        echo $msg;
    }

    public function purchase_table($purchase_no,$stat_date, $en_date)
    {
        $details = $this->MPurchase_details->get_by_purchase_no($purchase_no,$stat_date, $en_date);

        $msg = '
        <table id="datatables" class="table table-bordered table-striped responsive">
            <thead>
                <tr>
                    <th class="left">Item Code</th>
                    <th class="left">Item Name</th>
                    <th class="center">Quantity</th>
                    <th class="center">Total area / Weight</th>
                    <th class="center">Unit</th>
                    <th class="center">Unit Price</th>
                    <th class="center">Total</th>
                    <th class="center">Sale Tax %</th>
                    <th class="center">Sale Tax amount</th>
                    <th class="center">Total Price</th>
                    <th class="span3 center">Action</th>
                </tr>
            </thead>';
            $msg .= '<tbody>';
            $qty = 0;
            $price = 0;
            $sq_weight = 0;
            $tax = 0;
            if (count($details) > 0)
            {
                foreach ($details as $list)
                {
                    $to_price = round($list['sq_weight'] * $list['purchase_price']);
                    $msg .= '
                    <tr>
                        <td>' . $list['item_code'] . '</td>
                        <td>' . $list['item_name'] . '</td>
                        <td class="center">' . $list['quantity'] . '</td>
                        <td class="center">' . $list['sq_weight'] . '</td>
                        <td class="right">' . $list['unit'] . '</td>
                        <td class="right">' . $list['purchase_price'] . '</td>
                        <td class="right">' . $to_price . '</td>
                        <td class="right">' . $list['vat_percent'] . '</td>
                        <td class="right">' . $list['vat_amount'] . '</td>
                        <td class="right">' . round($to_price + $list['vat_amount']) . '</td>
                        
                        <td class="center">
                            <input type="hidden" value="' . $list['id'] . '" /><span class="btn btn-danger purchase_item_delete"><i class="icon-trash icon-white"></i>Delete</span>
                        </td>
                    </tr>';
                    $qty += $list['quantity'];
                    $sq_weight += $list['sq_weight'];
                    $price += round($to_price);
                    $tax += round($list['vat_amount']);
                }
            }
            $msg .= '</tbody>
            <tfoot>
                <tr>
                    <th class="left" colspan="11">Order Totals</th>
                </tr>
                <tr>
                    <td colspan="2">&nbsp</td>
                    <td class="center">' . $qty . '</td>
                    <td class="center">' . $sq_weight . '</td>
                    <td></td>
                    <td></td>
                    <td>' . $price . '</td>
                     
                     <td></td>
                     <td>' . $tax . '</td>
                    <td class="right"><input type="hidden" id="totl_price" value="'. ($price + $tax) .'">'. ($price + $tax).'</td>
                    <td></td>
                </tr>
                <tr>
                    <td colspan="9" class="right"> Total Paid Amount</td>
                    <td class="right"><input type="text" name="paid_amount" id="paid_amount"></td>
                    <td></td>
                </tr>
            </tfoot>
        </table>';

        return $msg;
    }
public function purchase_return_list()
    {

        $data['title'] = 'POS System';
        $data['menu'] = 'inventory';
        $data['content'] = 'admin/inventory/purchase_return/list';
        $data['privileges'] = $this->privileges;
        $this->load->view('admin/template', $data);
    }

    public function purchase_return_list_data(){
        $accounts_date = $this->MItems->get_dates();
        $stat_date = $accounts_date['start_date'];
        $en_date = $accounts_date['end_date'];
        $limit = $this->input->get('limit');
        $search = $this->input->get('search');
        $order = $this->input->get('order');
        $offset = $this->input->get('offset');
        $sort = $this->input->get('sort');
        if ($search) {
            
            $this->db->group_start();
            $this->db->like('purchase_return_master.purchase_return_no', $search, 'both');
            $this->db->or_like('purchase_return_master.purchase_return_date', $search, 'both');
            $this->db->or_like('suppliers.name', $search, 'both');
            $this->db->group_end();
        }

        $this->db->select('purchase_return_master.ac_id, purchase_return_master.id as master_id, purchase_return_master.purchase_return_no, purchase_return_master.purchase_return_date,suppliers.name as supplier_name, sum(purchase_return_details.quantity) as item_qty, sum(purchase_return_details.sq_weight) as item_area, sum(purchase_return_details.total_price) as price_total');
        $this->db->from("purchase_return_master");
        $this->db->join("suppliers","purchase_return_master.supplier_id = suppliers.id","left");
        $this->db->join("purchase_return_details","purchase_return_master.purchase_return_no = purchase_return_details.purchase_return_no","left");
        $this->db->where('purchase_return_master.purchase_return_date >= ', $stat_date);
        $this->db->where('purchase_return_master.purchase_return_date <= ', $en_date);
        $this->db->where('purchase_return_master.company_id', $this->session->userdata('user_company'));
        $this->db->where('purchase_return_details.purchase_return_date >= ', $stat_date);
        $this->db->where('purchase_return_details.purchase_return_date <= ', $en_date);
        $this->db->where('purchase_return_details.company_id', $this->session->userdata('user_company'));
        $total = $this->db->get()->num_rows();
        $this->db->limit($limit);
        if ($sort == '') {
            $sort = 'purchase_return_master.id';
            $order = 'DESC';
        }
        $this->db->order_by($sort, $order);
        if ($search) {
            $this->db->group_start();
            $this->db->like('purchase_return_master.purchase_return_no', $search, 'both');
            $this->db->or_like('purchase_return_master.purchase_return_date', $search, 'both');
            $this->db->or_like('suppliers.name', $search, 'both');
            $this->db->group_end();
        }

        $this->db->select('purchase_return_master.ac_id, purchase_return_master.id as master_id, purchase_return_master.purchase_return_no, purchase_return_master.purchase_return_date,suppliers.name as supplier_name, sum(purchase_return_details.quantity) as item_qty, sum(purchase_return_details.sq_weight) as item_area, sum(purchase_return_details.total_price) as price_total');
        $this->db->from("purchase_return_master");
        $this->db->join("suppliers","purchase_return_master.supplier_id = suppliers.id","left");
        $this->db->join("purchase_return_details","purchase_return_master.purchase_return_no = purchase_return_details.purchase_return_no","left");
        $this->db->where('purchase_return_master.purchase_return_date >= ', $stat_date);
        $this->db->where('purchase_return_master.purchase_return_date <= ', $en_date);
        $this->db->where('purchase_return_master.company_id', $this->session->userdata('user_company'));
        $this->db->where('purchase_return_details.purchase_return_date >= ', $stat_date);
        $this->db->where('purchase_return_details.purchase_return_date <= ', $en_date);
        $this->db->where('purchase_return_details.company_id', $this->session->userdata('user_company'));
        $this->db->group_by("purchase_return_master.purchase_return_no");
        $this->db->limit($limit, $offset);
        $products = $this->db->get()->result_array();
         
        $data = array();
        foreach ($products as $row) {
            $res = array(
                'purchase_return_no' => '',
                'voucher' => '',
                'purchase_return_date' => '',
                'supplier_name' => '',
                'item_qty' => '',
                'item_area' => '',
                'price_total' => '',
                'status' => '',
                'option' => ''
            );

            $res['purchase_return_no'] = "<a href=\"inventory/purchase_return_preview/".$row['purchase_return_no']."\"  target=\"_blank\">".$row['purchase_return_no']."</a>";
            $res['voucher'] = "<a href=\"accounts/journal_preview/".$row['ac_id']."\"  target=\"_blank\">"."Purchase_Return_".$row['purchase_return_no']."</a>";
            $res['purchase_return_date'] = date('Y-m-d', strtotime($row['purchase_return_date']));
            $res['supplier_name'] = $row['supplier_name'];
            $res['item_qty'] = $row['item_qty'];
            $res['item_area'] = $row['item_area'];
            $res['price_total'] = $row['price_total'];
           if($this->session->userdata['user_type'] == 'Admin'){
                $res['options'] = "<a  class=\"btn btn-edit\" href=\"inventory/purchase_return_save/".$row['master_id']."\"><i class=\"icon-edit icon-white\"></i> Edit</a>
                              <a  class=\"btn del btn-danger\" onclick=\"delete_confirm('".$row['master_id']."')\"><i class=\"icon-trash icon-white\")\"></i> Delete</a>";
             }else{
                 $res['options'] = "";
             }          
            $data[] = $res;
        }
        $result = array(
            'total' => $total,
            'rows' => $data
        );
        echo json_encode($result);
    }

// public function get_product_stock(){

//     $stock = $this->MReports->get_raw_stock_balance($this->input->post('product_id'));
//     echo json_encode($stock);
// }

    public function purchase_return_save($id = NULL)
    {
            $accounts_date = $this->MItems->get_dates();
            $stat_date = $accounts_date['start_date'];
            $en_date = $accounts_date['end_date'];
        if ($this->input->post())
        {

            $purchase_return = $this->MPurchase_return_master->get_by_purchase_return_no($this->input->post('purchase_return_no'), $stat_date, $en_date);
            if (count($purchase_return) > 0)
            {
                $this->MPurchase_return_master->update($purchase_return[0]['id']);
            }
            else
            {
                $this->MPurchase_return_master->create();
            }

            $this->MPurchase_return_details->create($this->input->post('purchase_return_no'));
            $msg = $this->purchase_return_table($this->input->post('purchase_return_no'),$stat_date, $en_date);
            echo $msg;

        }
        else
        {
            $data['title'] = 'POS System';
            $data['menu'] = 'inventory';
            $data['content'] = 'admin/inventory/purchase_return/save';
            // $data['account'] = $this->MAc_charts->get_coa_tree();
            $purchase = $this->MPurchase_return_master->get_last_purchase_return_no($stat_date, $en_date);
            if (count($purchase) > 0)
            {
                $data['purchase_return_no'] = (int)$purchase['purchase_return_no'] + 1;
            }
            else
            {
                $data['purchase_return_no'] = 1001;
            }
            // $data['suppliers'] = $this->MSuppliers->get_all();
            if ($id)
            {
                $data['purchase_return'] = $this->MPurchase_return_master->get_by_id($id, $stat_date, $en_date);
                $data['suppliers'] = $this->MSuppliers->get_by_id($data['purchase_return']['supplier_id']);
                $purchase_return_no = $data['purchase_return']['purchase_return_no'];
            }
            else
            {
                $data['purchase_return'] = NULL;
                $data['suppliers'] = NULL;
                $purchase_return_no = $data['purchase_return_no'];
            }
            $data['details'] = $this->MPurchase_return_details->get_by_purchase_return_no($purchase_return_no, $stat_date, $en_date);
            $data['privileges'] = $this->privileges;
            $this->load->view('admin/template', $data);
        }
    }

    public function purchase_return_preview($purchase_return_no = NULL)
    {

        $accounts_date = $this->MItems->get_dates();
        $stat_date = $accounts_date['start_date'];
        $en_date = $accounts_date['end_date'];
        $data['title'] = 'Purchase Return Invoice Preview';
        $data['master'] = $this->MPurchase_return_master->get_by_purchase_return_no($purchase_return_no, $stat_date, $en_date);
        $data['details'] = $this->MPurchase_return_details->get_by_purchase_return_no($purchase_return_no, $stat_date, $en_date);
        $data['company'] = $this->MCompanies->get_by_id($data['master'][0]['company_id']);
        $data['privileges'] = $this->privileges;
        $this->load->view('admin/inventory/purchase_return/preview', $data);
    }

    public function purchase_return_complete()
    {

        $accounts_date = $this->MItems->get_dates();
        $stat_date = $accounts_date['start_date'];
        $en_date = $accounts_date['end_date'];
        $purchase_return_no = trim($this->input->post('purchase_return_no'));
        $purchase_return_date = trim($this->input->post('purchase_return_date'));
        $supplier_id = trim($this->input->post('supplier_id'));
        // $sale_credit_ac_id = trim($this->input->post('ac_id'));
        $purchase_return = $this->MPurchase_return_master->get_by_purchase_return_no($this->input->post('purchase_return_no'), $stat_date, $en_date);
        $this->MPurchase_return_master->update($purchase_return[0]['id']);

        //Check if previous sales journal exist then Remove sales journal details else make new sales journal master
        $journal = $this->MAc_journal_master->get_by_doc('Purchase Return', $purchase_return_no, $stat_date, $en_date);
        if (count($journal) > 0)
        {
            // $insert_id = $journal['id'];
            $journal_no = $journal['journal_no'];
            $this->MAc_journal_details->delete_by_journal_no($journal_no,$stat_date, $en_date);
            $this->MAc_journal_master->delete($journal['id']);
        }
        else
        {
            // $sales_journal_no = $this->MAc_journal_master->get_journal_number();
            $journal_no = 'Purchase_Return_'.$purchase_return_no;
        }
            $insert_id = $this->MAc_journal_master->create_by_purchase_return($journal_no, $purchase_return_no, $purchase_return_date);
            
        // }
        $this->MPurchase_return_master->update_voucher_id($purchase_return[0]['id'], $insert_id);
        $settings = $this->MSettings->get_by_company_id($this->session->userdata('user_company'));

        $total_purchase = $this->MPurchase_return_details->get_total_price($purchase_return_no, NULL, $stat_date, $en_date);
        $total_paid = $this->input->post('paid_amount');
        $dues = (float)$total_purchase - (float)$total_paid;
        $supplier = $this->MSuppliers->get_by_id($this->input->post('supplier_id'));
        //Determine Credit or Cash Sale and make journal
        
        $total_cogs = $this->MPurchase_return_details->get_total_cogs($purchase_return_no,'', $stat_date, $en_date);
        
        $details = $this->MSales_return_details->get_by_sales_return_no($sales_return_no, $stat_date, $en_date);
        $purchase_return_details = $this->MPurchase_return_details->get_by_purchase_return_no($purchase_return_no, $stat_date, $en_date);
        if ($dues == 0)
        {
            $this->MAc_journal_details->create_by_inventory($journal_no, $settings['ac_cash'], $total_purchase, NULL,$this->input->post('purchase_return_date'), $this->input->post('notes'));
        }
        else
        {
            
            $this->MAc_journal_details->create_by_inventory($journal_no, $supplier['ac_id'], $dues,NULL,$this->input->post('purchase_return_date'), $this->input->post('notes'));

            if ((float)$total_paid != 0)
            {
             
                    $this->MAc_journal_details->create_by_inventory($journal_no, $settings['ac_cash'], $total_paid,NULL,$this->input->post('purchase_return_date'), $this->input->post('notes'));
            }
             
        }

        for($i = 0; $i<sizeof($purchase_return_details); $i++){
                // if($details[$i]['vat_amount'] <> 0){
                //     $this->MAc_journal_details->create_by_inventory($journal_no, $details[$i]['ac_id'], $details[$i]['total_price'] - $details[$i]['vat_amount'], NULL, $this->input->post('purchase_date'), $this->input->post('notes'));
                //     $this->MAc_journal_details->create_by_inventory($journal_no, $settings['ac_tax'], $details[$i]['vat_amount'], NULL, $this->input->post('purchase_date'), $this->input->post('notes'));
                // }else{
            $this->MAc_journal_details->create_by_inventory($journal_no, $purchase_return_details[$i]['ac_id'], NULL, $purchase_return_details[$i]['total_price'], $this->input->post('purchase_return_date'), $this->input->post('notes'));
                // }
                
            }

        echo 'inventory/purchase_return_save/';
    }

    public function purchase_return_delete($id)
    {
        $accounts_date = $this->MItems->get_dates();
        $stat_date = $accounts_date['start_date'];
        $en_date = $accounts_date['end_date'];

        $purchase = $this->MPurchase_return_master->get_by_id($id, $stat_date, $en_date);
        // Remove auto created sales journalPurchase Return
        $journal = $this->MAc_journal_master->get_by_doc('Purchase Return', $purchase['purchase_return_no'], $stat_date, $en_date);
        if (count($journal) > 0)
        {
            $this->MAc_journal_details->delete_by_journal_no($journal['journal_no'],$stat_date, $en_date);
            $this->MAc_journal_master->delete_by_journal_no($journal['journal_no'],$stat_date, $en_date);
        }

        // //Remove auto created sales Expanse
        //  $expense = $this->MAc_journal_master->get_by_doc('Purchase Return Expense', $purchase['purchase_return_no'], $stat_date, $en_date);
        // if (count($expense) > 0)
        // {
        //     $this->MAc_journal_details->delete_by_journal_no($expense['journal_no'],$stat_date, $en_date);

        //     $this->MAc_journal_master->delete_by_journal_no($expense['journal_no'],$stat_date, $en_date);
        // }

        $this->MPurchase_return_details->delete_by_purchase_return_no($purchase['purchase_return_no'], $stat_date, $en_date);
        $this->MPurchase_return_master->delete($id);
         echo json_encode(true);
    }

    public function purchase_return_item_delete()
    {
        $accounts_date = $this->MItems->get_dates();
        $stat_date = $accounts_date['start_date'];
        $en_date = $accounts_date['end_date'];
        $this->MPurchase_return_details->delete($this->input->post('item_id'));
        $msg = $this->purchase_return_table( $this->input->post('purchase_return_no'), $stat_date, $en_date);
        echo $msg;
    }

    public function purchase_return_table($purchase_return_no,$stat_date, $en_date)
    {
        $details = $this->MPurchase_return_details->get_by_purchase_return_no($purchase_return_no, $stat_date, $en_date);

        $msg = '
        <table id="sample_1" class="table table-striped table-bordered">
            <thead>
                <tr>
                    <th class="left">Item Code</th>
                    <th class="left">Item Name</th>
                    <th class="center" >Quantity (pcs)</th>
                    <th class="center" >Total Area (sq ft)</th>
                    <th class="center">Unit Price</th>
                    <th class="center">Total Price</th>
                    <th class="span3 center">Action</th>
                </tr>
            </thead>';
            $msg .= '<tbody>';
            $qty = 0;
            $price = 0;
            $sq_weight = 0;
            if (count($details) > 0)
            {
                foreach ($details as $list)
                {
                    $msg .= '
                    <tr>
                        <td>' . $list['item_code'] . '</td>
                        <td>' . $list['item_name'] . '</td>
                        <td class="right">' . $list['quantity'] . '</td>
                        <td class="right">' . $list['sq_weight'] . '</td>
                        <td class="right">' . $list['purchase_price'] . '</td>
                        <td class="right">' . round($list['sq_weight'] * $list['purchase_price']) . '</td>
                        <td class="center">
                            <input type="hidden" value="' . $list['id'] . '" /><span class="btn del btn-danger purchase_return_item_delete"><i class="icon-trash icon-white"></i>Delete</span>
                        </td>
                    </tr>';
                    $qty += $list['quantity'];
                    $sq_weight += $list['sq_weight'];
                    $price += round($list['sq_weight'] * $list['purchase_price']);
                }
            }
            $msg .= '</tbody>
            <tfoot>
                <tr>
                    <th class="left" colspan="7">Order Totals</th>
                </tr>
                <tr>
                    <td colspan="2">&nbsp;</td>
                    <td class="right">' . $qty . '</td>
                    <td class="right">' . $sq_weight . '</td>
                    <td></td>
                    <td class="right"><input type="hidden" id="totl_price" value="'. $price .'">'. $price.'</td>
                    <td></td>
                </tr>
                <tr>
                    <td colspan="5" class="right"> Total Paid Amount</td>
                    <td class="right"><input type="text" name="paid_amount" id="paid_amount"></td>
                    <td></td>
                </tr>
            </tfoot>
        </table>';

        return $msg;
    }
    
    /* ------------------- Customer Start ------------------------- */

    public function recipe_list()
    {
        $data['title'] = 'POS System';
        $data['menu'] = 'inventory';
        $data['content'] = 'admin/inventory/recipe/list';
        $data['privileges'] = $this->privileges;
        $this->load->view('admin/template', $data);
    }
public function recipe_list_data(){
        
        $limit = $this->input->get('limit');
        $search = $this->input->get('search');
        $order = $this->input->get('order');
        $offset = $this->input->get('offset');
        $sort = $this->input->get('sort');
        if ($search) {
            if($search == 'Active'){
                $search = "0";
            }else if($search == 'Inactive'){
                $search = "1";
            }
            $this->db->group_start();
            $this->db->like('code', $search, 'both');
            $this->db->or_like('name', $search, 'both');
            $this->db->or_like('status', $search, 'both');
            $this->db->group_end();
        }
        $this->db->select("id, code, name, status,");
        $this->db->where('company_id', $this->session->userdata('user_company'));
        $total = $this->db->get("recipe")->num_rows();
        $this->db->limit($limit);
        if ($sort == '') {
            $sort = 'id';
            $order = 'DESC';
        }
        $this->db->order_by($sort, $order);
        if ($search) {
            if($search == 'Active'){
                $search = "0";
            }else if($search == 'Inactive'){
                $search = "1";
            }
            $this->db->group_start();
            $this->db->like('code', $search, 'both');
            $this->db->or_like('name', $search, 'both');
            $this->db->or_like('status', $search, 'both');
            $this->db->group_end();
        }
        $this->db->select("id, code, name, status,");
        $this->db->where('company_id', $this->session->userdata('user_company'));
        $this->db->limit($limit, $offset);
        $products = $this->db->get("recipe")->result_array();
         
        $data = array();
        foreach ($products as $row) {
            $res = array(
                'code' => '',
                'name' => '',
                'status' => '',
                'option' => ''
            );
            $res['code'] = $row['code'];
            $res['name'] = $row['name'];
            if($row['status'] == '0'){
                $res['status'] = "Active";
            }else if($row['status'] == '1'){
                $res['status'] = "Inactive";
            }
            
            if($this->session->userdata['user_type'] == 'Admin'){
                $res['options'] = "<a  class=\"btn btn-info\" target=\"_blank\" href=\"inventory/recipe_detail/".$row['id']."\"><i class=\"icon-edit icon-white\"></i> Detail</a>
                                <a  class=\"btn btn-edit\" href=\"inventory/recipe_save/".$row['id']."\"><i class=\"icon-edit icon-white\"></i> Edit</a>
                                <a  class=\"btn del btn-danger\" onclick=\"delete_confirm('".$row['id']."')\"><i class=\"icon-trash icon-white\")\"></i> Delete</a>";
            }        
            $data[] = $res;
        }
        $result = array(
            'total' => $total,
            'rows' => $data
        );
        echo json_encode($result);
    }

    public function recipe_save($id = NULL)
    {
        if ($this->input->post())
        {
            if ($this->input->post('id'))
            {

                $this->MRecipe->update();
                $this->session->set_flashdata('success', 'Recipe Updated successfully.');
                redirect('inventory/recipe_list', 'refresh');
            }
            else
            {
                
                $this->MRecipe->create();
                $this->session->set_flashdata('success', 'Recipe saved successfully.');
                redirect('inventory/recipe_save', 'refresh');
            }

            
        }
        else
        {
            $data['title'] = 'POS System';
            $data['menu'] = 'inventory';
            $data['content'] = 'admin/inventory/recipe/save';
            $recipe = $this->MRecipe->get_latest();
            if (count($recipe) > 0)
            {
                $data['code'] = (int)$recipe['code'] + 1;
            }
            else
            {
                $data['code'] = 1001;
            }
            $data['recipe'] = $this->MRecipe->get_by_id($id);
            $data['items'] = $this->MItems->get_all('', 'Chemical');
            $data['privileges'] = $this->privileges;
            $this->load->view('admin/template', $data);
        }
    }
public function get_raw_stock_balance()
    {
        $item_id = $this->input->post('result');
        $data['raw'] = $this->MReports->get_raw_stock_balance($item_id);
        echo json_encode($data);
    }
// public function get_chemical_stock_balance()
//     {
//         $chemical_id = $this->input->post('chemical_id');
//         $data['chemical'] = $this->MReports->get_chemical_stock_balance($chemical_id);
//         echo json_encode($data);
//     }
public function get_item_by_type()
    {
        $type = $this->input->post('type');
        $data['items'] = $this->MItems->get_all('active', $type);
        echo json_encode($data);
    }
    // public function get_supplier_inv_no()
    // {
    //     $id = $this->input->post('id');
    //     $purchase = $this->MPurchase_master->get_inv_by_supplier_id($id);
    //     // var_dump($purchase);
    //     // die;
    //      if (count($purchase) > 0){

    //         $string = $purchase['supplier_no'];
    //         $delimiter = "_";
    //         $array = explode($delimiter, $string) ;
    //         $aa = (int)$array[1] + 1;
    //         $code =  (int)$array[0].'_'.$aa;  
    //         // var_dump($code);
    //         // die;
    //         // $purchase['']
    //      }else{
    //          $supplier = $this->MSuppliers->get_by_id($id);
    //          $code = $supplier['code'].'_1';
    //      }
       
    //     // $data['items'] = $this->MItems->get_all('active', $type);
    //     echo json_encode($code);
    // }
    public function get_chemiclas()
    {
        $id = $this->input->post('id');
        $html = $this->MRecipe->get_chemiclas($id); 
        echo $html;
    }

public function recipe_detail($id)
    {

        $data['recipe'] = $this->MRecipe->get_by_id_with_chemical($id);
        $data['title'] = 'POS System';
        $data['privileges'] = $this->privileges;
        $this->load->view('admin/inventory/recipe/detail', $data);
    }

public function recipe_delete($id)
    {
        // $sales = $this->MSales_master->get_by_customer_id($id);
        // if (count($sales) > 0)
        // {
        //     $this->session->set_flashdata('error', 'Customer can\'t delete, S/He is in Sales List.');
        // }
        // else
        // {
            // $this->session->set_flashdata('success', 'Recipe deleted successfully.');
            $this->MRecipe->delete($id);
        // }
            $data['error'] = false;
            $data['message'] = 'success';

                              echo json_encode($data);
        // redirect('inventory/recipe_list', 'refresh');
    }
public function get_production_chemiclas()
    {
        $id = $this->input->post('id');
        $html = $this->MProduction->get_chemiclas($id); 
        echo $html;
    }

    public function get_production_selection_item()
    {
        $id = $this->input->post('id');
        $html = $this->MProduction->get_production_selection_itm($id); 
        echo $html;
    }

    public function get_recipe_stock_balance()
    {
        $recipe_id = $this->input->post('recipe_id');
        $raw_weight = $this->input->post('raw_weight');
        $html = $this->MProduction->get_chemiclas_with_quantity($recipe_id, $raw_weight); 
        echo $html;
    }
    public function production_recipe_detail($id)
    {
       
        $data['production'] = $this->MProduction->get_by_id($id);
        // $data['recipe'] = $this->MRecipe->get_by_id_with_chemical($data['production']['recipe']);
        $data['recipe'] = $this->MProduction->get_chemical_by_production_id_and_recipe_id($id, $data['production']['recipe']);
        $data['title'] = 'POS System';
        $data['privileges'] = $this->privileges;
        $this->load->view('admin/inventory/production/recipe_detail', $data);
    }
    public function production_status($id = NULL)
    {
           
        if ($this->input->post('id'))
        {
            $this->MProduction->update_status($this->input->post('id'), $this->input->post('status'));
             echo 'inventory/production_list';
        }else{
            $data['production'] = $this->MProduction->get_by_id($id);
            $data['title'] = 'POS System';
            $data['menu'] = 'inventory';
            $data['content'] = 'admin/inventory/production/status';
            $data['privileges'] = $this->privileges;
            $this->load->view('admin/template', $data);
        }

    }
public function production_list()
    {
        $data['title'] = 'POS System';
        $data['menu'] = 'inventory';
        $data['content'] = 'admin/inventory/production/list';
        $data['privileges'] = $this->privileges;
        $this->load->view('admin/template', $data);
    }
    public function production_list_data(){
        // $accounts_date = $this->MItems->get_dates();
        // $stat_date = $accounts_date['start_date'];
        // $en_date = $accounts_date['end_date'];
        $limit = $this->input->get('limit');
        $search = $this->input->get('search');
        $order = $this->input->get('order');
        $offset = $this->input->get('offset');
        $sort = $this->input->get('sort');
        if ($search) {
           
            $this->db->group_start();
            $this->db->like('production.status', $search, 'after');
            $this->db->or_like('production.code', $search, 'both');
            $this->db->or_like('production.raw_weight', $search, 'both');
            $this->db->or_like('production.production_date', $search, 'both');
            $this->db->or_like('production.quantity', $search, 'both');
            $this->db->or_like('production.name', $search, 'both');
            $this->db->or_like('production.raw_quantity', $search, 'both');
            $this->db->or_like('recipe.name', $search, 'both');
            $this->db->or_like('items.name', $search, 'both');
            $this->db->group_end();
        }
       
        $this->db->select('production.id,production.status,production.type as production_type, production.code,production.raw_weight, production.recipe as recipe_id, production.production_date, production.quantity, production.name as production_name, production.raw_quantity, items.name as raw_name,items.avco_price as raw_avco, recipe.name as recipe_name');
        $this->db->from('production');
        $this->db->join('items', 'production.raw = items.id', 'left');
        $this->db->join('recipe', 'production.recipe = recipe.id', 'left');
        // $this->db->where('production.production_date >=', $stat_date);
        // $this->db->where('production.production_date <=', $en_date);
        $this->db->where('production.company_id', $this->session->userdata('user_company'));

        $total = $this->db->get()->num_rows();

        $this->db->limit($limit);
        if ($sort == '') {
            $sort = 'production.code';
            $order = 'DESC';
        }
        $this->db->order_by($sort, $order);
        if ($search) {

            $this->db->group_start();
            $this->db->like('production.status', $search, 'after');
            $this->db->or_like('production.code', $search, 'both');
            $this->db->or_like('production.raw_weight', $search, 'both');
            $this->db->or_like('production.production_date', $search, 'both');
            $this->db->or_like('production.quantity', $search, 'both');
            $this->db->or_like('production.name', $search, 'both');
            $this->db->or_like('production.raw_quantity', $search, 'both');
            $this->db->or_like('recipe.name', $search, 'both');
            $this->db->or_like('items.name', $search, 'both');
            $this->db->group_end();
        }
        $this->db->select('production.id,production.status,production.type as production_type, production.code,production.raw_weight, production.recipe as recipe_id, production.production_date, production.quantity, production.name as production_name, production.raw_quantity, items.name as raw_name,items.avco_price as raw_avco, recipe.name as recipe_name');
        $this->db->from('production');
        $this->db->join('items', 'production.raw = items.id', 'left');
        $this->db->join('recipe', 'production.recipe = recipe.id', 'left');
        // $this->db->where('production.production_date >=', $stat_date);
        // $this->db->where('production.production_date <=', $en_date);
        $this->db->where('production.company_id', $this->session->userdata('user_company'));
        $this->db->limit($limit, $offset);
        $products = $this->db->get()->result_array();
         // var_dump($this->db->last_query());
         // die;
        $data = array();
        foreach ($products as $row) {
           
            $res = array(
                'code' => '',
                'production_date' => '',
                'production_name' => '',
                'raw_name' => '',
                'quantity' => '',
                'raw_quantity' => '',
                'recipe_name' => '',
                'status' => '',
                'option' => ''
            );

            $res['code'] = $row['code'];
            $res['production_date'] = $row['production_date'];
            $res['production_name'] = $row['production_name'];
            $res['raw_name'] = $row['raw_name'];
            $res['quantity'] = $row['quantity'];
            $res['raw_quantity'] = $row['raw_quantity'];
            $res['status'] = $row['status'];
            $res['recipe_name'] = $row['recipe_name'];

            if($this->session->userdata['user_type'] == 'Admin'){
                if($row['status'] == "Locked"){
                    $res['options'] = "<a  class=\"btn btn-info\" href=\"inventory/production_status/".$row['id']."\"><i class=\"icon-check icon-white\"></i> Status</a> 
                                <a  target=\"_blank\" class=\"btn btn-info\" href=\"inventory/production_recipe_detail/".$row['id']."\"><i class=\"icon-edit icon-white\"></i> Recipe Detail</a>
                                <a  target=\"_blank\" class=\"btn btn-info\" href=\"inventory/production_detail/".$row['id']."\"><i class=\"icon-edit icon-white\"></i> Production Detail</a>";
                }else{
                    $res['options'] = "<a  class=\"btn btn-info\" href=\"inventory/production_status/".$row['id']."\"><i class=\"icon-check icon-white\"></i> Status</a> 

                                <a  target=\"_blank\" class=\"btn btn-info\" href=\"inventory/production_recipe_detail/".$row['id']."\"><i class=\"icon-edit icon-white\"></i> Recipe Detail</a>
                                <a  target=\"_blank\" class=\"btn btn-purple\" href=\"inventory/production_selection_box/".$row['id']."\"><i class=\"icon-edit icon-white\"></i> Selection Box</a>
                                <a  target=\"_blank\" class=\"btn btn-info\" href=\"inventory/production_detail/".$row['id']."\"><i class=\"icon-edit icon-white\"></i> Production Detail</a>
                                
                                <a  class=\"btn btn-edit\" href=\"inventory/production_save/".$row['id']."\"><i class=\"icon-edit icon-white\"></i> Edit</a>
                              <a  class=\"btn del btn-danger\" onclick=\"delete_confirm('".$row['id']."')\"><i class=\"icon-trash icon-white\")\"></i> Delete</a>";
                }
                
             }else{
                if($this->privileges['recipe_detail'] ==1){
                    $res['options'] .= "<a  target=\"_blank\" class=\"btn btn-info\" href=\"inventory/production_recipe_detail/".$row['id']."\"><i class=\"icon-edit icon-white\"></i> Recipe Detail</a> ";
                }else{
                    $res['options'] .= "<a  class=\"btn btn-info disabled\" href=\"JavaScript:Void(0);\"><i class=\"icon-lock icon-white\"></i> Recipe Detail</a> ";
                }
                if($this->privileges['production_details'] ==1){
                    $res['options'] .= "<a  target=\"_blank\" class=\"btn btn-info\" href=\"inventory/production_detail/".$row['id']."\"><i class=\"icon-edit icon-white\"></i> Production Detail</a> ";
                }else{
                    $res['options'] .= "<a  class=\"btn btn-info disabled\" href=\"JavaScript:Void(0);\"><i class=\"icon-lock icon-white\"></i> Production Detail</a> ";
                }
                if($this->privileges['selection_box'] ==1 || $row['status'] == "Unapproved"){
                    $res['options'] .= "<a  target=\"_blank\" class=\"btn btn-purple\" href=\"inventory/production_selection_box/".$row['id']."\"><i class=\"icon-edit icon-white\"></i> Selection Box</a> ";
                }else{
                    $res['options'] .= "<a  class=\"btn btn-purple disabled\" href=\"JavaScript:Void(0);\"><i class=\"icon-lock icon-white\"></i> Selection Box</a> ";
                }

             }        
            $data[] = $res;
        }
        $result = array(
            'total' => $total,
            'rows' => $data
        );
        echo json_encode($result);
    }
    public function production_list_completed()
    {
        $data['title'] = 'POS System';
        $data['menu'] = 'inventory';
        $data['content'] = 'admin/inventory/production/list';
        $data['productions'] = $this->MProduction->get_all_with_join('','production.status = 2');
        $data['privileges'] = $this->privileges;
        $this->load->view('admin/template', $data);
    }
    
    public function production_save($id = NULL)
    {
        if ($this->input->post())
        {
            if ($this->input->post('id'))
            {

                // if($this->input->post('type') == 'Raw'){
                // $type = 'Wet Blue';
                // }elseif($this->input->post('type') == 'Wet Blue'){
                //     $type = 'FG';
                // }
                $this->MProduction->update();
                // $insert_id =  $this->MItems->create_by_production($type, $this->input->post('id'));
                // $this->MPurchase_master->create_by_production($insert_id, $type, $this->input->post('id'));
                $this->session->set_flashdata('success', 'Production Updated successfully.');
                redirect('inventory/production_list', 'refresh');
            }
            else
            {
                // if($this->input->post('type') == 'Raw'){
                // $type = 'Wet Blue';
                // }elseif($this->input->post('type') == 'Wet Blue'){
                //     $type = 'FG';
                // }
                
                $production_id = $this->MProduction->create();
                // $insert_id =  $this->MItems->create_by_production($type, $production_id);
                // $this->MPurchase_master->create_by_production($insert_id, $type, $production_id);
                $this->session->set_flashdata('success', 'Production saved successfully.');
                redirect('inventory/production_save', 'refresh');
            }

            
        }
        else
        {
            $data['title'] = 'POS System';
            $data['menu'] = 'inventory';
            $data['content'] = 'admin/inventory/production/save';
            $production = $this->MProduction->get_latest();
            if (count($production) > 0)
            {
                $data['code'] = (int)$production['code'] + 1;
            }
            else
            {
                $data['code'] = 1001;
            }
            $data['recipes'] = $this->MRecipe->get_all('0');
            $data['production'] = $this->MProduction->get_by_id($id);
            if($id){
                 $data['items'] = $this->MItems->get_all('', $data['production']['type']);
            }
           
            $data['chemicals'] = $this->MItems->get_all('', 'Chemical');
            $data['privileges'] = $this->privileges;
            $this->load->view('admin/template', $data);
        }
    }

public function production_delete($id)
    {
        // $sales = $this->MSales_master->get_by_customer_id($id);
        // if (count($sales) > 0)
        // {
        //     $this->session->set_flashdata('error', 'Customer can\'t delete, S/He is in Sales List.');
        // }
        // else
        // {
            // $this->session->set_flashdata('success', 'Production deleted successfully.');
            $this->MProduction->delete($id);
        // }
            $data['error'] = false;
            $data['message'] = 'success';
            echo json_encode($data);
    }
public function production_detail($id)
    {

        $data['production'] = $this->MProduction->get_all_with_join($id);
        $data['title'] = 'POS System';
        $data['privileges'] = $this->privileges;
        $this->load->view('admin/inventory/production/detail', $data);
    }
    public function production_selection_box($id = NULL)
    {
        //     $accounts_date = $this->MItems->get_dates();
        //     $stat_date = $accounts_date['start_date'];
        //     $en_date = $accounts_date['end_date'];
        $data['production'] = $this->MProduction->get_by_id($id);
       if($this->session->userdata['user_type'] == 'Admin' || $data['production']['status'] == 0 || $this->privileges['selection_box'] == 1){
            if ($this->input->post('id'))
            {

            $this->MProduction->create_selection_item();
        //      echo 'inventory/purchase_list';
            $this->MProduction->update_status($this->input->post('id'), 'Unapproved');
            $this->session->set_flashdata('success', 'Selection Item added successfully.');
            redirect('inventory/production_list', 'refresh');
        }else{
            if($data['production']['type'] == 'Raw'){
                $type = 'Wet Blue';
                }elseif($data['production']['type'] == 'Wet Blue'){
                    $type = 'FG';
                }
            $data['selections'] = $this->MItems->get_all('active', $type);
            $data['title'] = 'POS System';
            $data['menu'] = 'inventory';
            $data['content'] = 'admin/inventory/production/selection_box';
            $data['privileges'] = $this->privileges;
            $this->load->view('admin/template', $data);
        }
       }else{
        redirect('inventory/production_list', 'refresh');
       }
        

    }
    public function selection_list()
    {
        $data['title'] = 'POS System';
        $data['menu'] = 'inventory';
        $data['content'] = 'admin/inventory/selection/list';
        $data['selection'] = $this->MSelection->get_all();
        $data['privileges'] = $this->privileges;
        $this->load->view('admin/template', $data);
    }

    public function selection_save($id = NULL)
    {
        if ($this->input->post())
        {
            if ($this->input->post('id'))
            {
                $this->MSelection->update();
                $this->session->set_flashdata('success', 'Selection item Updated successfully.');
                redirect('inventory/selection_list', 'refresh');
            }
            else
            {
                $this->MSelection->create();
                $this->session->set_flashdata('success', 'Selection item saved successfully.');
                redirect('inventory/selection_save', 'refresh');
            }

            
        }
        else
        {
            $data['title'] = 'POS System';
            $data['menu'] = 'inventory';
            $data['content'] = 'admin/inventory/selection/save';
            $data['selection'] = $this->MSelection->get_by_id($id);
            $data['privileges'] = $this->privileges;
            $this->load->view('admin/template', $data);
        }
    }

    public function selection_delete($id)
    {
        // $sales = $this->MSales_master->get_by_customer_id($id);
        // if (count($sales) > 0)
        // {
        //     $this->session->set_flashdata('error', 'Customer can\'t delete, S/He is in Sales List.');
        // }
        // else
        // {
            $this->session->set_flashdata('success', 'selection item deleted successfully.');
            $this->MSelection->delete($id);
        // }

        redirect('inventory/selection_list', 'refresh');
    }

    public function customer_list()
    {
        $data['title'] = 'POS System';
        $data['menu'] = 'inventory';
        $data['content'] = 'admin/inventory/customer/list';
        $data['privileges'] = $this->privileges;
        $this->load->view('admin/template', $data);
    }
     public function customer_list_data(){

        $limit = $this->input->get('limit');
        $search = $this->input->get('search');
        $order = $this->input->get('order');
        $offset = $this->input->get('offset');
        $sort = $this->input->get('sort');
        if ($search) {
            
            $this->db->group_start();
            $this->db->like('customers.code', $search, 'both');
            $this->db->or_like('customers.name', $search, 'both');
            $this->db->or_like('customers.address', $search, 'both');
            $this->db->or_like('customers.mobile', $search, 'both');
            $this->db->or_like('customers.email', $search, 'both');
            $this->db->or_like('ac_charts.name', $search, 'both');
            $this->db->or_like('ac_charts.code', $search, 'both');
            $this->db->or_like('customers.status', $search, 'after');
            $this->db->group_end();
        }
        $this->db->select('customers.id, customers.code, customers.name, customers.address,customers.mobile, customers.email, customers.status, ac_charts.name as chart_name, ac_charts.code as chart_code');
        $this->db->from("customers");
        $this->db->join("ac_charts","customers.ac_id = ac_charts.id","left");
        $this->db->where('customers.company_id', $this->session->userdata('user_company'));
        $total = $this->db->get()->num_rows();
        $this->db->limit($limit);
        if ($sort == '') {
            $sort = 'customers.id';
            $order = 'DESC';
        }
        $this->db->order_by($sort, $order);
        if ($search) {
            
            $this->db->group_start();
            $this->db->like('customers.code', $search, 'both');
            $this->db->or_like('customers.name', $search, 'both');
            $this->db->or_like('customers.address', $search, 'both');
            $this->db->or_like('customers.mobile', $search, 'both');
            $this->db->or_like('customers.email', $search, 'both');
            $this->db->or_like('ac_charts.name', $search, 'both');
            $this->db->or_like('ac_charts.code', $search, 'both');
            $this->db->or_like('customers.status', $search, 'after');
            $this->db->group_end();
        }
        $this->db->select('customers.id, customers.code, customers.name, customers.address,customers.mobile, customers.email, customers.status, ac_charts.name as chart_name, ac_charts.code as chart_code');
        $this->db->from("customers");
        $this->db->join("ac_charts","customers.ac_id = ac_charts.id","left");
        $this->db->where('customers.company_id', $this->session->userdata('user_company'));
        $this->db->limit($limit, $offset);
        $products = $this->db->get()->result_array();
         
        $data = array();
        foreach ($products as $row) {
            $res = array(
                'code' => '',
                'name' => '',
                'address' => '',
                'mobile' => '',
                'email' => '',
                'chart_name' => '',
                'status' => '',
                'option' => ''
            );
            $res['code'] = $row['code'];
            $res['name'] = $row['name'];
            $res['address'] = $row['address'];
            $res['mobile'] = $row['mobile'];
            $res['email'] = $row['email'];
            if(!empty($row['chart_name'])){
                $res['chart_name'] = $row['chart_name']." (".$row['chart_code'].")";
            }
            $res['status'] = $row['status'];
            if($this->session->userdata['user_type'] == 'Admin'){
                $res['options'] = "<a  class=\"btn btn-edit\" href=\"inventory/customer_save/".$row['id']."\"><i class=\"icon-edit icon-white\"></i> Edit</a>
                             <a  class=\"btn del btn-danger\" onclick=\"delete_confirm('".$row['id']."')\"><i class=\"icon-trash icon-white\")\"></i> Delete</a>";
            }        
            $data[] = $res;
        }
        $result = array(
            'total' => $total,
            'rows' => $data
        );
        echo json_encode($result);
    }
    public function customer_save($id = NULL)
    {
        if ($this->input->post())
        {
            if ($this->input->post('id'))
            {
                $this->MCustomers->update(trim($this->input->post('code')));
                $cust = $this->MCustomers->get_by_id($this->input->post('id'));
                $ac_id = $this->MAc_charts->account_update($cust['ac_id'], $this->input->post('name'));
                $this->session->set_flashdata('success', 'Customer Updated successfully.');
                redirect('inventory/customer_list', 'refresh');
            }
            else
            {
                $ac_receivable = $this->MSettings->get_by_company_id($this->session->userdata('user_company'));

                $chart = $this->MAc_charts->get_by_id($ac_receivable['ac_receivable']);

                $siblings = $this->MAc_charts->get_by_parent_id($ac_receivable['ac_receivable']);

                if (count($siblings) > 0)
                {
                    $ac_code_temp = explode('.', $siblings['code']);

                    $ac_last = count($ac_code_temp) - 1;

                    $ac_new = (int) $ac_code_temp[$ac_last] + 10;

                    $ac_code = $chart['code'] . '.' . $ac_new;

                }
                else
                {
                    $ac_code = $chart['code'] . '.10';
                }

                $ac_id = $this->MAc_charts->account_create($ac_receivable['ac_receivable'], $ac_code, $this->input->post('name'));

                // $customer = $this->MCustomers->get_latest();
                // if ( count( $customer ) > 0 ) {
                //     $code = (int) $customer['code'] + 1;
                // } else {
                //     $code = 1001;
                // }
                // $ac_id = $this->input->post('ac_chart');
                $this->MCustomers->create(trim($this->input->post('code')), $ac_id);
                $this->session->set_flashdata('success', 'Customer saved successfully.');
                redirect('inventory/customer_save', 'refresh');
            }

            
        }
        else
        {
            $data['title'] = 'POS System';
            $data['menu'] = 'inventory';
            $data['content'] = 'admin/inventory/customer/save';
            $customer = $this->MCustomers->get_latest();
            if (count($customer) > 0)
            {
                $data['code'] = (int)$customer['code'] + 1;
            }
            else
            {
                $data['code'] = 1001;
            }
            if($id){
                $data['customer'] = $this->MCustomers->get_by_id($id);
            }else{
                $data['customer'] = NULL;
            }
            
            // $chart_id = $data['customer']['ac_id'];
            // $data['charts'] = $this->MAc_charts->get_coa_tree($chart_id);
            $data['privileges'] = $this->privileges;
            $this->load->view('admin/template', $data);
        }
    }
    public function add_new_customer()
    {
        if ($this->input->post())
        {
            $ac_receivable = $this->MSettings->get_by_company_id($this->session->userdata('user_company'));
            $chart = $this->MAc_charts->get_by_id($ac_receivable['ac_receivable']);
            $siblings = $this->MAc_charts->get_by_parent_id($ac_receivable['ac_receivable']);
            if (count($siblings) > 0)
            {
                $ac_code_temp = explode('.', $siblings['code']);
                $ac_last = count( $ac_code_temp ) - 1;
                $ac_new = (int)$ac_code_temp[$ac_last] + 10;
                $ac_code = $chart['code'] . '.' . $ac_new;
            }
            else
            {
                $ac_code = $chart['code'] . '.10';
            }

            $ac_id = $this->MAc_charts->account_create($ac_receivable['ac_receivable'], $ac_code, $this->input->post('name'));
            $insert_id = $this->MCustomers->create(trim($this->input->post('code')), $ac_id);
            $customers = $this->MCustomers->get_all();
            $html = '';
            foreach ($customers as $customer)
            {
                if ($insert_id == $customer['id'])
                {
                    $html .= '<option value="' . $customer['id'] . '" selected>' . $customer['name'] . '</option>';
                }
                else
                {
                    $html .= '<option value="' . $customer['id'] . '">' . $customer['name'] . '</option>';
                }
            }
            echo $html;
        }
    }

    public function customer_delete($id)
    {
        $sales = $this->MSales_master->get_by_customer_id($id);
        if (count($sales) > 0)
        {
            $data['error'] = true;
            $data['message'] = 'Customer can\'t delete, S/He is in Sales List.';
        }
        else
        {
            $data['error'] = false;
            $data['message'] = 'success';
            $this->MCustomers->delete($id);
        }
        
        echo json_encode($data);
    }

    /* ---------------------- Supplier Start -------------- */

    public function supplier_list()
    {
        $data['title'] = 'POS System';
        $data['menu'] = 'inventory';
        $data['content'] = 'admin/inventory/supplier/list';
        $data['privileges'] = $this->privileges;
        $this->load->view('admin/template', $data);
    }
public function supplier_list_data(){

        $limit = $this->input->get('limit');
        $search = $this->input->get('search');
        $order = $this->input->get('order');
        $offset = $this->input->get('offset');
        $sort = $this->input->get('sort');
        if ($search) {
            
            $this->db->group_start();
            $this->db->like('suppliers.code', $search, 'both');
            $this->db->or_like('suppliers.name', $search, 'both');
            $this->db->or_like('suppliers.address', $search, 'both');
            $this->db->or_like('suppliers.phone_no', $search, 'both');
            $this->db->or_like('suppliers.contact_person', $search, 'both');
            $this->db->or_like('ac_charts.name', $search, 'both');
            $this->db->or_like('ac_charts.code', $search, 'both');
            $this->db->or_like('suppliers.status', $search, 'after');
            $this->db->group_end();
        }
        $this->db->select('suppliers.id, suppliers.code, suppliers.name, suppliers.address,suppliers.phone_no, suppliers.contact_person, suppliers.status, ac_charts.name as chart_name, ac_charts.code as chart_code');
        $this->db->from("suppliers");
        $this->db->join("ac_charts","suppliers.ac_id = ac_charts.id","left");
        $this->db->where('suppliers.company_id', $this->session->userdata('user_company'));
        $total = $this->db->get()->num_rows();
        $this->db->limit($limit);
        if ($sort == '') {
            $sort = 'suppliers.id';
            $order = 'DESC';
        }
        $this->db->order_by($sort, $order);
        if ($search) {
            
            $this->db->group_start();
            $this->db->like('suppliers.code', $search, 'both');
            $this->db->or_like('suppliers.name', $search, 'both');
            $this->db->or_like('suppliers.address', $search, 'both');
            $this->db->or_like('suppliers.phone_no', $search, 'both');
            $this->db->or_like('suppliers.contact_person', $search, 'both');
            $this->db->or_like('ac_charts.name', $search, 'both');
            $this->db->or_like('ac_charts.code', $search, 'both');
            $this->db->or_like('suppliers.status', $search, 'after');
            $this->db->group_end();
        }
        $this->db->select('suppliers.id, suppliers.code, suppliers.name, suppliers.address,suppliers.phone_no, suppliers.contact_person, suppliers.status, ac_charts.name as chart_name, ac_charts.code as chart_code');
        $this->db->from("suppliers");
        $this->db->join("ac_charts","suppliers.ac_id = ac_charts.id","left");
        $this->db->where('suppliers.company_id', $this->session->userdata('user_company'));
        $this->db->limit($limit, $offset);
        $products = $this->db->get()->result_array();
         
        $data = array();
        foreach ($products as $row) {
            $res = array(
                'code' => '',
                'name' => '',
                'address' => '',
                'mobile' => '',
                'contact_person' => '',
                'chart_name' => '',
                'status' => '',
                'option' => ''
            );
            $res['code'] = $row['code'];
            $res['name'] = $row['name'];
            $res['address'] = $row['address'];
            $res['mobile'] = $row['phone_no'];
            $res['contact_person'] = $row['contact_person'];
            if(!empty($row['chart_name'])){
                $res['chart_name'] = $row['chart_name']." (".$row['chart_code'].")";
            }
            $res['status'] = $row['status'];
            if($this->session->userdata['user_type'] == 'Admin'){
                $res['options'] = "<a  class=\"btn btn-edit\" href=\"inventory/supplier_save/".$row['id']."\"><i class=\"icon-edit icon-white\"></i> Edit</a>
                <a  class=\"btn del btn-danger\" onclick=\"delete_confirm('".$row['id']."')\"><i class=\"icon-trash icon-white\")\"></i> Delete</a>";
            }        
            $data[] = $res;
        }
        $result = array(
            'total' => $total,
            'rows' => $data
        );
        echo json_encode($result);
    }
    public function supplier_save($id = NULL)
    {
        if ($this->input->post())
        {
            if ($this->input->post('id'))
            {
               
                $sup = $this->MSuppliers->get_by_id($this->input->post('id'));
                if($sup['ac_id'] <> 0 && $sup['ac_id'] <> '' && $sup['ac_id'] <> NULL){
                    $this->MSuppliers->update(trim($this->input->post('code')));
                   $ac_id = $this->MAc_charts->account_update($sup['ac_id'], $this->input->post('name'));
                }else{
                    $ac_payable = $this->MSettings->get_by_company_id($this->session->userdata('user_company'));

                    $chart = $this->MAc_charts->get_by_id($ac_payable['ac_payable']);
                    $siblings = $this->MAc_charts->get_by_parent_id($ac_payable['ac_payable']);

                    if (count($siblings) > 0)
                    {
                        $ac_code_temp = explode('.', $siblings['code']);
                        $ac_last = count($ac_code_temp) - 1;
                        $ac_new = (int) $ac_code_temp[$ac_last] + 10;
                        $ac_code = $chart['code'] . '.' . $ac_new;
                    }
                    else
                    {
                        $ac_code = $chart['code'] . '.10';
                    }
                   
                    $ac_id = $this->MAc_charts->account_create($ac_payable['ac_payable'], $ac_code, $this->input->post('name'));
                    $this->MSuppliers->update(trim($this->input->post('code')), $ac_id);
                }
                 
                
                $this->session->set_flashdata('message', 'Supplier Updated successfully.');
            redirect('inventory/supplier_list', 'refresh');
            }
            else
            {
                $ac_payable = $this->MSettings->get_by_company_id($this->session->userdata('user_company'));

                $chart = $this->MAc_charts->get_by_id($ac_payable['ac_payable']);
                $siblings = $this->MAc_charts->get_by_parent_id($ac_payable['ac_payable']);

                if (count($siblings) > 0)
                {
                    $ac_code_temp = explode('.', $siblings['code']);
                    $ac_last = count($ac_code_temp) - 1;
                    $ac_new = (int) $ac_code_temp[$ac_last] + 10;
                    $ac_code = $chart['code'] . '.' . $ac_new;
                }
                else
                {
                    $ac_code = $chart['code'] . '.10';
                }

                $ac_id = $this->MAc_charts->account_create($ac_payable['ac_payable'], $ac_code, $this->input->post('name'));

                // $supplier = $this->MSuppliers->get_latest();
                // if ( count( $supplier ) > 0 ) {
                //     $code = (int) $supplier['code'] + 1;
                // } else {
                //     $code = 1001;
                // }
                $this->MSuppliers->create(trim($this->input->post('code')), $ac_id);
                $this->session->set_flashdata('message', 'Supplier saved successfully.');
                redirect('inventory/supplier_save', 'refresh');
            }
            
        }
        else
        {
            $data['title'] = 'POS System';
            $data['menu'] = 'inventory';
            $data['content'] = 'admin/inventory/supplier/save';
            $supplier = $this->MSuppliers->get_latest();
            if (count($supplier) > 0)
            {
                $data['code'] = (int)$supplier['code'] + 1;
            }
            else
            {
                $data['code'] = 1001;
            }
            if($id){
                $data['supplier'] = $this->MSuppliers->get_by_id($id);
            }else{
                $data['supplier'] = NULL;
            }
            
            $data['privileges'] = $this->privileges;
            $this->load->view('admin/template', $data);
        }
    }

    public function add_new_supplier()
    {
        if ($this->input->post())
        {
            $ac_payable = $this->MSettings->get_by_company_id($this->session->userdata('user_company'));
            $chart = $this->MAc_charts->get_by_id($ac_payable['ac_payable']);
            $siblings = $this->MAc_charts->get_by_parent_id($ac_payable['ac_payable']);
            if (count($siblings) > 0)
            {
                $ac_code_temp = explode('.', $siblings['code']);
                $ac_last = count($ac_code_temp) - 1;
                $ac_new = (int)$ac_code_temp[$ac_last] + 10;
                $ac_code = $chart['code'] . '.' . $ac_new;
            }
            else
            {
                $ac_code = $chart['code'] . '.10';
            }
            $ac_id = $this->MAc_charts->account_create($ac_payable['ac_payable'], $ac_code, $this->input->post('name'));
            $insert_id = $this->MSuppliers->create(trim($this->input->post('code')), $ac_id);
            $suppliers = $this->MSuppliers->get_all();
            $html = '';
            foreach ($suppliers as $supplier)
            {
                if ($insert_id == $supplier['id'])
                {
                    $html .= '<option value="' . $supplier['id'] . '" selected>' . $supplier['name'] . '</option>';
                }
                else
                {
                    $html .= '<option value="' . $supplier['id'] . '">' . $supplier['name'] . '</option>';
                }
            }
            echo $html;
        }
    }

    public function supplier_delete($id)
    {
        $purchase = $this->MPurchase_master->get_by_supplier_id($id);
        if (count($purchase) > 0)
        {
            $data['error'] = true;
            $data['message'] = 'Supplier can\'t delete, S/He is in Purchase List.';
        }
        else
        {
            $data['error'] = false;
            $data['message'] = 'success';
            $this->MSuppliers->delete($id);
        }

        echo json_encode($data);
    }

    public function item_grade_save($id = NULL)
    {
        if ($this->input->post())
        {
            if ($this->input->post('id'))
            {
                $this->MItems->update_grade();
                $this->session->set_flashdata('success', 'Item Grade updated successfully.');
                redirect('inventory/item_grade_list', 'refresh');
            }
            else
            {
                
                $this->form_validation->set_rules('grade_name', 'Grade Name', 'required');
                
                if ($this->form_validation->run() == FALSE)
                {
                    $data['title'] = 'POS System';
                    $data['menu'] = 'inventory';
                    $data['content'] = 'admin/inventory/item_grade/save';
                    $data['item'] = $this->MItems->get_by_id_grade($id);
                    $data['privileges'] = $this->privileges;
                    $this->load->view('admin/template', $data);
                }
                else
                {

                    $this->MItems->create_grade();
                    $this->session->set_flashdata('success', 'Item Grade created successfully.');
                    redirect('inventory/item_grade_list', 'refresh');
                }
            }
        }
        else
        {
            $data['title'] = 'POS System';
            $data['menu'] = 'inventory';
            $data['content'] = 'admin/inventory/item_grade/save';
            $data['item'] = $this->MItems->get_by_id_grade($id);
            $data['privileges'] = $this->privileges;
            $this->load->view('admin/template', $data);
        }
    }

    public function item_grade_list()
    {
        $data['title'] = 'POS System';
        $data['menu'] = 'inventory';
        $data['content'] = 'admin/inventory/item_grade/list';
        $data['items'] = $this->MItems->get_all_grade();
        $data['privileges'] = $this->privileges;
        $this->load->view('admin/template', $data);
    }

     public function item_grade_delete($id)
    {
        
            $this->MItems->delete_grade($id);
            $this->session->set_flashdata('success', 'Item Grade deleted successfully.');
       
        redirect('inventory/item_grade_list', 'refresh');
    }
    /* -------------------- Item ----------------- */

    public function item_list()
    {

            $data['title'] = 'POS System';
            $data['menu'] = 'inventory';
            $data['content'] = 'admin/inventory/item/list';
            $data['privileges'] = $this->privileges;
            $this->load->view('admin/template', $data);
     }
    public function item_list_data(){
        
        $limit = $this->input->get('limit');
        $search = $this->input->get('search');
        $order = $this->input->get('order');
        $offset = $this->input->get('offset');
        $sort = $this->input->get('sort');
        if ($search) {
            
            $this->db->group_start();
            $this->db->like('items.code', $search, 'both');
            $this->db->or_like('items.name', $search, 'both');
            $this->db->or_like('items.avco_price', $search, 'both');
            $this->db->or_like('items.status', $search, 'after');
            $this->db->or_like('items.re_order', $search, 'both');
            $this->db->or_like('items.type', $search, 'both');
            $this->db->or_like('ac_charts.name', $search, 'both');
            $this->db->or_like('ac_charts.code', $search, 'both');
            $this->db->group_end();
        }
        $this->db->select("items.id, items.code, items.type, items.name,items.avco_price,items.status, items.re_order, ac_charts.name as chart_name, ac_charts.code as chart_code ");
        $this->db->from("items");
        $this->db->join("ac_charts","items.ac_id = ac_charts.id","left");
        $this->db->where('items.company_id', $this->session->userdata('user_company'));
        $total = $this->db->get()->num_rows();
        $this->db->limit($limit);
        if ($sort == '') {
            $sort = 'items.id';
            $order = 'DESC';
        }
        $this->db->order_by($sort, $order);
        if ($search) {
            
            $this->db->group_start();
            $this->db->like('items.code', $search, 'both');
            $this->db->or_like('items.name', $search, 'both');
            $this->db->or_like('items.avco_price', $search, 'both');
            $this->db->or_like('items.status', $search, 'after');
            $this->db->or_like('items.re_order', $search, 'both');
            $this->db->or_like('items.type', $search, 'both');
            $this->db->or_like('ac_charts.name', $search, 'both');
            $this->db->or_like('ac_charts.code', $search, 'both');
            $this->db->group_end();
        }
        $this->db->select("items.id, items.code, items.type, items.name,items.avco_price,items.status, items.re_order, ac_charts.name as chart_name, ac_charts.code as chart_code ");
        $this->db->from("items");
        $this->db->join("ac_charts","items.ac_id = ac_charts.id","left");
        $this->db->where('items.company_id', $this->session->userdata('user_company'));
        $this->db->limit($limit, $offset);
        $products = $this->db->get()->result_array();
         
        $data = array();
        foreach ($products as $row) {
            $res = array(
                'code' => '',
                'name' => '',
                'avco_price' => '',
                're_order' => '',
                'chart_name' => '',
                'type' => '',
                'status' => '',
                'option' => ''
            );
            $res['code'] = $row['code'];
            $res['name'] = $row['name'];
            $res['avco_price'] = $row['avco_price'];
            $res['re_order'] = $row['re_order'];
            if(!empty($row['chart_name'])){
                $res['chart_name'] = $row['chart_name']." (".$row['chart_code'].")";
            }
            $res['status'] = $row['status'];
            $res['type'] = $row['type'];
            if($this->session->userdata['user_type'] == 'Admin'){
                $res['options'] = "<a  class=\"btn btn-edit\" href=\"inventory/item_save/".$row['id']."\"><i class=\"icon-edit icon-white\"></i> Edit</a>
                              <a  class=\"btn del btn-danger\" onclick=\"delete_confirm('".$row['id']."')\"><i class=\"icon-trash icon-white\")\"></i> Delete</a>";
            }        
            $data[] = $res;
        }
        $result = array(
            'total' => $total,
            'rows' => $data
        );
        echo json_encode($result);
    }
    // public function item_master()
    // {
    //     $data['title'] = 'POS System';
    //     $data['menu'] = 'inventory';
    //     $data['content'] = 'admin/inventory/item/inventory_master';
    //     $data['privileges'] = $this->privileges;
    //     $this->load->view('admin/template', $data);
    // }

    public function item_save($id = NULL)
    {
        if ($this->input->post())
        {
            if ($this->input->post('id'))
            {
                $this->MItems->update();
                $itm = $this->MItems->get_by_id($this->input->post('id'));
              
                $ac_id = $this->MAc_charts->account_update($itm['ac_id'], $this->input->post('name'), $this->input->post('type'));
                $this->session->set_flashdata('success', 'Item updated successfully.');
                redirect('inventory/item_list', 'refresh');
            }
            else
            {
                $this->form_validation->set_rules('code', 'Item Code', 'callback_code_check');
                $this->form_validation->set_rules('name', 'Item Name', 'required');
                // $this->form_validation->set_rules('min_sale_price', 'Min. Sale Price', 'required');

                if ($this->form_validation->run() == FALSE)
                {
                    $data['title'] = 'POS System';
                    $data['menu'] = 'inventory';
                    $data['content'] = 'admin/inventory/item/save';
                    $data['item'] = $this->MItems->get_by_id($id);
                    $data['privileges'] = $this->privileges;
                    $this->load->view('admin/template', $data);
                }
                else
                {
                    $inventory = $this->MSettings->get_by_company_id($this->session->userdata('user_company'));
                if($this->input->post('type') == 'Raw'){
                    $chart = $this->MAc_charts->get_by_id($inventory['ac_inventory_raw']);
                    $siblings = $this->MAc_charts->get_by_parent_id($inventory['ac_inventory_raw']);
                    $parient_id = $inventory['ac_inventory_raw'];
                }else if($this->input->post('type') == 'Wet Blue'){
                    $chart = $this->MAc_charts->get_by_id($inventory['ac_inventory_wet_blue']);
                    $siblings = $this->MAc_charts->get_by_parent_id($inventory['ac_inventory_wet_blue']);
                    $parient_id = $inventory['ac_inventory_wet_blue'];
                }else if($this->input->post('type') == 'Chemical'){
                        $chart = $this->MAc_charts->get_by_id($inventory['ac_inventory_chemical']);
                    $siblings = $this->MAc_charts->get_by_parent_id($inventory['ac_inventory_chemical']);
                    $parient_id = $inventory['ac_inventory_chemical'];
                }else if($this->input->post('type') == 'FG'){
                        $chart = $this->MAc_charts->get_by_id($inventory['ac_inventory_fg']);
                    $siblings = $this->MAc_charts->get_by_parent_id($inventory['ac_inventory_fg']);
                    $parient_id = $inventory['ac_inventory_fg'];
                }
                    if (count($siblings) > 0)
                    {
                        $ac_code_temp = explode('.', $siblings['code']);
                        $ac_last = count($ac_code_temp) - 1;
                        $ac_new = (int) $ac_code_temp[$ac_last] + 1;
                        $ac_code = $chart['code'] . '.' . $ac_new;
                    }
                    else
                    {
                        $ac_code = $chart['code'] . '.10';
                    }
                    $ac_id = $this->MAc_charts->account_create($parient_id, $ac_code, $this->input->post('name'), $this->input->post('type'));
                    $this->MItems->create($ac_id);
                    $this->session->set_flashdata('success', 'Item created successfully.');
                    redirect('inventory/item_save', 'refresh');
                }
            }
        }
        else
        {
            $data['title'] = 'POS System';
            $data['menu'] = 'inventory';
            $data['content'] = 'admin/inventory/item/save';
            $data['item'] = $this->MItems->get_by_id($id);
            $items = $this->MItems->get_latest();
            if (count($items) > 0)
            {
                $data['code'] = (int)$items['code'] + 1;
            }
            else
            {
                $data['code'] = 1001;
            }

            $data['privileges'] = $this->privileges;
            $this->load->view('admin/template', $data);
        }
    }

    public function code_check($code)
    {
        if( $this->MItems->get_by_code($code))
        {
            $this->form_validation->set_message('code_check', 'Item code can\'t be duplicate. Please choose different item code.');
            return false;
        }
        else
        {
            return true;
        }
    }

    public function item_delete($id)
    {
        $purchase = $this->MPurchase_details->get_by_item_id($id);
        if (count($purchase) > 0)
        {
            $data['error'] = true;
            $data['message'] = 'Item Can\'t delete, It is in Purchase List.';
        }
        else
        {
            $data['error'] = false;
            $data['message'] = 'success';
            $this->MItems->delete($id);
            
        }

        echo json_encode($data);
    }

    public function test_avco()
    {
        echo $this->MPurchase_details->get_avco(1);
    }

}
