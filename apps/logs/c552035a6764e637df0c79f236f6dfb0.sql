INSERT INTO `sync_info` (`filename`) VALUES ('c552035a6764e637df0c79f236f6dfb0');
DELETE FROM `emps`
WHERE `id` = '64';
UPDATE `sync_info` SET `is_sent` = 1
WHERE `id` = '9';
UPDATE `sync_info` SET `is_ack` = 1
WHERE `id` = '9';
UPDATE `accounts_calander` SET `start_date` = '2018-07-01', `end_date` = '2019-06-30', `created` = '2019-06-17 20:18:36'
WHERE `user_id` = '8';
UPDATE `accounts_calander` SET `start_date` = '2019-07-01', `end_date` = '2020-06-30', `created` = '2019-11-17 11:20:36'
WHERE `user_id` = '8';
UPDATE `accounts_calander` SET `start_date` = '2019-07-01', `end_date` = '2020-06-30', `created` = '2019-11-17 11:21:54'
WHERE `user_id` = '8';
INSERT INTO `purchase_master` (`company_id`, `supplier_id`, `purchase_no`, `supplier_no`, `gate_no`, `purchase_date`, `notes`, `status`, `created`, `created_by`) VALUES ('2', '39', '1715', '12', '12', '2019-11-17', 'sdsad', '1', '2019-11-17 11:29:41', '8');
INSERT INTO `purchase_details` (`company_id`, `purchase_no`, `item_id`, `item_type`, `ac_id`, `quantity`, `sq_weight`, `unit`, `vat_percent`, `vat_amount`, `purchase_date`, `purchase_price`, `total_price`, `created`, `created_by`) VALUES ('2', '1715', '44', 'Raw', '577', '10', '12', 'sq ft', '12', 17.28, '2019-11-17', '12', 144, '2019-11-17 11:29:41', '8');
UPDATE `items` SET `avco_price` = 12, `modified` = '2019-11-17 11:29:41', `modified_by` = '8'
WHERE `id` = '44';
UPDATE `purchase_master` SET `company_id` = '2', `supplier_id` = '39', `purchase_no` = '1715', `supplier_no` = '12', `gate_no` = '12', `draft` = 0, `purchase_date` = '2019-11-17', `notes` = 'sdsad', `modified` = '2019-11-17 11:29:43', `modified_by` = '8'
WHERE `id` = '981';
INSERT INTO `ac_journal_master` (`company_id`, `journal_no`, `journal_date`, `memo`, `doc_type`, `doc_no`, `created`, `created_by`) VALUES ('2', 'Purchase_1715', '2019-11-17', 'Direct Purchase', 'Purchase', '1715', '2019-11-17 11:29:43', '8');
UPDATE `purchase_master` SET `ac_id` = 3230
WHERE `id` = '981';
INSERT INTO `ac_journal_details` (`company_id`, `journal_no`, `chart_id`, `debit`, `credit`, `memo`, `journal_date`, `created`, `created_by`) VALUES ('2', 'Purchase_1715', '970', NULL, 161, 'sdsad', '2019-11-17', '2019-11-17 11:29:43', '8');
INSERT INTO `ac_journal_details` (`company_id`, `journal_no`, `chart_id`, `debit`, `credit`, `memo`, `journal_date`, `created`, `created_by`) VALUES ('2', 'Purchase_1715', '577', '144', NULL, 'sdsad', '2019-11-17', '2019-11-17 11:29:43', '8');
INSERT INTO `ac_journal_details` (`company_id`, `journal_no`, `chart_id`, `debit`, `credit`, `memo`, `journal_date`, `created`, `created_by`) VALUES ('2', 'Purchase_1715', '1034', 17, NULL, 'sdsad', '2019-11-17', '2019-11-17 11:29:43', '8');
INSERT INTO `ac_charts` (`company_id`, `parent_id`, `code`, `name`, `type`, `created_at`, `created_by`) VALUES ('2', NULL, '.10', 'Other chemical', 'Other Items', '2019-11-17 11:42:31', '8');
INSERT INTO `items` (`company_id`, `code`, `name`, `ac_id`, `type`, `description`, `re_order`, `status`, `created`, `created_by`) VALUES ('2', '1503', 'Other chemical', 1292, 'Other Items', 'asd', '', 'Active', '2019-11-17 11:42:31', '8');
INSERT INTO `purchase_master` (`company_id`, `supplier_id`, `purchase_no`, `supplier_no`, `gate_no`, `purchase_date`, `notes`, `status`, `created`, `created_by`) VALUES ('2', '42', '1716', '12', '23', '2019-11-17', 'sdsd', '1', '2019-11-17 11:44:54', '8');
INSERT INTO `purchase_details` (`company_id`, `purchase_no`, `item_id`, `item_type`, `ac_id`, `quantity`, `sq_weight`, `unit`, `vat_percent`, `vat_amount`, `purchase_date`, `purchase_price`, `total_price`, `created`, `created_by`) VALUES ('2', '1716', '537', 'Other Items', '1292', 'asd', 'asd', 'sq ft', '', 0, '2019-11-17', 'asd', 0, '2019-11-17 11:44:54', '8');
UPDATE `items` SET `avco_price` = NAN, `modified` = '2019-11-17 11:44:54', `modified_by` = '8'
WHERE `id` = '537';
DELETE FROM `purchase_details`
WHERE `id` = '2920';
UPDATE `items` SET `avco_price` = NAN, `modified` = '2019-11-17 11:45:02', `modified_by` = '8'
WHERE `id` = '2920';
UPDATE `purchase_master` SET `company_id` = '2', `supplier_id` = '42', `purchase_no` = '1716', `supplier_no` = '12', `gate_no` = '23', `draft` = 0, `purchase_date` = '2019-11-17', `notes` = 'asd', `modified` = '2019-11-17 11:45:25', `modified_by` = '8'
WHERE `id` = '982';
INSERT INTO `purchase_details` (`company_id`, `purchase_no`, `item_id`, `item_type`, `ac_id`, `quantity`, `sq_weight`, `unit`, `vat_percent`, `vat_amount`, `purchase_date`, `purchase_price`, `total_price`, `created`, `created_by`) VALUES ('2', '1716', '537', 'Other Items', '1292', '12', '13', 'sq ft', '12', 18.72, '2019-11-17', '12', 156, '2019-11-17 11:45:25', '8');
UPDATE `items` SET `avco_price` = 12, `modified` = '2019-11-17 11:45:25', `modified_by` = '8'
WHERE `id` = '537';
INSERT INTO `purchase_master` (`company_id`, `supplier_id`, `purchase_no`, `supplier_no`, `gate_no`, `purchase_date`, `notes`, `status`, `created`, `created_by`) VALUES ('2', '42', '1717', '12', 'sd', '2019-11-17', 'asd', '1', '2019-11-17 12:09:31', '8');
INSERT INTO `purchase_details` (`company_id`, `purchase_no`, `item_id`, `item_type`, `ac_id`, `quantity`, `sq_weight`, `unit`, `vat_percent`, `vat_amount`, `purchase_date`, `purchase_price`, `total_price`, `created`, `created_by`) VALUES ('2', '1717', '537', 'Other Items', '1292', '10', '10', 'Kg', '10', 10, '2019-11-17', '10', 100, '2019-11-17 12:09:31', '8');
UPDATE `items` SET `avco_price` = 11.130434782609, `modified` = '2019-11-17 12:09:31', `modified_by` = '8'
WHERE `id` = '537';
UPDATE `purchase_master` SET `company_id` = '2', `supplier_id` = '42', `purchase_no` = '1717', `supplier_no` = '12', `gate_no` = 'sd', `draft` = 0, `purchase_date` = '2019-11-17', `notes` = 'asd', `modified` = '2019-11-17 12:09:34', `modified_by` = '8'
WHERE `id` = '983';
INSERT INTO `ac_journal_master` (`company_id`, `journal_no`, `journal_date`, `memo`, `doc_type`, `doc_no`, `created`, `created_by`) VALUES ('2', 'Purchase_1717', '2019-11-17', 'Direct Purchase', 'Purchase', '1717', '2019-11-17 12:09:34', '8');
UPDATE `purchase_master` SET `ac_id` = 3231
WHERE `id` = '983';
INSERT INTO `ac_journal_details` (`company_id`, `journal_no`, `chart_id`, `debit`, `credit`, `memo`, `journal_date`, `created`, `created_by`) VALUES ('2', 'Purchase_1717', '967', NULL, 110, 'asd', '2019-11-17', '2019-11-17 12:09:34', '8');
INSERT INTO `ac_journal_details` (`company_id`, `journal_no`, `chart_id`, `debit`, `credit`, `memo`, `journal_date`, `created`, `created_by`) VALUES ('2', 'Purchase_1717', '1292', '100', NULL, 'asd', '2019-11-17', '2019-11-17 12:09:34', '8');
INSERT INTO `ac_journal_details` (`company_id`, `journal_no`, `chart_id`, `debit`, `credit`, `memo`, `journal_date`, `created`, `created_by`) VALUES ('2', 'Purchase_1717', '1034', 10, NULL, 'asd', '2019-11-17', '2019-11-17 12:09:34', '8');
DELETE FROM `ac_journal_details`
WHERE `journal_date` >= '2019-07-01'
AND `journal_date` <= '2020-06-30'
AND `journal_no` = 'Purchase_1717';
DELETE FROM `ac_journal_master`
WHERE `journal_no` = 'Purchase_1717'
AND `journal_date` >= '2019-07-01'
AND `journal_date` <= '2020-06-30';
DELETE FROM `purchase_details`
WHERE `purchase_date` >= '2019-07-01'
AND `purchase_date` <= '2020-06-30'
AND `purchase_no` = '1717';
DELETE FROM `purchase_master`
WHERE `id` = '983';
DELETE FROM `purchase_details`
WHERE `purchase_date` >= '2019-07-01'
AND `purchase_date` <= '2020-06-30'
AND `purchase_no` = '1716';
DELETE FROM `purchase_master`
WHERE `id` = '982';
DELETE FROM `ac_journal_details`
WHERE `journal_date` >= '2019-07-01'
AND `journal_date` <= '2020-06-30'
AND `journal_no` = 'Purchase_1715';
DELETE FROM `ac_journal_master`
WHERE `journal_no` = 'Purchase_1715'
AND `journal_date` >= '2019-07-01'
AND `journal_date` <= '2020-06-30';
DELETE FROM `purchase_details`
WHERE `purchase_date` >= '2019-07-01'
AND `purchase_date` <= '2020-06-30'
AND `purchase_no` = '1715';
DELETE FROM `purchase_master`
WHERE `id` = '981';
DELETE FROM `purchase_details`
WHERE `purchase_date` >= '2019-07-01'
AND `purchase_date` <= '2020-06-30'
AND `purchase_no` = '1714';
DELETE FROM `purchase_master`
WHERE `id` = '774';
DELETE FROM `purchase_details`
WHERE `purchase_date` >= '2019-07-01'
AND `purchase_date` <= '2020-06-30'
AND `purchase_no` = '1654';
DELETE FROM `purchase_master`
WHERE `id` = '708';
INSERT INTO `purchase_master` (`company_id`, `supplier_id`, `purchase_no`, `supplier_no`, `gate_no`, `purchase_date`, `notes`, `status`, `created`, `created_by`) VALUES ('2', '42', '1715', 'asd', 'asd', '2019-11-17', 'asd', '1', '2019-11-17 12:14:21', '8');
INSERT INTO `purchase_details` (`company_id`, `purchase_no`, `item_id`, `item_type`, `ac_id`, `quantity`, `sq_weight`, `unit`, `vat_percent`, `vat_amount`, `purchase_date`, `purchase_price`, `total_price`, `created`, `created_by`) VALUES ('2', '1715', '537', 'Other Items', '1292', '10', '10', 'Kg', '', 0, '2019-11-17', '10', 100, '2019-11-17 12:14:21', '8');
UPDATE `items` SET `avco_price` = 10, `modified` = '2019-11-17 12:14:21', `modified_by` = '8'
WHERE `id` = '537';
UPDATE `purchase_master` SET `company_id` = '2', `supplier_id` = '42', `purchase_no` = '1715', `supplier_no` = 'asd', `gate_no` = 'asd', `draft` = 0, `purchase_date` = '2019-11-17', `notes` = 'asd', `modified` = '2019-11-17 12:14:22', `modified_by` = '8'
WHERE `id` = '984';
INSERT INTO `ac_journal_master` (`company_id`, `journal_no`, `journal_date`, `memo`, `doc_type`, `doc_no`, `created`, `created_by`) VALUES ('2', 'Purchase_1715', '2019-11-17', 'Direct Purchase', 'Purchase', '1715', '2019-11-17 12:14:22', '8');
UPDATE `purchase_master` SET `ac_id` = 3232
WHERE `id` = '984';
INSERT INTO `ac_journal_details` (`company_id`, `journal_no`, `chart_id`, `debit`, `credit`, `memo`, `journal_date`, `created`, `created_by`) VALUES ('2', 'Purchase_1715', '967', NULL, 100, 'asd', '2019-11-17', '2019-11-17 12:14:22', '8');
INSERT INTO `ac_journal_details` (`company_id`, `journal_no`, `chart_id`, `debit`, `credit`, `memo`, `journal_date`, `created`, `created_by`) VALUES ('2', 'Purchase_1715', '1292', '100', NULL, 'asd', '2019-11-17', '2019-11-17 12:14:22', '8');
UPDATE `accounts_calander` SET `start_date` = '2019-07-01', `end_date` = '2020-06-30', `created` = '2020-01-11 15:12:12'
WHERE `user_id` = '8';
UPDATE `user_privileges` SET `company_id` = '2', `ref_user` = '8', `inventory_menu` = '0', `sales` = '0', `sales_return` = '0', `purchase` = '0', `purchase_return` = '0', `supplier` = '0', `item` = '0', `customer` = '0', `hr_menu` = '1', `employee` = '1', `accounts_menu` = '1', `journal` = '1', `ac_head` = '1', `chart_of_account` = '1', `money_receipt` = NULL, `payment_receipt` = NULL, `report_menu` = '1', `purchase_report` = '1', `purchase_return_report` = '1', `sales_report` = '1', `sales_return_report` = '1', `inventory_report` = '1', `production_report` = '1', `ledger_report` = '1', `trial_balance_report` = '1', `balance_sheet_report` = '1', `income_statement_report` = '1', `bills_receivable_report` = '1', `bills_payable_report` = '1', `cash_book_report` = '1', `bank_book_report` = '1', `settings_menu` = '1', `basic_settings` = '1', `company_settings` = '1', `default_ac_head_settings` = '1', `user_menu` = '1', `user_section` = '1', `recipe` = '0', `production` = '0', `recipe_detail` = '0', `production_details` = '0', `selection_box` = '0', `modified` = '2020-01-11 15:15:42', `modified_by` = '8'
WHERE `ref_user` = '8';
UPDATE `user_privileges` SET `company_id` = '2', `ref_user` = '8', `inventory_menu` = '0', `sales` = '0', `sales_return` = '0', `purchase` = '0', `purchase_return` = '0', `supplier` = '0', `item` = '0', `customer` = '0', `hr_menu` = '1', `employee` = '1', `accounts_menu` = '1', `journal` = '1', `ac_head` = '1', `chart_of_account` = '1', `money_receipt` = NULL, `payment_receipt` = NULL, `report_menu` = '1', `purchase_report` = '0', `purchase_return_report` = '0', `sales_report` = '0', `sales_return_report` = '0', `inventory_report` = '0', `production_report` = '0', `ledger_report` = '1', `trial_balance_report` = '1', `balance_sheet_report` = '1', `income_statement_report` = '1', `bills_receivable_report` = '1', `bills_payable_report` = '1', `cash_book_report` = '1', `bank_book_report` = '1', `settings_menu` = '1', `basic_settings` = '1', `company_settings` = '1', `default_ac_head_settings` = '1', `user_menu` = '1', `user_section` = '1', `recipe` = '0', `production` = '0', `recipe_detail` = '0', `production_details` = '0', `selection_box` = '0', `modified` = '2020-01-11 15:16:28', `modified_by` = '8'
WHERE `ref_user` = '8';
UPDATE `accounts_calander` SET `start_date` = '2019-07-01', `end_date` = '2020-06-30', `created` = '2020-01-11 15:19:18'
WHERE `user_id` = '8';
UPDATE `companies` SET `name` = 'Cybex Tech', `address` = '97 B Gulberg - II', `area` = 'Main Market', `city` = 'Lahore', `zip` = '54400', `country` = 'Pakistan', `phone` = '+92 (42) 0000000', `email` = 'info@cybextech.com', `contact_person` = 'Umair Iqbal', `mobile_no` = '+92310000000', `currency_id` = '87', `currency_symbol_position` = 'Before', `status` = 'Active', `modified` = '2020-01-11 15:21:55', `modified_by` = '8'
WHERE `id` = '2';
UPDATE `accounts_calander` SET `start_date` = '2019-07-01', `end_date` = '2020-06-30', `created` = '2020-01-11 15:22:07'
WHERE `user_id` = '8';
UPDATE `accounts_calander` SET `start_date` = '2019-07-01', `end_date` = '2020-06-30', `created` = '2020-01-11 17:56:28'
WHERE `user_id` = '8';
INSERT INTO `ac_journal_master` (`company_id`, `journal_no`, `journal_date`, `payment_type`, `created`, `created_by`) VALUES ('2', 'BPV_100165', '2020-01-11', 'BPV', '2020-01-11 18:01:43', '8');
INSERT INTO `ac_journal_details` (`company_id`, `journal_no`, `journal_date`, `chart_id`, `debit`, `credit`, `memo`, `created`, `created_by`) VALUES ('2', 'BPV_100165', '2020-01-11', '553', '1000', NULL, '', '2020-01-11 18:01:43', '8');
UPDATE `ac_journal_master` SET `journal_no` = 'BPV_100165', `journal_date` = '2020-01-11', `modified` = '2020-01-11 18:01:45', `modified_by` = '8'
WHERE `id` = '3623';
INSERT INTO `ac_journal_details` (`company_id`, `journal_no`, `journal_date`, `chart_id`, `debit`, `credit`, `memo`, `created`, `created_by`) VALUES ('2', 'BPV_100165', '2020-01-11', '546', NULL, '1000', '', '2020-01-11 18:01:45', '8');
DELETE FROM `ac_journal_details`
WHERE `id` = '23876';
UPDATE `ac_journal_master` SET `journal_no` = 'BPV_100165', `journal_date` = '2020-01-11', `modified` = '2020-01-11 18:02:00', `modified_by` = '8'
WHERE `id` = '3623';
INSERT INTO `ac_journal_details` (`company_id`, `journal_no`, `journal_date`, `chart_id`, `debit`, `credit`, `memo`, `created`, `created_by`) VALUES ('2', 'BPV_100165', '2020-01-11', '546', NULL, '200', '', '2020-01-11 18:02:00', '8');
UPDATE `ac_journal_master` SET `journal_no` = 'BPV_100165', `journal_date` = '2020-01-11', `modified` = '2020-01-11 18:02:10', `modified_by` = '8'
WHERE `id` = '3623';
INSERT INTO `ac_journal_details` (`company_id`, `journal_no`, `journal_date`, `chart_id`, `debit`, `credit`, `memo`, `created`, `created_by`) VALUES ('2', 'BPV_100165', '2020-01-11', '524', NULL, '500', '', '2020-01-11 18:02:10', '8');
UPDATE `ac_journal_master` SET `journal_no` = 'BPV_100165', `journal_date` = '2020-01-11', `modified` = '2020-01-11 18:02:13', `modified_by` = '8'
WHERE `id` = '3623';
INSERT INTO `ac_journal_details` (`company_id`, `journal_no`, `journal_date`, `chart_id`, `debit`, `credit`, `memo`, `created`, `created_by`) VALUES ('2', 'BPV_100165', '2020-01-11', '524', NULL, '300', '', '2020-01-11 18:02:13', '8');
DELETE FROM `ac_journal_details`
WHERE `id` = '23879';
UPDATE `ac_journal_master` SET `journal_no` = 'BPV_100165', `journal_date` = '2020-01-11', `modified` = '2020-01-11 18:02:57', `modified_by` = '8'
WHERE `id` = '3623';
INSERT INTO `ac_journal_details` (`company_id`, `journal_no`, `journal_date`, `chart_id`, `debit`, `credit`, `memo`, `created`, `created_by`) VALUES ('2', 'BPV_100165', '2020-01-11', '524', NULL, '300', '', '2020-01-11 18:02:57', '8');
UPDATE `ac_journal_details` SET `journal_no` = 'BPV_100165', `journal_date` = '2020-01-11', `created` = '2020-01-11 18:03:00', `created_by` = '8'
WHERE `id` = '23875';
UPDATE `ac_journal_details` SET `journal_no` = 'BPV_100165', `journal_date` = '2020-01-11', `created` = '2020-01-11 18:03:00', `created_by` = '8'
WHERE `id` = '23877';
UPDATE `ac_journal_details` SET `journal_no` = 'BPV_100165', `journal_date` = '2020-01-11', `created` = '2020-01-11 18:03:00', `created_by` = '8'
WHERE `id` = '23878';
UPDATE `ac_journal_details` SET `journal_no` = 'BPV_100165', `journal_date` = '2020-01-11', `created` = '2020-01-11 18:03:00', `created_by` = '8'
WHERE `id` = '23880';
UPDATE `ac_journal_master` SET `journal_no` = 'BPV_100165', `journal_date` = '2020-01-11', `modified` = '2020-01-11 18:03:00', `modified_by` = '8'
WHERE `id` = '3623';
UPDATE `accounts_calander` SET `start_date` = '2019-07-01', `end_date` = '2020-06-30', `created` = '2020-01-11 22:10:52'
WHERE `user_id` = '8';
INSERT INTO `ac_journal_master` (`company_id`, `journal_no`, `journal_date`, `payment_type`, `created`, `created_by`) VALUES ('2', 'JV_100004', '2020-01-11', 'JV', '2020-01-11 22:11:15', '8');
INSERT INTO `ac_journal_details` (`company_id`, `journal_no`, `journal_date`, `chart_id`, `debit`, `credit`, `memo`, `created`, `created_by`) VALUES ('2', 'JV_100004', '2020-01-11', '510', '100', NULL, '', '2020-01-11 22:11:15', '8');
UPDATE `ac_journal_master` SET `journal_no` = 'JV_100004', `journal_date` = '2020-01-11', `modified` = '2020-01-11 22:11:19', `modified_by` = '8'
WHERE `id` = '3624';
INSERT INTO `ac_journal_details` (`company_id`, `journal_no`, `journal_date`, `chart_id`, `debit`, `credit`, `memo`, `created`, `created_by`) VALUES ('2', 'JV_100004', '2020-01-11', '516', NULL, '100', '', '2020-01-11 22:11:19', '8');
UPDATE `ac_journal_details` SET `journal_no` = 'JV_100004', `journal_date` = '2020-01-11', `created` = '2020-01-11 22:11:22', `created_by` = '8'
WHERE `id` = '23881';
UPDATE `ac_journal_details` SET `journal_no` = 'JV_100004', `journal_date` = '2020-01-11', `created` = '2020-01-11 22:11:22', `created_by` = '8'
WHERE `id` = '23882';
UPDATE `ac_journal_master` SET `journal_no` = 'JV_100004', `journal_date` = '2020-01-11', `modified` = '2020-01-11 22:11:22', `modified_by` = '8'
WHERE `id` = '3624';
UPDATE `accounts_calander` SET `start_date` = '2019-07-01', `end_date` = '2020-06-30', `created` = '2020-02-08 13:33:28'
WHERE `user_id` = '8';
DELETE FROM `users`
WHERE `id` = '9';
DELETE FROM `user_privileges`
WHERE `ref_user` = '9';
UPDATE `users` SET `email` = 'alw@gmail.com', `name` = 'Hamid Riaz', `status` = 'Active', `modified` = '2020-02-08 13:34:13', `modified_by` = '8', `company_id` = '2', `password` = '40bd001563085fc3', `type` = 'Admin'
WHERE `id` = '8';
UPDATE `accounts_calander` SET `start_date` = '2019-07-01', `end_date` = '2020-06-30', `created` = '2020-02-08 13:34:22'
WHERE `user_id` = '8';
INSERT INTO `ac_journal_master` (`company_id`, `journal_no`, `journal_date`, `payment_type`, `created`, `created_by`) VALUES ('2', '1920_001', '2020-02-08', 'JV', '2020-02-08 15:14:46', '8');
INSERT INTO `ac_journal_details` (`master_id`, `chart_id`, `debit`, `credit`, `memo`, `created`, `created_by`) VALUES (1, '513', '10', NULL, 'expenses 10 debit', '2020-02-08 15:14:46', '8');
INSERT INTO `ac_journal_details` (`master_id`, `chart_id`, `debit`, `credit`, `memo`, `created`, `created_by`) VALUES ('1', '516', '20', NULL, 'non current 20', '2020-02-08 15:17:01', '8');
INSERT INTO `ac_journal_master` (`company_id`, `journal_no`, `journal_date`, `payment_type`, `created`, `created_by`) VALUES ('2', '1920_001', '2020-02-08', 'JV', '2020-02-08 15:17:36', '8');
INSERT INTO `ac_journal_details` (`master_id`, `chart_id`, `debit`, `credit`, `memo`, `created`, `created_by`) VALUES (2, '510', '20', NULL, 'asdasd', '2020-02-08 15:17:36', '8');
INSERT INTO `ac_journal_details` (`master_id`, `chart_id`, `debit`, `credit`, `memo`, `created`, `created_by`) VALUES ('2', '510', '10', NULL, 'sadasd', '2020-02-08 15:18:48', '8');
INSERT INTO `ac_journal_master` (`company_id`, `journal_no`, `journal_date`, `payment_type`, `created`, `created_by`) VALUES ('2', '1920_001', '2020-02-08', 'JV', '2020-02-08 15:19:22', '8');
INSERT INTO `ac_journal_details` (`master_id`, `chart_id`, `debit`, `credit`, `memo`, `created`, `created_by`) VALUES (3, '510', '10', NULL, 'liablities 10', '2020-02-08 15:19:22', '8');
INSERT INTO `ac_journal_details` (`master_id`, `chart_id`, `debit`, `credit`, `memo`, `created`, `created_by`) VALUES ('3', '516', '20', NULL, 'asdas', '2020-02-08 15:21:07', '8');
INSERT INTO `ac_journal_details` (`master_id`, `chart_id`, `debit`, `credit`, `memo`, `created`, `created_by`) VALUES ('3', '515', NULL, '30', 'asdsa', '2020-02-08 15:21:15', '8');
UPDATE `ac_journal_details` SET `memo` = 'liablities 102'
WHERE `id` = '5';
UPDATE `ac_journal_details` SET `memo` = 'asdsa2'
WHERE `id` = '7';
UPDATE `ac_journal_details` SET `memo` = 'asdsa2'
WHERE `id` = '7';
UPDATE `ac_journal_details` SET `memo` = 'asdsa2'
WHERE `id` = '7';
INSERT INTO `ac_journal_details` (`master_id`, `chart_id`, `debit`, `credit`, `memo`, `created`, `created_by`) VALUES ('3', '516', '2', NULL, 'wa', '2020-02-08 15:53:44', '8');
INSERT INTO `ac_journal_details` (`master_id`, `chart_id`, `debit`, `credit`, `memo`, `created`, `created_by`) VALUES ('3', '510', '1', NULL, '', '2020-02-08 15:55:32', '8');
INSERT INTO `ac_journal_master` (`company_id`, `journal_no`, `journal_date`, `payment_type`, `created`, `created_by`) VALUES ('2', '1920_1001', '2020-02-08', 'CPV', '2020-02-08 16:01:54', '8');
INSERT INTO `ac_journal_details` (`master_id`, `chart_id`, `debit`, `credit`, `memo`, `created`, `created_by`) VALUES (4, '510', '12', NULL, 'adasd', '2020-02-08 16:01:54', '8');
UPDATE `ac_journal_details` SET `memo` = 'adasd'
WHERE `id` = '10';
INSERT INTO `ac_journal_details` (`master_id`, `chart_id`, `debit`, `credit`, `memo`, `created`, `created_by`) VALUES ('4', '510', '12', NULL, 'sad', '2020-02-08 16:04:47', '8');
UPDATE `ac_journal_details` SET `memo` = 'sad'
WHERE `id` = '11';
UPDATE `ac_journal_details` SET `memo` = 'sadsss'
WHERE `id` = '11';
DELETE FROM `ac_journal_details`
WHERE `id` = '11';
INSERT INTO `ac_journal_master` (`company_id`, `journal_no`, `journal_date`, `payment_type`, `created`, `created_by`) VALUES ('2', '1920_1002', '2020-02-08', 'CPV', '2020-02-08 16:07:39', '8');
INSERT INTO `ac_journal_details` (`master_id`, `chart_id`, `debit`, `credit`, `memo`, `created`, `created_by`) VALUES (5, '510', '12', NULL, 'asasasd', '2020-02-08 16:07:39', '8');
INSERT INTO `ac_journal_details` (`master_id`, `chart_id`, `debit`, `credit`, `memo`, `created`, `created_by`) VALUES ('5', '515', NULL, '12', 'sdsd', '2020-02-08 16:07:44', '8');
UPDATE `ac_journal_details` SET `memo` = 'asasasd1111111'
WHERE `id` = '12';
UPDATE `ac_journal_details` SET `memo` = 'sdsd1111111111'
WHERE `id` = '13';
DELETE FROM `ac_journal_details`
WHERE `id` = '13';
DELETE FROM `ac_journal_details`
WHERE `id` = '12';
UPDATE `accounts_calander` SET `start_date` = '2019-07-01', `end_date` = '2020-06-30', `created` = '2020-02-08 16:08:43'
WHERE `user_id` = '8';
INSERT INTO `ac_journal_master` (`company_id`, `journal_no`, `journal_date`, `payment_type`, `created`, `created_by`) VALUES ('2', '1920_1003', '2020-02-08', 'CPV', '2020-02-08 16:08:54', '8');
INSERT INTO `ac_journal_details` (`master_id`, `chart_id`, `debit`, `credit`, `memo`, `created`, `created_by`) VALUES (6, '510', '12', NULL, 'dsds', '2020-02-08 16:08:54', '8');
INSERT INTO `ac_journal_details` (`master_id`, `chart_id`, `debit`, `credit`, `memo`, `created`, `created_by`) VALUES ('6', '517', NULL, '12', 'asdsad', '2020-02-08 16:09:01', '8');
UPDATE `ac_journal_details` SET `memo` = 'dsds'
WHERE `id` = '14';
UPDATE `accounts_calander` SET `start_date` = '2019-07-01', `end_date` = '2020-06-30', `created` = '2020-02-08 16:12:16'
WHERE `user_id` = '8';
INSERT INTO `ac_journal_master` (`company_id`, `journal_no`, `journal_date`, `payment_type`, `created`, `created_by`) VALUES ('2', '1920_1004', '2020-02-08', 'CPV', '2020-02-08 16:13:23', '8');
INSERT INTO `ac_journal_details` (`master_id`, `chart_id`, `debit`, `credit`, `memo`, `created`, `created_by`) VALUES (7, '509', '12', NULL, 'sad', '2020-02-08 16:13:23', '8');
INSERT INTO `ac_journal_details` (`master_id`, `chart_id`, `debit`, `credit`, `memo`, `created`, `created_by`) VALUES ('7', '513', NULL, '12', 'sads', '2020-02-08 16:13:28', '8');
INSERT INTO `ac_journal_details` (`master_id`, `chart_id`, `debit`, `credit`, `memo`, `created`, `created_by`) VALUES ('7', '513', NULL, '2', '', '2020-02-08 16:13:50', '8');
INSERT INTO `ac_journal_master` (`company_id`, `journal_no`, `journal_date`, `payment_type`, `created`, `created_by`) VALUES ('2', '1920_1005', '2020-02-08', 'CPV', '2020-02-08 16:20:03', '8');
INSERT INTO `ac_journal_details` (`master_id`, `chart_id`, `debit`, `credit`, `memo`, `created`, `created_by`) VALUES (8, '', '12', NULL, '', '2020-02-08 16:20:03', '8');
INSERT INTO `ac_journal_details` (`master_id`, `chart_id`, `debit`, `credit`, `memo`, `created`, `created_by`) VALUES ('8', '', '13', NULL, '', '2020-02-08 16:20:19', '8');
INSERT INTO `ac_journal_details` (`master_id`, `chart_id`, `debit`, `credit`, `memo`, `created`, `created_by`) VALUES ('8', '513', '13', NULL, 'sadsd', '2020-02-08 16:20:30', '8');
INSERT INTO `ac_journal_details` (`master_id`, `chart_id`, `debit`, `credit`, `memo`, `created`, `created_by`) VALUES ('8', '', NULL, '13', '', '2020-02-08 16:20:39', '8');
INSERT INTO `ac_journal_details` (`master_id`, `chart_id`, `debit`, `credit`, `memo`, `created`, `created_by`) VALUES ('8', '513', NULL, '13', 'asdasd', '2020-02-08 16:20:47', '8');
INSERT INTO `ac_journal_master` (`company_id`, `journal_no`, `journal_date`, `payment_type`, `created`, `created_by`) VALUES ('2', '1920_1006', '2020-02-08', 'CPV', '2020-02-08 16:28:46', '8');
INSERT INTO `ac_journal_details` (`master_id`, `chart_id`, `debit`, `credit`, `memo`, `created`, `created_by`) VALUES (9, '513', '12', NULL, 'sdad', '2020-02-08 16:28:46', '8');
INSERT INTO `ac_journal_details` (`master_id`, `chart_id`, `debit`, `credit`, `memo`, `created`, `created_by`) VALUES ('9', '531', NULL, '12', 'sadsa', '2020-02-08 16:28:47', '8');
INSERT INTO `ac_journal_master` (`company_id`, `journal_no`, `journal_date`, `payment_type`, `created`, `created_by`) VALUES ('2', '1920_1007', '2020-02-08', 'CPV', '2020-02-08 16:29:55', '8');
INSERT INTO `ac_journal_details` (`master_id`, `chart_id`, `debit`, `credit`, `memo`, `created`, `created_by`) VALUES (10, '513', '23', NULL, 'asdasd', '2020-02-08 16:29:55', '8');
INSERT INTO `ac_journal_details` (`master_id`, `chart_id`, `debit`, `credit`, `memo`, `created`, `created_by`) VALUES ('10', '516', NULL, '23', 'sad', '2020-02-08 16:29:58', '8');
INSERT INTO `ac_journal_master` (`company_id`, `journal_no`, `journal_date`, `payment_type`, `created`, `created_by`) VALUES ('2', '1920_1008', '2020-02-08', 'CPV', '2020-02-08 16:31:19', '8');
INSERT INTO `ac_journal_details` (`master_id`, `chart_id`, `debit`, `credit`, `memo`, `created`, `created_by`) VALUES (11, '513', '23', NULL, 'asdsa', '2020-02-08 16:31:19', '8');
INSERT INTO `ac_journal_details` (`master_id`, `chart_id`, `debit`, `credit`, `memo`, `created`, `created_by`) VALUES ('11', '515', NULL, '23', 'asd', '2020-02-08 16:31:22', '8');
DELETE FROM `ac_journal_details`
WHERE `journal_date` >= '2019-07-01'
AND `journal_date` <= '2020-06-30'
AND `journal_no` = '1920_1008';
DELETE FROM `ac_journal_master`
WHERE `id` = '11';
INSERT INTO `ac_journal_details` (`master_id`, `chart_id`, `debit`, `credit`, `memo`, `created`, `created_by`) VALUES ('10', '515', '12', NULL, 'sdsa', '2020-02-08 16:59:10', '8');
INSERT INTO `ac_journal_details` (`master_id`, `chart_id`, `debit`, `credit`, `memo`, `created`, `created_by`) VALUES ('10', '516', NULL, '35', 'sdas', '2020-02-08 16:59:17', '8');
INSERT INTO `ac_journal_details` (`master_id`, `chart_id`, `debit`, `credit`, `memo`, `created`, `created_by`) VALUES ('10', '516', NULL, '-23', 'sda', '2020-02-08 16:59:19', '8');
DELETE FROM `ac_journal_details`
WHERE `id` = '32';
DELETE FROM `ac_journal_details`
WHERE `id` = '27';
INSERT INTO `ac_charts` (`company_id`, `parent_id`, `code`, `name`, `memo`, `opening`, `status`, `created_at`, `created_by`) VALUES ('2', '515', '10.10.90', 'test asset', 'sadas', '', 'Active', '2020-02-08 17:03:11', '8');
UPDATE `accounts_calander` SET `start_date` = '2019-07-01', `end_date` = '2020-06-30', `created` = '2020-02-09 12:10:33'
WHERE `user_id` = '8';
INSERT INTO `ac_journal_details` (`master_id`, `chart_id`, `debit`, `credit`, `memo`, `created`, `created_by`) VALUES ('4', '515', NULL, '12', 'ssd', '2020-02-09 12:16:00', '8');
INSERT INTO `emps` (`company_id`, `code`, `name`, `father_name`, `cnic`, `joining`, `resign`, `present_address`, `permanent_address`, `voter_id`, `department`, `designation`, `mobile`, `email`, `salary`, `house_rent`, `account_no`, `notes`, `status`, `created`, `created_by`) VALUES ('2', 1001, 'sohaill', '', '', '2020-02-09', '--', '', '', NULL, '', '', '', '', '', '', '', '', '', '2020-02-09 12:17:13', '8');
UPDATE `emps` SET `name` = 'sohaill', `father_name` = '', `cnic` = '', `joining` = '2020-02-09', `resign` = '0000-00-00', `present_address` = '', `permanent_address` = '', `voter_id` = NULL, `department` = '', `designation` = '', `mobile` = '', `email` = '', `salary` = '0', `house_rent` = '0', `account_no` = '', `notes` = '', `status` = 'Active', `modified` = '2020-02-09 12:17:27', `modified_by` = '8'
WHERE `id` = '63';
INSERT INTO `emps_salary` (`company_id`, `code`, `emps_id`, `basic_salary`, `days`, `date`, `arrears`, `bonus`, `i_others`, `eobi`, `pessi`, `advance`, `d_others`, `amount_type`, `salary_date`, `created`, `created_by`) VALUES ('2', 1001, '63', '0', '30', '2020-02-29', '1200', '100', '200', '10', '50', '30', '', 'Cash', '2020-02-01', '2020-02-09 12:18:08', '8');
INSERT INTO `ac_journal_details` (`master_id`, `chart_id`, `debit`, `credit`, `memo`, `created`, `created_by`) VALUES ('4', '515', '100', NULL, 'sdasd', '2020-02-09 12:48:48', '8');
INSERT INTO `ac_journal_details` (`master_id`, `chart_id`, `debit`, `credit`, `memo`, `created`, `created_by`) VALUES ('4', '510', NULL, '112', '', '2020-02-09 12:48:54', '8');
INSERT INTO `ac_journal_details` (`master_id`, `chart_id`, `debit`, `credit`, `memo`, `created`, `created_by`) VALUES ('4', '510', NULL, '-12', '', '2020-02-09 12:48:56', '8');
INSERT INTO `ac_journal_master` (`company_id`, `journal_no`, `journal_date`, `payment_type`, `created`, `created_by`) VALUES ('2', '1920_1008', '2020-02-09', 'CPV', '2020-02-09 12:51:09', '8');
INSERT INTO `ac_journal_details` (`master_id`, `chart_id`, `debit`, `credit`, `memo`, `created`, `created_by`) VALUES (12, '517', '12', NULL, 'sda', '2020-02-09 12:51:09', '8');
INSERT INTO `ac_journal_details` (`master_id`, `chart_id`, `debit`, `credit`, `memo`, `created`, `created_by`) VALUES ('12', '513', NULL, '12', 'asd', '2020-02-09 12:51:13', '8');
INSERT INTO `ac_journal_details` (`master_id`, `chart_id`, `debit`, `credit`, `memo`, `created`, `created_by`) VALUES ('4', '513', '122', NULL, 'asd', '2020-02-09 12:52:29', '8');
INSERT INTO `ac_journal_details` (`master_id`, `chart_id`, `debit`, `credit`, `memo`, `created`, `created_by`) VALUES ('4', '513', '10', NULL, 'sadsa', '2020-02-09 13:02:02', '8');
INSERT INTO `ac_journal_master` (`company_id`, `journal_no`, `journal_date`, `payment_type`, `created`, `created_by`) VALUES ('2', '1920_1001', '2020-02-09', 'BPV', '2020-02-09 13:05:48', '8');
INSERT INTO `ac_journal_details` (`master_id`, `chart_id`, `debit`, `credit`, `memo`, `created`, `created_by`) VALUES (13, '513', '12', NULL, 'sdasd', '2020-02-09 13:05:48', '8');
INSERT INTO `ac_journal_master` (`company_id`, `journal_no`, `journal_date`, `payment_type`, `created`, `created_by`) VALUES ('2', '1920_1001', '2020-02-09', NULL, '2020-02-09 13:05:52', '8');
INSERT INTO `ac_journal_details` (`master_id`, `chart_id`, `debit`, `credit`, `memo`, `created`, `created_by`) VALUES (0, '515', NULL, '12', 'sdas', '2020-02-09 13:05:52', '8');
INSERT INTO `ac_journal_master` (`company_id`, `journal_no`, `journal_date`, `payment_type`, `created`, `created_by`) VALUES ('2', '1920_1001', '2020-02-09', NULL, '2020-02-09 13:05:56', '8');
INSERT INTO `ac_journal_details` (`master_id`, `chart_id`, `debit`, `credit`, `memo`, `created`, `created_by`) VALUES (0, '515', NULL, '12', '', '2020-02-09 13:05:56', '8');
INSERT INTO `ac_journal_master` (`company_id`, `journal_no`, `journal_date`, `payment_type`, `created`, `created_by`) VALUES ('2', '1920_1001', '2020-02-09', NULL, '2020-02-09 13:05:57', '8');
INSERT INTO `ac_journal_details` (`master_id`, `chart_id`, `debit`, `credit`, `memo`, `created`, `created_by`) VALUES (0, '515', NULL, '12', '', '2020-02-09 13:05:57', '8');
INSERT INTO `ac_journal_master` (`company_id`, `journal_no`, `journal_date`, `payment_type`, `created`, `created_by`) VALUES ('2', '1920_1001', '2020-02-09', NULL, '2020-02-09 13:06:52', '8');
INSERT INTO `ac_journal_details` (`master_id`, `chart_id`, `debit`, `credit`, `memo`, `created`, `created_by`) VALUES (0, '515', NULL, '12', '', '2020-02-09 13:06:52', '8');
INSERT INTO `ac_journal_master` (`company_id`, `journal_no`, `journal_date`, `payment_type`, `created`, `created_by`) VALUES ('2', '1920_1002', '2020-02-09', 'BPV', '2020-02-09 13:07:00', '8');
INSERT INTO `ac_journal_details` (`master_id`, `chart_id`, `debit`, `credit`, `memo`, `created`, `created_by`) VALUES (14, '515', '23', NULL, 'sad', '2020-02-09 13:07:00', '8');
INSERT INTO `ac_journal_master` (`company_id`, `journal_no`, `journal_date`, `payment_type`, `created`, `created_by`) VALUES ('2', '1920_1002', '2020-02-09', NULL, '2020-02-09 13:07:03', '8');
INSERT INTO `ac_journal_details` (`master_id`, `chart_id`, `debit`, `credit`, `memo`, `created`, `created_by`) VALUES (0, '516', NULL, '23', 'asd', '2020-02-09 13:07:03', '8');
INSERT INTO `ac_journal_master` (`company_id`, `journal_no`, `journal_date`, `payment_type`, `created`, `created_by`) VALUES ('2', '1920_1003', '2020-02-09', 'BPV', '2020-02-09 13:08:46', '8');
INSERT INTO `ac_journal_details` (`master_id`, `chart_id`, `debit`, `credit`, `memo`, `created`, `created_by`) VALUES (15, '516', '23', NULL, 'asd', '2020-02-09 13:08:46', '8');
INSERT INTO `ac_journal_details` (`master_id`, `chart_id`, `debit`, `credit`, `memo`, `created`, `created_by`) VALUES ('15', '516', NULL, '23', 'asd', '2020-02-09 13:08:49', '8');
INSERT INTO `ac_journal_master` (`company_id`, `journal_no`, `journal_date`, `payment_type`, `created`, `created_by`) VALUES ('2', '1920_1004', '2020-02-09', 'BPV', '2020-02-09 13:11:12', '8');
INSERT INTO `ac_journal_details` (`master_id`, `chart_id`, `debit`, `credit`, `memo`, `created`, `created_by`) VALUES (16, '526', '100', NULL, 'sdas', '2020-02-09 13:11:12', '8');
INSERT INTO `ac_journal_details` (`master_id`, `chart_id`, `debit`, `credit`, `memo`, `created`, `created_by`) VALUES ('16', '510', NULL, '100', 'sada', '2020-02-09 13:11:17', '8');
INSERT INTO `ac_journal_master` (`company_id`, `journal_no`, `journal_date`, `payment_type`, `created`, `created_by`) VALUES ('2', '1920_1005', '2020-02-09', 'BPV', '2020-02-09 13:18:59', '8');
INSERT INTO `ac_journal_details` (`master_id`, `chart_id`, `debit`, `credit`, `memo`, `created`, `created_by`) VALUES (17, '513', '100', NULL, 'asdasd', '2020-02-09 13:18:59', '8');
INSERT INTO `ac_journal_details` (`master_id`, `chart_id`, `debit`, `credit`, `memo`, `created`, `created_by`) VALUES ('17', '514', NULL, '100', 'asdas', '2020-02-09 13:19:41', '8');
UPDATE `ac_journal_master` SET `journal_no` = '1920_1001', `journal_date` = '2020-02-06', `modified` = '2020-02-09 13:20:09', `modified_by` = '8'
WHERE `id` = '17';
UPDATE `ac_journal_master` SET `journal_no` = '1920_1001', `payment_type` = 'CRV', `journal_date` = '2020-02-06', `modified` = '2020-02-09 13:21:22', `modified_by` = '8'
WHERE `id` = '17';
INSERT INTO `ac_journal_master` (`company_id`, `journal_no`, `journal_date`, `payment_type`, `created`, `created_by`) VALUES ('2', '1920_1005', '2020-02-09', 'BPV', '2020-02-09 14:33:01', '8');
INSERT INTO `ac_journal_details` (`master_id`, `chart_id`, `debit`, `credit`, `memo`, `created`, `created_by`) VALUES (18, '515', '23', NULL, 'asdsd', '2020-02-09 14:33:01', '8');
INSERT INTO `ac_journal_details` (`master_id`, `chart_id`, `debit`, `credit`, `memo`, `created`, `created_by`) VALUES ('18', '515', NULL, '23', 'asdas', '2020-02-09 14:33:05', '8');
UPDATE `ac_journal_master` SET `journal_no` = '1920_1005', `payment_type` = 'BPV', `journal_date` = '2020-02-09', `modified` = '2020-02-09 14:33:06', `modified_by` = '8'
WHERE `id` = '18';
UPDATE `accounts_calander` SET `start_date` = '2019-07-01', `end_date` = '2020-06-30', `created` = '2020-02-15 11:55:36'
WHERE `user_id` = '8';
UPDATE `companies` SET `name` = 'Cybex Tech', `address` = '97 B Gulberg - II', `area` = 'Main Market', `city` = 'Lahore', `zip` = '54400', `country` = 'Pakistan', `phone` = '+92 (42) 0000000', `email` = 'info@cybextech.com', `contact_person` = 'Umair Iqbal', `mobile_no` = '+92310000000', `currency_id` = '87', `currency_symbol_position` = 'Before', `status` = 'Active', `modified` = '2020-02-15 11:57:26', `modified_by` = '8'
WHERE `id` = '2';
UPDATE `companies` SET `logo` = '2.png'
WHERE `id` = '2';
UPDATE `users` SET `email` = 'admin@admin.com', `name` = 'Hamid Riaz', `status` = 'Active', `modified` = '2020-02-15 11:57:56', `modified_by` = '8', `company_id` = '2', `type` = 'Admin'
WHERE `id` = '8';
DELETE FROM `users`
WHERE `id` = '5';
DELETE FROM `user_privileges`
WHERE `ref_user` = '5';
DELETE FROM `users`
WHERE `id` = '10';
DELETE FROM `user_privileges`
WHERE `ref_user` = '10';
DELETE FROM `users`
WHERE `id` = '11';
DELETE FROM `user_privileges`
WHERE `ref_user` = '11';
DELETE FROM `users`
WHERE `id` = '12';
DELETE FROM `user_privileges`
WHERE `ref_user` = '12';
INSERT INTO `ac_journal_master` (`company_id`, `journal_no`, `journal_date`, `payment_type`, `created`, `created_by`) VALUES ('2', '1920_2', '2020-02-15', 'JV', '2020-02-15 12:07:14', '8');
INSERT INTO `ac_journal_details` (`master_id`, `chart_id`, `debit`, `credit`, `memo`, `created`, `created_by`) VALUES (19, '515', '30', NULL, 'asdsad', '2020-02-15 12:07:14', '8');
INSERT INTO `ac_journal_details` (`master_id`, `chart_id`, `debit`, `credit`, `memo`, `created`, `created_by`) VALUES ('19', '516', NULL, '30', 'asdasd', '2020-02-15 12:07:20', '8');
UPDATE `ac_journal_master` SET `journal_no` = '1920_2', `payment_type` = 'JV', `journal_date` = '2020-02-15', `modified` = '2020-02-15 12:07:33', `modified_by` = '8'
WHERE `id` = '19';
UPDATE `ac_journal_master` SET `journal_no` = '1920_2', `payment_type` = 'JV', `journal_date` = '2020-01-28', `modified` = '2020-02-15 12:08:01', `modified_by` = '8'
WHERE `id` = '19';
DELETE FROM `ac_journal_master`
WHERE `id` = '19';
DELETE FROM `ac_journal_master`
WHERE `id` = '18';
DELETE FROM `ac_journal_master`
WHERE `id` = '17';
DELETE FROM `ac_journal_master`
WHERE `id` = '16';
DELETE FROM `ac_journal_master`
WHERE `id` = '3';
UPDATE `accounts_calander` SET `start_date` = '2019-07-01', `end_date` = '2020-06-30', `created` = '2020-02-15 12:11:03'
WHERE `user_id` = '8';
UPDATE `accounts_calander` SET `start_date` = '2019-07-01', `end_date` = '2020-06-30', `created` = '2020-02-16 12:24:29'
WHERE `user_id` = '8';
INSERT INTO `ac_charts` (`company_id`, `parent_id`, `code`, `name`, `memo`, `opening`, `status`, `created_at`, `created_by`) VALUES ('2', '515', '10.10.100', 'projects', '', '', 'Active', '2020-02-16 12:38:13', '8');
INSERT INTO `ac_charts` (`company_id`, `parent_id`, `code`, `name`, `memo`, `opening`, `status`, `created_at`, `created_by`) VALUES ('2', '1334', '10.10.100.10', 'descon', '', '', 'Active', '2020-02-16 12:38:27', '8');
INSERT INTO `ac_charts` (`company_id`, `parent_id`, `code`, `name`, `memo`, `opening`, `status`, `created_at`, `created_by`) VALUES ('2', '1335', '10.10.100.10.10', 'expenses', '', '', 'Active', '2020-02-16 12:38:38', '8');
INSERT INTO `ac_charts` (`company_id`, `parent_id`, `code`, `name`, `memo`, `opening`, `status`, `created_at`, `created_by`) VALUES ('2', '1335', '10.10.100.10.20', 'operating expenses', '', '', 'Active', '2020-02-16 12:39:26', '8');
INSERT INTO `ac_charts` (`company_id`, `parent_id`, `code`, `name`, `memo`, `opening`, `status`, `created_at`, `created_by`) VALUES ('2', '1334', '10.10.100.20', 'project 2', '', '', 'Active', '2020-02-16 12:40:19', '8');
INSERT INTO `ac_charts` (`company_id`, `parent_id`, `code`, `name`, `memo`, `opening`, `status`, `created_at`, `created_by`) VALUES ('2', '1338', '10.10.100.20.10', 'operating expense', '', '', 'Active', '2020-02-16 12:40:34', '8');
INSERT INTO `ac_charts` (`company_id`, `parent_id`, `code`, `name`, `memo`, `opening`, `status`, `created_at`, `created_by`) VALUES ('2', '1338', '10.10.100.20.20', 'material', '', '', 'Active', '2020-02-16 12:40:48', '8');
INSERT INTO `ac_journal_master` (`company_id`, `journal_no`, `journal_date`, `payment_type`, `created`, `created_by`) VALUES ('2', '1920_1009', '2020-02-16', 'CPV', '2020-02-16 12:41:56', '8');
INSERT INTO `ac_journal_details` (`master_id`, `chart_id`, `debit`, `credit`, `memo`, `created`, `created_by`) VALUES (20, '1336', '100', NULL, '', '2020-02-16 12:41:56', '8');
INSERT INTO `ac_journal_details` (`master_id`, `chart_id`, `debit`, `credit`, `memo`, `created`, `created_by`) VALUES ('20', '526', NULL, '100', '', '2020-02-16 12:42:03', '8');
UPDATE `ac_journal_master` SET `journal_no` = '1920_1009', `payment_type` = 'CPV', `journal_date` = '2020-02-16', `modified` = '2020-02-16 12:42:21', `modified_by` = '8'
WHERE `id` = '20';
INSERT INTO `ac_journal_master` (`company_id`, `journal_no`, `journal_date`, `payment_type`, `created`, `created_by`) VALUES ('2', '1920_1010', '2020-02-16', 'CPV', '2020-02-16 12:43:16', '8');
INSERT INTO `ac_journal_details` (`master_id`, `chart_id`, `debit`, `credit`, `memo`, `created`, `created_by`) VALUES (21, '1336', '50', NULL, 'asds', '2020-02-16 12:43:16', '8');
INSERT INTO `ac_journal_details` (`master_id`, `chart_id`, `debit`, `credit`, `memo`, `created`, `created_by`) VALUES ('21', '526', NULL, '50', '', '2020-02-16 12:43:23', '8');
UPDATE `ac_journal_master` SET `journal_no` = '1920_1010', `payment_type` = 'CPV', `journal_date` = '2020-02-16', `modified` = '2020-02-16 12:44:28', `modified_by` = '8'
WHERE `id` = '21';
INSERT INTO `ac_journal_master` (`company_id`, `journal_no`, `journal_date`, `payment_type`, `created`, `created_by`) VALUES ('2', '1920_1011', '2020-02-16', 'CPV', '2020-02-16 12:44:58', '8');
INSERT INTO `ac_journal_details` (`master_id`, `chart_id`, `debit`, `credit`, `memo`, `created`, `created_by`) VALUES (22, '1339', '123', NULL, 'asd', '2020-02-16 12:44:58', '8');
INSERT INTO `ac_journal_details` (`master_id`, `chart_id`, `debit`, `credit`, `memo`, `created`, `created_by`) VALUES ('22', '526', NULL, '123', '', '2020-02-16 12:45:10', '8');
UPDATE `ac_journal_master` SET `journal_no` = '1920_1011', `payment_type` = 'CPV', `journal_date` = '2020-02-16', `modified` = '2020-02-16 12:45:14', `modified_by` = '8'
WHERE `id` = '22';
INSERT INTO `ac_journal_master` (`company_id`, `journal_no`, `journal_date`, `payment_type`, `created`, `created_by`) VALUES ('2', '1920_1004', '2020-02-16', 'BPV', '2020-02-16 12:45:48', '8');
INSERT INTO `ac_journal_details` (`master_id`, `chart_id`, `debit`, `credit`, `memo`, `created`, `created_by`) VALUES (23, '1340', '1000', NULL, 'asd', '2020-02-16 12:45:48', '8');
INSERT INTO `ac_journal_details` (`master_id`, `chart_id`, `debit`, `credit`, `memo`, `created`, `created_by`) VALUES ('23', '553', NULL, '1000', 'asd', '2020-02-16 12:45:55', '8');
UPDATE `ac_journal_master` SET `journal_no` = '1920_1004', `payment_type` = 'BPV', `journal_date` = '2020-02-16', `modified` = '2020-02-16 12:46:03', `modified_by` = '8'
WHERE `id` = '23';
INSERT INTO `ac_journal_master` (`company_id`, `journal_no`, `journal_date`, `payment_type`, `created`, `created_by`) VALUES ('2', '1920_1001', '2020-02-16', 'JV', '2020-02-16 13:12:00', '8');
INSERT INTO `ac_journal_details` (`master_id`, `chart_id`, `debit`, `credit`, `memo`, `created`, `created_by`) VALUES (24, '526', '400', NULL, '', '2020-02-16 13:12:01', '8');
INSERT INTO `ac_journal_details` (`master_id`, `chart_id`, `debit`, `credit`, `memo`, `created`, `created_by`) VALUES ('24', '1340', NULL, '400', '', '2020-02-16 13:12:02', '8');
UPDATE `ac_journal_master` SET `journal_no` = '1920_1001', `payment_type` = 'JV', `journal_date` = '2020-02-16', `modified` = '2020-02-16 13:12:03', `modified_by` = '8'
WHERE `id` = '24';
UPDATE `emps` SET `name` = 'sohaill', `father_name` = '', `cnic` = '', `joining` = '2020-02-09', `resign` = '0000-00-00', `present_address` = '', `permanent_address` = '', `voter_id` = NULL, `department` = '', `designation` = '', `mobile` = '', `email` = '', `salary` = '10000', `house_rent` = '0', `account_no` = '', `notes` = '', `status` = 'Active', `modified` = '2020-02-16 13:23:25', `modified_by` = '8'
WHERE `id` = '63';
INSERT INTO `emps_salary` (`company_id`, `code`, `emps_id`, `basic_salary`, `days`, `date`, `arrears`, `bonus`, `i_others`, `eobi`, `pessi`, `advance`, `d_others`, `amount_type`, `salary_date`, `created`, `created_by`) VALUES ('2', 1002, '63', '10000', '30', '2020-02-16', '', '', 2666.6666666667, '', '', '', '', 'Cash', '2020-02-16', '2020-02-16 13:23:43', '8');
UPDATE `emps_salary` SET `basic_salary` = '10000', `days` = '30', `date` = '2020-02-16', `arrears` = '0', `bonus` = '0', `i_others` = '8.00', `eobi` = '0', `pessi` = '0', `advance` = '0', `d_others` = '0', `amount_type` = 'Cash', `salary_date` = '2020-02-16', `modified` = '2020-02-16 13:27:05', `modified_by` = '8'
WHERE `id` = '4';
UPDATE `emps_salary` SET `basic_salary` = '10000', `days` = '30', `date` = '2020-02-16', `arrears` = '0', `bonus` = '0', `i_others` = 341.33333333333, `eobi` = '0', `pessi` = '0', `advance` = '0', `d_others` = '0', `amount_type` = 'Cash', `salary_date` = '2020-02-16', `modified` = '2020-02-16 13:29:33', `modified_by` = '8'
WHERE `id` = '4';
UPDATE `emps_salary` SET `basic_salary` = '10000', `days` = '30', `date` = '2020-02-16', `arrears` = '0', `bonus` = '0', `i_others` = 2674.3333333333, `eobi` = '0', `pessi` = '0', `advance` = '0', `d_others` = '0', `amount_type` = 'Cash', `salary_date` = '2020-02-16', `modified` = '2020-02-16 13:30:00', `modified_by` = '8'
WHERE `id` = '4';
UPDATE `emps_salary` SET `basic_salary` = '10000', `days` = '30', `date` = '2020-02-16', `arrears` = '0', `bonus` = '0', `i_others` = 2674, `eobi` = '0', `pessi` = '0', `advance` = '0', `d_others` = '0', `amount_type` = 'Cash', `salary_date` = '2020-02-16', `modified` = '2020-02-16 13:30:05', `modified_by` = '8'
WHERE `id` = '4';
UPDATE `emps_salary` SET `basic_salary` = '10000', `days` = '30', `date` = '2020-02-16', `arrears` = '0', `bonus` = '0', `i_others` = 2674, `eobi` = '0', `pessi` = '0', `advance` = '0', `d_others` = '0', `amount_type` = 'Cash', `salary_date` = '2020-02-16', `modified` = '2020-02-16 13:30:10', `modified_by` = '8'
WHERE `id` = '4';
UPDATE `emps_salary` SET `basic_salary` = '10000', `days` = '30', `date` = '2020-02-16', `arrears` = '0', `bonus` = '0', `i_others` = 2674, `eobi` = '0', `pessi` = '0', `advance` = '0', `d_others` = '0', `amount_type` = 'Cash', `salary_date` = '2020-02-16', `modified` = '2020-02-16 13:30:15', `modified_by` = '8'
WHERE `id` = '4';
UPDATE `emps_salary` SET `basic_salary` = '10000', `days` = '30', `date` = '2020-02-29', `arrears` = '1200', `bonus` = '100', `i_others` = 200, `eobi` = '10', `pessi` = '50', `advance` = '30', `d_others` = '0', `amount_type` = 'Cash', `salary_date` = '2020-02-01', `modified` = '2020-02-16 13:30:23', `modified_by` = '8'
WHERE `id` = '3';
INSERT INTO `emps_salary` (`company_id`, `code`, `emps_id`, `basic_salary`, `days`, `date`, `arrears`, `bonus`, `i_others`, `eobi`, `pessi`, `advance`, `d_others`, `amount_type`, `salary_date`, `created`, `created_by`) VALUES ('2', 1003, '63', '10000', '30', '2020-02-16', '', '', 3333.3333333333, '', '', '', '', 'Cash', '2020-02-16', '2020-02-16 13:30:44', '8');
UPDATE `emps_salary` SET `basic_salary` = '10000', `days` = '30', `date` = '2020-02-16', `arrears` = '0', `bonus` = '0', `i_others` = 3333, `eobi` = '0', `pessi` = '0', `advance` = '0', `d_others` = '0', `amount_type` = 'Cash', `salary_date` = '2020-02-16', `modified` = '2020-02-16 13:31:05', `modified_by` = '8'
WHERE `id` = '5';
UPDATE `accounts_calander` SET `start_date` = '2019-07-01', `end_date` = '2020-06-30', `created` = '2020-02-16 13:55:40'
WHERE `user_id` = '8';
UPDATE `accounts_calander` SET `start_date` = '2019-07-01', `end_date` = '2020-06-30', `created` = '2020-03-07 14:01:16'
WHERE `user_id` = '8';
INSERT INTO `ac_journal_master` (`company_id`, `journal_no`, `journal_date`, `payment_type`, `created`, `created_by`) VALUES ('2', '1920_1002', '2020-03-07', 'JV', '2020-03-07 14:04:14', '8');
INSERT INTO `ac_journal_details` (`master_id`, `chart_id`, `debit`, `credit`, `memo`, `created`, `created_by`) VALUES (25, '1053', '10', NULL, 'sadas', '2020-03-07 14:04:14', '8');
INSERT INTO `ac_journal_details` (`master_id`, `chart_id`, `debit`, `credit`, `memo`, `created`, `created_by`) VALUES ('25', '553', NULL, '10', '', '2020-03-07 14:05:05', '8');
UPDATE `ac_journal_master` SET `journal_no` = '1920_1002', `payment_type` = 'JV', `journal_date` = '2020-03-07', `modified` = '2020-03-07 14:05:07', `modified_by` = '8'
WHERE `id` = '25';
UPDATE `accounts_calander` SET `start_date` = '2019-07-01', `end_date` = '2020-06-30', `created` = '2020-03-07 14:16:48'
WHERE `user_id` = '8';
UPDATE `settings` SET `ac_receivable` = '1018', `ac_payable` = '542', `ac_cash` = '535', `ac_bank` = '1056', `ac_sales` = '545', `ac_purchase` = '525', `ac_inventory_raw` = NULL, `ac_inventory_wet_blue` = NULL, `ac_inventory_chemical` = NULL, `ac_inventory_fg` = NULL, `project` = '1334', `ac_cogs` = '514', `ac_tax` = '1034', `modified_at` = '2020-03-07 14:30:51', `modified_by` = '8'
WHERE `id` = '2';
UPDATE `settings` SET `ac_receivable` = '1018', `ac_payable` = '542', `ac_cash` = '535', `ac_bank` = '1056', `ac_sales` = '545', `ac_purchase` = '525', `ac_inventory_raw` = NULL, `ac_inventory_wet_blue` = NULL, `ac_inventory_chemical` = NULL, `ac_inventory_fg` = NULL, `project` = '1334', `ac_cogs` = '514', `ac_tax` = '1034', `modified_at` = '2020-03-07 14:31:31', `modified_by` = '8'
WHERE `id` = '2';
UPDATE `settings` SET `ac_receivable` = '1018', `ac_payable` = '542', `ac_cash` = '535', `ac_bank` = '1056', `ac_sales` = '545', `ac_purchase` = '525', `ac_inventory_raw` = NULL, `ac_inventory_wet_blue` = NULL, `ac_inventory_chemical` = NULL, `ac_inventory_fg` = NULL, `project` = '1334', `ac_cogs` = '514', `ac_tax` = '1034', `modified_at` = '2020-03-07 14:32:34', `modified_by` = '8'
WHERE `id` = '2';
UPDATE `settings` SET `ac_receivable` = '1018', `ac_payable` = '542', `ac_cash` = '535', `ac_bank` = '1056', `ac_sales` = '545', `ac_purchase` = '525', `ac_inventory_raw` = NULL, `ac_inventory_wet_blue` = NULL, `ac_inventory_chemical` = NULL, `ac_inventory_fg` = NULL, `project` = '1334', `ac_cogs` = '514', `ac_tax` = '1034', `modified_at` = '2020-03-07 14:32:42', `modified_by` = '8'
WHERE `id` = '2';
UPDATE `settings` SET `ac_receivable` = '1018', `ac_payable` = '542', `ac_cash` = '535', `ac_bank` = '1056', `ac_sales` = '545', `ac_purchase` = '525', `project` = '1334', `ac_cogs` = '514', `ac_tax` = '1034', `modified_at` = '2020-03-07 14:33:16', `modified_by` = '8'
WHERE `id` = '2';
UPDATE `settings` SET `ac_receivable` = '1018', `ac_payable` = '542', `ac_cash` = '535', `ac_bank` = '1056', `ac_sales` = '545', `ac_purchase` = '525', `project` = '1334', `ac_cogs` = '514', `ac_tax` = '1034', `modified_at` = '2020-03-07 14:33:29', `modified_by` = '8'
WHERE `id` = '2';
INSERT INTO `ac_journal_master` (`company_id`, `journal_no`, `journal_date`, `payment_type`, `created`, `created_by`) VALUES ('2', '1920_1003', '2020-03-07', 'JV', '2020-03-07 14:46:41', '8');
INSERT INTO `ac_journal_details` (`master_id`, `chart_id`, `debit`, `credit`, `memo`, `created`, `created_by`) VALUES (26, '516', '100', NULL, 'asd', '2020-03-07 14:46:41', '8');
INSERT INTO `ac_journal_details` (`master_id`, `chart_id`, `debit`, `credit`, `memo`, `created`, `created_by`) VALUES ('26', '516', '10', NULL, 'asd', '2020-03-07 14:46:51', '8');
INSERT INTO `ac_journal_master` (`company_id`, `journal_no`, `project_ac`, `journal_date`, `payment_type`, `created`, `created_by`) VALUES ('2', '1920_1004', '1335', '2020-03-07', 'JV', '2020-03-07 14:54:02', '8');
INSERT INTO `ac_journal_details` (`master_id`, `chart_id`, `debit`, `credit`, `memo`, `created`, `created_by`) VALUES (27, '513', '100', NULL, 'asd', '2020-03-07 14:54:02', '8');
INSERT INTO `ac_journal_details` (`master_id`, `chart_id`, `debit`, `credit`, `memo`, `created`, `created_by`) VALUES ('27', '513', NULL, '100', '', '2020-03-07 14:54:12', '8');
UPDATE `ac_journal_master` SET `journal_no` = '1920_1004', `payment_type` = 'JV', `project_ac` = '1338', `journal_date` = '2020-03-07', `modified` = '2020-03-07 14:54:24', `modified_by` = '8'
WHERE `id` = '27';
UPDATE `ac_journal_master` SET `journal_no` = '1920_1004', `payment_type` = 'JV', `project_ac` = '1335', `journal_date` = '2020-03-07', `modified` = '2020-03-07 15:10:02', `modified_by` = '8'
WHERE `id` = '27';
UPDATE `ac_journal_master` SET `journal_no` = '1920_1001', `payment_type` = 'JV', `project_ac` = '1335', `journal_date` = '2020-02-16', `modified` = '2020-03-07 15:16:09', `modified_by` = '8'
WHERE `id` = '24';
UPDATE `ac_journal_master` SET `journal_no` = '1920_1004', `payment_type` = 'BPV', `project_ac` = '1335', `journal_date` = '2020-02-16', `modified` = '2020-03-07 15:16:19', `modified_by` = '8'
WHERE `id` = '23';
UPDATE `ac_journal_master` SET `journal_no` = '1920_1009', `payment_type` = 'CPV', `project_ac` = '1335', `journal_date` = '2020-02-16', `modified` = '2020-03-07 15:16:37', `modified_by` = '8'
WHERE `id` = '20';
DELETE FROM `ac_journal_master`
WHERE `id` = '27';
DELETE FROM `ac_journal_master`
WHERE `id` = '26';
DELETE FROM `ac_journal_master`
WHERE `id` = '25';
DELETE FROM `ac_journal_master`
WHERE `id` = '24';
DELETE FROM `ac_journal_master`
WHERE `id` = '23';
DELETE FROM `ac_journal_master`
WHERE `id` = '22';
DELETE FROM `ac_journal_master`
WHERE `id` = '21';
DELETE FROM `ac_journal_master`
WHERE `id` = '20';
DELETE FROM `ac_journal_master`
WHERE `id` = '15';
DELETE FROM `ac_journal_master`
WHERE `id` = '14';
DELETE FROM `ac_journal_master`
WHERE `id` = '13';
DELETE FROM `ac_journal_master`
WHERE `id` = '12';
DELETE FROM `ac_journal_master`
WHERE `id` = '10';
DELETE FROM `ac_journal_master`
WHERE `id` = '9';
DELETE FROM `ac_journal_master`
WHERE `id` = '8';
DELETE FROM `ac_journal_master`
WHERE `id` = '7';
DELETE FROM `ac_journal_master`
WHERE `id` = '6';
DELETE FROM `ac_journal_master`
WHERE `id` = '5';
DELETE FROM `ac_journal_master`
WHERE `id` = '4';
DELETE FROM `ac_charts`
WHERE `id` = '515';
INSERT INTO `ac_journal_master` (`company_id`, `journal_no`, `project_ac`, `journal_date`, `payment_type`, `created`, `created_by`) VALUES ('2', '1920_1001', '0', '2020-03-07', 'JV', '2020-03-07 15:43:35', '8');
INSERT INTO `ac_journal_details` (`master_id`, `chart_id`, `debit`, `credit`, `memo`, `created`, `created_by`) VALUES (28, '1336', '100', NULL, '', '2020-03-07 15:43:35', '8');
INSERT INTO `ac_journal_details` (`master_id`, `chart_id`, `debit`, `credit`, `memo`, `created`, `created_by`) VALUES ('28', '517', NULL, '100', '', '2020-03-07 15:43:40', '8');
UPDATE `ac_journal_master` SET `journal_no` = '1920_1001', `payment_type` = 'JV', `project_ac` = '0', `journal_date` = '2020-03-07', `modified` = '2020-03-07 15:43:43', `modified_by` = '8'
WHERE `id` = '28';
DELETE FROM `ac_charts`
WHERE `id` = '1336';
DELETE FROM `ac_journal_master`
WHERE `id` = '28';
DELETE FROM `ac_charts`
WHERE `id` = '1339';
DELETE FROM `ac_charts`
WHERE `id` = '1340';
INSERT INTO `ac_journal_master` (`company_id`, `journal_no`, `project_ac`, `journal_date`, `payment_type`, `created`, `created_by`) VALUES ('2', '1920_1001', '1335', '2020-03-07', 'JV', '2020-03-07 15:53:18', '8');
INSERT INTO `ac_journal_details` (`master_id`, `chart_id`, `debit`, `credit`, `memo`, `created`, `created_by`) VALUES (29, '524', '100', NULL, 'asd', '2020-03-07 15:53:18', '8');
INSERT INTO `ac_journal_details` (`master_id`, `chart_id`, `debit`, `credit`, `memo`, `created`, `created_by`) VALUES ('29', '516', NULL, '100', '', '2020-03-07 15:53:23', '8');
UPDATE `ac_journal_master` SET `journal_no` = '1920_1001', `payment_type` = 'JV', `project_ac` = '1335', `journal_date` = '2020-03-07', `modified` = '2020-03-07 15:53:24', `modified_by` = '8'
WHERE `id` = '29';
INSERT INTO `ac_journal_master` (`company_id`, `journal_no`, `project_ac`, `journal_date`, `payment_type`, `created`, `created_by`) VALUES ('2', '1920_1002', '1335', '2020-03-07', 'JV', '2020-03-07 15:53:35', '8');
INSERT INTO `ac_journal_details` (`master_id`, `chart_id`, `debit`, `credit`, `memo`, `created`, `created_by`) VALUES (30, '518', '100', NULL, '', '2020-03-07 15:53:35', '8');
INSERT INTO `ac_journal_details` (`master_id`, `chart_id`, `debit`, `credit`, `memo`, `created`, `created_by`) VALUES ('30', '510', NULL, '100', '', '2020-03-07 15:53:38', '8');
UPDATE `ac_journal_master` SET `journal_no` = '1920_1002', `payment_type` = 'JV', `project_ac` = '1335', `journal_date` = '2020-03-07', `modified` = '2020-03-07 15:53:39', `modified_by` = '8'
WHERE `id` = '30';
INSERT INTO `ac_journal_master` (`company_id`, `journal_no`, `project_ac`, `journal_date`, `payment_type`, `created`, `created_by`) VALUES ('2', '1920_1003', '1335', '2020-03-07', 'JV', '2020-03-07 15:54:01', '8');
INSERT INTO `ac_journal_details` (`master_id`, `chart_id`, `debit`, `credit`, `memo`, `created`, `created_by`) VALUES (31, '510', '20', NULL, 'dgd', '2020-03-07 15:54:01', '8');
INSERT INTO `ac_journal_details` (`master_id`, `chart_id`, `debit`, `credit`, `memo`, `created`, `created_by`) VALUES ('31', '510', NULL, '20', 'sda', '2020-03-07 15:54:12', '8');
UPDATE `ac_journal_master` SET `journal_no` = '1920_1003', `payment_type` = 'JV', `project_ac` = '1335', `journal_date` = '2020-03-07', `modified` = '2020-03-07 15:54:13', `modified_by` = '8'
WHERE `id` = '31';
UPDATE `accounts_calander` SET `start_date` = '2019-07-01', `end_date` = '2020-06-30', `created` = '2020-03-07 18:15:01'
WHERE `user_id` = '8';
DELETE FROM `ac_journal_master`
WHERE `id` = '31';
DELETE FROM `ac_journal_master`
WHERE `id` = '30';
DELETE FROM `ac_journal_master`
WHERE `id` = '29';
INSERT INTO `ac_journal_master` (`company_id`, `journal_no`, `journal_date`, `payment_type`, `created`, `created_by`) VALUES ('2', '1920_1001', '2020-03-07', 'JV', '2020-03-07 18:26:01', '8');
INSERT INTO `ac_journal_details` (`master_id`, `chart_id`, `debit`, `credit`, `project_ac_debit`, `project_ac_credit`, `memo`, `created`, `created_by`) VALUES (32, '513', '100', NULL, 0, 0, '', '2020-03-07 18:26:01', '8');
INSERT INTO `ac_journal_details` (`master_id`, `chart_id`, `debit`, `credit`, `project_ac_debit`, `project_ac_credit`, `memo`, `created`, `created_by`) VALUES ('32', '', NULL, '100', 0, 0, '', '2020-03-07 18:26:03', '8');
INSERT INTO `ac_journal_details` (`master_id`, `chart_id`, `debit`, `credit`, `project_ac_debit`, `project_ac_credit`, `memo`, `created`, `created_by`) VALUES ('32', '513', NULL, '100', 0, 0, '', '2020-03-07 18:26:09', '8');
UPDATE `ac_journal_master` SET `journal_no` = '1920_1001', `payment_type` = 'JV', `journal_date` = '2020-03-07', `modified` = '2020-03-07 18:26:13', `modified_by` = '8'
WHERE `id` = '32';
INSERT INTO `ac_journal_master` (`company_id`, `journal_no`, `journal_date`, `payment_type`, `created`, `created_by`) VALUES ('2', '1920_1002', '2020-03-07', 'JV', '2020-03-07 18:27:40', '8');
INSERT INTO `ac_journal_details` (`master_id`, `chart_id`, `debit`, `credit`, `project_ac_debit`, `project_ac_credit`, `memo`, `created`, `created_by`) VALUES (33, '512', '100', NULL, '1335', 0, '', '2020-03-07 18:27:40', '8');
INSERT INTO `ac_journal_details` (`master_id`, `chart_id`, `debit`, `credit`, `project_ac_debit`, `project_ac_credit`, `memo`, `created`, `created_by`) VALUES ('33', '1239', NULL, '100', 0, 0, 'asd', '2020-03-07 18:27:53', '8');
DELETE FROM `ac_journal_details`
WHERE `id` = '86';
INSERT INTO `ac_journal_details` (`master_id`, `chart_id`, `debit`, `credit`, `project_ac_debit`, `project_ac_credit`, `memo`, `created`, `created_by`) VALUES ('33', '1239', NULL, '100', 0, '1335', '', '2020-03-07 18:28:14', '8');
UPDATE `ac_journal_master` SET `journal_no` = '1920_1002', `payment_type` = 'JV', `journal_date` = '2020-03-07', `modified` = '2020-03-07 18:28:20', `modified_by` = '8'
WHERE `id` = '33';
INSERT INTO `ac_journal_master` (`company_id`, `journal_no`, `journal_date`, `payment_type`, `created`, `created_by`) VALUES ('2', '1920_1003', '2020-03-07', 'JV', '2020-03-07 18:30:51', '8');
INSERT INTO `ac_journal_details` (`master_id`, `chart_id`, `debit`, `credit`, `project_ac_debit`, `project_ac_credit`, `memo`, `created`, `created_by`) VALUES (34, '1239', '200', NULL, '1335', 0, '', '2020-03-07 18:30:51', '8');
INSERT INTO `ac_journal_details` (`master_id`, `chart_id`, `debit`, `credit`, `project_ac_debit`, `project_ac_credit`, `memo`, `created`, `created_by`) VALUES ('34', '512', NULL, '200', 0, '1335', '', '2020-03-07 18:31:01', '8');
UPDATE `ac_journal_master` SET `journal_no` = '1920_1003', `payment_type` = 'JV', `journal_date` = '2020-03-07', `modified` = '2020-03-07 18:31:03', `modified_by` = '8'
WHERE `id` = '34';
UPDATE `accounts_calander` SET `start_date` = '2019-07-01', `end_date` = '2020-06-30', `created` = '2020-03-07 20:00:33'
WHERE `user_id` = '8';
UPDATE `accounts_calander` SET `start_date` = '2019-07-01', `end_date` = '2020-06-30', `created` = '2020-05-05 14:47:55'
WHERE `user_id` = '8';
INSERT INTO `ac_charts` (`company_id`, `parent_id`, `code`, `name`, `memo`, `opening`, `status`, `created_at`, `created_by`) VALUES ('2', '1072', '10.10.30.10.1009', 'test 1', '', '', 'Active', '2020-05-05 14:49:56', '8');
INSERT INTO `ac_charts` (`company_id`, `parent_id`, `code`, `name`, `memo`, `opening`, `status`, `created_at`, `created_by`) VALUES ('2', '1072', '10.10.30.10.1019', 'test ', '', '', 'Active', '2020-05-05 14:53:23', '8');
UPDATE `accounts_calander` SET `start_date` = '2019-07-01', `end_date` = '2020-06-30', `created` = '2020-05-05 17:25:45'
WHERE `user_id` = '8';
UPDATE `accounts_calander` SET `start_date` = '2019-07-01', `end_date` = '2020-06-30', `created` = '2020-05-20 12:15:13'
WHERE `user_id` = '8';
INSERT INTO `ac_journal_master` (`company_id`, `journal_no`, `journal_date`, `payment_type`, `created`, `created_by`) VALUES ('2', '1920_1004', '2020-05-20', 'JV', '2020-05-20 12:43:45', '8');
INSERT INTO `ac_journal_details` (`master_id`, `chart_id`, `debit`, `credit`, `project_ac_debit`, `project_ac_credit`, `memo`, `created`, `created_by`) VALUES (35, '513', '100', NULL, '1335', 0, 'nghg', '2020-05-20 12:43:45', '8');
INSERT INTO `ac_journal_details` (`master_id`, `chart_id`, `debit`, `credit`, `project_ac_debit`, `project_ac_credit`, `memo`, `created`, `created_by`) VALUES ('35', '516', NULL, '100', 0, '1335', 'hjggh', '2020-05-20 12:43:53', '8');
UPDATE `ac_journal_master` SET `journal_no` = '1920_1004', `payment_type` = 'JV', `journal_date` = '2020-05-20', `modified` = '2020-05-20 12:43:56', `modified_by` = '8'
WHERE `id` = '35';
UPDATE `accounts_calander` SET `start_date` = '2019-07-01', `end_date` = '2020-06-30', `created` = '2020-05-20 21:56:31'
WHERE `user_id` = '8';
UPDATE `ac_charts` SET `parent_id` = '1334', `code` = '10.10.100.10', `name` = 'descon', `memo` = '', `opening` = '500', `status` = 'Active', `modified_at` = '2020-05-20 22:02:21', `modified_by` = '8'
WHERE `id` = '1335';
UPDATE `ac_charts` SET `parent_id` = '1334', `code` = '10.10.100.10', `name` = 'descon', `memo` = '', `opening` = '-500', `status` = 'Active', `modified_at` = '2020-05-20 22:03:26', `modified_by` = '8'
WHERE `id` = '1335';
UPDATE `ac_charts` SET `parent_id` = '1334', `code` = '10.10.100.10', `name` = 'descon', `memo` = '', `opening` = '500', `status` = 'Active', `modified_at` = '2020-05-20 22:04:47', `modified_by` = '8'
WHERE `id` = '1335';
INSERT INTO `ac_charts` (`company_id`, `parent_id`, `code`, `name`, `memo`, `opening`, `status`, `created_at`, `created_by`) VALUES ('2', '1335', '10.10.100.10.10', 'test 1', 'dfsd', '0', 'Active', '2020-05-20 22:14:29', '8');
INSERT INTO `ac_charts` (`company_id`, `parent_id`, `code`, `name`, `memo`, `opening`, `status`, `created_at`, `created_by`) VALUES ('2', '1335', '10.10.100.10.20', 'sdsad', 'sd', '0', 'Active', '2020-05-20 22:14:41', '8');
INSERT INTO `ac_journal_master` (`company_id`, `journal_no`, `journal_date`, `payment_type`, `created`, `created_by`) VALUES ('2', '1920_1005', '2020-05-20', 'JV', '2020-05-20 22:15:10', '8');
INSERT INTO `ac_journal_details` (`master_id`, `chart_id`, `debit`, `credit`, `project_ac_debit`, `project_ac_credit`, `memo`, `created`, `created_by`) VALUES (36, '1341', '123', NULL, '1335', 0, 'sdasd', '2020-05-20 22:15:10', '8');
INSERT INTO `ac_journal_details` (`master_id`, `chart_id`, `debit`, `credit`, `project_ac_debit`, `project_ac_credit`, `memo`, `created`, `created_by`) VALUES ('36', '1342', NULL, '123', 0, '1335', 'asdasd', '2020-05-20 22:15:16', '8');
UPDATE `ac_journal_master` SET `journal_no` = '1920_1005', `payment_type` = 'JV', `journal_date` = '2020-05-20', `modified` = '2020-05-20 22:15:18', `modified_by` = '8'
WHERE `id` = '36';
INSERT INTO `ac_journal_master` (`company_id`, `journal_no`, `journal_date`, `payment_type`, `created`, `created_by`) VALUES ('2', '1920_1006', '2020-05-06', 'JV', '2020-05-20 22:44:24', '8');
INSERT INTO `ac_journal_details` (`master_id`, `chart_id`, `debit`, `credit`, `project_ac_debit`, `project_ac_credit`, `memo`, `created`, `created_by`) VALUES (37, '1341', '1234', NULL, '1335', 0, 'asd', '2020-05-20 22:44:24', '8');
INSERT INTO `ac_journal_details` (`master_id`, `chart_id`, `debit`, `credit`, `project_ac_debit`, `project_ac_credit`, `memo`, `created`, `created_by`) VALUES ('37', '513', NULL, '1234', 0, '1335', 'asdas', '2020-05-20 22:44:28', '8');
UPDATE `ac_journal_master` SET `journal_no` = '1920_1006', `payment_type` = 'JV', `journal_date` = '2020-05-06', `modified` = '2020-05-20 22:44:31', `modified_by` = '8'
WHERE `id` = '37';
UPDATE `accounts_calander` SET `start_date` = '2019-07-01', `end_date` = '2020-06-30', `created` = '2020-06-12 10:03:31'
WHERE `user_id` = '8';
UPDATE `accounts_calander` SET `start_date` = '2019-07-01', `end_date` = '2020-06-30', `created` = '2020-06-28 22:40:40'
WHERE `user_id` = '8';
UPDATE `accounts_calander` SET `start_date` = '2020-07-01', `end_date` = '2021-06-30', `created` = '2020-07-04 14:37:43'
WHERE `user_id` = '8';
INSERT INTO `ac_journal_master` (`company_id`, `journal_no`, `journal_date`, `payment_type`, `created`, `created_by`) VALUES ('2', '2021_1001', '2020-07-04', 'JV', '2020-07-04 15:55:14', '8');
INSERT INTO `ac_journal_details` (`master_id`, `chart_id`, `debit`, `credit`, `project_ac_debit`, `project_ac_credit`, `memo`, `created`, `created_by`) VALUES (38, '553', '100', NULL, 0, 0, 'asdsa', '2020-07-04 15:55:14', '8');
INSERT INTO `ac_journal_details` (`master_id`, `chart_id`, `debit`, `credit`, `project_ac_debit`, `project_ac_credit`, `memo`, `created`, `created_by`) VALUES ('38', '562', NULL, '100', 0, 0, 'sdasd', '2020-07-04 15:55:23', '8');
UPDATE `ac_journal_master` SET `journal_no` = '2021_1001', `payment_type` = 'JV', `journal_date` = '2020-07-04', `modified` = '2020-07-04 15:55:26', `modified_by` = '8'
WHERE `id` = '38';
INSERT INTO `ac_journal_master` (`company_id`, `journal_no`, `journal_date`, `payment_type`, `created`, `created_by`) VALUES ('2', '2021_1002', '2020-07-04', 'JV', '2020-07-04 15:55:56', '8');
INSERT INTO `ac_journal_details` (`master_id`, `chart_id`, `debit`, `credit`, `project_ac_debit`, `project_ac_credit`, `memo`, `created`, `created_by`) VALUES (39, '555', '344', NULL, 0, 0, 'erer', '2020-07-04 15:55:56', '8');
INSERT INTO `ac_journal_details` (`master_id`, `chart_id`, `debit`, `credit`, `project_ac_debit`, `project_ac_credit`, `memo`, `created`, `created_by`) VALUES ('39', '565', NULL, '344', 0, 0, 'ewr', '2020-07-04 15:55:59', '8');
UPDATE `ac_journal_master` SET `journal_no` = '2021_1002', `payment_type` = 'JV', `journal_date` = '2020-07-04', `modified` = '2020-07-04 15:56:01', `modified_by` = '8'
WHERE `id` = '39';
