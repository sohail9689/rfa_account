<?php

defined('BASEPATH') or exit('No direct script access allowed');

class MEmps extends CI_Model
{

    public function __construct()
    {
        parent::__construct();
    }

    public function get_by_id($id)
    {
        $data = array();
        $this->db->select('emps.*, companies.name as company_name, companies.url as company_url');
        $this->db->from('emps');
        $this->db->join('companies', 'emps.company_id = companies.id', 'left');
        $this->db->where('emps.id', $id);
        $this->db->where('emps.company_id', $this->session->userdata('user_company'));
        $q = $this->db->get();
        // $sql = $this->db->last_query();
        // print_r($sql);
        // die();
        if ($q->num_rows() > 0) {
            foreach ($q->result_array() as $row) {
                $data = $row;
            }
        }

        $q->free_result();
        return $data;
    }

    public function get_latest()
    {
        $data = array();
        $this->db->order_by('id', 'DESC');
        $this->db->limit(1);
        $this->db->where('company_id', $this->session->userdata('user_company'));
        $q = $this->db->get('emps');
        if ($q->num_rows() > 0) {
            foreach ($q->result_array() as $row) {
                $data = $row;
            }
        }

        $q->free_result();
        return $data;
    }

    public function get_latest_salary()
    {
        $data = array();
        $this->db->order_by('id', 'DESC');
        $this->db->limit(1);
        $this->db->where('company_id', $this->session->userdata('user_company'));
        $q = $this->db->get('emps_salary');
        if ($q->num_rows() > 0) {
            foreach ($q->result_array() as $row) {
                $data = $row;
            }
        }

        $q->free_result();
        return $data;
    }

    public function create($code)
    {
        $data = array(
            'company_id' => $this->session->userdata('user_company'),
            'code' => $code,
            'name' => $this->input->post('name'),
            'father_name' => $this->input->post('father_name'),
            'cnic' => $this->input->post('cnic'),
            'joining' => date_to_db($this->input->post('joining')),
            'resign' => date_to_db($this->input->post('resign')),
            'present_address' => $this->input->post('present_address'),
            'permanent_address' => $this->input->post('permanent_address'),
            'voter_id' => $this->input->post('voter_id'),
            'department' => $this->input->post('department'),
            'designation' => $this->input->post('designation'),
            'mobile' => $this->input->post('mobile'),
            'email' => $this->input->post('email'),
            'salary' => $this->input->post('salary'),
            'house_rent' => $this->input->post('house_rent'),
            'account_no' => $this->input->post('account_no'),
            'notes' => $this->input->post('notes'),
            'status' => $this->input->post('status'),
            'created' => date('Y-m-d H:i:s', time()),
            'created_by' => $this->session->userdata('user_id')
        );
        $this->db->insert('emps', $data);
    }

    public function update()
    {
        $data = array(
            'name' => $this->input->post('name'),
            'father_name' => $this->input->post('father_name'),
            'cnic' => $this->input->post('cnic'),
            'joining' => date_to_db($this->input->post('joining')),
            'resign' => date_to_db($this->input->post('resign')),
            'present_address' => $this->input->post('present_address'),
            'permanent_address' => $this->input->post('permanent_address'),
            'voter_id' => $this->input->post('voter_id'),
            'department' => $this->input->post('department'),
            'designation' => $this->input->post('designation'),
            'mobile' => $this->input->post('mobile'),
            'email' => $this->input->post('email'),
            'salary' => $this->input->post('salary'),
            'house_rent' => $this->input->post('house_rent'),
            'account_no' => $this->input->post('account_no'),
            'notes' => $this->input->post('notes'),
            'status' => $this->input->post('status'),
            'modified' => date('Y-m-d H:i:s', time()),
            'modified_by' => $this->session->userdata('user_id')
        );
        $this->db->where('id', $this->input->post('id'));
        $this->db->update('emps', $data);
    }

    public function delete($id)
    {
        $this->db->where('id', $id);
        $this->db->delete('emps');
    }

    public function delete_by_cmp($cmp_id)
    {
        $this->db->where('company_id', $cmp_id);
        $this->db->delete('emps');
    }
    public function get_all($status = NULL)
    {
        $data = array();
        if ($status) {
            $this->db->where('status', $status);
        }
        $this->db->where('company_id', $this->session->userdata('user_company'));
        $q = $this->db->get('emps');
        if ($q->num_rows() > 0) {
            foreach ($q->result_array() as $row) {
                $data[] = $row;
            }
        }

        $q->free_result();
        return $data;
    }

    public function get_alll($status = 'Active')
    {
        $data = array();
        if ($status) {
            $this->db->where('status', $status);
        }
        $this->db->where('company_id', $this->session->userdata('user_company'));
        $q = $this->db->get('emps');
        if ($q->num_rows() > 0) {
            foreach ($q->result_array() as $row) {
                $data[] = $row;
            }
        }

        $q->free_result();
        return $data;
    }

    public function get_salary_al()
    {
        $data = array();
        // if ($status)
        // {
        //     $this->db->where('status', $status);
        // }

        $this->db->where('company_id', $this->session->userdata('user_company'));
        $q = $this->db->get('emps_salary');
        if ($q->num_rows() > 0) {
            foreach ($q->result_array() as $row) {
                $data[] = $row;
            }
        }

        $q->free_result();
        // print_r($data);
        // die();
        return $data;
    }

    public function create_salary($code)
    {
        $basic_salary = $this->MEmps->get_salary($this->input->post('emps_id'));
        $overtime = ($basic_salary / 30) * $this->input->post('i_others');
        $data = array(
            'company_id' => $this->session->userdata('user_company'),
            'code' => $code,
            'emps_id' => $this->input->post('emps_id'),
            'basic_salary' => $basic_salary,
            'days' => $this->input->post('days'),
            'date' => date_to_db($this->input->post('date')),
            'arrears' => $this->input->post('arrears'),
            'bonus' => $this->input->post('bonus'),
            'i_others' => $overtime,
            'eobi' => $this->input->post('eobi'),
            'pessi' => $this->input->post('pessi'),
            'advance' => $this->input->post('advance'),
            'd_others' => $this->input->post('d_others'),
            'amount_type' => $this->input->post('amount_type'),
            'salary_date' => date_to_db($this->input->post('salary_date')),
            'created' => date('Y-m-d H:i:s', time()),
            'created_by' => $this->session->userdata('user_id')
        );

        $this->db->insert('emps_salary', $data);
    }

    public function update_salary()
    {
        $basic_salary = $this->MEmps->get_salary($this->input->post('emps_id'));
        $overtime = ($basic_salary / 30) * $this->input->post('i_others');
        $data = array(
            'basic_salary' => $basic_salary,
            'days' => $this->input->post('days'),
            'date' => date_to_db($this->input->post('date')),
            'arrears' => $this->input->post('arrears'),
            'bonus' => $this->input->post('bonus'),
            'i_others' => $overtime,
            'eobi' => $this->input->post('eobi'),
            'pessi' => $this->input->post('pessi'),
            'advance' => $this->input->post('advance'),
            'd_others' => $this->input->post('d_others'),
            'amount_type' => $this->input->post('amount_type'),
            'salary_date' => date_to_db($this->input->post('salary_date')),
            'modified' => date('Y-m-d H:i:s', time()),
            'modified_by' => $this->session->userdata('user_id')
        );
        $this->db->where('id', $this->input->post('id'));
        $this->db->update('emps_salary', $data);
    }

    public function get_salary_all($status = NULL)
    {
        $data = array();
        $this->db->select('emps_salary.*, emps.name as name, emps.house_rent as house_rent, emps.account_no as account_no, emps.salary as salary, emps.code as emps_code');
        $this->db->from('emps_salary');
        $this->db->join('emps', 'emps_salary.emps_id=emps.id');
        $this->db->where('emps_salary.company_id', $this->session->userdata('user_company'));
        $q = $this->db->get();
        if ($q->num_rows() > 0) {
            foreach ($q->result_array() as $row) {
                $data[] = $row;
            }
        }

        $q->free_result();
        // print_r($data);
        // die();
        return $data;
    }

    public function get_salary($id)
    {


        $data = array();
        // if ($status)
        // {
        //     $this->db->where('status', $status);
        // }
        $this->db->where('company_id', $this->session->userdata('user_company'));
        $this->db->where('id', $id);
        $q = $this->db->get('emps');
        if ($q->num_rows() > 0) {
            foreach ($q->result_array() as $row) {
                $data = $row['salary'];
            }
        }

        $q->free_result();
        return $data;
    }

    public function delete_salary($id)
    {


        $this->db->where('id', $id);
        $this->db->delete('emps_salary');
        // $sql = $this->db->last_query();
        // print_r($sql);
        // die();
    }

    public function get_by_idd($id)
    {

        $data = array();
        $this->db->select('emps_salary.*, emps.name as name');
        $this->db->from('emps_salary');
        $this->db->join('emps', 'emps.id = emps_salary.emps_id');
        $this->db->where('emps_salary.id', $id);
        $this->db->where('emps_salary.company_id', $this->session->userdata('user_company'));
        $q = $this->db->get();
        $sql = $this->db->last_query();
        // print_r($sql);
        // die();
        if ($q->num_rows() > 0) {
            foreach ($q->result_array() as $row) {
                $data = $row;
            }
        }

        $q->free_result();

        // print_r($data);
        // die();
        return $data;
    }
    public function get_by_id_salary($id)
    {
        $data = array();
        $this->db->select('emps_salary.*, emps.name as name, emps.house_rent as house_rent, emps.account_no as account_no, emps.salary as salary, emps.code as emps_code, emps.designation as designation, emps.present_address as region,emps.department as department,emps.status as status,emps.cnic as cnic');
        $this->db->from('emps');
        $this->db->join('emps_salary', 'emps.id = emps_salary.emps_id', 'left');
        $this->db->where('emps_salary.id', $id);
        $this->db->where('emps_salary.company_id', $this->session->userdata('user_company'));
        $q = $this->db->get();
        // $sql = $this->db->last_query();
        // print_r($sql);
        // die();
        if ($q->num_rows() > 0) {
            foreach ($q->result_array() as $row) {
                $data = $row;
            }
        }

        $q->free_result();
        return $data;
    }

    public function get_all_between_date($amount_type = NULL, $employee_id = NULL)
    {
        $data = array();
        $start_date = date_to_db($this->input->post('start_date'));
        $end_date = date_to_db($this->input->post('end_date'));
        $this->db->where("salary_date BETWEEN '$start_date' AND '$end_date'");
        if ($employee_id) {
            $this->db->where('emps_id', $employee_id);
        }
        if ($amount_type) {
            $this->db->where('amount_type', $amount_type);
        }
        $this->db->where('company_id', $this->session->userdata('user_company'));
        $this->db->order_by('code', 'ASC');
        $q = $this->db->get('emps_salary');
        if ($q->num_rows() > 0) {
            foreach ($q->result_array() as $row) {

                $row['name'] = $this->MEmps->get_name($row['emps_id']);
                $row['house_rent'] = $this->MEmps->get_rent($row['emps_id']);

                $data[] = $row;
            }
        }

        $q->free_result();
        $sql = $this->db->last_query();
        // print_r($data);
        // die();
        return $data;
    }

    public function get_name($id)
    {


        $data = array();
        $this->db->where('company_id', $this->session->userdata('user_company'));
        $this->db->where('id', $id);
        $q = $this->db->get('emps');
        if ($q->num_rows() > 0) {
            foreach ($q->result_array() as $row) {
                $data = $row['name'];
            }
        }

        $q->free_result();
        return $data;
    }

    public function get_rent($id)
    {


        $data = array();
        $this->db->where('company_id', $this->session->userdata('user_company'));
        $this->db->where('id', $id);
        $q = $this->db->get('emps');
        if ($q->num_rows() > 0) {
            foreach ($q->result_array() as $row) {
                $data = $row['house_rent'];
            }
        }

        $q->free_result();
        return $data;
    }
}
