<?php

defined('BASEPATH') OR exit('No direct script access allowed');

class MProduction extends CI_Model {

	public function __construct()
	{
		parent::__construct();
	}

	public function get_by_id($id, $raw_id = NULL)
	{
		$data = array();
		if($raw_id <> NULL){
			$this->db->where('raw', $raw_id);
		}
		$this->db->where('id', $id);
		$this->db->where('company_id', $this->session->userdata('user_company'));
		$q = $this->db->get('production');
		if ($q->num_rows() > 0)
		{
			foreach ($q->result_array() as $row)
			{
				$data = $row;
			}
		}

		$q->free_result();
		return $data;
	}
	public function update_status($id, $status)
    {
        $data = array(
            'status' => $status
            );
        $this->db->where('id', $id);
        $this->db->update('production', $data);
    }
	public function get_chemical_by_production_id($id)
	{

		$data = array();
		$this->db->where('production_id', $id);
		$this->db->where('company_id', $this->session->userdata('user_company'));
		$q = $this->db->get('production_chemical');
		if ($q->num_rows() > 0)
		{
			foreach ($q->result_array() as $row)
			{
				$data[] = $row;
			}
		}

		$q->free_result();
		return $data;
	}

	public function get_recipe_chemical_by_production_id($id, $chemical_id = NULL)
	{

		$data = array();
		$this->db->where('production_id', $id);
		if($chemical_id <> NULL){
			$this->db->where('chemical_id', $chemical_id);
		}
		$this->db->where('company_id', $this->session->userdata('user_company'));
		$q = $this->db->get('production_recipe_chemical');
		if ($q->num_rows() > 0)
		{
			foreach ($q->result_array() as $row)
			{
				$data[] = $row;
			}
		}

		$q->free_result();
		return $data;
	}

	public function get_chemical_by_production_id_in_recipe_chemical($id)
	{

		$data = array();
		$this->db->where('production_id', $id);
		$this->db->where('recipe_id', '0');
		$this->db->where('company_id', $this->session->userdata('user_company'));
		$q = $this->db->get('production_recipe_chemical');
		if ($q->num_rows() > 0)
		{
			foreach ($q->result_array() as $row)
			{
				$data[] = $row;
			}
		}

		$q->free_result();
		return $data;
	}
	public function get_chemical_by_production_id_and_recipe_id($production_id, $recipe_id)
	{
		$data = array();
		$this->db->where('production_id', $production_id);
		$this->db->where('recipe_id', $recipe_id);
		$this->db->where('company_id', $this->session->userdata('user_company'));
		$q = $this->db->get('production_recipe_chemical');
		if ($q->num_rows() > 0)
		{
			foreach ($q->result_array() as $row)
			{
				$data[] = $row;
			}
		}
		$q->free_result();
		return $data;
	}

	public function get_recipe_chemical_by_production_id2($id, $chemical_id = NULL)
	{

		$data = array();
		$this->db->select('production_recipe_chemical.*, items.name as item_name, items.ac_id as item_ac_id');
		$this->db->from('production_recipe_chemical');
		$this->db->join('items','production_recipe_chemical.chemical_id = items.id','left');
		$this->db->where('production_recipe_chemical.production_id', $id);
		if($chemical_id <> NULL){
			$this->db->where('production_recipe_chemical.chemical_id', $chemical_id);
		}
		$this->db->where('production_recipe_chemical.company_id', $this->session->userdata('user_company'));

		$q = $this->db->get();
		if ($q->num_rows() > 0)
		{
			foreach ($q->result_array() as $row)
			{
				$data[] = $row;
			}
		}

		$q->free_result();
		return $data;
	}

public function get_chemiclas($id)
	{

		$options = "";
        $q1 = $this->get_chemical_by_production_id($id);
        $items = $this->MItems->get_all('', 'Chemical');
            for($i=0; $i < sizeof($q1); $i++){
            	$options .= '<div class="description_text">';
	            $options .= '<div class="control-group">';
	            if($i == 0){
	            $options .=	'<label for="name" class="span4">Chemical Name</label>';
	            $options .='<label for="name" class="span5">Quantity Kg</label>';
	        	}
	            $options .= '<select name="chemical_name[]" id="chemical_name[]" class="span4 chzn-select" data-placeholder="Select Chemical">';
	            $options .= '<option value="0">Select Chemical</option>';
	             foreach($items as $item){ 
	            	$options .=	'<option value="'. $item['id'] .'"';
	            	if($item['id'] == $q1[$i]['chemical_id']){ 
	            		$options .= 'selected';
	            	 } 
	            	$options .=	'> '. $item['name'] .'</option>';
	            	} 
	            $options .='</select>';
	            $options .='<input type="number" name="quantity[]" min="0" id="name" value="'.$q1[$i]['quantity'].'"  step="any" class="span3" placeholder="Quantity in Kg">';
	            $options .='<a href="javascript:void(0);"> Remove Chemical <i  class="icon-minus icon-white"></i></a></div></div>';
               }
               
        return $options;
	}
	

	public function get_join_selection_item_production_id($id)
	{

		$data = array();
		$this->db->select('production_selection_item.*, items.name as item_name, items.ac_id as item_ac_id');
		$this->db->from('production_selection_item');
		$this->db->join('items','production_selection_item.selection_item_id = items.id','left');
		$this->db->where('production_selection_item.production_id', $id);
		$this->db->where('production_selection_item.company_id', $this->session->userdata('user_company'));
		$q = $this->db->get()->result_array();
		return $q;
	}

	public function get_production_selection_itm($id)
	{

		$options = "";
		$pr = $this->MProduction->get_by_id($id);
        $q1 = $this->get_selection_item_production_id($id);
        if($pr['type'] == 'Raw'){
                $type = 'Wet Blue';
                }elseif($pr['type'] == 'Wet Blue'){
                    $type = 'FG';
                }
        $items = $this->MItems->get_all('active', $type);

            for($i=0; $i < sizeof($q1); $i++){

            	$options .= '<div class="description_text">';
            	$options .= '<div class="control-group" >';
            	
            	$options .= '<select name="selection_name[]" id="selection_name[]" class="span3 chzn-select" data-placeholder="Select selection name">';
            	$options .= '<option value="0">Selection Name</option>';
            		foreach($items as $item){ 
            			$total_price = $q1[$i]['price'] * $q1[$i]['sq_ft'];
            		$options .= '<option value="'. $item['id'].'"';
            		if($item['id'] == $q1[$i]['selection_item_id']){ 
	            		$options .= 'selected';
	            	 } 
            		$options .=	'> '. $item['name'] .'</option>';
            		}
            		$options .= '</select>';
            		$options .= '<input type="number" name="price[]" min="0" id="price" step="any" value="'.$q1[$i]['price'].'" class="span2 price" placeholder="Prices(Rs)">';
            		$options .= '<input type="number" name="quntit[]" min="0" step="any" id="quntit" value="'.$q1[$i]['quantity'].'" class="span2 quntit" placeholder="Quantity">';
            		$options .= '<input type="number" name="sq_ft[]" min="0" step="any" id="sq_ft" value="'.$q1[$i]['sq_ft'].'" class="span2 sq_ft" placeholder="Sq Ft">';
            		$options .= '<input type="number" name="total_price[]" min="0" step="any" id="total_price" value="'.$total_price.'" class="span2 total_price" placeholder="0" disabled>';
            		$options .= '<a href="javascript:void(0);"> Remove Selection <i  class="icon-minus icon-white"></i></a></div></div>';
               }
              
        return $options;
	}
	public function get_chemiclas_with_quantity($recipe_id, $raw_quantity)
	{

		$options = "";
		
        $q1 = $this->MRecipe->get_by_id_with_chemical($recipe_id);
        
        	$options .= '<input  class="span3" value="Chemicals" readonly>';
			$options .= '<input  class="span3" style="text-align:center;" value="QTY %" readonly>';
			$options .= '<input  class="span3" style="text-align:center;" value="Req Weight kg" readonly>';
			$options .= '<input  class="span3" style="text-align:center;" value="Avail Weight kg" readonly>';
			$options .= '<br>';
            for($i=0; $i < sizeof($q1); $i++){
            	$obtained_weight = ($q1[$i]['quantity'] * $raw_quantity)/100 ;
            	$stock = $this->MReports->get_chemical_stock_balance($q1[$i]['chemical_id']);

				$options .= '<input class="chemical_name span3" value="'.$q1[$i]['item_name'].'" placeholder="Chemical" readonly>';
				$options .= '<input  style="text-align:center;" id="chemical_qunatity_percent[]" value="'.$q1[$i]['quantity'].' %'.'" class="span3" placeholder="QTY %" readonly>';
				$options .= '<input  style="text-align:center;" class="req_chemical_qunatity_kg span3" value="'.$obtained_weight.' kg'.'" placeholder="Req Weight kg" readonly>';
				$options .= '<input  style="text-align:center;" class="avail_chemical_qunatity_kg span3" value="'.$stock['total_quantity'].'" placeholder="Avail Weight kg" readonly>';
				$options .= '<br>';
               }
        return $options;
	}

	public function get_latest()
	{
		$data = array();
		$this->db->where('company_id', $this->session->userdata('user_company'));
		$this->db->order_by('code', 'DESC');
		$this->db->limit(1);
		$q = $this->db->get('production');
		if ($q->num_rows() > 0)
		{
			foreach ($q->result_array() as $row)
			{
				$data = $row;
			}
		}

		$q->free_result();
		return $data;
	}

	public function get_all()
	{
		$data = array();
		$this->db->where('company_id', $this->session->userdata('user_company'));
		$q = $this->db->get('production');
		if ($q->num_rows() > 0)
		{
			foreach ($q->result_array() as $row)
			{
				$data[] = $row;
			}
		}

		$q->free_result();
		return $data;
	}

	public function get_all_with_join($id = NULL, $status = NULL, $start_date = NULL, $end_date = NULL, $type = NULL)
	{
		$data = array();
		$this->db->select('production.id,production.status,production.type as production_type, production.code,production.raw_weight, production.recipe as recipe_id, production.production_date, production.quantity, production.name as production_name, production.raw_quantity, production.raw_total_price,  items.name as raw_name,items.avco_price as raw_avco, recipe.name as recipe_name');
		$this->db->from('production');
		$this->db->join('items', 'production.raw = items.id', 'left');
		$this->db->join('recipe', 'production.recipe = recipe.id', 'left');
		if($type){
			$this->db->where('production.type', $type);
		}
		if($id){
			$this->db->where('production.id', $id);
		}
		if($status){
			$this->db->where($status);
		}
		if($start_date){
			$this->db->where('production.production_date >=', $start_date);
			$this->db->where('production.production_date <=', $end_date);
		}
		$this->db->where('production.company_id', $this->session->userdata('user_company'));
		$this->db->order_by("production.code", "DESC");
		$data = $this->db->get()->result_array();
		return $data;
	}

	public function get_avco($item_id)
    {
        $this->db->select('SUM(sq_ft) as total_quantity, SUM(total_price) as total_price');
        $this->db->where('selection_item_id', $item_id);
        $q = $this->db->get('production_selection_item');

        $avco = 0;
        if ($q->num_rows() > 0)
        {
            foreach ($q->result_array() as $row)
            {
                $data[] = $row;
            }
            if(($data[0]['total_price'] <> NULL || $data[0]['total_price'] <> 0) && ($data[0]['total_quantity'] <> NULL || $data[0]['total_quantity'] <> 0)){
            	
            	$temp = (double)$data[0]['total_price'] / (double)$data[0]['total_quantity'];

        	$avco = $temp;

            }
            
        }
        $q->free_result();

        return $avco;
    }
	public function create_selection_item()
	{


		$this->db->where('production_id', $this->input->post('id'));
		$this->db->delete('production_selection_item');

		$selection_name = $this->input->post('selection_name[]');
		$price = $this->input->post('price[]');
		$sq_ft = $this->input->post('sq_ft[]');
		$quntit = $this->input->post('quntit[]');
		$selection_date = $this->input->post('production_date');
		for ($i=0; $i < sizeof($selection_name); $i++) { 
			$data = array(
			'company_id' => $this->session->userdata('user_company'),
			'production_id' => $this->input->post('id'),
			'selection_item_id' => $selection_name[$i],
			'price' => $price[$i],
			'sq_ft' => $sq_ft[$i],
			'quantity' => $quntit[$i],
			'total_price' => round($sq_ft[$i] * $price[$i]),
			'selection_date' => date_to_db($selection_date),
			'created' => date('Y-m-d H:i:s', time()),
			'created_by' => $this->session->userdata('user_id')
			);

		$this->db->insert('production_selection_item', $data);
		$avco = $this->get_avco(trim($selection_name[$i]));
        $this->MItems->update_field($selection_name[$i], 'avco_price', $avco);
		}
		// journal entry
		$accounts_date = $this->MItems->get_dates();
        $stat_date = $accounts_date['start_date'];
        $en_date = $accounts_date['end_date'];
		$code = $this->input->post('code');
		$journal = $this->MAc_journal_master->get_by_doc('Production', $code,$stat_date, $en_date);
        if (count($journal) > 0)
        {
            $journal_no = $journal['journal_no'];
            $this->MAc_journal_details->delete_by_journal_no($journal_no,$stat_date, $en_date);
        }
        else
        {
            $journal_no = 'Production_'.$code;
            $this->MAc_journal_master->create_by_production($journal_no, $code);
        }
        
        // credit chemicals
		$recipe_chemical = $this->get_recipe_chemical_by_production_id2($this->input->post('id'));
				foreach ($recipe_chemical as $chemical) {
					$this->MAc_journal_details->create_by_inventory($journal_no, $chemical['item_ac_id'], NULL, $chemical['total_price'], $this->input->post('production_date'));	
				}
		// credit raw		
		$item = $this->MItems->get_by_id($this->input->post('raw_id'));
		$raw_cost = round($this->input->post('raw_qnt') * $item['avco_price']);
		$this->MAc_journal_details->create_by_inventory($journal_no, $item['ac_id'], NULL, $raw_cost, $this->input->post('production_date'));	
		// Debit production
		$details = $this->get_join_selection_item_production_id($this->input->post('id'));
		
            for($i = 0; $i<sizeof($details); $i++){
            	$debit += $details[$i]['total_price'];
                $this->MAc_journal_details->create_by_inventory($journal_no, $details[$i]['item_ac_id'], $details[$i]['total_price'], NULL, $this->input->post('production_date'));
            }

		
	}

	public function create()
	{

		$raw_item = $this->MItems->get_by_id($this->input->post('item_id'));
		$data = array(
			'company_id' => $this->session->userdata('user_company'),
			'code' => $this->input->post('code'),
			'quantity' => $this->input->post('quantit'),
			'name' => $this->input->post('name'),
			'type' => $this->input->post('type'),
			'raw' => $this->input->post('item_id'),
			'raw_quantity' => $this->input->post('raw_quantity'),
			'raw_weight' => $this->input->post('raw_weight'),
			'status' => 'Unapproved',
			'production_date' => date_to_db($this->input->post('production_date')),
			'recipe' => $this->input->post('recipe'),
			'raw_unit_price' => $raw_item['avco_price'],
			'raw_total_price' => round($raw_item['avco_price'] * $this->input->post('raw_quantity')),
			'created' => date('Y-m-d H:i:s', time()),
			'created_by' => $this->session->userdata('user_id')
			);

		$this->db->insert('production', $data);
		$insert_id = $this->db->insert_id();
		if($insert_id){

			$recipes = $this->MRecipe->get_by_id_with_chemical($this->input->post('recipe'));

			foreach ($recipes as $recipe) {
				$chemicl = $this->MItems->get_by_id($recipe['chemical_id']); 
				$obtained_weight = ($recipe['quantity'] * $this->input->post('raw_weight'))/100 ;
				$data2 = array(
				'company_id' => $this->session->userdata('user_company'),
				'production_id' => $insert_id,
				'recipe_id' => $recipe['id'],
				'recipe_name' => $recipe['recipe_name'],
				'chemical_id' => $recipe['chemical_id'],
				'chemical_name' => $recipe['item_name'],
				'qty_percent' => $recipe['quantity'],
				'qty_kg' => $obtained_weight,
				'unit_price' => $chemicl['avco_price'],
				'total_price' => round($chemicl['avco_price'] * $obtained_weight),
				'created' => date('Y-m-d H:i:s', time()),
				'created_by' => $this->session->userdata('user_id')
				);

				$this->db->insert('production_recipe_chemical', $data2);

			}

			$keys = $this->input->post('chemical_name[]');
			$values = $this->input->post('quantity[]');
			$output = array();

			$size = sizeof($keys);
			for ( $i = 0; $i < $size; $i++ ) {
			    if ( !isset($output[$keys[$i]]) ) {
			        $output[$keys[$i]] = array();
			    }
			    $output[$keys[$i]][] = $values[$i];
			}
			
			foreach ($output as $key => $value) {
				foreach ($value as $key1 => $value1) {
				$data1 = array(
				'company_id' => $this->session->userdata('user_company'),
				'production_id' => $insert_id,
				'chemical_id' => $key,
				'quantity' => $value1,
				'created' => date('Y-m-d H:i:s', time()),
				'created_by' => $this->session->userdata('user_id')
				);
			$this->db->insert('production_chemical', $data1);
			$chemicl = $this->MItems->get_by_id($key); 
		
			$data3 = array(
				'company_id' => $this->session->userdata('user_company'),
				'production_id' => $insert_id,
				'chemical_id' => $key,
				'qty_kg' => $value1,
				'unit_price' => $chemicl['avco_price'],
				'total_price' => round($chemicl['avco_price'] * $value1),
				'created' => date('Y-m-d H:i:s', time()),
				'created_by' => $this->session->userdata('user_id')
				);
				$this->db->insert('production_recipe_chemical', $data3);

			}
		}
	
	}
		return $insert_id;
		
	}

	public function update()
	{
		$this->db->where('production_id', $this->input->post('id'));
		$this->db->delete('production_chemical');

		$this->db->where('production_id', $this->input->post('id'));
		$this->db->delete('production_recipe_chemical');

		$raw_item = $this->MItems->get_by_id($this->input->post('item_id'));

		$data = array(
			'code' => $this->input->post('code'),
			'name' => $this->input->post('name'),
			'raw' => $this->input->post('item_id'),
			'quantity' => $this->input->post('quantit'),
			'type' => $this->input->post('type'),
			'raw_quantity' => $this->input->post('raw_quantity'),
			'raw_weight' => $this->input->post('raw_weight'),
			'production_date' => date_to_db($this->input->post('production_date')),
			'recipe' => $this->input->post('recipe'),
			'raw_unit_price' => $raw_item['avco_price'],
			'raw_total_price' => round($raw_item['avco_price'] * $this->input->post('raw_quantity')),
			'updated' => date('Y-m-d H:i:s', time()),
			'updated_by' => $this->session->userdata('user_id')
			);
		$this->db->where('id', $this->input->post('id'));
		if($this->db->update('production', $data)){
			
			$recipes = $this->MRecipe->get_by_id_with_chemical($this->input->post('recipe'));

			foreach ($recipes as $recipe) {
				$chemicl = $this->MItems->get_by_id($recipe['chemical_id']); 
				$obtained_weight = ($recipe['quantity'] * $this->input->post('raw_weight'))/100 ;
				$data2 = array(
				'company_id' => $this->session->userdata('user_company'),
				'production_id' => $this->input->post('id'),
				'recipe_id' => $recipe['id'],
				'recipe_name' => $recipe['recipe_name'],
				'chemical_id' => $recipe['chemical_id'],
				'chemical_name' => $recipe['item_name'],
				'qty_percent' => $recipe['quantity'],
				'qty_kg' => $obtained_weight,
				'unit_price' => $chemicl['avco_price'],
				'total_price' => round($chemicl['avco_price'] * $obtained_weight),
				'created' => date('Y-m-d H:i:s', time()),
				'created_by' => $this->session->userdata('user_id')
				);

				$this->db->insert('production_recipe_chemical', $data2);

			}
			$keys = $this->input->post('chemical_name[]');
			$values = $this->input->post('quantity[]');
			$output = array();

			$size = sizeof($keys);
			for ( $i = 0; $i < $size; $i++ ) {
			    if ( !isset($output[$keys[$i]]) ) {
			        $output[$keys[$i]] = array();
			    }
			    $output[$keys[$i]][] = $values[$i];
			}

			foreach ($output as $key => $value) {
				foreach ($value as $key1 => $value1) {
					$data1 = array(
						'company_id' => $this->session->userdata('user_company'),
						'production_id' => $this->input->post('id'),
						'chemical_id' => $key,
						'quantity' => $value1,
						'created' => date('Y-m-d H:i:s', time()),
						'created_by' => $this->session->userdata('user_id')
					);
					$this->db->insert('production_chemical', $data1);
					$chemicl = $this->MItems->get_by_id($key); 
					$data3 = array(
							'company_id' => $this->session->userdata('user_company'),
							'production_id' => $this->input->post('id'),
							'chemical_id' => $key,
							'qty_kg' => $value1,
							'unit_price' => $chemicl['avco_price'],
							'total_price' => round($chemicl['avco_price'] * $value1),
							'created' => date('Y-m-d H:i:s', time()),
							'created_by' => $this->session->userdata('user_id')
							);
							$this->db->insert('production_recipe_chemical', $data3);
					}
				}
		}

		
	}

	public function delete($id)
	{

		$production = $this->get_by_id($id);
		$accounts_date = $this->MItems->get_dates();
        $stat_date = $accounts_date['start_date'];
        $en_date = $accounts_date['end_date'];
		$journal = $this->MAc_journal_master->get_by_doc('Production', $production['code'], $stat_date, $en_date);
        if (count($journal) > 0)
        {
            $journal_no = $journal['journal_no'];
            $this->MAc_journal_details->delete_by_journal_no($journal_no,$stat_date, $en_date);
            $this->MAc_journal_master->delete_by_journal_no($journal_no,$stat_date, $en_date);
        }

		$this->db->where('recipe_id', $id);
		$this->db->delete('production_chemical');

		$this->db->where('production_id', $id);
		$this->db->delete('production_recipe_chemical');

		$this->db->where('production_id', $id);
		$this->db->delete('production_selection_item');

		$this->db->where('id', $id);
		$this->db->delete('production');
	}

	public function get_before_date($date)
    {
        $data = array();
        $this->db->where('company_id', $this->session->userdata('user_company'));
        $this->db->where('production_date <', date_to_db($date));
        $q = $this->db->get('production');
        if ($q->num_rows() > 0)
        {
            foreach ($q->result_array() as $row)
            {
                $data[] = $row;
            }
        }

        $q->free_result();
        return $data;
    }
    public function get_before_date_selection($date)
    {
        $data = array();
        $this->db->where('company_id', $this->session->userdata('user_company'));
        $this->db->where('selection_date <', date_to_db($date));
        $q = $this->db->get('production_selection_item');
        if ($q->num_rows() > 0)
        {
            foreach ($q->result_array() as $row)
            {
                $data[] = $row;
            }
        }

        $q->free_result();
        return $data;
    }
    public function get_by_date($date)
    {
        $data = array();
        $this->db->where('production_date', $date);
        $this->db->where('company_id', $this->session->userdata('user_company'));
        $q = $this->db->get('production');
        if ($q->num_rows() > 0)
        {
            foreach ($q->result_array() as $row)
            {
                $data[] = $row;
            }
        }

        $q->free_result();
        return $data;
    }
    public function get_by_date_selection($date)
    {
        $data = array();
        $this->db->where('selection_date', $date);
        $this->db->where('company_id', $this->session->userdata('user_company'));
        $q = $this->db->get('production_selection_item');
        if ($q->num_rows() > 0)
        {
            foreach ($q->result_array() as $row)
            {
                $data[] = $row;
            }
        }

        $q->free_result();
        return $data;
    }
    public function get_selection_item_production_id($id)
	{

		$data = array();
		$this->db->where('production_id', $id);
		$this->db->where('company_id', $this->session->userdata('user_company'));
		$q = $this->db->get('production_selection_item');
		if ($q->num_rows() > 0)
		{
			foreach ($q->result_array() as $row)
			{
				$data[] = $row;
			}
		}

		$q->free_result();
		return $data;
	}

}
