<?php

defined('BASEPATH') OR exit('No direct script access allowed');

class MPurchase_details extends CI_Model {

    public function __construct()
    {
        parent::__construct();
    }

    public function get_by_id($id)
    {
        $data = array();
        $this->db->where('id', $id);
        $this->db->where('company_id', $this->session->userdata('user_company'));
        $q = $this->db->get('purchase_details');
        if ($q->num_rows() > 0)
        {
            foreach ($q->result_array() as $row)
            {
                $item = $this->MItems->get_by_id($row['item_id']);
                $row['item_name'] = $item['name'];
                $data = $row;
            }
        }

        $q->free_result();
        return $data;
    }

    public function get_by_barcode($barcode)
    {
        $data = array();
        $this->db->where('barcode', $barcode);
        $this->db->where('company_id', $this->session->userdata('user_company'));
        $q = $this->db->get('purchase_details');
        if ($q->num_rows() > 0)
        {
            foreach ($q->result_array() as $row)
            {
                $data = $row;
            }
        }

        $q->free_result();
        return $data;
    }

    public function get_latest()
    {
        $data = array();
        $this->db->order_by('id', 'DESC');
        $this->db->where('company_id', $this->session->userdata('user_company'));
        $this->db->limit(1);
        $q = $this->db->get('purchase_details');
        if ($q->num_rows() > 0)
        {
            foreach ($q->result_array() as $row)
            {
                $data = $row;
            }
        }

        $q->free_result();
        return $data;
    }

    public function get_by_purchase_no($purchase_no = 0,$stat_date, $en_date)
    {
        $data = array();
        $this->db->where('purchase_no', $purchase_no);
        $this->db->where('purchase_date >= ', ($stat_date));
        $this->db->where('purchase_date <= ', ($en_date));
        $this->db->where('company_id', $this->session->userdata('user_company'));
        $this->db->order_by('id', 'DESC');
        $q = $this->db->get('purchase_details');
        if ($q->num_rows() > 0)
        {
            foreach ($q->result_array() as $row)
            {
                $item = $this->MItems->get_by_id($row['item_id']);
                $row['item_code'] = $item['code'];
                $row['item_name'] = $item['name'];
                $data[] = $row;
            }
        }

        $q->free_result();
        return $data;
    }

    public function get_total_quantity($purchase_no, $items = NULL, $stat_date, $en_date, $type)
    {
        $this->db->select_sum('quantity');
        $this->db->where('company_id', $this->session->userdata('user_company'));
        if ($items)
        {
            $this->db->where_in('item_id', $items);
        }
        if ($type)
        {
            $this->db->where_in('item_type', $type);
        }
        $this->db->where('purchase_date >= ', ($stat_date));
        $this->db->where('purchase_date <= ', ($en_date));
        $this->db->where('purchase_no', $purchase_no);
        $q = $this->db->get('purchase_details');
        if ($q->num_rows() > 0)
        {
            foreach ($q->result_array() as $row)
            {
                $data = $row['quantity'];
            }
        }

        $q->free_result();
        return $data;
    }

    public function get_total_price($purchase_no, $items = NULL,$stat_date, $en_date, $type)
    {
        $this->db->select_sum('total_price');
        if ($items)
        {
            $this->db->where_in('item_id', $items);
        }
        if ($type)
        {
            $this->db->where_in('item_type', $type);
        }
        $this->db->where('purchase_date >= ', ($stat_date));
        $this->db->where('purchase_date <= ', ($en_date));
        $this->db->where('company_id', $this->session->userdata( 'user_company' ) );
        $this->db->where('purchase_no', $purchase_no);
        $q = $this->db->get('purchase_details');
        if ($q->num_rows() > 0)
        {
            foreach ($q->result_array() as $row)
            {
                $data = $row['total_price'];
            }
        }

        $q->free_result();
        return $data;
    }
    public function get_total_sale_tax($purchase_no, $items = NULL,$stat_date, $en_date, $type)
    {
        $this->db->select_sum('vat_amount');
        if ($items)
        {
            $this->db->where_in('item_id', $items);
        }
        if ($type)
        {
            $this->db->where_in('item_type', $type);
        }
        $this->db->where('purchase_date >= ', ($stat_date));
        $this->db->where('purchase_date <= ', ($en_date));
        $this->db->where('company_id', $this->session->userdata( 'user_company' ) );
        $this->db->where('purchase_no', $purchase_no);
        $q = $this->db->get('purchase_details');
        if ($q->num_rows() > 0)
        {
            foreach ($q->result_array() as $row)
            {
                $data = $row['vat_amount'];
            }
        }

        $q->free_result();
        return $data;
    }

    public function get_all()
    {
        $data = array();
        $this->db->where( 'company_id', $this->session->userdata( 'user_company' ) );
        $q = $this->db->get('items');
        if ($q->num_rows() > 0)
        {
            foreach ($q->result_array() as $row)
            {
                $company = $this->MCompanies->get_by_id($row['company_id']);
                $row['company'] = $company['name'];
                $data[] = $row;
            }
        }

        $q->free_result();
        return $data;
    }

    public function get_by_item_id($item_id)
    {
        $data = array();
        $this->db->where('company_id', $this->session->userdata('user_company'));
        $this->db->where('item_id', $item_id);
        $q = $this->db->get('purchase_details');
        if ($q->num_rows() > 0)
        {
            foreach ($q->result_array() as $row)
            {
                $data = $row;
            }
        }

        return $data;
    }

    public function get_by_purchase_no_item_id($purchase_no, $item_id)
    {
        $data = null;
        $this->db->select('purchase.*, items.name as item_name, items.min_sale_price as item_sale');
        $this->db->from('purchase_details AS purchase');
        $this->db->join('items', 'items.id = purchase.item_id');
        $this->db->where('purchase.purchase_no', $purchase_no);
        $this->db->where('purchase.item_id', $item_id);
        $this->db->where( 'purchase.company_id', $this->session->userdata( 'user_company' ) );
        $q = $this->db->get();
        if ($q->num_rows() > 0)
        {
            foreach ($q->result_array() as $row)
            {
                $data = $row;
            }
        }

        $q->free_result();
        return $data;
    }

    public function get_by_item_with_purchase_no()
    {
        $data = array();
        $this->db->distinct();
        $this->db->select('sales_id');
        if ($this->input->post('item_id') != 'all')
        {
            $this->db->where('item_id', $this->input->post('item_id'));
        }
        $sdate = $this->input->post('s_date');
        $edate = $this->input->post('e_date');
        $this->db->where("edate BETWEEN '$sdate' AND '$edate'");
        $this->db->where( 'company_id', $this->session->userdata( 'user_company' ) );
        $this->db->order_by('id', 'DESC');
        $q = $this->db->get('sales_details');
        if ($q->num_rows() > 0)
        {
            foreach ($q->result_array() as $row)
            {
                $data[] = $this->MSales_master->get_by_id($row['sales_id']);
                //$data[] = $row;
            }
        }

        $q->free_result();
        return $data;
    }

    public function get_avco($item_id)
    {
        $this->db->select('SUM(sq_weight) as total_quantity, SUM(total_price) as total_price');
        $this->db->where('item_id', $item_id);
        $q = $this->db->get('purchase_details');
        if ($q->num_rows() > 0)
        {
            foreach ($q->result_array() as $row)
            {
                $data[] = $row;
            }
        }
        $temp = (double)$data[0]['total_price'] / (double)$data[0]['total_quantity'];
        $avco = $temp;
        $q->free_result();
        return $avco;
    }

    public function create($purchase_no)
    {
        $items = $this->MItems->get_by_id($this->input->post('item_id'));
        $total_price = round($this->input->post('sq_ft') * $this->input->post('price'));
        $vat_amount = ($total_price * $this->input->post('vat_percent'))/100;
        $data = array(
            'company_id' => $this->session->userdata('user_company'),
            'purchase_no' => $purchase_no,
            'item_id' => $this->input->post('item_id'),
            'item_type' => $this->input->post('type'),
            'ac_id' => $items['ac_id'],
            'quantity' => $this->input->post('quantity'),
            'sq_weight' => $this->input->post('sq_ft'),
            'unit' => $this->input->post('unit'),
            'vat_percent' => $this->input->post('vat_percent'),
            'vat_amount' => $vat_amount,
		    'purchase_date' => date_to_db($this->input->post('purchase_date')),
            'purchase_price' => $this->input->post('price'),
            'total_price' => $total_price,
            'created' => date('Y-m-d H:i:s', time()),
            'created_by' => $this->session->userdata('user_id'),
            );
        $this->db->insert('purchase_details', $data);
    }

    public function delete_by_purchase_no($purchase_no,$stat_date, $en_date)
    {
        $this->db->where('purchase_date >= ', ($stat_date));
        $this->db->where('purchase_date <= ', ($en_date));
        $this->db->where('purchase_no', $purchase_no);
        $this->db->delete('purchase_details');
    }

    public function delete($id)
    {
        $this->db->where('id', $id);
        $this->db->delete('purchase_details');
    }

    public function delete_by_cmp($cmp_id)
    {
        $this->db->where('company_id', $cmp_id);
        $this->db->delete('purchase_details');
    }

}