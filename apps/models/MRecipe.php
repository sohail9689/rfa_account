<?php

defined('BASEPATH') OR exit('No direct script access allowed');

class MRecipe extends CI_Model {

	public function __construct()
	{
		parent::__construct();
	}

	public function get_by_id($id)
	{
		$data = array();
		$this->db->where('id', $id);
		$this->db->where('company_id', $this->session->userdata('user_company'));
		$q = $this->db->get('recipe');
		if ($q->num_rows() > 0)
		{
			foreach ($q->result_array() as $row)
			{
				$data = $row;
			}
		}

		$q->free_result();
		return $data;
	}

	public function get_chemical_by_recipe_id($id)
	{

		$data = array();
		$this->db->where('recipe_id', $id);
		$this->db->where('company_id', $this->session->userdata('user_company'));
		$q = $this->db->get('recipe_chemical');
		if ($q->num_rows() > 0)
		{
			foreach ($q->result_array() as $row)
			{
				$data[] = $row;
			}
		}

		$q->free_result();
		return $data;
	}

public function get_chemiclas($id)
	{

		$options = "";
        $q1 = $this->get_chemical_by_recipe_id($id);
        $items = $this->MItems->get_all('', 'Chemical');
            for($i=1; $i<sizeof($q1); $i++){
            	$options .= '<div class="description_text">';
	            $options .= '<div class="control-group">';

	            $options .= '<select name="chemical_name[]" id="chemical_name[]" class="span4 chzn-select" data-placeholder="Select Chemical">';
	            $options .= '<option value="0">Select Item</option>';
	             foreach($items as $item){ 

	            	$options .=	'<option value="'. $item['id'] .'"';
	            	if($item['id'] == $q1[$i]['chemical_id']){ 
	            		$options .= 'selected';
	            	 } 
	            	$options .=	'> '. $item['name'] .'</option>';
	            	} 
	            $options .='</select>';
	            $options .='<input type="number" step="any" name="quantity[]" min="0" id="name" value="'.$q1[$i]['quantity'].'" class="span3" placeholder="Quantity">';
	            $options .='<a href="javascript:void(0);"> Remove Chemical <i  class="icon-minus icon-white"></i></a></div>';
	            $options .='</div>';
               }
               
        return $options;
	}
	// public function get_by_name($id)
	// {
	// 	$this->db->where('company_id', $this->session->userdata('user_company'));
	// 	$this->db->where('id', $id);
	// 	$q = $this->db->get('customers');
	// 	if ($q->num_rows() > 0)
	// 	{
	// 		foreach ($q->result_array() as $row)
	// 		{
	// 			$data = $row['full_name'];
	// 		}
	// 	}

	// 	$q->free_result();
	// 	return $data;
	// }

	public function get_latest()
	{
		$data = array();
		$this->db->where('company_id', $this->session->userdata('user_company'));
		$this->db->order_by('code', 'DESC');
		$this->db->limit(1);
		$q = $this->db->get('recipe');
		if ($q->num_rows() > 0)
		{
			foreach ($q->result_array() as $row)
			{
				$data = $row;
			}
		}

		$q->free_result();
		return $data;
	}

	// public function getCustomerNameBySalesID($id)
	// {
	// 	//$data = array();
	// 	$this->db->select('customers.id, customers.name');
	// 	$this->db->from('customers');
	// 	$this->db->join('sales_master', 'customers.id=sales_master.customer_id');
	// 	$this->db->where('sales_master.id', $id);
	// 	$this->db->where('customers.company_id', $this->session->userdata('user_company'));
	// 	$q = $this->db->get();
	// 	if ($q->num_rows() > 0)
	// 	{
	// 		foreach ($q->result_array() as $row)
	// 		{
	// 			$data = $row;
	// 		}
	// 	}

	// 	$q->free_result();
	// 	return $data;
	// }

	public function get_all($status = NULL)
	{
		$data = array();
		if($status != NULL){
			$this->db->where('status', $status);
		}
		$this->db->where('company_id', $this->session->userdata('user_company'));
		$this->db->order_by('code', 'DESC');
		$q = $this->db->get('recipe');
		if ($q->num_rows() > 0)
		{
			foreach ($q->result_array() as $row)
			{
				$data[] = $row;
			}
		}

		$q->free_result();
		return $data;
	}

	// public function get_code_name($id)
	// {
	// 	// var_dump($id);
	// 	// die;
	// 	$data = array();
	// 	$this->db->where('company_id', $this->session->userdata('user_company'));
	// 	$this->db->where('id', $id);
	// 	$q = $this->db->get('ac_charts');
	// 	if ($q->num_rows() > 0)
	// 	{
	// 		foreach ($q->result_array() as $row)
	// 		{
	// 			$data = $row['name'];
	// 		}
	// 	}

	// 	$q->free_result();
	// 	return $data;
	// }

	public function create()
	{

		// $chemical = $this->input->post('chemical_name[]');
		// $quantity = $this->input->post('quantity[]');
		// $c = array_combine($chemical, $quantity);
			$keys = $this->input->post('chemical_name[]');
		$values = $this->input->post('quantity[]');
		$output = array();

		$size = sizeof($keys);
		for ( $i = 0; $i < $size; $i++ ) {
		    if ( !isset($output[$keys[$i]]) ) {
		        $output[$keys[$i]] = array();
		    }
		    $output[$keys[$i]][] = $values[$i];
		}

		$data = array(
			'company_id' => $this->session->userdata('user_company'),
			'code' => $this->input->post('code'),
			// 'weight' => $this->input->post('weight'),
			'name' => $this->input->post('name'),
			'status' => $this->input->post('status'),
			'created' => date('Y-m-d H:i:s', time()),
			'created_by' => $this->session->userdata('user_id')
			);
		$this->db->insert('recipe', $data);

		$insert_id = $this->db->insert_id();
		if($insert_id){
			foreach ($output as $key => $value) {
				foreach ($value as $key1 => $value1) {
				$data1 = array(
				'company_id' => $this->session->userdata('user_company'),
				'recipe_id' => $insert_id,
				'chemical_id' => $key,
				'quantity' => $value1,
				'created' => date('Y-m-d H:i:s', time()),
				'created_by' => $this->session->userdata('user_id')
				);
			$this->db->insert('recipe_chemical', $data1);
			}
		}
		}
	}

	public function update()
	{
		$this->db->where('recipe_id', $this->input->post('id'));
		$this->db->delete('recipe_chemical');

		$keys = $this->input->post('chemical_name[]');
		$values = $this->input->post('quantity[]');
		$output = array();

		$size = sizeof($keys);
		for ( $i = 0; $i < $size; $i++ ) {
		    if ( !isset($output[$keys[$i]]) ) {
		        $output[$keys[$i]] = array();
		    }
		    $output[$keys[$i]][] = $values[$i];
		}
		
		// $c = array_combine($chemical, $quantity);
		$data = array(
			'code' => $this->input->post('code'),
			'name' => $this->input->post('name'),
			// 'weight' => $this->input->post('weight'),
			'status' => $this->input->post('status'),
			'updated' => date('Y-m-d H:i:s', time()),
			'updated_by' => $this->session->userdata('user_id')
			);
		$this->db->where('id', $this->input->post('id'));
		if($this->db->update('recipe', $data)){
			foreach ($output as $key => $value) {
				foreach ($value as $key1 => $value1) {
				$data1 = array(
				'company_id' => $this->session->userdata('user_company'),
				'recipe_id' => $this->input->post('id'),
				'chemical_id' => $key,
				'quantity' => $value1,
				'created' => date('Y-m-d H:i:s', time()),
				'created_by' => $this->session->userdata('user_id')
				);
			$this->db->insert('recipe_chemical', $data1);
		}
	}
		}

		
	}

	public function delete($id)
	{
		$this->db->where('recipe_id', $id);
		$this->db->delete('recipe_chemical');

		$this->db->where('id', $id);
		$this->db->delete('recipe');
	}

public function get_by_id_with_chemical($id)
	{

		$data = array();
		$this->db->select('recipe.id,recipe.weight, recipe.name as recipe_name,items.avco_price, items.code, items.name as item_name, recipe_chemical.quantity, recipe_chemical.chemical_id');
		$this->db->from('recipe');
		$this->db->where('recipe.id', $id);
		$this->db->where('recipe.company_id', $this->session->userdata('user_company'));
		$this->db->join("recipe_chemical", "recipe.id = recipe_chemical.recipe_id","left");
		$this->db->join("items" , "recipe_chemical.chemical_id = items.id" , "left");
		$q = $this->db->get()->result_array();
		
		return $q;
	}
	// public function delete_by_cmp($cmp_id)
	// {
	// 	$this->db->where('company_id', $cmp_id);
	// 	$this->db->delete('customers');
	// }

}
