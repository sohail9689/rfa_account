<?php

defined('BASEPATH') OR exit('No direct script access allowed');

class MSelection extends CI_Model {

	public function __construct()
	{
		parent::__construct();
	}

	public function get_by_id($id)
	{
		$data = array();
		$this->db->where('id', $id);
		$this->db->where('company_id', $this->session->userdata('user_company'));
		$q = $this->db->get('selection');
		if ($q->num_rows() > 0)
		{
			foreach ($q->result_array() as $row)
			{
				$data = $row;
			}
		}

		$q->free_result();
		return $data;
	}

	// public function get_by_name($id)
	// {
	// 	$this->db->where('company_id', $this->session->userdata('user_company'));
	// 	$this->db->where('id', $id);
	// 	$q = $this->db->get('customers');
	// 	if ($q->num_rows() > 0)
	// 	{
	// 		foreach ($q->result_array() as $row)
	// 		{
	// 			$data = $row['full_name'];
	// 		}
	// 	}

	// 	$q->free_result();
	// 	return $data;
	// }

	// public function get_latest()
	// {
	// 	$data = array();
	// 	$this->db->where('company_id', $this->session->userdata('user_company'));
	// 	$this->db->order_by('code', 'DESC');
	// 	$this->db->limit(1);
	// 	$q = $this->db->get('customers');
	// 	if ($q->num_rows() > 0)
	// 	{
	// 		foreach ($q->result_array() as $row)
	// 		{
	// 			$data = $row;
	// 		}
	// 	}

	// 	$q->free_result();
	// 	return $data;
	// }

	// public function getCustomerNameBySalesID($id)
	// {
	// 	//$data = array();
	// 	$this->db->select('customers.id, customers.name');
	// 	$this->db->from('customers');
	// 	$this->db->join('sales_master', 'customers.id=sales_master.customer_id');
	// 	$this->db->where('sales_master.id', $id);
	// 	$this->db->where('customers.company_id', $this->session->userdata('user_company'));
	// 	$q = $this->db->get();
	// 	if ($q->num_rows() > 0)
	// 	{
	// 		foreach ($q->result_array() as $row)
	// 		{
	// 			$data = $row;
	// 		}
	// 	}

	// 	$q->free_result();
	// 	return $data;
	// }

	public function get_all($status = NULL, $type = NULL)
	{

		$data = array();
		if($status != NULL){
			$this->db->where('status', $status);
		}
		if($type != NULL){
			$this->db->where('type', $type);
		}
		$this->db->where('company_id', $this->session->userdata('user_company'));
		$q = $this->db->get('selection');
		if ($q->num_rows() > 0)
		{
			foreach ($q->result_array() as $row)
			{
				$data[] = $row;
			}
		}

		$q->free_result();
		return $data;
	}

	// public function get_code_name($id)
	// {
	// 	// var_dump($id);
	// 	// die;
	// 	$data = array();
	// 	$this->db->where('company_id', $this->session->userdata('user_company'));
	// 	$this->db->where('id', $id);
	// 	$q = $this->db->get('ac_charts');
	// 	if ($q->num_rows() > 0)
	// 	{
	// 		foreach ($q->result_array() as $row)
	// 		{
	// 			$data = $row['name'];
	// 		}
	// 	}

	// 	$q->free_result();
	// 	return $data;
	// }

	public function create($code, $ac_id)
	{
		$data = array(
			'company_id' => $this->session->userdata('user_company'),
			'type' => $this->input->post('type'),
			'price' => $this->input->post('price'),
			'name' => $this->input->post('name'),
			'status' => $this->input->post('status'),
			'created' => date('Y-m-d H:i:s', time()),
			'created_by' => $this->session->userdata('user_id')
			);
		$this->db->insert('selection', $data);
	}

	public function update()
	{
		$data = array(
			'type' => $this->input->post('type'),
			'name' => $this->input->post('name'),
			'status' => $this->input->post('status')
			);

		$this->db->where('id', $this->input->post('id'));
		$this->db->update('selection', $data);
	}

	public function delete($id)
	{
		$this->db->where('id', $id);
		$this->db->delete('selection');

	}

	// public function delete_by_cmp($cmp_id)
	// {
	// 	$this->db->where('company_id', $cmp_id);
	// 	$this->db->delete('customers');
	// }

}
