<div id="main-content">
	<!-- BEGIN PAGE CONTAINER-->
	<div class="container-fluid">
		<!-- BEGIN PAGE HEADER-->   
		<div class="row-fluid">
			<div class="span12">
				<!-- BEGIN PAGE TITLE & BREADCRUMB-->
				<h3 class="page-title">
					Cash/Bank Received Voucher
				</h3>
				<ul class="breadcrumb">
					<li>
						<a href="dashboard">Dashboard</a>
						<span class="divider">/</span>
					</li>
					<li>
						<a href="accounts">Accounts</a>
						<span class="divider">/</span>
					</li>
					<li class="active">
						Cash/Bank Received Voucher List
					</li>
				</ul>
				<!-- END PAGE TITLE & BREADCRUMB-->
			</div>
		</div>
		<!-- END PAGE HEADER-->

		<!-- BEGIN Alert widget-->
		<?php if($this->session->flashdata('success') || $this->session->flashdata('info') || $this->session->flashdata('error')) { ?>
		<div class="row-fluid">
			<div class="span12">
				<?php if($this->session->flashdata('success')) { ?>
				<div class="alert alert-success">
					<button class="close" data-dismiss="alert">×</button>
					<strong>Success!</strong> <?php echo $this->session->flashdata('success'); ?>
				</div>
				<?php } ?>
				<?php if($this->session->flashdata('info')) { ?>
				<div class="alert alert-info">
					<button class="close" data-dismiss="alert">×</button>
					<strong>Info!</strong> <?php echo $this->session->flashdata('info'); ?>
				</div>
				<?php } ?>
				<?php if($this->session->flashdata('error')) { ?>
				<div class="alert alert-error">
					<button class="close" data-dismiss="alert">×</button>
					<strong>Error!</strong> <?php echo $this->session->flashdata('error'); ?>
				</div>
				<?php } ?>
			</div>
		</div>
		<?php } ?>
		<div class="row-fluid right">
			<a class="btn btn-primary" href="accounts/mr_save"><i class="icon-plus icon-white"></i> Add New</a>
		</div>
		<br>
		<!-- END Alert widget-->

		<!-- BEGIN ADVANCED TABLE widget-->
		<div class="row-fluid">
			<div class="span12">
				<!-- BEGIN EXAMPLE TABLE widget-->
				<div class="widget blue">
					<div class="widget-title">
						<h4><i class="icon-reorder"></i> List All</h4>
						<span class="tools">
							<a href="javascript:;" class="icon-chevron-down"></a>
						</span>
					</div>
					<div class="widget-body">
						<table class="table table-striped table-bordered" id="sample_1">
							<thead>
								<tr align="left" style="font-family: Arial; text-decoration: none;">
									<th>MR No</th>
									<th class="center span3">MR Date</th>
									<th class="center">Amount</th>
									<th class="center">Ref. Employee</th>
									<th class="center span3">Action</th>
								</tr>
							</thead>
							<tbody>
								<?php
								$i = 0;
								foreach ($mrs as $key => $value) {
									?>
									<tr>
										<td><a href="accounts/mr_preview/<?php echo $value['id']; ?>" target="_blank"><strong><?php echo $value['mr_no']; ?></strong></a></td>
										<td class="right"><?php echo date('jS F Y ', strtotime($value['mr_date'])); ?></td>
										<td class="right"><?php echo number_format($value['amount'], 2); ?></td>
										<td class="right"><?php echo $value['emp_name']; ?></td>
										<td class="center">
											<a class="btn btn-edit" href="accounts/mr_save/<?php echo $value['id']; ?>"><i class="icon-edit icon-white"></i> Edit</a>
											<a class="btn del btn-danger" href="accounts/mr_delete/<?php echo $value['id']; ?>"><i class="icon-trash icon-white"></i> Delete</a>
										</td>
									</tr>
									<?php
								}
								?>
							</tbody>
						</table>
					</div>
				</div>
				<!-- END EXAMPLE TABLE widget-->
			</div>
		</div>

		<!-- END ADVANCED TABLE widget-->
	</div>
	<!-- END PAGE CONTAINER-->
</div>