<!DOCTYPE html>
<!--[if IE 8]> <html lang="en" class="ie8"> <![endif]-->
<!--[if IE 9]> <html lang="en" class="ie9"> <![endif]-->
<!--[if !IE]><!--> <html lang="en"> <!--<![endif]-->
<!-- BEGIN HEAD -->
<head>
	<?php $this->load->view('admin/head'); ?>
</head>
<!-- END HEAD -->

<!-- BEGIN BODY -->
<body>

	<!-- BEGIN CONTAINER -->
	<div id="container" class="row-fluid">


		<!-- BEGIN PAGE -->
		<div class="row-fluid">
			<div class="span12">
				<!-- BEGIN BLANK PAGE PORTLET-->
				<div class="widget grey">
					<div class="widget-body">
						<div class="row-fluid" >
							<div class="span12" >
								<div class="pull-right">
                                    <img src="<?php echo $this->session->userdata('company_logo'); ?>" width="115" class="img">
                                </div>
                                <!-- <h3 class="center">Bank Payment Voucher</h3> -->
                                <h2 class="center"><?php echo $details['payment_type'].'  Recevied Voucher'; ?></h2>
                                <h3 class="center">

                                <?php echo $this->session->userdata('company_name'); ?></h3>
                                <hr>
							</div>
						</div>
						<div class="row-fluid invoice-list">
							<div class="span5 pull-left">
								<h4>Voucher # <?php echo $details['mr_no']; ?></h4>
								<h4>Date: <?php echo date('jS F Y ', strtotime($details['mr_date'])); ?></h4>
							</div>
							<div class="span2">
								&nbsp;
							</div>
							<div class="span5 pull-left">
								<h4>Customer Name: <?php echo $customer['name']; ?></h4>
								<h4>Mobile : <?php echo $customer['mobile']; ?></h4>
								<h4>Email : <?php echo $customer['email']; ?></h4>
								<h4>Ref. Employee : <?php if( isset($emp['name']) ){ echo $emp['name']; } ?></h4>
							</div>
						</div>
						<div class="space20"></div> 
						<div class="row-fluid" style="height: 1000%">
							<div class="invoice-table">
								<fieldset>
									<div class="span12">
										<div id="debit_details">
											<table class="table table-bordered table-striped responsive">
												<thead>
													<tr>
														<th class="center">Details</th>
														<th class="center">Memo</th>
														<th class="center">Debit</th>
														<th class="center">Credit</th>
														<th class="center">Amount</th>
													</tr>
												</thead>
												<tbody>
													<tr>
														<td>Receive from Customer</td>
														<td width="400px";>
													
													<?php echo $details['memo']; ?></td>
														
														<td><?php echo $this->session->userdata('company_name'); ?></td>
														<td><?php echo $customer['name']; ?></td>
														<td class="right"><?php echo number_format($details['amount'], 2); ?></td>
													</tr>
												</tbody>
												<tfoot>
													<tr>
													
													</th>
														<th>Grand Total</th>
														<th class=" right" colspan="4"><?php if ($this->session->userdata('currency_symbol_position') == 'Before') { echo $this->session->userdata('currency_symbol'); } ?> <?php echo number_format($details['amount'], 2); ?> <?php if ($this->session->userdata('currency_symbol_position') == 'After') { echo $this->session->userdata('currency_symbol'); } ?></th>
													</tr>
												</tfoot>
											</table>
										</div>
									</div>
								</fieldset>
							</div>
						</div>
						<div class="col-lg-12 container" style="margin-top:30px";>
                        <div class="row">
                        <div class="pull-left">
                        <b>Prepared By____________________</b>
                        </div>
                        <div class="center" style="margin-right:220px";>
                        <b>checked  By____________________</b>
                        </div>
                        <div class="pull-right" style="margin-top:-20px";>
                        <b>Approved By____________________</b>
                        </div>
                        </div>
                        </div>
						<div class="space20"></div>
						<div class="row-fluid text-center">
							<a class="btn btn-inverse btn-large hidden-print" onclick="javascript:window.print();">Print <i class="icon-print icon-big"></i></a>
						</div>
					</div>
				</div>
				<!-- END BLANK PAGE PORTLET-->
			</div>
		</div>
		<!-- END PAGE -->
	</div>
	<!-- END CONTAINER -->

	<!-- BEGIN FOOTER -->
	<?php $this->load->view('admin/footer'); ?>
	<!-- END FOOTER -->

	<!-- BEGIN JAVASCRIPTS -->
	<?php $this->load->view('admin/js'); ?>
	<!-- END JAVASCRIPTS -->
</body>
<!-- END BODY -->

</html>