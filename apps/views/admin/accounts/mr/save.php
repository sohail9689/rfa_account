<div id="main-content">
	<!-- BEGIN PAGE CONTAINER-->
	<div class="container-fluid">
		<!-- BEGIN PAGE HEADER-->
		<div class="row-fluid">
			<div class="span12">
				<!-- BEGIN PAGE TITLE & BREADCRUMB-->
				<h3 class="page-title">
					Cash/Bank Received Voucher
				</h3>
				<ul class="breadcrumb">
					<li>
						<a href="dashboard">Dashboard</a>
						<span class="divider">/</span>
					</li>
					<li>
						<a href="accounts">Accounts</a>
						<span class="divider">/</span>
					</li>
                     <li>
                        <a href="accounts/mr_list">Recived List</a>
                        <span class="divider">/</span>
                    </li>
					<li class="active">
						<?php if (count($mr) > 0) { ?>Edit<?php } else { ?>Add New<?php } ?> Cash/Bank Received Voucher
					</li>
				</ul>
				<!-- END PAGE TITLE & BREADCRUMB-->
			</div>
		</div>
		<!-- END PAGE HEADER-->

		<!-- BEGIN PAGE CONTENT-->
		<div class="row-fluid">
			<div class="span12">
				<!-- BEGIN SAMPLE FORM PORTLET-->
				<div class="widget blue">
					<div class="widget-title">
						<h4><i class="icon-reorder"></i> Entry Form </h4>
						<span class="tools">
							<a href="javascript:;" class="icon-chevron-down"></a>
						</span>
					</div>
					<div class="widget-body">
						<!-- BEGIN FORM-->
						<form class="form-horizontal" id="form-validate" action="accounts/mr_save" method="post">
                            <fieldset>
                                <div class="control-group">
                                    <label class="control-label" for="mr_no">Voucher No</label>
                                    <div class="controls">
                                        <input type="text" name="mr_no" id="mr_no" class="span5" value="<?php if(count($mr) > 0){ echo $mr['mr_no']; }else{ echo $mr_no; } ?>" data-validate="{required: true, messages:{required:'Please enter field required'}}" />
                                    </div>
                                </div>
                                <div class="control-group">
                                    <label class="control-label" for="mr_date">Voucher Date</label>
                                    <div class="controls">
                                        <div class="input-append date" data-form="datepicker" data-date="<?php echo date('d/m/Y'); ?>" data-date-format="dd/mm/yyyy">
                                            <input name="mr_date" id="mr_date" data-form="datepicker" size="16" type="text" value="<?php if(count($mr) > 0){ echo date_to_ui($mr['mr_date']); } else { echo date('d/m/Y'); } ?>">
                                            <span class="add-on"><i class="icon-th"></i></span>
                                        </div>
                                    </div>
                                </div>
                                <div class="control-group">
                                    <label class="control-label" for="customer_id">Select Customer</label>
                                    <div class="controls">
                                        <select name="customer_id" id="customer_id" class="span5 chzn-select">
                                            <?php foreach ($customers as $customer){ ?>
                                            <option value="<?php echo $customer['id']; ?>" <?php if( count($mr) > 0 ){ if( $mr['customer_id'] == $customer['id'] ){ echo 'selected'; } } ?>><?php echo $customer['name']; ?></option>
                                            <?php } ?>
                                        </select>
                                    </div>
                                </div>
                                <div class="control-group">
                                    <label class="control-label" for="amount">Amount</label>
                                    <div class="controls">
                                        <input type="text" name="amount" id="amount" class="span5" value="<?php if(count($mr) > 0){ echo $mr['amount']; } ?>" placeholder="00.00" />
                                    </div>
                                </div>
                                <div class="control-group">
                                    <label class="control-label" for="payment_type">Payment Type</label>
                                    <div class="controls">
                                        <select name="payment_type" id="payment_type" class="span5 chzn-select">
                                            <option value="Cash" <?php if( count($mr) > 0 ){ if( $mr['payment_type'] == 'Cash' ){ echo 'selected'; } } ?>>Cash</option>
                                            <option value="Bank" <?php if( count($mr) > 0 ){ if( $mr['payment_type'] == 'Bank' ){ echo 'selected'; } } ?>>Bank</option>
                                        </select>
                                    </div>
                                </div>
                                <div class="control-group">
                                    <label class="control-label" for="emp_id">Ref. Employee</label>
                                    <div class="controls">
                                        <select name="emp_id" id="emp_id" class="span5 chzn-select">
                                            <?php foreach ($emps as $emp){ ?>
                                            <option value="<?php echo $emp['id']; ?>" <?php if( count($mr) > 0 ){ if( $mr['emp_id'] == $emp['id'] ){ echo 'selected'; } } ?>><?php echo $emp['name']; ?></option>
                                            <?php } ?>
                                        </select>
                                    </div>
                                </div>
                                <div class="control-group">
                                    <label class="control-label" for="memo">Memo</label>
                                    <div class="controls">
                                        <textarea name="memo" id="memo" class="span5" rows="5" data-form="wysihtml5" placeholder="Memo"><?php if(count($mr) > 0){ echo $mr['memo']; } ?></textarea>
                                    </div>
                                </div>
                                <input type="hidden" name="<?php echo $this->security->get_csrf_token_name(); ?>" value="<?php echo $this->security->get_csrf_hash(); ?>" />
                                <?php if(count($mr) > 0){ ?>
                                <input type="hidden" name="id" value="<?php echo $mr['id']; ?>" />
                                <?php } ?>
                                <div class="form-actions">
                                    <input type="submit" class="btn btn-success" value="Save changes" />
                                </div>
                            </fieldset>
                        </form>
                        <!-- END FORM-->
                    </div>
                </div>
                <!-- END SAMPLE FORM PORTLET-->
            </div>
        </div>

        <!-- END PAGE CONTAINER-->
    </div>
    <!-- END PAGE -->
</div>

<script>
  $('#mr_no').focus();
</script>