<div id="main-content">
	<!-- BEGIN PAGE CONTAINER-->
	<div class="container-fluid">
		<!-- BEGIN PAGE HEADER-->
		<div class="row-fluid">
			<div class="span12">
				<!-- BEGIN PAGE TITLE & BREADCRUMB-->
				<h3 class="page-title">
					Cash/Bank Payment Voucher
				</h3>
				<ul class="breadcrumb">
					<li>
						<a href="dashboard">Dashboard</a>
						<span class="divider">/</span>
					</li>
					<li>
						<a href="accounts">Accounts</a>
						<span class="divider">/</span>
					</li>
                    <li>
                        <a href="accounts/payment_list">Payment List</a>
                        <span class="divider">/</span>
                    </li>
					<li class="active">
						<?php if (count($payment) > 0) { ?>Edit<?php } else { ?>Add New<?php } ?> Cash/Bank Payment Voucher
					</li>
				</ul>
				<!-- END PAGE TITLE & BREADCRUMB-->
			</div>
		</div>
		<!-- END PAGE HEADER-->

		<!-- BEGIN PAGE CONTENT-->
		<div class="row-fluid">
			<div class="span12">
				<!-- BEGIN SAMPLE FORM PORTLET-->
				<div class="widget blue">
					<div class="widget-title">
						<h4><i class="icon-reorder"></i> Entry Form </h4>
						<span class="tools">
							<a href="javascript:;" class="icon-chevron-down"></a>
						</span>
					</div>
					<div class="widget-body">
						<!-- BEGIN FORM-->
						<form class="form-horizontal" id="form-validate" action="accounts/payment_save" method="post">
                            <fieldset>
                                <div class="control-group">
                                    <label class="control-label" for="payment_no">Voucher No</label>
                                    <div class="controls">
                                        <input type="text" name="payment_no" id="payment_no" class="span5" value="<?php if(count($payment) > 0){ echo $payment['payment_no']; }else{ echo $payment_no; } ?>" />
                                    </div>
                                </div>
                                <div class="control-group">
                                    <label class="control-label" for="payment_date">Voucher Date</label>
                                    <div class="controls">
                                        <div class="input-append date" data-form="datepicker" data-date="<?php echo date('d/m/Y'); ?>" data-date-format="dd/mm/yyyy">
                                            <input name="payment_date" id="payment_date" data-form="datepicker" size="16" type="text" value="<?php if(count($payment) > 0){ echo date_to_ui($payment['payment_date']); } else { echo date('d/m/Y'); } ?>">
                                            <span class="add-on"><i class="icon-th"></i></span>
                                        </div>
                                    </div>
                                </div>
                                <div class="control-group">
                                    <label class="control-label" for="supplier_id">Select Supplier</label>
                                    <div class="controls">
                                        <select name="supplier_id" id="customer_id" class="span5 chzn-select" data-placeholder="Select Supplier">
                                            <?php foreach ($suppliers as $supplier){ ?>
                                            <option value="<?php echo $supplier['id']; ?>" <?php if( count($payment) > 0 ){ if( $payment['supplier_id'] == $supplier['id'] ){ echo 'selected'; } } ?>><?php echo $supplier['name']; ?></option>
                                            <?php } ?>
                                        </select>
                                    </div>
                                </div>
                                <div class="control-group">
                                    <label class="control-label" for="amount">Amount</label>
                                    <div class="controls">
                                        <input type="text" name="amount" id="amount" class="span5" value="<?php if(count($payment) > 0){ echo $payment['amount']; } ?>" placeholder="00.00" />
                                    </div>
                                </div>
                                <div class="control-group">
                                    <label class="control-label" for="payment_type">Payment Type</label>
                                    <div class="controls">
                                        <select name="payment_type" id="payment_type" class="span5 chzn-select">
                                            <option value="Cash" <?php if( count($payment) > 0 ){ if( $payment['payment_type'] == 'Cash' ){ echo 'selected'; } } ?>>Cash</option>
                                            <option value="Bank" <?php if( count($payment) > 0 ){ if( $payment['payment_type'] == 'Bank' ){ echo 'selected'; } } ?>>Bank</option>
                                          <!--   <option value="Payorder" <?php if( count($payment) > 0 ){ if( $payment['payment_type'] == 'Payorder' ){ echo 'selected'; } } ?>>Payorder</option> -->
                                        </select>
                                    </div>
                                </div>
                                <!-- <div class="control-group">
                                    <label class="control-label" for="emp_id">Ref. Employee</label>
                                    <div class="controls">
                                        <select name="emp_id" id="emp_id" class="span5 chzn-select" data-placeholder="Ref. Employee">
                                            <?php foreach ($emps as $emp){ ?>
                                            <option value="<?php echo $emp['id']; ?>" <?php if( count($payment) > 0 ){ if( $payment['emp_id'] == $emp['id'] ){ echo 'selected'; } } ?>><?php echo $emp['name']; ?></option>
                                            <?php } ?>
                                        </select>
                                    </div>
                                </div> -->
                                <div class="control-group">
                                    <label class="control-label" for="memo">Memo</label>
                                    <div class="controls">
                                        <textarea name="memo" id="memo" class="span5" rows="5" data-form="wysihtml5" placeholder="Memo"><?php if(count($payment) > 0){ echo $payment['memo']; } ?></textarea>
                                    </div>
                                </div>
                                <input type="hidden" name="<?php echo $this->security->get_csrf_token_name(); ?>" value="<?php echo $this->security->get_csrf_hash(); ?>" />
                                <?php if(count($payment) > 0){ ?>
                                <input type="hidden" name="id" value="<?php echo $payment['id']; ?>" />
                                <?php } ?>
                                <div class="form-actions">
                                    <input type="submit" class="btn btn-success" value="Save changes" />
                                </div>
                            </fieldset>
                        </form>
                        <!-- END FORM-->
                    </div>
                </div>
                <!-- END SAMPLE FORM PORTLET-->
            </div>
        </div>

        <!-- END PAGE CONTAINER-->
    </div>
    <!-- END PAGE -->
</div>

<script>
  $('#payment_no').focus();
</script>