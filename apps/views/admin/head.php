<base href="<?php echo base_url(); ?>" />
<meta charset="utf-8" />
<title><?php echo $this->session->userdata('company_name'); ?></title>
<meta name="viewport" content="width=device-width, initial-scale=1.0" />
<meta name="description" content="" />
<meta name="author" content="Cybex Tech" />
<link rel="shortcut icon" href="<?php echo $this->session->userdata('company_logo'); ?>" type="image/x-icon" />

<link href="assets/backend/assets/bootstrap/css/bootstrap.min.css" rel="stylesheet" />
<link href="assets/backend/assets/bootstrap/css/bootstrap-responsive.min.css" rel="stylesheet" />
<link href="assets/backend/assets/bootstrap/css/bootstrap-fileupload.css" rel="stylesheet" />

<link href="assets/backend/assets/font-awesome/css/font-awesome.css" rel="stylesheet" />
<link href="assets/backend/css/style.css" rel="stylesheet" />
<link href="assets/backend/css/style-responsive.css" rel="stylesheet" />
<link href="assets/backend/css/style-default.css" rel="stylesheet" id="style_color" />
<link href="assets/backend/assets/fullcalendar/fullcalendar/bootstrap-fullcalendar.css" rel="stylesheet" />

<link href="assets/plugin/uniform/themes/default/css/uniform.default.css" rel="stylesheet" type="text/css" />

<!--<link href="assets/plugin/chosen_v1.4.2/chosen.min.css" rel="stylesheet" type="text/css" />-->
<link href="assets/plugin/select2_v4.0.0/css/select2.min.css" rel="stylesheet" type="text/css" />

<link href="assets/backend/assets/datepicker/datepicker.css" rel="stylesheet">

<link href="assets/plugin/bootstrap-table.min.css" rel="stylesheet">
<link href="assets/plugin/bootstrap-table.css" rel="stylesheet">
<link rel="stylesheet" type="text/css" href="assets/plugin/bootstrap-datepicker-master/dist/css/bootstrap-datepicker.css" />

<script src="assets/js/jquery-1.11.1.min.js"></script>
<script src="//cdnjs.cloudflare.com/ajax/libs/jquery-form-validator/2.1.47/jquery.form-validator.min.js"></script>


<script type="text/javascript" src="assets/plugin/bootstrap-datepicker-master/js/bootstrap-datepicker.js"></script>
<script type="text/javascript" src="assets/plugin/filter_column.js"></script>
<!-- <script src="assets/plugin/bootstrap-table-export.js"></script> -->
<script type="text/javascript" src="assets/plugin/tableExport.js"></script>
<!-- <script src="assets/plugin/bootstrap-table.js"></script> -->