<div id="header" class="navbar navbar-inverse navbar-fixed-top">
	<!-- BEGIN TOP NAVIGATION BAR -->
	<div class="navbar-inner">
		<div class="container-fluid">
			<!--BEGIN SIDEBAR TOGGLE-->
<?php if($error = $this->session->flashdata('user_privileges')):
    ?>
<div class="row">
  <div class="col-lg-12">
    <div class="alert alert-dismissible alert-danger center" style="width: 100%; height: 43px">
<h4 style="margin-top: 10px">
<?= $error ?>
</h4>
</div>
</div>
</div>
<?php endif?>

<?php if(!$error):
?>
			<div class="sidebar-toggle-box hidden-phone">

				<div class="icon-reorder tooltips" data-placement="right" data-original-title="Toggle Navigation"></div>
			</div>
<?php endif?>
			<!--END SIDEBAR TOGGLE-->
			<!-- BEGIN LOGO -->
			<?php if(!$error):
?>
			
			<!-- END LOGO -->
			<!-- BEGIN RESPONSIVE MENU TOGGLER -->
			<a class="btn btn-navbar collapsed" id="main_menu_trigger" data-toggle="collapse" data-target=".nav-collapse">
				<span class="icon-bar"></span>
				<span class="icon-bar"></span>
				<span class="icon-bar"></span>
				<span class="arrow"></span>
			</a>

			<div class="top-nav ">
			<ul class="nav pull-left top-menu" style="margin-top: 10px;">
				<li >
					<b><?php $accounts_date = $this->MItems->get_dates();
						$stat_date = $accounts_date['start_date'];
						$en_date = $accounts_date['end_date'];
						echo $stat_date.'<br>';
						echo $en_date;
						?></b>
						
					</li>
			</ul>
				<ul class="nav pull-right top-menu" >
					<!-- BEGIN SUPPORT -->
					<li class="dropdown mtop5">
						<a class="dropdown-toggle element" data-placement="bottom" data-toggle="tooltip" href="http://cybextech.com/contact/" target="_blank" data-original-title="Help">
							<i class="icon-question-sign"></i>
						</a>
					</li>
					<!-- END SUPPORT -->
					<!-- BEGIN USER LOGIN DROPDOWN -->
					<li class="dropdown">
						<a href="javascript:;" class="dropdown-toggle" data-toggle="dropdown">
                            
                            <span class="username"><?php echo $this->session->userdata('user_name'); ?></span>
							<b class="caret"></b>
							
							<img src="<?php echo $this->session->userdata('company_logo'); ?>"   width="25">
							<!-- <a class="brand" href="dashboard"><img src="" alt="POS Logo" /></a> -->
						
						</a>
						<ul class="dropdown-menu extended logout">

							<li><a href="user/profile_view"><i class="icon-user"></i> My Profile</a></li>
							<li><a href="login/logout"><i class="icon-key"></i> Log Out</a></li>
						</ul>
					</li>
					

					<!-- END USER LOGIN DROPDOWN -->
				</ul>
				<!-- END TOadminlogo. NAVIGATION MENU -->
			</div>
		<?php endif?>

		</div>
	</div>
	<!-- END TOP NAVIGATION BAR -->
</div>