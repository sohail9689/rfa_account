<div id="main-content">
	<!-- BEGIN PAGE CONTAINER-->
	<div class="container-fluid">
		<!-- BEGIN PAGE HEADER-->
		<div class="row-fluid">
			<div class="span12">
				<!-- BEGIN PAGE TITLE & BREADCRUMB-->
				<h3 class="page-title">
					Employee
				</h3>
				<ul class="breadcrumb">
					<li>
						<a href="dashboard">Dashboard</a>
						<span class="divider">/</span>
					</li>
					<li>
						<a href="hr">H R</a>
						<span class="divider">/</span>
					</li>
					<li class="active">
						List All
					</li>
				</ul>
				<!-- END PAGE TITLE & BREADCRUMB-->
			</div>
		</div>
		<!-- END PAGE HEADER-->
		<?php if($this->session->flashdata('success') || $this->session->flashdata('info') || $this->session->flashdata('error')) { ?>
		<!-- BEGIN Alert widget-->
		<div class="row-fluid">
			<div class="span12">
				<?php if($this->session->flashdata('success')) { ?>
				<div class="alert alert-success">
					<button class="close" data-dismiss="alert">×</button>
					<strong>Success!</strong> <?php echo $this->session->flashdata('success'); ?>
				</div>
				<?php } ?>
				<?php if($this->session->flashdata('info')) { ?>
				<div class="alert alert-info">
					<button class="close" data-dismiss="alert">×</button>
					<strong>Info!</strong> <?php echo $this->session->flashdata('info'); ?>
				</div>
				<?php } ?>
				<?php if($this->session->flashdata('error')) { ?>
				<div class="alert alert-error">
					<button class="close" data-dismiss="alert">×</button>
					<strong>Error!</strong> <?php echo $this->session->flashdata('error'); ?>
				</div>
				<?php } ?>
			</div>
		</div>
		<!-- END Alert widget-->
		<?php } ?>
		<!-- BEGIN ADVANCED TABLE widget-->
		<div class="row-fluid">
			<div class="span12">
				<!-- BEGIN EXAMPLE TABLE widget-->
				<div class="widget blue">
					<div class="widget-title">
						<h4><i class="icon-reorder"></i> Employee List </h4>
						<span class="tools">
							<a href="javascript:;" class="icon-chevron-down"></a>
						</span>
					</div>
					<div class="widget-body">
						<table class="table table-striped table-bordered" id="sample_1">
							<thead>
								<tr>
								<th class="hidden"></th>
									
									<th>Code</th>
									<th>Full Name</th>
									<th>CNIC</th>
									<th>Mobile No</th>
									<th class="center">Joining</th>
									<th class="center">Resign</th>
									<th class="center span3">Status</th>
									<?php if($this->session->userdata['user_type'] == 'Admin'){?>		<th class="center span3">Action</th><?php }?>
								</tr>
							</thead>
							<tbody>
								<?php foreach ($emps as $emp) { ?>
								<tr>
								<td class="hidden"></td>
									<td><a href="hr/emp_details/<?php echo $emp['id']; ?>"><strong><?php echo $emp['code']; ?></strong></a></td>
									<td><?php echo $emp['name']; ?></td>
									<td><?php echo $emp['cnic']; ?></td>
									<td><?php echo $emp['mobile']; ?></td>
									<td class="right"><?php echo date('jS F Y ', strtotime($emp['joining'])); ?></td>
									<td class="center"><?php if($emp['resign']=='0000-00-00'){ echo '-';}else echo date('jS F Y ', strtotime($emp['resign'])); ?></td>
									<td class="center"><?php echo $emp['status']; ?></td>
									<?php if($this->session->userdata['user_type'] == 'Admin'){?>
										<td class="center">
										<a class="btn btn-edit" href="hr/emp_save/<?php echo $emp['id']; ?>"><i class="icon-edit icon-white"></i> Edit</a>
										<a class="btn del btn-danger" href="hr/emp_delete/<?php echo $emp['id']; ?>"><i class="icon-trash icon-white"></i> Delete</a>
									</td>
										<?php }?>
								</tr>
								<?php } ?>
							</tbody>
						</table>
					</div>
				</div>
				<!-- END EXAMPLE TABLE widget-->
			</div>
		</div>

		<!-- END ADVANCED TABLE widget-->
	</div>
	<!-- END PAGE CONTAINER-->
</div>