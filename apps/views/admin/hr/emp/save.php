<div id="main-content">
	<!-- BEGIN PAGE CONTAINER-->
	<div class="container-fluid">
		<!-- BEGIN PAGE HEADER-->
		<div class="row-fluid">
			<div class="span12">
				<!-- BEGIN PAGE TITLE & BREADCRUMB-->
				<h3 class="page-title">
					Employee
				</h3>
				<ul class="breadcrumb">
					<li>
						<a href="dashboard">Dashboard</a>
						<span class="divider">/</span>
					</li>
					<li>
						<a href="hr">H R</a>
						<span class="divider">/</span>
					</li>
					<li class="active">
						<?php if (count($emp) > 0) { ?>Edit<?php } else { ?>Add New<?php } ?> Employee
					</li>
				</ul>
				<!-- END PAGE TITLE & BREADCRUMB-->
			</div>
		</div>
		<!-- END PAGE HEADER-->

		<!-- BEGIN PAGE CONTENT-->
		<div class="row-fluid">
			<div class="span12">
				<!-- BEGIN SAMPLE FORM PORTLET-->
				<div class="widget blue">
					<div class="widget-title">
						<h4><i class="icon-reorder"></i> Entry Form </h4>
						<span class="tools">
							<a href="javascript:;" class="icon-chevron-down"></a>
						</span>
					</div>
					<div class="widget-body">
						<!-- BEGIN FORM-->
						<form class="form-horizontal" id="frmEmp" action="hr/emp_save" method="post">
                            <fieldset>
                                <div class="control-group">
                                    <label class="control-label" for="name">Employee Name</label>
                                    <div class="controls">
                                        <input name="name" value="<?php if(count($emp) > 0){ echo $emp['name']; } ?>" id="name" type="text" class="span5" placeholder="Employee Name" />
                                    </div>
                                </div>
                                <div class="control-group">
                                    <label class="control-label" for="father_name">Father's Name</label>
                                    <div class="controls">
                                        <input name="father_name" value="<?php if(count($emp) > 0){ echo $emp['father_name']; } ?>" id="name" type="text" class="span5" placeholder="Father's Name" />
                                    </div>
                                </div>
                                <div class="control-group">
                                    <label class="control-label" for="cnic">CNIC</label>
                                    <div class="controls">
                                        <input name="cnic" id="cnic" value="<?php if(count($emp) > 0){ echo $emp['cnic']; } ?>" type="number" class="span5" placeholder="CNIC" />
                                    </div>
                                </div>
                                <div class="control-group">
                                    <label class="control-label" for="present_address">Region</label>
                                    <div class="controls">
                                        <input name="present_address" value="<?php if(count($emp) > 0){ echo $emp['present_address']; } ?>" id="present_address" type="text" class="span5" placeholder="Region" />
                                    </div>
                                    <!-- <div class="controls">
                                        <textarea name="present_address" id="present_address" class="span5" rows="5" data-form="wysihtml5" placeholder="Present Address"><?php if(count($emp) > 0){ echo $emp['present_address']; } ?></textarea>
                                    </div> -->
                                </div>
                                <div class="control-group">
                                    <label class="control-label" for="permanent_address">Permanent Address</label>
                                    <div class="controls">
                                        <textarea name="permanent_address" id="permanent_address" class="span5" rows="5" data-form="wysihtml5" placeholder="Permanent Address"><?php if(count($emp) > 0){ echo $emp['permanent_address']; } ?></textarea>
                                    </div>
                                </div>
                                <div class="control-group">
                                    <label class="control-label" for="designation">Designation</label>
                                    <div class="controls">
                                        <input name="designation" value="<?php if(count($emp) > 0){ echo $emp['designation']; } ?>" id="name" type="text" class="span5" placeholder="Designation" />
                                    </div>
                                </div>
                                <div class="control-group">
                                    <label class="control-label" for="department">Department</label>
                                    <div class="controls">
                                        <input name="department" value="<?php if(count($emp) > 0){ echo $emp['department']; } ?>" id="name" type="text" class="span5" placeholder="Department" />
                                    </div>
                                </div>
                                <div class="control-group">
                                    <label class="control-label" for="joining">Date of Join</label>
                                    <div class="controls">
                                        <div class="input-append date" data-form="datepicker" data-date="<?php echo date('d/m/Y'); ?>" data-date-format="dd/mm/yyyy">
                                            <input name="joining" id="joining" data-form="datepicker" size="16" type="text" value="<?php if(count($emp) > 0){ echo date_to_ui($emp['joining']); } else { echo date('d/m/Y'); } ?>">
                                            <span class="add-on"><i class="icon-th"></i></span>
                                        </div>
                                    </div>
                                </div>
                                <div class="control-group">
                                    <label class="control-label" for="leaving">Date of Resign</label>
                                    <div class="controls">
                                        <div class="input-append date" data-form="datepicker"  data-date-format="dd/mm/yyyy">
                                            <input name="resign" id="resign" data-form="datepicker" size="16" type="text" value="<?php if(count($emp) > 0){ echo date_to_ui($emp['resign']); }  ?>">
                                            <span class="add-on"><i class="icon-th"></i></span>
                                        </div>
                                    </div>
                                </div>
                                <div class="control-group">
                                    <label class="control-label" for="mobile">Mobile No</label>
                                    <div class="controls">
                                        <input name="mobile" id="mobile" value="<?php if(count($emp) > 0){ echo $emp['mobile']; } ?>" type="number" class="span5" placeholder="Mobile No" />
                                    </div>
                                </div>
                                <div class="control-group">
                                    <label class="control-label" for="mobile">Account No</label>
                                    <div class="controls">
                                        <input name="account_no" id="account_no" value="<?php if(count($emp) > 0){ echo $emp['account_no']; } ?>" type="number" class="span5" placeholder="Account No" />
                                    </div>
                                </div>
                                <div class="control-group">
                                    <label class="control-label" for="email">Email</label>
                                    <div class="controls">
                                        <input name="email" id="email" value="<?php if(count($emp) > 0){ echo $emp['email']; } ?>" type="text" class="span5" placeholder="Email" />
                                    </div>
                                </div>
                                <div class="control-group">
                                    <label class="control-label" for="email">Basic Salary</label>
                                    <div class="controls">
                                        <input name="salary" id="salary" value="<?php if(count($emp) > 0){ echo $emp['salary']; } ?>" type="number" class="span5" placeholder="salary" />
                                    </div>
                                </div>
                                <div class="control-group">
                                    <label class="control-label" for="house_rent">House Rent</label>
                                    <div class="controls">
                                        <input name="house_rent" id="house_rent" value="<?php if(count($emp) > 0){ echo $emp['house_rent']; } ?>" type="number" class="span5" placeholder="House Rent" />
                                    </div>
                                </div>
                                <div class="control-group">
                                    <label class="control-label" for="notes">Notes</label>
                                    <div class="controls">
                                        <textarea name="notes" id="notes" class="span5" rows="5" data-form="wysihtml5" placeholder="Notes"><?php if(count($emp) > 0){ echo $emp['notes']; } ?></textarea>
                                    </div>
                                </div>
                                <div class="control-group">
                                    <label class="control-label" for="status">Status</label>
                                    <div class="controls">
                                        <select name="status" id="status" class="span5 status_" data-placeholder="Select Status">
                                            <option value=""></option>
                                            <option value="Active" <?php if (count($emp) > 0 && $emp['status'] == 'Active') { echo 'selected'; }?>>Active</option>
                                            <option value="Inactive" <?php if (count($emp) > 0 && $emp['status'] == 'Inactive') { echo 'selected'; }?>>Inactive</option>
                                        </select>
                                    </div>
                                </div>
                                <input type="hidden" name="<?php echo $this->security->get_csrf_token_name(); ?>" value="<?php echo $this->security->get_csrf_hash(); ?>" />
                                <?php if(count($emp) > 0){ ?>
                                <input type="hidden" name="id" value="<?php echo $emp['id']; ?>" />
                                <?php } ?>
                                <div class="form-actions">
                                    <input type="submit" class="btn btn-success" value="Save changes" />
                                </div>
                            </fieldset>
                        </form>
                        <!-- END FORM-->
                    </div>
                </div>
                <!-- END SAMPLE FORM PORTLET-->
            </div>
        </div>

        <!-- END PAGE CONTAINER-->
    </div>
    <!-- END PAGE -->
</div>

<script>
  $('#code').focus();
</script>