<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="utf-8">
    <title><?php echo $title; ?></title>
    <base href="<?php echo base_url(); ?>" />
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta name="description" content="">
    <meta name="author" content="InnovativeBD">

    <link rel="shortcut icon" href="assets/backend/img/cybex.png" type="image/x-icon" />

     <link rel="stylesheet" href="assets/bootstrap.min.css">
        <!-- Latest compiled and minified Jquery library -->
   
 
        <!-- Latest compiled and minified JavaScript -->
    <script src="assets/bootstrap.min.js"></script>
<script src='assets/vendors/echarts/test/lib/jquery.min.js'></script>

    <!-- styles -->
    <link href="assets/backend/assets/bootstrap/css/bootstrap.min.css" rel="stylesheet" />
    <link href="assets/backend/assets/bootstrap/css/bootstrap.min.css" rel="stylesheet" />
    <link href="assets/backend/css/stilearn.css" rel="stylesheet" />
    <link href="assets/vendors/datatables.net-bs/css/dataTables.bootstrap.min.css" rel="stylesheet"/>
    <link href="assets/vendors/datatables.net-buttons-bs/css/buttons.bootstrap.min.css" rel="stylesheet"/>
    <link href="assets/vendors/datatables.net-fixedheader-bs/css/fixedHeader.bootstrap.min.css" rel="stylesheet"/>
    <link href="assets/vendors/datatables.net-responsive-bs/css/responsive.bootstrap.min.css" rel="stylesheet"/>
    <link href="assets/vendors/datatables.net-scroller-bs/css/scroller.bootstrap.min.css" rel="stylesheet"/>
<!-- Datatables -->
    <script src="assets/vendors/datatables.net/js/jquery.dataTables.min.js"></script>
    <script src="assets/vendors/datatables.net-bs/js/dataTables.bootstrap.min.js"></script>
    <script src="assets/vendors/datatables.net-buttons/js/dataTables.buttons.min.js"></script>
    <script src="assets/vendors/datatables.net-buttons-bs/js/buttons.bootstrap.min.js"></script>
    <script src="assets/vendors/datatables.net-buttons/js/buttons.flash.min.js"></script>
    <script src="assets/vendors/datatables.net-buttons/js/buttons.html5.min.js"></script>
    <script src="assets/vendors/datatables.net-buttons/js/buttons.print.min.js"></script>
    <script src="assets/vendors/datatables.net-fixedheader/js/dataTables.fixedHeader.min.js"></script>
    <script src="assets/vendors/datatables.net-keytable/js/dataTables.keyTable.min.js"></script>
    <script src="assets/vendors/datatables.net-responsive/js/dataTables.responsive.min.js"></script>
    <script src="assets/vendors/datatables.net-responsive-bs/js/responsive.bootstrap.js"></script>
    <script src="assets/vendors/datatables.net-scroller/js/datatables.scroller.min.js"></script>
    <script src="assets/vendors/jszip/dist/jszip.min.js"></script>
    <script src="assets/vendors/pdfmake/build/pdfmake.min.js"></script>
    <script src="assets/vendors/pdfmake/build/vfs_fonts.js"></script>
    <!-- Le HTML5 shim, for IE6-8 support of HTML5 elements -->
    <!--[if lt IE 9]>
    <script src="http://html5shim.googlecode.com/svn/trunk/html5.js"></script>
    <![endif]-->
    <style>
        @media print{
            p.muted{
                font-weight: bold;
            }
            small.small{
                font-weight: normal;
            }
        }
    </style>
</head>
  <script>
      $(document).ready(function() {
  var table = $('#datatable-responsive').DataTable({
        "lengthMenu": [[10, 25, 50,100, -1], [10, 25, 50,100, "All"]]
    });
 
 var buttons = new $.fn.dataTable.Buttons(table, {
     buttons: [
       'copy',
       'csv',
       'excel',
       'pdf',
     'print'
    ]

 }).container().appendTo($('#buttons'));
 });
 </script>

<body>
    
    <!-- section content -->
    <section class="section">
        <div class="container pull-left">
            <!-- span content -->
            <div class="span13" >
                <!-- content -->
                <div class="content">
                    <!-- content-body -->
                    <div class="content-body">
                        <!-- invoice -->
                        <div id="invoice-container" class="invoice-container">
                            <div class="page-header">
                                <div class="pull-right">
                                    <img src="<?php echo $this->session->userdata('company_logo'); ?>" width="115" class="img">
                                </div>
                                <h3 class="left"><?php echo $this->session->userdata('company_name'); ?></h3>
                                <button id="buttons"></button>
                            </div>
                            <div class="row-fluid">
                                <div class="span12 center">
                                    <strong>Employee Report</strong>
                                </div>
                            </div>

                            <div class="row-fluid">
                                <div class="span8">
                                    <p class="muted">Date From</p>
                                    <p><?php echo date('jS M, Y ', strtotime(date_to_db($start_date))); ?></p>
                                </div>
                                <div class="span4">
                                    <p class="muted">Date To</p>
                                    <p><?php echo date('jS M, Y ', strtotime(date_to_db($end_date))); ?></p>
                                </div>
                            </div>
                            <div class="invoice-table">
                                <table class="table table-bordered invoice responsive" id="datatable-responsive">
                                    <thead>
                                        <tr>
                                            <th>Sr No</th>
                                            <th>Code</th>
                                            <th>Name</th>
                                            <th>Amount Type</th>
                                            <th>Gross Salary</th>
                                            <th>Basic Salary</th>
                                            <th>House Rent</th>
                                            <th>Bonus</th>
                                             <th>Overtime</th>
                                             <th>EOBI</th>
                                             <th>PESSI</th>
                                             <th>Advance</th>
                                             <th>Others dudction</th>
                                            <th>Net Salary</th>
                                            
                                        </tr>
                                    </thead>
                                    <tbody>
                                        <?php
                                        $i = 1;
                                        foreach ($salary as $salary) {
                                                
                                                $gross = $salary['basic_salary'] + $salary['house_rent'];                  
                                $basic = $salary['basic_salary'];
                                $salary_in_days = $basic/30;
                                $days = $salary['days'];
                                $given_salary = $salary_in_days * $days;
                                $house_rent = $salary['house_rent'];
                                $salary_arrears = $salary['arrears'];
                                $bonus = $salary['bonus'];
                                $i_others = $salary['i_others'];
                                $eobi = $salary['eobi'];
                                $pessi = $salary['pessi'];
                                $advance = $salary['advance'];
                                $d_others = $salary['d_others'];
                                $total_pay = $given_salary + $house_rent + $salary_arrears + $bonus + $i_others ;
                                $total_deduction = $eobi + $pessi + $advance + $d_others;
                                $net = $total_pay - $total_deduction;                            
                                            
                                                ?>
                                                <tr>
                                                    <td align="left"><?php echo $i; ?></td>
                                                    <td align="left"><a href="hr/emp_salary_details/<?php echo $salary['id']; ?>" target="_blank" ><strong><?php 
                                echo $salary['code'];
                                 ?></strong></a></td>
                                                    <td align="left"><?php echo $salary['name']; ?></td>
                                                    <td align="left"><?php echo $salary['amount_type']; ?></td>
                                                    <td align="left"><?php echo floor($gross); ?></td>
                                                    <td align="center"><?php echo $salary['basic_salary']; ?></td>
                                                    <td align="left"><?php echo $salary['house_rent']; ?></td>
                                                    <td align="left"><?php echo $bonus; ?></td>
                                                    <td align="left"><?php echo $i_others; ?></td>
                                                    <td align="left"><?php echo $eobi; ?></td>
                                                    <td align="left"><?php echo $pessi; ?></td>
                                                    <td align="left"><?php echo $advance; ?></td>
                                                    <td align="left"><?php echo $d_others; ?></td>
                                                    
                                                    <td align="left"><?php echo floor($net); ?></td>
                                                    
                                                </tr>
                                                <?php
                                                $quantity += $net;
                                                $i++;
                                            // }
                                        }
                                        ?>
                                    </tbody>
                                    <tfoot>
                                        <tr>
                                            <td align="center" colspan="13"><b>Grand Total</b></td>
                                            <td align="center"><b><?php echo floor($quantity);?></b></td>
                                            
                                        </tr>
                                    </tfoot>
                                </table>
                        
                        </div>
                        </div>
                        
                        <!--/invoice-->
                    </div><!--/content-body -->
                </div><!-- /content -->
            </div><!-- /span content -->

        </div><!-- /container -->
    </section>

</body>

</html>