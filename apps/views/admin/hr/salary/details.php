<!DOCTYPE html>
<!--[if IE 8]> <html lang="en" class="ie8"> <![endif]-->
<!--[if IE 9]> <html lang="en" class="ie9"> <![endif]-->
<!--[if !IE]><!--> <html lang="en"> <!--<![endif]-->
<!-- BEGIN HEAD -->
<head>
    <?php $this->load->view('admin/head'); ?>
</head>
<!-- END HEAD -->

<!-- BEGIN BODY -->
<body>
    <!-- BEGIN CONTAINER -->
    <div id="container" class="row-fluid">
        <!-- BEGIN PAGE -->
        <div class="row-fluid">
            <div class="span12">
                <!-- BEGIN BLANK PAGE PORTLET-->
                <div class="widget grey">
                    <div class="widget-body">
                        <div class="row-fluid">
                            <div class="span12">
                                <div class="pull-right">
                                    <img src="<?php echo $this->session->userdata('company_logo'); ?>" width="115" class="img">
                                </div>
                                <h3 class="center" style="color: black"><?php echo $this->session->userdata('company_name'); ?></h3>
                                <hr>
								<h4 class="center" style="color: black">Salary Slip</h4>
								<h4 class="center" style="color: black">Pay Slip For the Month of <?php echo date('F, Y', strtotime($emp['salary_date']));?></h4>
			
                            </div>
                        </div>
                        <div class="space20"></div>
                        <div class="row-fluid invoice-list">
                        <div class="container">
                            
                            <div class="col-md-6 pull-left">
                            <!-- <div class="span4 pull-left"> -->
                            <?php 
                            	$gross = $emp['salary'] + $emp['house_rent'];
                           	    
								

                            ?>
                            <div class="col-md-3 pull-left">
                            	<h4 style="color: black">Invoice No:</h4>
                            	<h4 style="color: black">Employee ID:</h4>
                            	<h4 style="color: black">Name: </h4>
                              <h4 style="color: black">CNIC: </h4>
                            	<h4 style="color: black">Designation:</h4>
                            	<h4 style="color: black">Region:</h4>
                            	<h4 style="color: black">Department:</h4>
                            	<h4 style="color: black">Status:</h4>
                            	<h4 style="color: #000; background: #bbb;">Gross Pay:</h4>
                            </div>

                             <div class="col-md-3 pull-right">
                             <h4 style="color: black"><?php echo $emp['code'];?></h4>
                             <h4 style="color: black"><?php echo $emp['emps_code'];?></h4>
                             <h4 style="color: black"><?php echo $emp['name']; ?></h4>
                             <h4 style="color: black"><?php echo $emp['cnic']; ?></h4>
							 <h4 style="color: black"><?php echo $emp['designation']; ?>
							 </h4>
							 <h4 style="color: black"><?php echo $emp['region']; ?></h4>
							 <h4 style="color: black"><?php echo $emp['department']; ?></h4>
							 <h4 style="color: black"><?php echo $emp['status']; ?></h4>
							 <h4 style="color: #000; background: #bbb;"><?php echo $gross; ?></h4>
                             </div>   
                             </div>
                            <!-- </div> -->
                            
                            <div class="col-md-6">
                                <ul class="unstyled center">

                                    <li style="color: black">Month Days :30 </li>
                                    <li style="color: black">Work Days : <?php echo $emp['days']?></li>
                                </ul>
                            </div>
                            
                            <!-- </div> -->
                        </div>
                            
                            
                        </div>
                        <div class="space20"></div>
                        <div class="space20"></div>
                        <div class="row-fluid" style="height: 600px">
                            <table class="table table-striped table-hover">
                                <thead>
                                    <tr>
                                        <th style="border:1px solid #000; color: white;background: #444;">Pay and Allowances</th>
                                        <th style="border:1px solid #000; color: white;background: #444;">Amount</th>
                                        <th style="border:1px solid #000; color: white;background: #444;">Deductions</th>
                                        <th style="border:1px solid #000; color: white;background: #444;">Amount</th>
                                        
                                    </tr>
                                </thead>
                                <tbody>
                                    <?php
								$basic = $emp['basic_salary'];
								$salary_in_days = $basic/30;
								$days = $emp['days'];
								$given_salary = $salary_in_days * $days;
								$house_rent = $emp['house_rent'];
								$salary_arrears = $emp['arrears'];
								$bonus = $emp['bonus'];
								$i_others = $emp['i_others'];
								$eobi = $emp['eobi'];
								$pessi = $emp['pessi'];
								$advance = $emp['advance'];
								$d_others = $emp['d_others'];
								$total_pay = $given_salary + $house_rent + $salary_arrears + $bonus + $i_others ;
								$total_deduction = $eobi + $pessi + $advance + $d_others;
								$net = $total_pay - $total_deduction;
								$words = $this->numbertowords->convert_number($net);
                                    ?>
                                        <tr>
                                        <td style="color: black"><b>Basic</b></td>
                                            <td style="border:1px solid #000; color: black"><?php echo floor($given_salary); ?></td>
                                           <td style="border:1px solid #000;color: black"><b>EOBI</b></td>
                                            <td style="border:1px solid #000; color: black"><?php echo $eobi; ?></td>
                                          </tr> 
                                          <tr>
                                        <td style="color: black"><b>House Rent</b></td>
                                            <td style="border:1px solid #000; color: black"><?php echo $house_rent; ?></td>
                                           <td style="border:1px solid #000;color: black"><b>PESSI</b></td>
                                            <td style="border:1px solid #000; color: black"><?php echo $pessi; ?></td>
                                          </tr> 
                                          <tr>
                                        <td style="color: black"><b>Salary Arrears</b></td>
                                            <td style="border:1px solid #000; color: black"><?php echo $salary_arrears; ?></td>
                                           <td style="border:1px solid #000;color: black"><b>Advance</b></td>
                                            <td style="border:1px solid #000; color: black"><?php echo $advance; ?></td>
                                          </tr> 
                                          <tr>
                                        <td style="color: black"><b>Bonus</b></td>
                                            <td style="border:1px solid #000; color: black"><?php echo $bonus; ?></td>
                                           <td style="border:1px solid #000;color: black"><b>Others</b></td>
                                            <td style="border:1px solid #000; color: black"><?php echo $d_others; ?></td>
                                          </tr> 
                                          <tr>
                                        <td style="color: black"><b>Overtime</b></td>
                                            <td style="border:1px solid #000; color: black"><?php echo $i_others; ?></td>
                                           <td style="border:1px solid #000;color: black"><b></b></td>
                                            <td style="border:1px solid #000; color: black"></td>
                                          </tr> 
                                          <tr>
                                        <th style="border:1px solid #000; color: white;background: #444;">Total Pay</th>
                                            <th style="border:1px solid #000; color: white;background: #444;"><?php echo floor($total_pay); ?></th>
                                           <th style="border:1px solid #000; color: white;background: #444;">Total Deductions</th>
                                            <th style="border:1px solid #000; color: white;background: #444;"><?php echo floor($total_deduction); ?></th>
                                          </tr> 
                                          <tr>
                                        <td style="color: black"><b>Net Transferred Rs.</b></td>
                                        <td style="color: black"><?php echo floor($net); ?></td>
                                        <td style="color: black" colspan="2"><b> <?php 
                                        		if($emp['amount_type']=='Bank'){
                                        			echo 'To Account No ___ '.$emp['account_no'];
                                        		}elseif($emp['amount_type']=='Cash')  {
                                        			echo "Cash";
                                        		}
                                        ?></b></td>

                                          <tr>
                                        <td style="color: black" colspan="4"><b>Amount In Words : <?php echo $words;?></b></td>
                                          </tr>
                                          <tr>
                                        <td style="color: black" colspan="4"><b><u>Note:</u> This is a computer generated slip and does not need any sign/stamp.</b></td>
                                          </tr>

                                </tbody>


                            </table>
                            </div>
                        <div class="space20"></div>
                        <div class="row-fluid text-center">
                            <a class="btn btn-inverse btn-large hidden-print" onclick="javascript:window.print();">Print <i class="icon-print icon-big"></i></a>
                        </div>
                    </div>
                </div>
                <!-- END BLANK PAGE PORTLET-->
            </div>
        </div>
        <!-- END PAGE -->
    </div>
    <!-- END CONTAINER -->

    <!-- BEGIN FOOTER -->
    <?php $this->load->view('admin/footer'); ?>
    <!-- END FOOTER -->

    <!-- BEGIN JAVASCRIPTS -->
    <?php $this->load->view('admin/js'); ?>
    <!-- END JAVASCRIPTS -->
</body>
<!-- END BODY -->

</html>