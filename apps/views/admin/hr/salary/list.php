<div id="main-content">
	<!-- BEGIN PAGE CONTAINER-->
	<div class="container-fluid">
		<!-- BEGIN PAGE HEADER-->
		<div class="row-fluid">
			<div class="span12">
				<!-- BEGIN PAGE TITLE & BREADCRUMB-->
				<h3 class="page-title">
					Employee's Salary
				</h3>
				<ul class="breadcrumb">
					<li>
						<a href="dashboard">Dashboard</a>
						<span class="divider">/</span>
					</li>
					<li>
						<a href="hr">H R</a>
						<span class="divider">/</span>
					</li>
					<li class="active">
						List Alls
					</li>
				</ul>
				<!-- END PAGE TITLE & BREADCRUMB-->
			</div>
		</div>
		<!-- END PAGE HEADER-->
		<?php if($this->session->flashdata('success') || $this->session->flashdata('info') || $this->session->flashdata('error')) { ?>
		<!-- BEGIN Alert widget-->
		<div class="row-fluid">
			<div class="span12">
				<?php if($this->session->flashdata('success')) { ?>
				<div class="alert alert-success">
					<button class="close" data-dismiss="alert">×</button>
					<strong>Success!</strong> <?php echo $this->session->flashdata('success'); ?>
				</div>
				<?php } ?>
				<?php if($this->session->flashdata('info')) { ?>
				<div class="alert alert-info">
					<button class="close" data-dismiss="alert">×</button>
					<strong>Info!</strong> <?php echo $this->session->flashdata('info'); ?>
				</div>
				<?php } ?>
				<?php if($this->session->flashdata('error')) { ?>
				<div class="alert alert-error">
					<button class="close" data-dismiss="alert">×</button>
					<strong>Error!</strong> <?php echo $this->session->flashdata('error'); ?>
				</div>
				<?php } ?>
			</div>
		</div>
		<!-- END Alert widget-->
		<?php } ?>
		<!-- BEGIN ADVANCED TABLE widget-->
		<div class="row-fluid">
			<div class="span12">
				<!-- BEGIN EXAMPLE TABLE widget-->
				<div class="widget blue">
					<div class="widget-title">
						<h4><i class="icon-reorder"></i> Employee Salary List </h4>
						<span class="tools">
							<a href="javascript:;" class="icon-chevron-down"></a>
						</span>
					</div>
					<div class="widget-body">
						<table class="table table-striped table-bordered" id="sample_1">
							<thead>
								<tr>
								<th class="hidden"></th>
									
									<th>Code</th>
									<th>Employee ID</th>
									<th>Full Name</th>
									<th>Basic Salary</th>
									<th>Net salary</th>
									<th>Amount Type</th>
									<th>Salary Month</th>
									<th class="center">Date</th>
									<?php if($this->session->userdata['user_type'] == 'Admin'){?>		<th class="center span3">Action</th><?php }?>
								</tr>
							</thead>
							<tbody>
								<?php foreach ($emps as $emp) { 

								$basic = $emp['basic_salary'];
								$salary_in_days = $basic/30;
								$days = $emp['days'];
								$given_salary = $salary_in_days * $days;
								$house_rent = $emp['house_rent'];
								$salary_arrears = $emp['arrears'];
								$bonus = $emp['bonus'];
								$i_others = $emp['i_others'];
								$eobi = $emp['eobi'];
								$pessi = $emp['pessi'];
								$advance = $emp['advance'];
								$d_others = $emp['d_others'];
								$total_incentive = $house_rent + $salary_arrears + $bonus + $i_others ;
								$total_deduction = $eobi + $pessi + $advance + $d_others;
								$net = $given_salary + $total_incentive - $total_deduction;
								?>
								<tr>
								<td class="hidden"></td>
								<td><a href="hr/emp_salary_details/<?php echo $emp['id']; ?>" target="_blank" ><strong><?php 
								echo $emp['code'];
								 ?></strong></a></td>
									<td><?php echo $emp['emps_code']; ?></td>
									<td><?php echo $emp['name']; ?></td>

									<td><?php echo $emp['basic_salary']; ?></td>
									<td><?php echo floor($net); ?></td>
									<td><?php echo $emp['amount_type']; ?></td>
									<td><?php echo date('F, Y', strtotime($emp['salary_date']));?></td>
									
									<td class="right"><?php echo date('jS F Y ', strtotime($emp['date'])); ?></td>
									<?php if($this->session->userdata['user_type'] == 'Admin'){?>
										<td class="center">
										<a class="btn btn-edit" href="hr/emp_salary_save/<?php echo $emp['id']; ?>"><i class="icon-edit icon-white"></i> Edit</a>
										<a class="btn del btn-danger" href="hr/emp_salary_delete/<?php echo $emp['id']; ?>"><i class="icon-trash icon-white"></i>Delete</a>
									</td>
										<?php }?>
								</tr>
								<?php } ?>
							</tbody>
						</table>
					</div>
				</div>
				<!-- END EXAMPLE TABLE widget-->
			</div>
		</div>

		<!-- END ADVANCED TABLE widget-->
	</div>
	<!-- END PAGE CONTAINER-->
</div>