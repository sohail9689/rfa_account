<div id="main-content">
	<!-- BEGIN PAGE CONTAINER-->
	<div class="container-fluid">
		<!-- BEGIN PAGE HEADER-->
		<div class="row-fluid">
			<div class="span12">
				<!-- BEGIN PAGE TITLE & BREADCRUMB-->
				<h3 class="page-title">
					Employee's Salary
				</h3>
				<ul class="breadcrumb">
					<li>
						<a href="dashboard">Dashboard</a>
						<span class="divider">/</span>
					</li>
					<li>
						<a href="hr">H R</a>
						<span class="divider">/</span>
					</li>
					<li class="active">
						<?php if (count($emp_s) > 0) { ?>Edit<?php } else { ?>Add New<?php } ?> Employee's Salary
					</li>
				</ul>
				<!-- END PAGE TITLE & BREADCRUMB-->
			</div>
		</div>
		<!-- END PAGE HEADER-->

		<!-- BEGIN PAGE CONTENT-->
		<div class="row-fluid">
			<div class="span12">
				<!-- BEGIN SAMPLE FORM PORTLET-->
				<div class="widget blue">
					<div class="widget-title">
						<h4><i class="icon-reorder"></i> Entry Form </h4>
						<span class="tools">
							<a href="javascript:;" class="icon-chevron-down"></a>
						</span>
					</div>
					<div class="widget-body">
						<!-- BEGIN FORM-->
						<form class="form-horizontal" id="frmEmpSal" action="hr/emp_salary_save" method="post">
                            <fieldset>
                                <div class="control-group">
                                    <label class="control-label" for="name">Employee Name</label>
                                    <div class="controls">
                                        <select name="emps_id" id="emps_id" tabindex="4" class="span5 chzn-select status_">
                                            <option value=""></option>
                                            <?php foreach ($emp_name as $emp_name) { ?>
                                            <option value="<?php echo $emp_name['id']; ?>" <?php if (count($emp_s) > 0) { if ($emp_name['id'] == $emp_s['emps_id']) { ?>selected<?php } } ?>><?php echo $emp_name['name']; ?></option>
                                            <?php } ?>
                                        </select>
                                       
                                    </div>
                                </div>
                                <div class="control-group">
                                    <label class="control-label" for="father_name">Attendance/Days</label>
                                    <div class="controls">
                                        <input name="days" value="<?php if(count($emp_s) > 0){ echo $emp_s['days']; } ?>" id="days" type="number" class="span5" placeholder="Days" min="0" />
                                    </div>
                                </div>
                                <div class="control-group">
                                    <label class="control-label" for="incentive"><b>Incentive</b></label>
                                </div>
                                <div class="control-group">
                                    <label class="control-label" for="arrears">salary Arrears</label> 
                                   <div class="controls">
                                        <input name="arrears" id="arrears" value="<?php if(count($emp_s) > 0){ echo $emp_s['arrears']; } ?>" type="number" class="span5" placeholder="Salary Arrears" />
                                    </div>
                                </div>

                                <div class="control-group">
                                    <label class="control-label" for="bonus">Bonus</label> 
                                   <div class="controls">
                                        <input name="bonus" id="bonus" value="<?php if(count($emp_s) > 0){ echo $emp_s['bonus']; } ?>" type="number" class="span5" placeholder="Bonus" />
                                    </div>
                                </div>
                                <div class="control-group">
                                    <label class="control-label" for="others">Overtime (hours)</label> 
                                   <div class="controls">
                                        <input name="i_others" id="i_others" value="<?php if(count($emp_s) > 0){ 
                                            
                                            echo ($emp_s['i_others']*30)/10000; } ?>" type="number" class="span5" placeholder="Overtime (hours)" />
                                    </div>
                                </div>
                                    <div class="control-group">
                                    <label class="control-label" for="deduction"><b>Deduction</b></label>
                                </div>
                                <div class="control-group">
                                <label class="control-label" for="eobi">EOBI</label>
                                    <div class="controls">

                                        <input name="eobi" id="eobi" value="<?php if(count($emp_s) > 0){ echo $emp_s['eobi']; } ?>" type="number" class="span5" placeholder="EOBI" />
                                    </div>
                                </div>
                                <div class="control-group">
                                <label class="control-label" for="pessi">PESSI</label>
                                    <div class="controls">

                                        <input name="pessi" id="pessi" value="<?php if(count($emp_s) > 0){ echo $emp_s['pessi']; } ?>" type="number" class="span5" placeholder="PESSI" />
                                    </div>
                                </div>
                                <div class="control-group">
                                <label class="control-label" for="advance">Advance</label>
                                    <div class="controls">

                                        <input name="advance" id="advance" value="<?php if(count($emp_s) > 0){ echo $emp_s['advance']; } ?>" type="number" class="span5" placeholder="Advance" />
                                    </div>
                                </div>
                                <div class="control-group">
                                <label class="control-label" for="d_others">Others</label>
                                    <div class="controls">

                                        <input name="d_others" id="d_others" value="<?php if(count($emp_s) > 0){ echo $emp_s['d_others']; } ?>" type="number" class="span5" placeholder="Others" />
                                    </div>
                                </div>
                                <div class="control-group">
                                    <label class="control-label" for="date">Salary For The Month</label>
                                    <div class="controls">
                                        <div class="input-append date" data-form="datepicker" data-date="<?php echo date('d/m/Y'); ?>" data-date-format="dd/mm/yyyy">
                                            <input name="salary_date" id="salary_date" data-form="datepicker" size="16" type="text" value="<?php if(count($emp_s) > 0){ echo date_to_ui($emp_s['salary_date']); } else { echo date('d/m/Y'); } ?>">
                                            <span class="add-on"><i class="icon-th"></i></span>
                                        </div>
                                    </div>
                                </div>
                                <div class="control-group">
                                    <label class="control-label" for="date">Date</label>
                                    <div class="controls">
                                        <div class="input-append date" data-form="datepicker" data-date="<?php echo date('d/m/Y'); ?>" data-date-format="dd/mm/yyyy">
                                            <input name="date" id="date" data-form="datepicker" size="16" type="text" value="<?php if(count($emp_s) > 0){ echo date_to_ui($emp_s['date']); } else { echo date('d/m/Y'); } ?>">
                                            <span class="add-on"><i class="icon-th"></i></span>
                                        </div>
                                    </div>
                                </div>
                                <div class="control-group">
                                    <label class="control-label" for="status">Amount Type</label>
                                    <div class="controls">
                                        <select name="amount_type" id="amount_type" class="span5 status_" data-placeholder="Select Amount Type">
                                            <option value=""></option>
                                            <option value="Cash" <?php if (count($emp_s) > 0 && $emp_s['amount_type'] == 'Cash') { echo 'selected'; }?>>Cash</option>
                                            <option value="Bank" <?php if (count($emp_s) > 0 && $emp_s['amount_type'] == 'Bank') { echo 'selected'; }?>>Bank</option>
                                        </select>
                                    </div>
                                </div>
                                
                                
                                
                                
                                <input type="hidden" name="<?php echo $this->security->get_csrf_token_name(); ?>" value="<?php echo $this->security->get_csrf_hash(); ?>" />
                                <?php if(count($emp_s) > 0){ ?>
                                <input type="hidden" name="id" value="<?php echo $emp_s['id']; ?>" />
                                <?php } ?>
                            
                                <div class="form-actions">
                                    <input type="submit" class="btn btn-success" value="Save changes" />
                                </div>
                            </fieldset>
                        </form>
                        <!-- END FORM-->
                    </div>
                </div>
                <!-- END SAMPLE FORM PORTLET-->
            </div>
        </div>

        <!-- END PAGE CONTAINER-->
    </div>
    <!-- END PAGE -->
</div>

<script>
  $('#code').focus();
</script>