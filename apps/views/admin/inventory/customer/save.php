<div id="main-content">
	<!-- BEGIN PAGE CONTAINER-->
	<div class="container-fluid">
		<!-- BEGIN PAGE HEADER-->
		<div class="row-fluid">
			<div class="span12">
				<!-- BEGIN PAGE TITLE & BREADCRUMB-->
				<h3 class="page-title">
					Customer
				</h3>
				<ul class="breadcrumb">
					<li>
						<a href="dashboard">Dashboard</a>
						<span class="divider">/</span>
					</li>
					<li>
						<a href="inventory">Inventory</a>
						<span class="divider">/</span>
					</li>
					<li>
						<a href="inventory/customer_list">Customer List</a>
						<span class="divider">/</span>
					</li>
					<li class="active">
						<?php if(count($customer) > 0){ ?>Edit<?php }else{ ?>Add New<?php } ?> Customer
					</li>
				</ul>
				<!-- END PAGE TITLE & BREADCRUMB-->
			</div>
		</div>
		<!-- END PAGE HEADER-->
<?php if($this->session->flashdata('success') || $this->session->flashdata('info') || $this->session->flashdata('error')) { ?>
		<!-- BEGIN Alert widget-->
		<div class="row-fluid">
			<div class="span12">
				<?php if($this->session->flashdata('success')) { ?>
				<div class="alert alert-success">
					<button class="close" data-dismiss="alert">×</button>
					<strong>Success!</strong> <?php echo $this->session->flashdata('success'); ?>
				</div>
				<?php } ?>
				<?php if($this->session->flashdata('info')) { ?>
				<div class="alert alert-info">
					<button class="close" data-dismiss="alert">×</button>
					<strong>Info!</strong> <?php echo $this->session->flashdata('info'); ?>
				</div>
				<?php } ?>
				<?php if($this->session->flashdata('error')) { ?>
				<div class="alert alert-error">
					<button class="close" data-dismiss="alert">×</button>
					<strong>Error!</strong> <?php echo $this->session->flashdata('error'); ?>
				</div>
				<?php } ?>
			</div>
		</div>
		<!-- END Alert widget-->
		<?php } ?>
		<!-- BEGIN PAGE CONTENT-->
		<div class="row-fluid">
			<div class="span12">
				<!-- BEGIN SAMPLE FORM PORTLET-->
				<div class="widget blue">
					<div class="widget-title">
						<h4><i class="icon-reorder"></i> Entry Form </h4>
						<span class="tools">
							<a href="javascript:;" class="icon-chevron-down"></a>
						</span>
					</div>
					<div class="widget-body">
						<!-- BEGIN FORM-->
						<form action="inventory/customer_save" method="post" class="form-horizontal form-bordered form-validate" id="frmCustomer">
							<div class="control-group">
								<label for="code" class="control-label">Company Code</label>
								<div class="controls">
									<input type="text" name="code" id="code" value="<?php if(count($customer) > 0){ echo $customer['code']; } else { echo $code; } ?>" class="span5" placeholder="Customer Code"> Digit Only
								</div>
							</div>
							<div class="control-group">
								<label for="name" class="control-label">Customer Name</label>
								<div class="controls">
									<input type="text" name="name" id="name" value="<?php if(count($customer) > 0){ echo $customer['name']; } ?>" class="span5" placeholder="Customer Name">
								</div>
							</div>
							<div class="control-group">
								<label for="address" class="control-label">Address</label>
								<div class="controls">
									<textarea name="address" id="address" class="span5" placeholder="Customer Address"><?php if(count($customer) > 0){ echo $customer['address']; } ?></textarea>
								</div>
							</div>
							<div class="control-group">
								<label for="mobile" class="control-label">Mobile</label>
								<div class="controls">
									<input type="text" name="mobile" id="mobile" value="<?php if(count($customer) > 0){ echo $customer['mobile']; } ?>" class="span5" placeholder="Customer Mobile Number">
								</div>
							</div>
							<div class="control-group">
								<label for="country" class="control-label">Sale Tax No</label>
								<div class="controls">
									<input type="text" name="country" id="country" value="<?php if(count($customer) > 0){ echo $customer['country']; } ?>" class="span5" placeholder="Sale Tax No">
								</div>
							</div>
							<div class="control-group">
								<label for="mobile" class="control-label">NTN No</label>
								<div class="controls">
									<input type="text" name="ntn" id="ntn" value="<?php if(count($customer) > 0){ echo $customer['ntn']; } ?>" class="span5" placeholder="NTN No">
								</div>
							</div>
							<div class="control-group">
								<label for="email" class="control-label">Email</label>
								<div class="controls">
									<input type="text" name="email" id="email" value="<?php if(count($customer) > 0){ echo $customer['email']; } ?>" class="span5" placeholder="Customer Email Address">
								</div>
							</div>
							<div class="control-group">
								<label for="notes" class="control-label">Notes</label>
								<div class="controls">
									<textarea name="notes" id="notes" class="span5" placeholder="Customer Notes"><?php if(count($customer) > 0){ echo $customer['notes']; } ?></textarea>
								</div>
							</div>
							<!-- <div class="control-group">
									<label class="control-label">Select A/C</label>
									<div class="controls">
										<select name="ac_chart" id="ac_chart" tabindex="4" class="chzn-select">
											<?php echo $charts; ?>
										</select>
										
									</div>
								</div> -->
							<div class="control-group">
								<label for="status" class="control-label">Select Status</label>
								<div class="controls">
									<select name="status" id="status" class="span5 chzn-select status_" data-placeholder="Select Status">
										<option value="active" <?php if (count($customer) > 0 && $customer['status'] == 'active') { echo 'selected'; }?>>Active</option>
										<option value="inactive" <?php if (count($customer) > 0 && $customer['status'] == 'inactive') { echo 'selected'; }?>>Inactive</option>
									</select>
								</div>
							</div>
							<input type="hidden" name="<?php echo $this->security->get_csrf_token_name(); ?>" value="<?php echo $this->security->get_csrf_hash(); ?>" />
							<?php if(count($customer) > 0){ ?>
							<input type="hidden" name="id" value="<?php echo $customer['id']; ?>" />
							<?php } ?>
							<div class="form-actions">
								<input type="submit" class="btn btn-success" value="Save Changes">
							</div>
						</form>
						<!-- END FORM-->
					</div>
				</div>
				<!-- END SAMPLE FORM PORTLET-->
			</div>
		</div>

		<!-- END PAGE CONTAINER-->
	</div>
	<!-- END PAGE -->
</div>

<script>
	$('#code').focus();
</script>