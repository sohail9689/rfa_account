<div id="main-content">
	<!-- BEGIN PAGE CONTAINER-->
	<div class="container-fluid">
		<!-- BEGIN PAGE HEADER-->
		<div class="row-fluid">
			<div class="span12">
				<!-- BEGIN PAGE TITLE & BREADCRUMB-->
				<h3 class="page-title">
					Item Grades
				</h3>
				<ul class="breadcrumb">
					<li>
						<a href="dashboard">Dashboard</a>
						<span class="divider">/</span>
					</li>
					<li>
						<a href="inventory">Inventory</a>
						<span class="divider">/</span>
					</li>
					<li class="active">
						Item Grades List
					</li>
				</ul>
				<!-- END PAGE TITLE & BREADCRUMB-->
			</div>
		</div>
		<!-- END PAGE HEADER-->

		<!-- BEGIN Alert widget-->
		<?php if($this->session->flashdata('success') || $this->session->flashdata('info') || $this->session->flashdata('error')) { ?>
		<div class="row-fluid">
			<div class="span12">
				<?php if($this->session->flashdata('success')) { ?>
				<div class="alert alert-success">
					<button class="close" data-dismiss="alert">×</button>
					<strong>Success!</strong> <?php echo $this->session->flashdata('success'); ?>
				</div>
				<?php } ?>
				<?php if($this->session->flashdata('info')) { ?>
				<div class="alert alert-info">
					<button class="close" data-dismiss="alert">×</button>
					<strong>Info!</strong> <?php echo $this->session->flashdata('info'); ?>
				</div>
				<?php } ?>
				<?php if($this->session->flashdata('error')) { ?>
				<div class="alert alert-error">
					<button class="close" data-dismiss="alert">×</button>
					<strong>Error!</strong> <?php echo $this->session->flashdata('error'); ?>
				</div>
				<?php } ?>
			</div>
		</div>
		<?php } ?>
		<!-- END Alert widget-->
		<div class="row-fluid right">
			<a class="btn btn-primary" href="inventory/item_grade_save"><i class="icon-plus icon-white"></i> Add New</a>
		</div>
		<br>
		<!-- BEGIN ADVANCED TABLE widget-->
		<div class="row-fluid">
			<div class="span12">
				<!-- BEGIN EXAMPLE TABLE widget-->
				<div class="widget blue">
					<div class="widget-title">
						<h4><i class="icon-reorder"></i> List All </h4>
						<span class="tools">
							<a href="javascript:;" class="icon-chevron-down"></a>
						</span>
					</div>
					<div class="widget-body">
						<table class="table table-striped table-bordered" id="sample_1">
							<thead>
								<tr>
								<th class="hidden"></th>
									<th>Name</th>
									<th class="center">Status</th>
									<?php if($this->session->userdata['user_type'] == 'Admin'){?>		<th class="center span3">Action</th><?php }?>
								</tr>
							</thead>
							<tbody>
								<?php
								foreach ($items as $item) {
									?>
									<tr>
									<td class="hidden"></td>
										<td><?php echo $item['grade_name']; ?></td>
										<td class="center"> <?php if($item['status'] == 0){
											echo 'Active';
											}else{ echo 'Inactive'; }  ?></td>
										<?php if($this->session->userdata['user_type'] == 'Admin'){?>
										<td class="actions center">
											<a class="btn btn-edit" href="inventory/item_grade_save/<?php echo $item['id']; ?>"><i class="icon-edit icon-white"></i> Edit</a>
											<a class="btn btn-danger del" href="inventory/item_grade_delete/<?php echo $item['id']; ?>"><i class="icon-trash icon-white"></i> Delete</a>
										</td>
										<?php }?>
									</tr>
									<?php
								}
								?>
							</tbody>
						</table>
					</div>
				</div>
				<!-- END EXAMPLE TABLE widget-->
			</div>
		</div>

		<!-- END ADVANCED TABLE widget-->
	</div>
	<!-- END PAGE CONTAINER-->
</div>