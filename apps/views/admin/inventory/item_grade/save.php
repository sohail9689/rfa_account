<div id="main-content">
	<!-- BEGIN PAGE CONTAINER-->
	<div class="container-fluid">
		<!-- BEGIN PAGE HEADER-->
		<div class="row-fluid">
			<div class="span12">
				<!-- BEGIN PAGE TITLE & BREADCRUMB-->
				<h3 class="page-title">
					Item Grades
				</h3>
				<ul class="breadcrumb">
					<li>
						<a href="dashboard">Dashboard</a>
						<span class="divider">/</span>
					</li>
					<li>
						<a href="inventory">Inventory </a>
						<span class="divider">/</span>
					</li>
					<li class="active">
						<?php if (count($item) > 0) { ?>Edit<?php } else { ?>Add New<?php } ?> Item Grades
					</li>
				</ul>
				<!-- END PAGE TITLE & BREADCRUMB-->
			</div>
		</div>
		<!-- END PAGE HEADER-->

		<!-- BEGIN Alert widget-->
		<?php if( validation_errors() ) { ?>
		<div class="row-fluid">
			<div class="span12">
				<div class="alert alert-error">
					<button class="close" data-dismiss="alert">×</button>
					<strong>Error!</strong> <?php echo validation_errors(); ?>
				</div>
			</div>
		</div>
		<?php } ?>
		<!-- END Alert widget-->

		<!-- BEGIN PAGE CONTENT-->
		<div class="row-fluid">
			<div class="span12">
				<!-- BEGIN SAMPLE FORM PORTLET-->
				<div class="widget blue">
					<div class="widget-title">
						<h4><i class="icon-reorder"></i> Entry Form </h4>
						<span class="tools">
							<a href="javascript:;" class="icon-chevron-down"></a>
						</span>
					</div>
					<div class="widget-body">
						<!-- BEGIN FORM-->
						<form action="inventory/item_grade_save" method="post" class="form-horizontal form-bordered form-validate" id="frmItem">
							
							<div class="control-group">
								<label for="name" class="control-label">Grade Name</label>
								<div class="controls">
									<input type="text" name="grade_name" value="<?php if (count($item) > 0) { echo $item['grade_name']; } ?>" id="grade_name" class="span5" placeholder="Item Name">
								</div>
							</div>
							
							<div class="control-group">
								<label for="textfield" class="control-label">Select Status</label>
								<div class="controls">
									<select name="status" id="status" class="span5 chzn-select">
										<option value="0" <?php if (count($item) > 0 && $item['status'] == '0') { echo 'selected'; }?>>Active</option>
										<option value="1" <?php if (count($item) > 0 && $item['status'] == '1') { echo 'selected'; }?>>Inactive</option>
									</select>
								</div>
							</div>
							<input type="hidden" name="<?php echo $this->security->get_csrf_token_name(); ?>" value="<?php echo $this->security->get_csrf_hash(); ?>" />
							<?php if (count($item) > 0) { ?><input type="hidden" name="id" value="<?php echo $item['id']; ?>" /><?php } ?>
							<div class="form-actions">
								<input type="submit" class="btn btn-success" value="Save Changes">
							</div>
						</form>
						<!-- END FORM-->
					</div>
				</div>
				<!-- END SAMPLE FORM PORTLET-->
			</div>
		</div>

		<!-- END PAGE CONTAINER-->
	</div>
	<!-- END PAGE -->
</div>

