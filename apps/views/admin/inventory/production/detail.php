<!DOCTYPE html>
<!--[if IE 8]> <html lang="en" class="ie8"> <![endif]-->
<!--[if IE 9]> <html lang="en" class="ie9"> <![endif]-->
<!--[if !IE]><!--> <html lang="en"> <!--<![endif]-->
<!-- BEGIN HEAD -->
<head>
	<?php $this->load->view('admin/head'); ?>
</head>
<!-- END HEAD -->

<!-- BEGIN BODY -->
<body>

	<!-- BEGIN CONTAINER -->
	<div id="container" class="row-fluid">
	<!-- BEGIN PAGE -->
		<div class="row-fluid">
			<div class="span12">
				<!-- BEGIN BLANK PAGE PORTLET-->
				<div class="widget grey">
					<div class="widget-body">
						<div class="row-fluid">
							<div class="span12">
								<div class="">
                                    <img src="<?php echo $this->session->userdata('company_logo'); ?>" width="115" class="img">
                                </div>
                                <h3 class="center" style="color: black"><?php echo $this->session->userdata('company_name'); ?></h3>
                                <h5 class="center" style="color: black">Production: <?php echo $production[0]['production_name'] .' - '. $production[0]['code']; ?></h5>
                                <hr>
							</div>
						</div>
						<div class="space20"></div>
						<div class="row-fluid">
							<div class="invoice-table">
								<fieldset>
								<div class="span1"></div>
									<div class="span10">
										<div id="debit_details">
											<table class="table  table-striped responsive" style="border: 2px solid #000000;">
												<tbody>
												<tr>
														<th  style="border: 1px solid #000000; color: black">Production Date</th>
														<td  style="border: 1px solid #000000; color: black" colspan="3"><?php echo date('Y-m-d', strtotime($production[0]['production_date'])); ?></td>
													</tr>
													<tr>
														<th  style="border: 1px solid #000000; color: black">Production Type</th>
														<td  style="border: 1px solid #000000; color: black" colspan="3"><?php echo $production[0]['production_type']; ?></td>
													</tr>
													<tr>
														<th  style="border: 1px solid #000000; color: black">Material</th>
														<td  style="border: 1px solid #000000; color: black" colspan="3"><?php echo $production[0]['raw_name']; ?></td>
													</tr>
													<tr>
														<th  style="border: 1px solid #000000; color: black">Quantity (pcs)</th>
														<td  style="border: 1px solid #000000; color: black" colspan="3"><?php echo $production[0]['quantity']; ?></td>
													</tr>
													<tr>
														<th  style="border: 1px solid #000000; color: black">Total Area (sq ft)</th>
														<td  style="border: 1px solid #000000; color: black" colspan="3"><?php echo $production[0]['raw_quantity']; ?></td>
													</tr>
													<tr>
														<th  style="border: 1px solid #000000; color: black">Total Weight (kg)</th>
														<td  style="border: 1px solid #000000; color: black" colspan="3"><?php echo $production[0]['raw_weight']; ?></td>
													</tr>
													<tr>
														<th  style="border: 1px solid #000000; color: black">Recipe / Process</th>
														<td  style="border: 1px solid #000000; color: black" colspan="3"><?php echo $production[0]['recipe_name']; ?></td>
													</tr>
												<?php
												// $total_cost1 = 0;
												$chemical = $this->MProduction->get_chemical_by_production_id_in_recipe_chemical($production[0]['id']);
												if(count($chemical) > 0){ 
													foreach ($chemical as $key => $value) {
														$name = $this->MItems->get_by_id($value['chemical_id']); 
														$avg_cost = ($value['total_price']) / $production[0]['raw_quantity'];
														?>
													<tr>
														<th  style="border: 1px solid #000000; color: black">Chemical Name & Quantity</th>
														<td  style="border: 1px solid #000000; color: black">

															<?php echo $name['name']; ?></td>
															<td  style="border: 1px solid #000000; color: black">
															
															<?php echo $value['qty_kg'].' Kg'; ?></td>
															<td  style="border: 1px solid #000000; color: black">
															
															<?php echo 'Avg-Cost '.number_format($avg_cost, 2); ?></td>
													</tr>

												<?php } } ?>
												<tr>
														<th  style="border: 1px solid #000000; color: black">Total Recipe Cost</th>
<td  style="border: 1px solid #000000; color: black" colspan="3"><?php
					$recipe_chemical = $this->MProduction->get_recipe_chemical_by_production_id($production[0]['id']);
					$total_cost = 0;
					foreach ($recipe_chemical as $chemical) {
						$sm = round($chemical['total_price']);
						$total_cost += $sm;	
					}

					echo $total_cost;
					 ?>
 	
 </td>

					</tr>
					<tr>
						<th  style="border: 1px solid #000000; color: black">Total Raw Cost</th>
						<td  style="border: 1px solid #000000; color: black" colspan="3"><?php
							$raw_cost = round($production[0]['raw_total_price']);
							echo $raw_cost;
								 ?>
								 </td>

					</tr>
					<tr>
						<th  style="border: 1px solid #000000; color: black">Total Production Cost</th>
						<td  style="border: 1px solid #000000; color: black" colspan="3"><?php
							$total_prod_cost = $raw_cost + $total_cost;
							echo $total_prod_cost;
								 ?>
								 </td>
					</tr>
													<tr>
														<th  style="border: 1px solid #000000; color: black">Average Chemical expense per sq ft</th>
														<td  style="border: 1px solid #000000; color: black" colspan="3"><?php
														 echo number_format($total_cost/$production[0]['raw_quantity'], 3);
														   ?></td>
													</tr>.
													<tr>
														<th  style="border: 1px solid #000000; color: black">Average Cost per sq ft</th>
														<td  style="border: 1px solid #000000; color: black" colspan="3"><?php
														 echo number_format($total_prod_cost/$production[0]['raw_quantity'], 3);
														   ?></td>
													</tr>
												</tbody>

												
											</table>

										</div>
									</div>
									</fieldset>
									<br><br>
									<fieldset>
								<div class="span1"></div>
									<div class="span10">
									<h4 class="center" style="color: black">Selection Box Detail</h4>
										<div id="debit_details">
											<table class="table  table-striped responsive" style="border: 2px solid #000000;">
											
											<thead>
													<tr>
														<th  style="border: 1px solid #000000; color: black">Selections</th>
														<th  style="border: 1px solid #000000; color: black">Prices</th>
														<th  style="border: 1px solid #000000; color: black">Quantity(pcs)</th>
														<th  style="border: 1px solid #000000; color: black">Area(Sq ft)</th>
													</tr>
												</thead>
												<tbody>
													
												<?php $recipe_chemical = $this->MProduction->get_join_selection_item_production_id($production[0]['id']);
												$qntit = 0;
												$sq = 0;
												foreach ($recipe_chemical as $key => $value) {
													
												 ?>

												<tr>
														<td  style="border: 1px solid #000000; color: black"><?php echo $value['item_name']; ?></td>
														<td  style="border: 1px solid #000000; color: black"><?php echo $value['price']; ?></td>
														<td  style="border: 1px solid #000000; color: black"><?php echo $value['quantity']; ?></td>
														<td  style="border: 1px solid #000000; color: black"><?php echo $value['sq_ft']; ?></td>
													</tr>
													
													<?php 
													$qntit += $value['quantity'];
													$sq += $value['sq_ft'];
													}
													?>
												</tbody>
												<tfoot>
													<tr>
														<td  style="border: 1px solid #000000; color: black">Total</td>
														<td  style="border: 1px solid #000000; color: black"></td>
														<td  style="border: 1px solid #000000; color: black"><?php echo $qntit; ?></td>
														<td  style="border: 1px solid #000000; color: black"><?php echo $sq; ?></td>
													</tr>
													<tr>
														<td  style="border: 1px solid #000000; color: black">Difference</td>
														<td  style="border: 1px solid #000000; color: black"></td>
														<td  style="border: 1px solid #000000; color: black"></td>
														<td  style="border: 1px solid #000000; color: black"><?php echo $production[0]['raw_quantity'] - $sq; ?></td>
													</tr>
												</tfoot>
												
											</table>
											
										</div>
									</div>
									</fieldset>
							</div>
						</div>
						
						<div class="space20"></div>
						<div class="row-fluid text-center">
							<a class="btn btn-inverse btn-large hidden-print" onclick="javascript:window.print();">Print <i class="icon-print icon-big"></i></a>
						</div>
					</div>
				</div>
				<!-- END BLANK PAGE PORTLET-->
			</div>
		</div>
		<!-- END PAGE -->
	</div>
	<!-- END CONTAINER -->

	<!-- BEGIN FOOTER -->
	<?php $this->load->view('admin/footer'); ?>
	<!-- END FOOTER -->

	<!-- BEGIN JAVASCRIPTS -->
	<?php $this->load->view('admin/js'); ?>
	<!-- END JAVASCRIPTS -->
</body>
<!-- END BODY -->

</html>