<div id="main-content">
	<!-- BEGIN PAGE CONTAINER-->
	<div class="container-fluid">
		<!-- BEGIN PAGE HEADER-->
		<div class="row-fluid">
			<div class="span12">
				<!-- BEGIN PAGE TITLE & BREADCRUMB-->
				<h3 class="page-title">
					Production
				</h3>
				<ul class="breadcrumb">
					<li>
						<a href="dashboard">Dashboard</a>
						<span class="divider">/</span>
					</li>
					<li>
						<a href="inventory">Inventory</a>
						<span class="divider">/</span>
					</li>
					<li>
						<a href="inventory/production_list">Production List</a>
						<span class="divider">/</span>
					</li>
					<li class="active">
						<?php if(count($production) > 0){ ?>Edit<?php }else{ ?>Add New<?php } ?> Production
					</li>
				</ul>
				<!-- END PAGE TITLE & BREADCRUMB-->
			</div>
		</div>
		<!-- END PAGE HEADER-->
<?php if($this->session->flashdata('success') || $this->session->flashdata('info') || $this->session->flashdata('error')) { ?>
		<!-- BEGIN Alert widget-->
		<div class="row-fluid">
			<div class="span12">
				<?php if($this->session->flashdata('success')) { ?>
				<div class="alert alert-success">
					<button class="close" data-dismiss="alert">×</button>
					<strong>Success!</strong> <?php echo $this->session->flashdata('success'); ?>
				</div>
				<?php } ?>
				<?php if($this->session->flashdata('info')) { ?>
				<div class="alert alert-info">
					<button class="close" data-dismiss="alert">×</button>
					<strong>Info!</strong> <?php echo $this->session->flashdata('info'); ?>
				</div>
				<?php } ?>
				<?php if($this->session->flashdata('error')) { ?>
				<div class="alert alert-error">
					<button class="close" data-dismiss="alert">×</button>
					<strong>Error!</strong> <?php echo $this->session->flashdata('error'); ?>
				</div>
				<?php } ?>
			</div>
		</div>
		<!-- END Alert widget-->
		<?php } ?>
		<!-- BEGIN PAGE CONTENT-->
		<div class="row-fluid">
			<div class="span12">
				<!-- BEGIN SAMPLE FORM PORTLET-->
				<div class="widget blue">
					<div class="widget-title">
						<h4><i class="icon-reorder"></i> Entry Form </h4>
						<span class="tools">
							<a href="javascript:;" class="icon-chevron-down"></a>
						</span>
					</div>
					<div class="widget-body">
						<!-- BEGIN FORM-->
						<form action="inventory/production_save" method="post" class="form-horizontal form-bordered form-validate" id="frmCustomer">
							<div class="control-group">
								<label for="code" class="control-label">Production Sr #</label>
								<div class="controls">
									<input type="text" name="code" id="code" value="<?php if(count($production) > 0){ echo $production['code']; } else { echo $code; } ?>" class="span5" placeholder="Production Code">
								</div>
							</div>
							<div class="control-group">
										<label class="control-label" for="purchase_date">Production Date</label>
										<div class="controls">
											<div class="input-append date" data-form="datepicker" data-date="<?php echo date('d/m/Y'); ?>" data-date-format="dd/mm/yyyy">
												<input name="production_date" id="production_date" tabindex="2" data-form="datepicker" size="16" type="text" value="<?php if(count($production) > 0){ echo date_to_ui($production['production_date']); } else { echo date('d/m/Y'); } ?>">
												<span class="add-on"><i class="icon-th"></i></span>
											</div>
										</div>
									</div>
							<div class="control-group">
								<label for="textfield" class="control-label">Select Type</label>
								<div class="controls">
									<select name="type" id="type" class="span5 chzn-select item_type">
									<option value="0">Select Type</option>
										<option value="Raw" <?php if (count($production) > 0 && $production['type'] == 'Raw') { echo 'selected'; }?>>Raw</option>
										<option value="Wet Blue" <?php if (count($production) > 0 && $production['type'] == 'Wet Blue') { echo 'selected'; }?>>Wet Blue</option>
									</select>
								</div>
							</div>
							<div class="control-group">
								<label for="code" class="control-label">Name</label>
								<div class="controls">
									<input type="text" name="name" id="name" value="<?php if(count($production) > 0){ echo $production['name']; }  ?>" class="span5" placeholder="Name">
									<input  style="text-align:center;" class="span2" value="Quantity" readonly>
									<input  style="text-align:center;" class="span2" value="Sq Ft" readonly>

								</div>

							</div>
							
							<div class="control-group">
								<label for="name" class="control-label" id="type_name">Select</label>
								<div class="controls">
								<select name="item_id" id="item_id" class="span5 chzn-select item_type" data-placeholder="Select">
								<option value="0">Select</option>
									<?php foreach($items as $item){ ?>
										<option value="<?php echo $item['id']; ?>" <?php 
										if(count($production) > 0){ 
												if($item['id'] == $production['raw']){
													echo "selected";
											}
										}
										?>><?php echo $item['name']; ?></option>
										<?php } ?>
									</select>
									<input  id="quant"  style="text-align:center;" class="span2"  readonly>
									<input  id="sq_ft"  style="text-align:center;" class="span2"  readonly>
								</div>
							</div>
							<div class="control-group">
								<label for="code" class="control-label">Quantity(pcs)</label>
								<div class="controls">
									<input type="number" name="quantit" id="quantit" value="<?php if(count($production) > 0){ echo $production['quantity']; }  ?>" class="span5" placeholder="Quantity">
								</div>
							</div>
							<div class="control-group">
								<label for="mobile" class="control-label">Total Area (sq ft)</label>
								<div class="controls">
									<input type="number" name="raw_quantity" id="raw_quantity" value="<?php if(count($production) > 0){ echo $production['raw_quantity']; } ?>" class="span5" min="0" placeholder="Total Area (sq ft)">
								</div>
							</div>
							<div class="control-group">
								<label for="mobile" class="control-label">Total Weight</label>
								<div class="controls">
									<input type="number" name="raw_weight" id="raw_weight" value="<?php if(count($production) > 0){ echo $production['raw_weight']; } ?>" class="span5" min="0" step="any" placeholder="Total Weight">
								</div>
							</div>
							<div class="control-group">
								<label for="name" class="control-label">Recipe</label>
								<div class="controls">
								<select name="recipe" id="recipe" class="span5 chzn-select item_type" data-placeholder="Select Recipe">
								<option value="0">Select Recipe</option>
									<?php foreach($recipes as $recipe){ ?>
										<option value="<?php echo $recipe['id']; ?>" <?php 
										if(count($production) > 0){ 
												if($recipe['id'] == $production['recipe']){
													echo "selected";
											}
										}
										?>><?php echo $recipe['name']; ?></option>
										<?php } ?>
				</select>
				<br>
				<br>
				<div id="recipe_chemical">
				</div>
				
								</div>
							</div>
							<div id="child"></div>
							<a id="add" class="btn btn-default ">Add more Chemicals <i class="icon-plus"></i></a>
							</div>
							<input type="hidden" name="<?php echo $this->security->get_csrf_token_name(); ?>" value="<?php echo $this->security->get_csrf_hash(); ?>" />
							<?php if(count($production) > 0){ ?>
							<input type="hidden" name="id" id='id' value="<?php echo $production['id']; ?>" />
							<?php } ?>
							<div class="form-actions">
								<input type="submit" class="btn btn-success" id="save" value="Save Changes">
							</div>
						</form>
						<!-- END FORM-->
					</div>
				</div>
				<!-- END SAMPLE FORM PORTLET-->
			</div>
		</div>

		<!-- END PAGE CONTAINER-->
	</div>
	<!-- END PAGE -->
</div>

<script>
	$(document).ready(function() {
	var id = $('#id').val();
	if(id){
		$.ajax({
			type: "POST",
			url: "inventory/get_production_chemiclas",
			data: {id: id},
			success: function(msg) {
				$("#child").append(msg);
				$('select').select2();
			}
		});
		var result = '<?php echo $production['raw']; ?>';

		var pro_qnty = '<?php echo $production['quantity']; ?>';
		var pro_sq = '<?php echo $production['raw_quantity']; ?>';
		// console.log(pro_sq);
		$.ajax({
				url:"inventory/get_raw_stock_balance",
				type:"POST",
				data:{result: result},
				success:function(data) {
					var res = $.parseJSON(data);
					var tot_qn = parseInt(res['raw']['total_quantity']) + parseInt(pro_qnty);
					var tot_sq = parseInt(res['raw']['total_sw_weight']) + parseInt(pro_sq);
					$('#quant').val(tot_qn);
					$('#sq_ft').val(tot_sq);
					}
				
				})

	}
	
	
});
$(function() {
$('#add').click(function(){
	var newDiv = $('<div><div class="control-group" ><select name="chemical_name[]" id="chemical_name[]" class="span4 chzn-select" data-placeholder="Select Chemical"><option value="0">Select Chemical</option><?php foreach($chemicals as $chemical){ ?><option value="<?php echo $chemical['id']; ?>"><?php echo $chemical['name']; ?></option><?php } ?></select><input type="number" name="quantity[]" step="any" min="0" id="name" value="" class="span3" placeholder="Quantity in Kg"><a href="javascript:void(0);"> Remove Chemical <i  class="icon-minus icon-white"></i></a></div></div>');

  newDiv.on("click", "a", function(){$(this).parent().remove();});
  $('#child').append(newDiv);
  $('select').select2();
	});		

});
$('#child').on('click','.description_text a',function(){
     $(this).closest('.description_text').remove();
});
$("#item_id").change(function(){
            var result = $(this).find("option:selected").val();
			$.ajax({
				url:"inventory/get_raw_stock_balance",
				type:"POST",
				data:{result: result},
				success:function(data) {
					var res = $.parseJSON(data);
					$('#quant').val(res['raw']['total_quantity']);
					$('#sq_ft').val(res['raw']['total_sw_weight']);
					}
				
				})
			});
$("#type").change(function(){
            var type = $(this).find("option:selected").val();
            $('#type_name').text('Select '+ type);
			$.ajax({
				url:"inventory/get_item_by_type",
				type:"POST",
				data:{type: type},
				success:function(data) {
					var res = $.parseJSON(data);
					var result = res.items;
					var $select_elem = $("#item_id");
					$select_elem.empty();
					$select_elem.append('<option value="0">Select</option>');
	                $.each(result, function (idx, obj) {
	                    $select_elem.append('<option value="' + obj.id + '">' + obj.code + ' - '+ obj.name + '</option>');   
	                });
					}
				
				})
			});
$("#recipe").change(function(){
			var raw_weight = $('#raw_weight').val();
            var recipe_id = $(this).find("option:selected").val();
            if(recipe_id != 0){
            	$('#raw_weight').attr('readonly', true);
            }else{
            	$('#raw_weight').attr('readonly', false);
            }
            $("#recipe_chemical").empty();
			$.ajax({
				url:"inventory/get_recipe_stock_balance",
				type:"POST",
				data:{recipe_id: recipe_id,raw_weight:raw_weight},
				success:function(msg) {
					
					$("#recipe_chemical").append(msg);
					}
				
				})
			});

$('#save').click(function(){

	var name = $('#name').val();
	var raw = $('#item_id').val();
	var raw_quantity = $('#raw_quantity').val();
	var quantit = $('#quantit').val();
	var recipe = $('#recipe').val();
	var available_quant = $('#quant').val();
	var available_sq_ft = $('#sq_ft').val();
	var type = $('#type').val();

	if(! type || type == 0){
			alert('Select Type.');
			return false;
		}

	if(!name){
		alert('Name cannot be empty.');
		return false;
	}
	if (!raw || raw == 0) {
    alert(type+ ' item must be selected.');
    return false;
	}
	if (!quantit || quantit == 0) {
    alert('Quantity must be greater than 0.');
    return false;
	}
	if(parseInt(quantit) > parseInt(available_quant)){
		alert('Quantity not greater than available Quantity.');
    	return false;
	}

	if (!raw_quantity || raw_quantity == 0) {
    alert('Total Area / Weight must be greater than 0.');
    return false;
	}

	if(parseInt(raw_quantity) > parseInt(available_sq_ft)){
		alert('Total Area / Weight not greater than available Area.');
    	return false;
	}
	if (!recipe || recipe == 0) {
    alert('Recipe must be must be selected.');
    return false;
	}
	var req_chemical_qunatity_kg = document.getElementsByClassName("req_chemical_qunatity_kg");
	var avail_chemical_qunatity_kg = document.getElementsByClassName("avail_chemical_qunatity_kg");
	var chemical_name = document.getElementsByClassName("chemical_name");
		for(var i=0; i<req_chemical_qunatity_kg.length; i++) {
			if(parseInt(req_chemical_qunatity_kg[i].value) > parseInt(avail_chemical_qunatity_kg[i].value)){
				alert('In Recipe '+chemical_name[i].value+' has insufficient Stock');
				return false;
			}
	}
	var check = true;
	$("select[name='chemical_name[]']").each(function() {
	var chemical_name =	$(this).val();
    if(chemical_name == 0){
    	check = false;
		alert('Chemical Name must be selected.');
		return false;
	}


});
	if (!check) {
    return false;
}

$("input[name='quantity[]']").each(function() {

	var quantity =	$(this).val();
    if(!quantity || quantity == 0){
    	check = false;
		alert('Quantity must be greater than 0.');
		return false;
	}
});

if (!check) {
    return false;
}	
});

</script>