<div id="main-content">
	<!-- BEGIN PAGE CONTAINER-->
	<div class="container-fluid">
		<!-- BEGIN PAGE HEADER-->
		<div class="row-fluid">
			<div class="span12">
				<!-- BEGIN PAGE TITLE & BREADCRUMB-->
				<h3 class="page-title">
					Production
				</h3>
				<ul class="breadcrumb">
					<li>
						<a href="dashboard">Dashboard</a>
						<span class="divider">/</span>
					</li>
					<li>
						<a href="inventory">Inventory</a>
						<span class="divider">/</span>
					</li>
					<li>
						<a href="inventory/production_list">Production List</a>
						<span class="divider">/</span>
					</li>
					<li class="active">Update Selection Box
					</li>
				</ul>
				<!-- END PAGE TITLE & BREADCRUMB-->
			</div>
		</div>
		<!-- END PAGE HEADER-->
<?php if($this->session->flashdata('success') || $this->session->flashdata('info') || $this->session->flashdata('error')) { ?>
		<!-- BEGIN Alert widget-->
		<div class="row-fluid">
			<div class="span12">
				<?php if($this->session->flashdata('success')) { ?>
				<div class="alert alert-success">
					<button class="close" data-dismiss="alert">×</button>
					<strong>Success!</strong> <?php echo $this->session->flashdata('success'); ?>
				</div>
				<?php } ?>
				<?php if($this->session->flashdata('info')) { ?>
				<div class="alert alert-info">
					<button class="close" data-dismiss="alert">×</button>
					<strong>Info!</strong> <?php echo $this->session->flashdata('info'); ?>
				</div>
				<?php } ?>
				<?php if($this->session->flashdata('error')) { ?>
				<div class="alert alert-error">
					<button class="close" data-dismiss="alert">×</button>
					<strong>Error!</strong> <?php echo $this->session->flashdata('error'); ?>
				</div>
				<?php } ?>
			</div>
		</div>
		<!-- END Alert widget-->
		<?php } ?>
		<!-- BEGIN PAGE CONTENT-->
		<div class="row-fluid">
			<div class="span12">
				<!-- BEGIN SAMPLE FORM PORTLET-->
				<div class="widget blue">
					<div class="widget-title">
						<h4><i class="icon-reorder"></i> Entry Form </h4>
						<span class="tools">
							<a href="javascript:;" class="icon-chevron-down"></a>
						</span>
					</div>
					<div class="widget-body">
						<!-- BEGIN FORM-->
						<form action="inventory/production_selection_box" method="post" class="form-horizontal form-bordered " id="">
						<fieldset class="span6">
							<div class="control-group">
								<label for="code" class="control-label">Production Sr #</label>
								<div class="controls">
									<input type="text" name="code" id="code" value="<?php if(count($production) > 0){ echo $production['code']; } else { echo $code; } ?>" class="span5" placeholder="Production Code" readonly>
								</div>
								</div>
									<div class="control-group">
								<label for="code" class="control-label">Type</label>
								<div class="controls">
									<input type="text" name="type" id="type" value="<?php if(count($production) > 0){ echo $production['type']; }  ?>" class="span5" placeholder="Name" readonly>
								</div>
							</div>
								<div class="control-group">
								<label for="code" class="control-label">Name</label>
								<div class="controls">
									<input type="text" name="name" id="name" value="<?php if(count($production) > 0){ echo $production['name']; }  ?>" class="span5" placeholder="Name" readonly>
									<input type="hidden" name="raw_id" value="<?php if(count($production) > 0){ echo $production['raw']; }  ?>">
								</div>
								</div>
									</fieldset>
									<fieldset class="span6">
									<div class="control-group">
										<label class="control-label" for="purchase_date">Production Date</label>
										<div class="controls">
											<div class="input-append date" data-form="datepicker" data-date="<?php echo date('d/m/Y'); ?>" data-date-format="dd/mm/yyyy" readonly>
												<input name="production_date" id="production_date" tabindex="2" data-form="datepicker" size="16" type="text" value="<?php if(count($production) > 0){ echo date_to_ui($production['production_date']); } else { echo date('d/m/Y'); } ?>" readonly>
												<span class="add-on"><i class="icon-th"></i></span>
											</div>
										</div>
									</div>

								<div class="control-group">
								<label for="code" class="control-label">Quantity(pcs)</label>
								<div class="controls">
									<input type="text" name="avai" id="avai" value="<?php if(count($production) > 0){ echo $production['quantity']; }  ?>" class="span5" placeholder="Name" disabled>
								</div>
								</div>
								<div class="control-group">
								<label for="code" class="control-label">Total Area(Sq ft)</label>
								<div class="controls">
									<input type="text"  name="raw_qnt" value="<?php if(count($production) > 0){ echo $production['raw_quantity']; }  ?>" class="span5" placeholder="Name" readonly>
								</div>
								</div>
								<?php 
								$recipe_chemical = $this->MProduction->get_recipe_chemical_by_production_id($production['id']);
								$total_cost = 0;
								foreach ($recipe_chemical as $chemical) {
									$total_cost += round($chemical['total_price']);	
								}
								$item = $this->MItems->get_by_id($production['raw']);
								$raw_cost = round($production['raw_total_price']);
								$tot_prod_cost = $raw_cost + $total_cost;
								// number_format($tot_prod_cost, 2, '.', '');
								?>
								<div class="control-group">
								<label for="code" class="control-label">Total Production Cost</label>
								<div class="controls">

									<input type="text"  value="<?php if(count($production) > 0){ echo ($tot_prod_cost); }  ?>"  name="production_cost" id="production_cost" class="span5" placeholder="Name" readonly>
								</div>
								</div>
								</fieldset>
								<fieldset class="span12">
							<div id="child">
							<label for="name" class="span3">Selection Name</label><label for="name" class="span2">Price(Rs)</label><label for="name" class="span2">Quantity(pcs)</label><label for="name" class="span1">Sq Ft</label><label for="name" class="span3">Total Price</label>
							</div>
							<div class="">
								<div class="span3"></div>
									<input type="text"  value="Grand Total" class="span2" disabled>
									<input type="text"  value="0"  name="to_qun" id="to_qun" class="span2" placeholder="Name" disabled>
									<input type="text"  value="0"  name="to_sq" id="to_sq" class="span2" placeholder="Name" disabled>
									<input type="text"  value="0"  name="generated_cost" id="generated_cost" class="span2" placeholder="Name" disabled>
								</div>
							</fieldset>
							<a id="add" class="btn btn-default ">Add Selection Box <i class="icon-plus"></i></a>
							<?php if (count($production) > 0) { ?>
								<input type="hidden" id="id" name="id" value="<?php echo $production['id']; ?>" />
								<?php } ?>

							<input type="hidden" name="<?php echo $this->security->get_csrf_token_name(); ?>" value="<?php echo $this->security->get_csrf_hash(); ?>" />
							<div class="form-actions">
								<input type="submit" class="btn btn-success" value="Save Changes" id="update">
							</div>
						</form>
						<!-- END FORM-->
					</div>
				</div>
				<!-- END SAMPLE FORM PORTLET-->
			</div>
		</div>

		<!-- END PAGE CONTAINER-->
	</div>
	<!-- END PAGE -->
</div>

<script>
$(document).ready(function() {
	var id = $('#id').val();
	if(id){
		$.ajax({
			type: "POST",
			url: "inventory/get_production_selection_item",
			data: {id: id},
			success: function(msg) {
				$("#child").append(msg);
				$('select').select2();
				calculateGrandTotal();
				calculateTotalQuant();
				calculateTotalSq();
			}
		});
	}
	$(document).on("change ,  keyup" , "input[name='sq_ft[]'] , input[name='price[]']" ,function(){
     var parent_element = $(this).closest(".description_text");
     var qty = $(parent_element).find("input[name='sq_ft[]']").val();
     var price = $(parent_element).find("input[name='price[]']").val();
     if( qty.trim() != "" && price.trim() != "")
      {
      	var sum = Math.round(qty * price);
        $(parent_element).find("input[name='total_price[]']").val(sum);
        // $("#generated_cost").val(grandTotal);
      }
      else
      {
        $(parent_element).find("input[name='total_price[]']").val("");
      }
      calculateGrandTotal();
      calculateTotalSq();
      
      
	});

		$(document).on("change ,  keyup" , "input[name='quntit[]']" ,function(){
      		calculateTotalQuant();
      
		});

});
	$(function() {
$('#add').click(function(){
	var newDiv = $('<div class="description_text"><div class="control-group" ><select name="selection_name[]" id="selection_name[]" class="span3 chzn-select" data-placeholder="Select selection name"><option value="0">Selection Name</option><?php foreach($selections as $selection){ ?><option value="<?php echo $selection['id']; ?>"><?php echo $selection['name']; ?></option><?php } ?></select><input type="number" name="price[]" min="0" id="price" step="any" value="0" class="span2 price" placeholder="Prices(Rs)"><input type="number" name="quntit[]" min="0" step="any" id="quntit" value="0" class="span2 quntit" placeholder="Quantity"><input type="number" name="sq_ft[]" min="0" step="any" id="sq_ft" value="0" class="span2 sq_ft" placeholder="Sq Ft"><input type="number" name="total_price[]" min="0" step="any" id="total_price" value="0" class="span2 total_price" placeholder="0" disabled><a href="javascript:void(0);"> Remove Selection <i  class="icon-minus icon-white"></i></a></div></div>');

  newDiv.on("click", "a", function(){
  	$(this).parent().remove();
  	calculateGrandTotal();
  	calculateTotalQuant();
  	calculateTotalSq();
  });
  $('#child').append(newDiv);
  $('select').select2();
	});	
});
	$('#child').on('click','.description_text a',function(){
     $(this).closest('.description_text').remove();
     calculateGrandTotal();
     calculateTotalQuant();
     calculateTotalSq();

});
	function calculateGrandTotal() {
    var grandTotal = 0;
    $("input[name='total_price[]']").each(function() {
        grandTotal += +$(this).val();
    });
    // $("#generated_cost").val(grandTotal.toLocaleString());
    $("#generated_cost").val(grandTotal);
}
function calculateTotalQuant() {
    var to_qun = 0;
    $("input[name='quntit[]']").each(function() {
        to_qun += +$(this).val();
    });
    // $("#generated_cost").val(grandTotal.toLocaleString());
    $("#to_qun").val(to_qun);
}
function calculateTotalSq() {
    var to_sq = 0;
    $("input[name='sq_ft[]']").each(function() {
        to_sq += +$(this).val();
    });
    // $("#generated_cost").val(grandTotal.toLocaleString());
    $("#to_sq").val(to_sq);
}
	$('#update').click(function(){

	var check = true;
	$("select[name='selection_name[]']").each(function() {
	var chemical_name =	$(this).val();
    if(chemical_name == 0){
    	check = false;
		alert('Selection Name must be selected.');
		return false;
	}


	});
		if (!check) {
	    return false;
	}

	$("input[name='price[]']").each(function() {

	var price =	$(this).val();
	
	// total = total + parseInt(quantity);
    if(!price || price == 0){
    	check = false;
		alert('Price must be greater than 0.');
		return false;
	}
	});

	if (!check) {
	    return false;
	}
	var add_quantity = 0;
	$("input[name='quntit[]']").each(function() {

	var sq_ft =	$(this).val();
	
	add_quantity = add_quantity + parseInt(sq_ft);
    if(!sq_ft || sq_ft == 0){
    	check = false;
		alert('Quantity must be greater than 0.');
		return false;
	}

	});
	var avai = $('#avai').val();
	if(parseInt(add_quantity) != parseInt(avai)){
		alert('Sum of all Quantities not less or greater than '+ parseInt(avai));
		return false;
	}
	$("input[name='sq_ft[]']").each(function() {

	var sq_ft =	$(this).val();
	
	// total = total + parseInt(quantity);
    if(!sq_ft || sq_ft == 0){
    	check = false;
		alert('Sq ft must be greater than 0.');
		return false;
	}
	});

	if (!check) {
	    return false;
	}
	var production_cost = $("#production_cost").val();
	var generated_cost = $("#generated_cost").val();
	// var countDecimal = countDecimals(production_cost);

	// production_cost = toTrunc(production_cost, 2); 
	// parseFloat(production_cost).toFixed(2);
	// generated_cost = toTrunc(generated_cost, 2); 
	// parseFloat(generated_cost).toFixed(2);
	// console.log(production_cost);
	// console.log(generated_cost);
	
	if(parseInt(production_cost) != parseInt(generated_cost)){
		alert('Grand Total Price must be equal to Production Cost');
		return false;
	}
	// return false;
	});
		function toTrunc(value,n){  
	    return Math.floor(value*Math.pow(10,n))/(Math.pow(10,n));
		}

		function countDecimals(value) {
	    if (Math.floor(value) !== value)
	        return value.toString().split(".")[1].length || 0;
	    return 0;
		}

	

</script>