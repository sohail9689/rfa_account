<div id="main-content">
	<!-- BEGIN PAGE CONTAINER-->
	<div class="container-fluid">
		<!-- BEGIN PAGE HEADER-->
		<div class="row-fluid">
			<div class="span12">
				<!-- BEGIN PAGE TITLE & BREADCRUMB-->
				<h3 class="page-title">
					Production
				</h3>
				<ul class="breadcrumb">
					<li>
						<a href="dashboard">Dashboard</a>
						<span class="divider">/</span>
					</li>
					<li>
						<a href="inventory">Inventory</a>
						<span class="divider">/</span>
					</li>
					<li>
						<a href="inventory/production_list">Production List</a>
						<span class="divider">/</span>
					</li>
					<li class="active">Update Production Status
					</li>
				</ul>
				<!-- END PAGE TITLE & BREADCRUMB-->
			</div>
		</div>
		<!-- END PAGE HEADER-->

		<!-- BEGIN PAGE CONTENT-->
		<div class="row-fluid">
			<div class="span12">
				<!-- BEGIN SAMPLE FORM PORTLET-->
				<div class="widget blue">
					<div class="widget-title">
						<h4><i class="icon-reorder"></i> Entry Form </h4>
						<span class="tools">
							<a href="javascript:;" class="icon-chevron-down"></a>
						</span>
					</div>
					<div class="widget-body">
						<!-- BEGIN FORM-->
						<form action="inventory/production_status" method="post" class="form-horizontal" id="form-validate">
							<fieldset>
								<div class="span6">
									<div class="control-group">
										<label class="control-label" for="purchase_no">Production No</label>
										<div class="controls">
											<input type="text" name="code" id="code" tabindex="1" class="span10" value="<?php  echo $production['code'] ?>" readonly />
										</div>
									</div>
									<div class="control-group">
										<label class="control-label" for="status">Select Status</label>
										<div class="controls">
											<select name="status" id="status" tabindex="6" class="span10 chzn-select status_" data-form="select2" data-placeholder="Item">

											<option>Select Status</option>
											<option value="Approved" <?php if (count($production) > 0) { if($production['status'] == "Approved"){ echo "selected"; } } ?>>Approved</option>
											<option value="Unapproved" <?php if (count($production) > 0) { if($production['status'] == "Unapproved"){ echo "selected"; } } ?>>Unapproved</option>
											<option value="Locked" <?php if (count($production) > 0) { if($production['status'] == "Locked"){ echo "selected"; } } ?>>Locked</option>
											</select>
										</div>
									</div>
								</div>
								<div class="span6">
									<div class="control-group">
										<label class="control-label" for="purchase_date">Production Date</label>
										<div class="controls">
											<div class="input-append date" >
												<input name="production_date" id="production_date" tabindex="2" data-form="datepicker" size="16" type="text" value="<?php  echo date_to_ui($production['production_date']); ?>" disabled>
												<span class="add-on"><i class="icon-th"></i></span>
											</div>
										</div>
									</div>
								</div>
								<?php if (count($production) > 0) { ?>
								<input type="hidden" id="id" name="id" value="<?php echo $production['id']; ?>" />
								<?php } ?>
							</fieldset>
						
							<div style="clear: both;"></div>
							<div class="center">
								<input type="button" class="btn btn-success" id="update" tabindex="8" value="Update" />
							</div>
						</form>
						<!-- END FORM-->
					</div>
				</div>
				<!-- END SAMPLE FORM PORTLET-->
			</div>
		</div>

		

		<!-- END PAGE CONTAINER-->
	</div>
	<!-- END PAGE -->
</div>
<script type="text/javascript">
$(document).on('click', '#update', function(event) {
var status = $('#status').val();
var id = $('#id').val();
	if(!status){
			alert('Please select staus.');
			return false;
		}

		$.ajax({
			type: "POST",
			url: "inventory/production_status",
			data: {status:status,id:id},
			success: function(msg) {
				window.location.replace(msg);
			}
		});
	});

</script>

