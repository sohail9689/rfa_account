<!DOCTYPE html>
<!--[if IE 8]> <html lang="en" class="ie8"> <![endif]-->
<!--[if IE 9]> <html lang="en" class="ie9"> <![endif]-->
<!--[if !IE]><!--> <html lang="en"> <!--<![endif]-->
<!-- BEGIN HEAD -->
<head>
	<?php $this->load->view('admin/head'); ?>
</head>
<!-- END HEAD -->

<!-- BEGIN BODY -->
<body>

	<!-- BEGIN CONTAINER -->
	<div id="container" class="row-fluid">
	<!-- BEGIN PAGE -->
		<div class="row-fluid">
			<div class="span12">
				<!-- BEGIN BLANK PAGE PORTLET-->
				<div class="widget grey">
					<div class="widget-body">
						<div class="row-fluid">
							<div class="span12">
								<div class="pull-right">
                                    <img src="<?php echo $this->session->userdata('company_logo'); ?>" width="115" class="img">
                                </div>
                                <h3 class="center" style="color: black"><?php echo $this->session->userdata('company_name'); ?></h3>
                                <h5 class="center" style="color: black">Recipe: <?php echo $recipe[0]['recipe_name']; ?></h5>
                                <hr>
							</div>
						</div>
						<div class="space20"></div>
						<div class="row-fluid">
							<div class="invoice-table">
								<fieldset>
									<div class="span12">
										<div id="debit_details">
											<table class="table  table-striped responsive" style="border: 2px solid #000000;">
												<thead>
													
													<tr>
														<th class="left" style="border: 1px solid #000000; color: black">Chemicals</th>
														<th class="left" style="border: 1px solid #000000; color: black">Quantity</th>
														<!-- <th class="left" style="border: 1px solid #000000; color: black">Weight</th> -->
													</tr>
													<!-- <tr>
														<th class="left" style="border: 1px solid #000000; color: black">Total Weight</th>
														<th></th>
														<th class="left" style="border: 1px solid #000000; color: black"><?php echo $recipe[0]['weight'].' Kg';; ?></th> -->
													</tr>
												</thead>
												<tbody>

													<?php 
													$total_quantity = 0;
													// $total_weight = 0;
													foreach ($recipe as $recip) {
													// $obtained_weight = ($recip['quantity'] * $recip['weight'])/100 ;
														?>
													<tr>
													<td class="left" style="border: 1px solid #000000; color: black"><?php echo $recip['item_name']; ?></td>
														<td style="border: 1px solid #000000; color: black"><?php echo $recip['quantity'].' %'; ?></td>
														<!-- <td style="border: 1px solid #000000; color: black"><?php echo $obtained_weight.' Kg'; ?></td> -->
														</tr>
														
															<?php 
															$total_quantity += $recip['quantity'];
															// $total_weight += $obtained_weight;
															} ?>
															
												</tbody>
												<tfoot>
													<tr>
													<td class="left" style="border: 1px solid #000000; color: black"><strong>Total:</strong> </td>
														<td style="border: 1px solid #000000; color: black"><strong><?php echo $total_quantity.' %'; ?></strong></td>
														<!-- <td style="border: 1px solid #000000; color: black"><strong><?php echo $total_weight.' Kg'; ?></strong></td> -->
														</tr>
												</tfoot>
											</table>
										</div>
									</div>
									</fieldset>
							</div>
						</div>
						
						<div class="space20"></div>
						<div class="row-fluid text-center">
							<a class="btn btn-inverse btn-large hidden-print" onclick="javascript:window.print();">Print <i class="icon-print icon-big"></i></a>
						</div>
					</div>
				</div>
				<!-- END BLANK PAGE PORTLET-->
			</div>
		</div>
		<!-- END PAGE -->
	</div>
	<!-- END CONTAINER -->

	<!-- BEGIN FOOTER -->
	<?php $this->load->view('admin/footer'); ?>
	<!-- END FOOTER -->

	<!-- BEGIN JAVASCRIPTS -->
	<?php $this->load->view('admin/js'); ?>
	<!-- END JAVASCRIPTS -->
</body>
<!-- END BODY -->

</html>