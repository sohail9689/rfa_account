<div id="main-content">
	<!-- BEGIN PAGE CONTAINER-->
	<div class="container-fluid">
		<!-- BEGIN PAGE HEADER-->
		<div class="row-fluid">
			<div class="span12">
				<!-- BEGIN PAGE TITLE & BREADCRUMB-->
				<h3 class="page-title">
					Recipes
				</h3>
				<ul class="breadcrumb">
					<li>
						<a href="dashboard">Dashboard</a>
						<span class="divider">/</span>
					</li>
					<li>
						<a href="inventory">Inventory</a>
						<span class="divider">/</span>
					</li>
					<li>
						<a href="inventory/recipe_list">Recipe List</a>
						<span class="divider">/</span>
					</li>
					<li class="active">
						<?php
						if(count($recipe) > 0){
							$chemical = $this->MRecipe->get_chemical_by_recipe_id($recipe['id']);
							// var_dump($chemical[0]['chemical_id']);
							// die;
						}

						 if(count($recipe) > 0){ ?>Edit<?php }else{ ?>Add New<?php } ?> Recipe
					</li>
				</ul>
				<!-- END PAGE TITLE & BREADCRUMB-->
			</div>
		</div>
		<!-- END PAGE HEADER-->
<?php if($this->session->flashdata('success') || $this->session->flashdata('info') || $this->session->flashdata('error')) { ?>
		<!-- BEGIN Alert widget-->
		<div class="row-fluid">
			<div class="span12">
				<?php if($this->session->flashdata('success')) { ?>
				<div class="alert alert-success">
					<button class="close" data-dismiss="alert">×</button>
					<strong>Success!</strong> <?php echo $this->session->flashdata('success'); ?>
				</div>
				<?php } ?>
				<?php if($this->session->flashdata('info')) { ?>
				<div class="alert alert-info">
					<button class="close" data-dismiss="alert">×</button>
					<strong>Info!</strong> <?php echo $this->session->flashdata('info'); ?>
				</div>
				<?php } ?>
				<?php if($this->session->flashdata('error')) { ?>
				<div class="alert alert-error">
					<button class="close" data-dismiss="alert">×</button>
					<strong>Error!</strong> <?php echo $this->session->flashdata('error'); ?>
				</div>
				<?php } ?>
			</div>
		</div>
		<!-- END Alert widget-->
		<?php } ?>
		<!-- BEGIN PAGE CONTENT-->
		<div class="row-fluid">
			<div class="span12">
				<!-- BEGIN SAMPLE FORM PORTLET-->
				<div class="widget blue">
					<div class="widget-title">
						<h4><i class="icon-reorder"></i> Entry Form </h4>
						<span class="tools">
							<a href="javascript:;" class="icon-chevron-down"></a>
						</span>
					</div>
					<div class="widget-body">
						<!-- BEGIN FORM-->
						<form action="inventory/recipe_save" method="post" class="form-horizontal form-bordered form-validate" id="frmCustomer">
							<div class="control-group">
								<label for="code" class="control-label">Recipe Sr #</label>
								<div class="controls">
									<input type="number" name="code" id="code" value="<?php if(count($recipe) > 0){ echo $recipe['code']; } else { echo $code; } ?>" class="span5" placeholder="Customer Code">
								</div>
							</div>
							<div class="control-group">
								<label for="name" class="control-label">Recipe Name</label>
								<div class="controls">
									<input type="text" name="name" id="name" value="<?php if(count($recipe) > 0){ echo $recipe['name']; } ?>" class="span5" placeholder="Recipe Name">
								</div>
							</div>
							<div class="control-group">
								<label for="name" class="span4">Chemical Name</label>
								<label for="name" class="span5">Quantity %</label>
								<select name="chemical_name[]" id="chemical_name[]" class="span4 chzn-select chemical_name" data-placeholder="Select Chemical">
								<option value="0">Select Chemical</option>
									<?php foreach($items as $item){ ?>
										<option value="<?php echo $item['id']; ?>" <?php 
										if(count($recipe) > 0){ 
												if($item['id'] == $chemical[0]['chemical_id']){
													echo "selected";
											}
										}
										?>><?php echo $item['name']; ?></option>
										<?php } ?>
									</select>
								<input type="number" name="quantity[]" id="name" value="<?php echo $chemical[0]['quantity']?>" min="0" class="span3" placeholder="Quantity" step="any">
							</div>

							<div id="child"></div>
							<a id="add" class="btn btn-default">Add more chemicals <i class="icon-plus"></i></a>
							<br>
							<br>
							<div class="control-group">
								<label for="status" class="control-label">Select Status</label>
								<div class="controls">
									<select name="status" id="status" class="span5 chzn-select" data-placeholder="Select Status">
										<option value="0" <?php if (count($recipe) > 0 && $recipe['status'] == '0') { echo 'selected'; }?>>Active</option>
										<option value="1" <?php if (count($recipe) > 0 && $recipe['status'] == '1') { echo 'selected'; }?>>Inactive</option>
									</select>
								</div>
							</div>
							<input type="hidden" name="<?php echo $this->security->get_csrf_token_name(); ?>" value="<?php echo $this->security->get_csrf_hash(); ?>" />
							<?php if(count($recipe) > 0){ ?>
							<input type="hidden" name="id" id='id' value="<?php echo $recipe['id']; ?>" />
							<?php } ?>
							<div class="form-actions">
								<input type="submit" class="btn btn-success" value="Save Changes" id="save">
							</div>
						</form>
						<!-- END FORM-->
					</div>
				</div>
				<!-- END SAMPLE FORM PORTLET-->
			</div>
		</div>

		<!-- END PAGE CONTAINER-->
	</div>
	<!-- END PAGE -->
</div>

<script>
$(document).ready(function() {
	var id = $('#id').val();
	if(id){


		$.ajax({
			type: "POST",
			url: "inventory/get_chemiclas",
			data: {id: id},
			success: function(msg) {
				$("#child").append(msg);
				$('select').select2();
			}
		});
	}
	

});
$(function() {
$('#add').click(function(){
	var newDiv = $('<div ><div class="control-group" ><select name="chemical_name[]" id="chemical_name[]" class="span4 chzn-select " data-placeholder="Select Chemical"><option value="0">Select Item</option><?php foreach($items as $item){ ?><option value="<?php echo $item['id']; ?>"><?php echo $item['name']; ?></option><?php } ?></select><input type="number" name="quantity[]" min="0" id="name" value="" step="any" class="span3" placeholder="Quantity"><a href="javascript:void(0);"> Remove Chemical <i  class="icon-minus icon-white"></i></a></div></div>');

  newDiv.on("click", "a", function(){$(this).parent().remove();});
  $('#child').append(newDiv);
  $('select').select2();
});
});
$('#child').on('click','.description_text a',function(){
     $(this).closest('.description_text').remove();
});
$('#save').click(function(){

	var name = $('#name').val();
	if(!name){
		alert('Name cannot be empty.');
		return false;
	}
	var check = true;
	$("select[name='chemical_name[]']").each(function() {
	var chemical_name =	$(this).val();
    if(chemical_name == 0){
    	check = false;
		alert('Chemical Name must be selected.');
		return false;
	}
});
	if (!check) {
    return false;
}

$("input[name='quantity[]']").each(function() {

	var quantity =	$(this).val();
    if(!quantity || quantity == 0){
    	check = false;
		alert('Quantity must be greater than 0.');
		return false;
	}
});

if (!check) {
    return false;
}

});


</script>
