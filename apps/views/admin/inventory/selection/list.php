<div id="main-content">
	<!-- BEGIN PAGE CONTAINER-->
	<div class="container-fluid">
		<!-- BEGIN PAGE HEADER-->   
		<div class="row-fluid">
			<div class="span12">
				<!-- BEGIN PAGE TITLE & BREADCRUMB-->
				<h3 class="page-title">
					Selection Item
				</h3>
				<ul class="breadcrumb">
					<li>
						<a href="dashboard">Dashboard</a>
						<span class="divider">/</span>
					</li>
					<li>
						<a href="inventory">Inventory</a>
						<span class="divider">/</span>
					</li>
					<li class="active">
						Selection Item List
					</li>
				</ul>
				<!-- END PAGE TITLE & BREADCRUMB-->
			</div>
		</div>
		<!-- END PAGE HEADER-->
		<?php if($this->session->flashdata('success') || $this->session->flashdata('info') || $this->session->flashdata('error')) { ?>
		<!-- BEGIN Alert widget-->
		<div class="row-fluid">
			<div class="span12">
				<?php if($this->session->flashdata('success')) { ?>
				<div class="alert alert-success">
					<button class="close" data-dismiss="alert">×</button>
					<strong>Success!</strong> <?php echo $this->session->flashdata('success'); ?>
				</div>
				<?php } ?>
				<?php if($this->session->flashdata('info')) { ?>
				<div class="alert alert-info">
					<button class="close" data-dismiss="alert">×</button>
					<strong>Info!</strong> <?php echo $this->session->flashdata('info'); ?>
				</div>
				<?php } ?>
				<?php if($this->session->flashdata('error')) { ?>
				<div class="alert alert-error">
					<button class="close" data-dismiss="alert">×</button>
					<strong>Error!</strong> <?php echo $this->session->flashdata('error'); ?>
				</div>
				<?php } ?>
			</div>
		</div>
		<!-- END Alert widget-->
		<?php } ?>
		<!-- BEGIN ADVANCED TABLE widget-->
		<div class="row-fluid right">
			<a class="btn btn-primary" href="inventory/selection_save"><i class="icon-plus icon-white"></i> Add New</a>
		</div>
		<br>
		<div class="row-fluid">
			<div class="span12">
				<!-- BEGIN EXAMPLE TABLE widget-->
				<div class="widget blue">
					<div class="widget-title">
						<h4><i class="icon-reorder"></i> List All </h4>
						<span class="tools">
							<a href="javascript:;" class="icon-chevron-down"></a>
						</span>
					</div>
					<div class="widget-body">
						<table class="table table-striped table-bordered" id="sample_1">
							<thead>
								<tr>
								<th class="hidden"></th>
									<th>Type</th>
									<th>Name</th>
									<th>Price</th>
									<th>Status</th>
									<?php if($this->session->userdata['user_type'] == 'Admin'){?>		<th class="center span3">Action</th><?php }?>
								</tr>
							</thead>
							<tbody>
								<?php
								foreach ($selection as $selection) {
									?>
									<tr>
									<td class="hidden"></td>
										<td align="left"><?php echo $selection['type']; ?></td>
										<td align="left"><?php echo $selection['name']; ?></td>
										<td align="left"><?php echo $selection['price']; ?></td>
										<td align="left"><?php if($selection['status'] == 0){ echo 'Active'; }else{ echo 'Inactive'; }  ?></td>
										<?php if($this->session->userdata['user_type'] == 'Admin'){?>
										<td class="actions center">
											<a class="btn btn-edit" href="inventory/selection_save/<?php echo $selection['id']; ?>"><i class="icon-edit icon-white"></i> Edit</a>
											<a class="btn btn-danger del" href="inventory/selection_delete/<?php echo $selection['id']; ?>"><i class="icon-trash icon-white"></i> Delete</a>
										</td>
										<?php }?>
									</tr>
									<?php
								}
								?>
							</tbody>
						</table>
					</div>
				</div>
				<!-- END EXAMPLE TABLE widget-->
			</div>
		</div>

		<!-- END ADVANCED TABLE widget-->
	</div>
	<!-- END PAGE CONTAINER-->
</div>