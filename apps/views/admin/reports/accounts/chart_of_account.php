<!DOCTYPE html>
<html lang="en">
<head>
    <base href="<?php echo base_url(); ?>" />
    <meta charset="utf-8">
    <title>Chart Of Account</title>
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta name="description" content="">
    <meta name="author" content="Tapan Kumer Das : InnovativeBD">
     <link rel="shortcut icon" href="<?php echo $this->session->userdata('company_logo');?>" type="image/x-icon" />

    <!-- styles -->
   <link href="assets/backend/assets/bootstrap/css/bootstrap.min.css" rel="stylesheet" />
    <link href="assets/backend/assets/bootstrap/css/bootstrap-responsive.min.css" rel="stylesheet" />
    <link href="assets/backend/css/style-responsive.css" rel="stylesheet" />

    <link href="assets/plugin/uniform/themes/default/css/uniform.default.css" rel="stylesheet" type="text/css" />
    <link href="assets/plugin/bootstrap-table.min.css" rel="stylesheet">
    <link href="assets/plugin/bootstrap-table.css" rel="stylesheet">
    <link rel="stylesheet" type="text/css" href="assets/plugin/bootstrap-datepicker-master/dist/css/bootstrap-datepicker.css" />

    <script src='assets/vendors/echarts/test/lib/jquery.min.js'></script>
    <script src="assets/vendors/datatables.net/js/jquery.dataTables.min.js"></script>
    <script type="text/javascript" src="assets/plugin/bootstrap-datepicker-master/js/bootstrap-datepicker.js"></script>
    <script type="text/javascript" src="assets/plugin/filter_column.js"></script>
    <script src="assets/plugin/bootstrap-table-export.js"></script>
    <script type="text/javascript" src="assets/plugin/tableExport.js"></script>
    <script type="text/javascript" src="assets/backend/assets/bootstrap/js/bootstrap.min.js"></script>

    <style>
        @media print{
            p.muted{
                font-weight: bold;
            }
            small.small{
                font-weight: normal;
            }
        }
    </style>
 
</head>
<body>
    <!-- section content -->
    <section class="section">
        <div class="container">
            <!-- span content -->
            <div class="span12">
                <!-- content -->
                <div class="content" style="border: 1px solid #d7d7d7;">
                    <!-- content-body -->
                    <div class="content-body">
                        <!-- invoice -->
                        <div id="invoice-container" class="invoice-container">
                            <div class="page-header">
                                <div class="pull-left">
                                    <img src="<?php echo $this->session->userdata('company_logo'); ?>" width="250" class="img">
                                    <h5 class="center">Chart Of Account</h5>
                                </div>
                                <!-- <h5 class="center"><?php echo $this->session->userdata('company_name'); ?></h5> -->
                                 
                            </div>
    <div class="table-responsive" id="demo_s">
    <table id="demo-table" class="table table-striped" data-url="report/chart_list_data/<?php echo $report_type; ?>/<?php echo $is_active; ?>" data-side-pagination="server"  data-page-list="[5, 10, 20, 50, 100, 200, 500, 1000]" data-pagination="true"  data-show-refresh="true" data-search="false" data-show-export="true" data-show-toggle="false" data-show-columns="true" data-filter-control="true" data-filter-show-clear="true" data-show-footer="true">
        
        <thead>
            <tr>
                <th data-field="code" data-align="left"  data-filter-control="input" data-sortable="true">
                    <?php echo "Code";?>
                </th>
                <th data-field="name" data-align="left"  data-filter-control="input" data-sortable="true">
                    <?php echo "Name";?>
                </th>
                <th data-field="opening" data-align="center" data-footer-formatter="totalFormatter" data-filter-control="input" data-sortable="true">
                    <?php echo "Opening Balance";?>
                </th>
                <th data-field="status" data-align="center" data-filter-control="input" data-sortable="true">
                    <?php echo "Status";?>
                </th>
            </tr>
        </thead>
    </table>
                        </div>
                        </div>
                        <!--/invoice-->
                    </div><!--/content-body -->
                </div><!-- /content -->
            </div><!-- /span content -->

        </div><!-- /container -->
    </section>

</body>
</html>
<script type="text/javascript">
var message1 = 'Active';
var message2 = 'Inactive';
var module = 'Chart Of Account';
function totalFormatter(data) {

  var total = 0;
  // console.log(data);
  if (data.length > 0) {

    var field = this.field;

    total = data.reduce(function(sum, row) {
      return sum + (+row[field]);
    }, 0);

    return  total.toLocaleString();
  }

  return '';
};

function totalFormatter1(data) {

  var total = 0;
  // var regex = /[+-]?\d+(\.\d+)?/g;
  //   var data = str.match(regex).map(function(v) { return parseFloat(v); });

  if (data.length > 0) {

    var field = this.field;

    total = data.reduce(function(sum, row) {
        // console.log(parseFloat(row[field]));
      return sum + (+parseFloat(row[field]));
    }, 0);

    return  total.toLocaleString();
  }

  return '';
};
    $(document).ready(function(){
        $('#demo-table').bootstrapTable({
        }).on('all.bs.table', function (e, name, args) {

        }).on('click-row.bs.table', function (e, row, $element) {
            
        }).on('dbl-click-row.bs.table', function (e, row, $element) {
            
        }).on('sort.bs.table', function (e, name, order) {
            
        }).on('check.bs.table', function (e, row) {
            
        }).on('uncheck.bs.table', function (e, row) {
            
        }).on('check-all.bs.table', function (e) {
            
        }).on('uncheck-all.bs.table', function (e) {
            
        }).on('load-success.bs.table', function (e, data) {
        }).on('load-error.bs.table', function (e, status) {
            
        }).on('column-switch.bs.table', function (e, field, checked) {
            
        }).on('page-change.bs.table', function (e, size, number) {
            //alert('1');
            //set_switchery();
        }).on('search.bs.table', function (e, text) {
            
        });
    });
    </script>