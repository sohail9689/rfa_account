<!DOCTYPE html>
<html lang="en">
<head>
    <base href="<?php echo base_url(); ?>" />
    <meta charset="utf-8">
    <title>Chart Of Account</title>
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta name="description" content="">
    <meta name="author" content="Tapan Kumer Das : InnovativeBD">
     <link rel="shortcut icon" href="<?php echo $this->session->userdata('company_logo');?>" type="image/x-icon" />

    <!-- styles -->
    <link rel="stylesheet" href="assets/bootstrap.min.css">
      <!-- styles -->
    <link href="assets/backend/assets/bootstrap/css/bootstrap.min.css" rel="stylesheet" />
    <link href="assets/backend/assets/bootstrap/css/bootstrap.min.css" rel="stylesheet" />
    <link href="assets/backend/css/stilearn.css" rel="stylesheet" />
    <link href="assets/vendors/datatables.net-bs/css/dataTables.bootstrap.min.css" rel="stylesheet"/>
    <link href="assets/vendors/datatables.net-buttons-bs/css/buttons.bootstrap.min.css" rel="stylesheet"/>
    <link href="assets/vendors/datatables.net-fixedheader-bs/css/fixedHeader.bootstrap.min.css" rel="stylesheet"/>
    <link href="assets/vendors/datatables.net-responsive-bs/css/responsive.bootstrap.min.css" rel="stylesheet"/>
    <link href="assets/vendors/datatables.net-scroller-bs/css/scroller.bootstrap.min.css" rel="stylesheet"/>
<!-- Datatables -->
    <script src='assets/vendors/echarts/test/lib/jquery.min.js'></script>
    <script src="assets/vendors/datatables.net/js/jquery.dataTables.min.js"></script>
    <script src="assets/vendors/datatables.net-bs/js/dataTables.bootstrap.min.js"></script>
    <script src="assets/vendors/datatables.net-buttons/js/dataTables.buttons.min.js"></script>
    <script src="assets/vendors/datatables.net-buttons-bs/js/buttons.bootstrap.min.js"></script>
    <script src="assets/vendors/datatables.net-buttons/js/buttons.flash.min.js"></script>
    <script src="assets/vendors/datatables.net-buttons/js/buttons.html5.min.js"></script>
    <script src="assets/vendors/datatables.net-buttons/js/buttons.print.min.js"></script>
    <script src="assets/vendors/datatables.net-fixedheader/js/dataTables.fixedHeader.min.js"></script>
    <script src="assets/vendors/datatables.net-keytable/js/dataTables.keyTable.min.js"></script>
    <script src="assets/vendors/datatables.net-responsive/js/dataTables.responsive.min.js"></script>
    <script src="assets/vendors/datatables.net-responsive-bs/js/responsive.bootstrap.js"></script>
    <script src="assets/vendors/datatables.net-scroller/js/datatables.scroller.min.js"></script>
    <script src="assets/vendors/jszip/dist/jszip.min.js"></script>
    <script src="assets/vendors/pdfmake/build/pdfmake.min.js"></script>
    <script src="assets/vendors/pdfmake/build/vfs_fonts.js"></script>
    <style>
        @media print{
            p.muted{
                font-weight: bold;
            }
            small.small{
                font-weight: normal;
            }
        }
    </style>
 
</head>
    <script>
      $(document).ready(function() {
  var table = $('#datatable-responsive').DataTable({
        "lengthMenu": [[10, 25, 50,100, -1], [10, 25, 50,100, "All"]]
    });
 
 var buttons = new $.fn.dataTable.Buttons(table, {
     buttons: [
       'copy',
       'csv',
       'excel',
       'pdf',
     'print'
    ]

 }).container().appendTo($('#buttons'));
 });
 </script>
<body>
    <!-- section content -->
    <section class="section">
        <div class="container">
            <!-- span content -->
            <div class="span12">
                <!-- content -->
                <div class="content" style="border: 1px solid #d7d7d7;">
                    <!-- content-body -->
                    <div class="content-body">
                        <!-- invoice -->
                        <div id="invoice-container" class="invoice-container">
                            <div class="page-header">
                                <div class="pull-right">
                                    <img src="<?php echo $this->session->userdata('company_logo'); ?>" width="115" class="img">
                                </div>
                                <h5 class="center"><?php echo $this->session->userdata('company_name'); ?></h5>
                                
                                 <h5 class="center">Chart Of Account</h5>
                            </div>
                            
                            <button id="buttons"></button>
                            <div class="invoice-table">
                                <table class="table table-striped table-bordered bootstrap-datatable datatable" id="datatable-responsive">
                                    <thead>
                                    <tr>
                                        <th>#</th>
                                        <th>Code of A/C</th>
                                        <th>Name of A/C</th>
                                        <th class="center">Opening Balance</th>
                                        <th class="center">Created</th>
                                        <th class="center">Status</th>
                                     </tr>
                                    </thead>
                                   
                                    <tbody>
                                    <?php $i=1; ?>
                                       <?php foreach($charts as $chart): ?>
                                    <tr>
                                        <td><?php echo $i; ?></td>
                                        <td><?php echo $chart['code']; ?></td>
                                        <td><?php echo $chart['name']; ?></td>
                                        <td class="right"><?php echo number_format($chart['opening'], 2); ?></td>
                                        <td class="right"><?php echo date('jS M, Y ', strtotime($chart['created_at'])); ?></td>
                                        <td class="center"><?php if($chart['status']== 'Active'){ ?><span class="label label-success">Active</span><?php }else{ ?><span class="label label-important">Inactive</span><?php } ?></td>
                                        
                                    </tr>
                                <?php 
                                    $i++;
                                endforeach; ?>

                                    </tbody>
                                </table>
                            </div>
                        </div>
                        <!--/invoice-->
                    </div><!--/content-body -->
                </div><!-- /content -->
            </div><!-- /span content -->

        </div><!-- /container -->
    </section>

</body>
</html>