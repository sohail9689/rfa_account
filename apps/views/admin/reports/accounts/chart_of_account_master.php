<div id="main-content">
	<!-- BEGIN PAGE CONTAINER-->
	<div class="container-fluid">
		<!-- BEGIN PAGE HEADER-->
		<div class="row-fluid">
			<div class="span12">
				<!-- BEGIN PAGE TITLE & BREADCRUMB-->
				<h3 class="page-title">
					Chart Of Account Reports
				</h3>
				<ul class="breadcrumb">
					<li>
						<a href="dashboard">Dashboard</a>
						<span class="divider">/</span>
					</li>
					<li>
						<a href="report">Report</a>
						<span class="divider">/</span>
					</li>
					<li class="active">
						Chart Of Account Reports
					</li>
				</ul>
				<!-- END PAGE TITLE & BREADCRUMB-->
			</div>
		</div>
		<!-- END PAGE HEADER-->

		<!-- BEGIN PAGE CONTENT-->
		<div class="row-fluid">
			<div class="span12">
				<!-- BEGIN SAMPLE FORM PORTLET-->
				<div class="widget blue">
					<div class="widget-title">
						<h4><i class="icon-reorder"></i> Chart Of Account Reports </h4>
						<span class="tools">
							<a href="javascript:;" class="icon-chevron-down"></a>
						</span>
					</div>

					<div class="widget-body">
						<!-- BEGIN FORM-->
						<form class="form-horizontal" id="form-validate" action="report/chart_of_account" method="post" target="_blank">
							<fieldset>
							<div class="control-group">
									<label class="control-label" for="payment_type">Report Type</label>
									<div class="controls">
										<select name="report_type" id="report_type" class="span5 chzn-select voucher_type" data-placeholder="Select A/C Head">
											<option value="all">All</option>
											<option value="ob">With Openning Balance</option>
											<option value="zob">With Zero Openning Balance</option>
										</select>
									</div>
								</div>
								<div class="control-group">
									<label class="control-label" for="payment_type">Active/InActive</label>
									<div class="controls">
										<select name="is_active" id="is_active" class="span5 chzn-select status_" data-placeholder="Select A/C Head">
											<option value="Active">Active</option>
											<option value="Inactive">InActive</option>
										</select>
									</div>
								</div>
								
								<input type="hidden" name="<?php echo $this->security->get_csrf_token_name(); ?>" value="<?php echo $this->security->get_csrf_hash(); ?>" />
								<div class="form-actions">
									<input type="submit" class="btn btn-success" value="View Report" />
								</div>
							</fieldset>
						</form>
						<!-- END FORM-->
					</div>
				</div>
				<!-- END SAMPLE FORM PORTLET-->
			</div>
		</div>
	<div class="row-fluid">
			<div class="span12">
				<!-- BEGIN SAMPLE FORM PORTLET-->
				<div class="widget blue">
					<div class="widget-title">
						<h4><i class="icon-reorder"></i> Level wise Reports </h4>
						<span class="tools">
							<a href="javascript:;" class="icon-chevron-down"></a>
						</span>
					</div>

					<div class="widget-body">
						<!-- BEGIN FORM-->
						<form class="form-horizontal" id="form-validate" action="report/chart_of_account_level" method="post" target="_blank">
							<fieldset>
							<div class="control-group">
									<label class="control-label" for="payment_type">Select Level</label>
									<div class="controls">
										<select name="report_type" id="report_type" class="span5 chzn-select voucher_type" data-placeholder="Select A/C Head">
											<option value="all">All</option>
											<option value="1">Level 1</option>
											<option value="2">Level 2</option>
											<option value="3">Level 3</option>
											<option value="4">Level 4</option>
											<option value="5">Level 5</option>
											<option value="6">Level 6</option>
										</select>
									</div>
								</div>
								
								<input type="hidden" name="<?php echo $this->security->get_csrf_token_name(); ?>" value="<?php echo $this->security->get_csrf_hash(); ?>" />
								<div class="form-actions">
									<input type="submit" class="btn btn-success" value="View Report" />
								</div>
							</fieldset>
						</form>
						<!-- END FORM-->
					</div>
				</div>
				<!-- END SAMPLE FORM PORTLET-->
			</div>
		</div>
		<!-- END PAGE CONTAINER-->
	</div>
	<!-- END PAGE -->
</div>