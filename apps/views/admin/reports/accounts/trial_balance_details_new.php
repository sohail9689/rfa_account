<!DOCTYPE html>
<html lang="en">
<head>
    <base href="<?php echo base_url(); ?>" />
    <meta charset="utf-8">
    <title>Trial Balance From: <?php echo $start_date; ?> To: <?php echo $end_date; ?></title>
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta name="description" content="">
    <meta name="author" content="Cybextech">
     <link rel="shortcut icon" href="<?php echo $this->session->userdata('company_logo');?>" type="image/x-icon" />
 <link href="assets/backend/assets/bootstrap/css/bootstrap.min.css" rel="stylesheet" />
    <link href="assets/backend/assets/bootstrap/css/bootstrap-responsive.min.css" rel="stylesheet" />
    <link href="assets/backend/css/style-responsive.css" rel="stylesheet" />

    <link href="assets/plugin/uniform/themes/default/css/uniform.default.css" rel="stylesheet" type="text/css" />
    <link href="assets/plugin/bootstrap-table.min.css" rel="stylesheet">
    <link href="assets/plugin/bootstrap-table.css" rel="stylesheet">
    <link rel="stylesheet" type="text/css" href="assets/plugin/bootstrap-datepicker-master/dist/css/bootstrap-datepicker.css" />

    <script src='assets/vendors/echarts/test/lib/jquery.min.js'></script>
    <script src="assets/vendors/datatables.net/js/jquery.dataTables.min.js"></script>
    <script type="text/javascript" src="assets/plugin/bootstrap-datepicker-master/js/bootstrap-datepicker.js"></script>
    <script type="text/javascript" src="assets/plugin/filter_column.js"></script>
    <script src="assets/plugin/bootstrap-table-export.js"></script>
    <script type="text/javascript" src="assets/plugin/tableExport.js"></script>
    <script type="text/javascript" src="assets/backend/assets/bootstrap/js/bootstrap.min.js"></script>

    <style>
        @media print{
            p.muted{
                font-weight: bold;
            }
            small.small{
                font-weight: normal;
            }
        }
    </style>
</head>

<body>
    <!-- section content -->
    <section class="section">
        <div class="container">
            <!-- span content -->
            <div class="span12">
                <!-- content -->
                <div class="content" >
                    <!-- content-body -->
                    <div class="content-body">
                        <!-- invoice -->
                        <div id="invoice-container" class="invoice-container">
                            <div class="page-header">
                               <h2 class="center" style="color: #000;">
                                <img src="<?php echo $this->session->userdata('company_logo'); ?>" width="50" class="img"><b><?php echo $this->session->userdata('company_name'); ?><b></h2>
                                <br>
                            </div>
                            <div class="row-fluid">
                            <div class="span5"></div>
                                <div class="span4">
                                   <strong>Trial Balance</strong>
                                </div>
                            </div>
                            <div class="row-fluid">
                                <div class="span1">
                                    <p class="muted">Date From</p>
                                    <p><?php echo date('d/m/Y ', strtotime(date_to_db($start_date))); ?></p>
                                </div>
                                <div class="span10"></div>
                                <div class="span1">
                                    <p class="muted">Date To</p>
                                    <p><?php echo date('d/m/Y ', strtotime(date_to_db($end_date))); ?></p>
                                </div>
                            </div>
    <div class="table-responsive" id="demo_s">
    <table id="demo-table" class="table table-striped" data-url="report/trial_balance_list_data/<?php echo strtotime(date_to_db($start_date)); ?>/<?php echo strtotime(date_to_db($end_date)); ?>" data-side-pagination="server"  data-page-list="[5, 10, 20, 50, 100, 200, 500, 1000]" data-pagination="true"  data-show-refresh="true" data-search="false" data-show-export="true" data-show-toggle="false" data-show-columns="true" data-filter-control="true" data-filter-show-clear="true" data-show-footer="true">
        
        <thead>
            <tr>
                <th data-field="chart_code" data-align="left"  data-filter-control="input" data-sortable="true">
                    <?php echo "Code";?>
                </th>
                <th data-field="chart_name" data-align="center"  data-filter-control="input">
                    <?php echo "Name";?>
                </th>
                <th data-field="chart_opening_debit" data-align="center" data-footer-formatter="totalFormatter">
                    <?php echo "Opening Dr.";?>
                </th>
                <th data-field="chart_opening_credit" data-align="center" data-footer-formatter="totalFormatter">
                    <?php echo "Opening Cr.";?>
                </th>
                 <th data-field="during_period_debit" data-align="center" data-footer-formatter="totalFormatter">
                    <?php echo "During Period Dr.";?>
                </th>
                 <th data-field="during_period_credit" data-align="center" data-footer-formatter="totalFormatter">
                    <?php echo "During Period Cr.";?>
                </th>
                <th data-field="closing_debit" data-align="center" data-footer-formatter="totalFormatter">
                    <?php echo "Closing Dr.";?>
                </th>
                 <th data-field="closing_credit" data-align="center" data-footer-formatter="totalFormatter">
                    <?php echo "Closing Cr.";?>
                </th>
            </tr>
        </thead>
    </table>
                        </div>
                    </div>
                    <!--/invoice-->
                </div><!--/content-body -->
            </div><!-- /content -->
        </div><!-- /span content -->

    </div><!-- /container -->
</section>

</body>
</html>
<script type="text/javascript">
var message1 = 'Active';
var message2 = 'Inactive';
var module = 'Trial Balance Between '+'<?php echo $start_date; ?>'+" - "+'<?php echo $end_date; ?>';
function totalFormatter(data) {

  var total = 0;
  // console.log(data);
  if (data.length > 0) {

    var field = this.field;

    total = data.reduce(function(sum, row) {
      return sum + (+row[field]);
    }, 0);

    return  total.toLocaleString();
  }

  return '';
};

function totalFormatter1(data) {

  var total = 0;
  // var regex = /[+-]?\d+(\.\d+)?/g;
  //   var data = str.match(regex).map(function(v) { return parseFloat(v); });

  if (data.length > 0) {

    var field = this.field;

    total = data.reduce(function(sum, row) {
        // console.log(parseFloat(row[field]));
      return sum + (+parseFloat(row[field]));
    }, 0);

    return  total.toLocaleString();
  }

  return '';
};
    $(document).ready(function(){
        $('#demo-table').bootstrapTable({
        }).on('all.bs.table', function (e, name, args) {

        }).on('click-row.bs.table', function (e, row, $element) {
            
        }).on('dbl-click-row.bs.table', function (e, row, $element) {
            
        }).on('sort.bs.table', function (e, name, order) {
            
        }).on('check.bs.table', function (e, row) {
            
        }).on('uncheck.bs.table', function (e, row) {
            
        }).on('check-all.bs.table', function (e) {
            
        }).on('uncheck-all.bs.table', function (e) {
            
        }).on('load-success.bs.table', function (e, data) {
        }).on('load-error.bs.table', function (e, status) {
            
        }).on('column-switch.bs.table', function (e, field, checked) {
            
        }).on('page-change.bs.table', function (e, size, number) {
            //alert('1');
            //set_switchery();
        }).on('search.bs.table', function (e, text) {
            
        });
    });
    </script>