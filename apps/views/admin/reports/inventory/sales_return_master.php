
<div id="main-content">
	<!-- BEGIN PAGE CONTAINER-->
	<div class="container-fluid">
		<!-- BEGIN PAGE HEADER-->
		<div class="row-fluid">
			<div class="span12">
				<!-- BEGIN PAGE TITLE & BREADCRUMB-->
				<h3 class="page-title">
					Report
				</h3>
				<ul class="breadcrumb">
					<li>
						<a href="dashboard">Dashboard</a>
						<span class="divider">/</span>
					</li>
					<li>
						<a href="report">Report</a>
						<span class="divider">/</span>
					</li>
					<li class="active">
						Delivery Notes Return
					</li>
				</ul>
				<!-- END PAGE TITLE & BREADCRUMB-->
			</div>
		</div>
		<!-- END PAGE HEADER-->

		<!-- BEGIN PAGE CONTENT-->
		<div class="row-fluid">
			<div class="span12">
				<!-- BEGIN SAMPLE FORM PORTLET-->
				<div class="widget blue">
					<div class="widget-title">
						<h4><i class="icon-reorder"></i> Delivery Notes Return Report </h4>
						<span class="tools">
							<a href="javascript:;" class="icon-chevron-down"></a>
						</span>
					</div>
					<div class="widget-body">
						<!-- BEGIN FORM-->
						<form action="report/sales_return_report" method="post" target="_blank" class="form-horizontal form-bordered form-validate" id="frm1">
                            <div class="row-fluid multiple-column">
                                <div class="span6">
                                    <div class="control-group">
                                        <label for="start_date" class="control-label">Start Date</label>
                                        <div class="controls">
                                            <div class="input-append date" data-form="datepicker" data-date="<?php echo date('01/m/Y'); ?>" data-date-format="dd/mm/yyyy">
                                                <input name="start_date" id="inputDate" class="grd-white" data-form="datepicker" size="16" type="text" value="<?php echo date('01/m/Y'); ?>" />
                                                <span class="add-on"><i class="icon-th"></i></span>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                                <div class="span6">
                                    <div class="control-group">
                                        <label for="end_date" class="control-label">End Date</label>
                                        <div class="controls">
                                            <div class="input-append date" data-form="datepicker" data-date="<?php echo date('t/m/Y'); ?>" data-date-format="dd/mm/yyyy">
                                                <input name="end_date" id="inputDate" class="grd-white" data-form="datepicker" size="16" type="text" value="<?php echo date('t/m/Y'); ?>" />
                                                <span class="add-on"><i class="icon-th"></i></span>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <input type="hidden" name="<?php echo $this->security->get_csrf_token_name(); ?>" value="<?php echo $this->security->get_csrf_hash(); ?>" />
                            <div class="form-actions">
                                <button type="submit" class="btn btn-success">Show Report</button>
                                <button type="reset" class="btn">Cancel</button>
                            </div>
                        </form>
                        <!-- END FORM-->
                    </div>
                </div>
                <!-- END SAMPLE FORM PORTLET-->
            </div>
        </div>

        <!-- END PAGE CONTAINER-->
    </div>
    <!-- END PAGE -->
</div>