<div id="main-content">
	<!-- BEGIN PAGE CONTAINER-->
	<div class="container-fluid">
		<!-- BEGIN PAGE HEADER-->   
		<div class="row-fluid">
			<div class="span12">
				<!-- BEGIN PAGE TITLE & BREADCRUMB-->
				<h3 class="page-title">
					A/C Head
				</h3>
				<ul class="breadcrumb">
					<li>
						<a href="dashboard">Dashboard</a>
						<span class="divider">/</span>
					</li>
					<li>
						<a href="settings">Settings</a>
						<span class="divider">/</span>
					</li>
					<li class="active">
						Default A/C Head List
					</li>
				</ul>
				<!-- END PAGE TITLE & BREADCRUMB-->
			</div>
		</div>
		<!-- END PAGE HEADER-->
		<?php if($this->session->flashdata('success') || $this->session->flashdata('info') || $this->session->flashdata('error')) { ?>
		<!-- BEGIN Alert widget-->
		<div class="row-fluid">
			<div class="span12">
				<?php if($this->session->flashdata('success')) { ?>
				<div class="alert alert-success">
					<button class="close" data-dismiss="alert">×</button>
					<strong>Success!</strong> <?php echo $this->session->flashdata('success'); ?>
				</div>
				<?php } ?>
				<?php if($this->session->flashdata('info')) { ?>
				<div class="alert alert-info">
					<button class="close" data-dismiss="alert">×</button>
					<strong>Info!</strong> <?php echo $this->session->flashdata('info'); ?>
				</div>
				<?php } ?>
				<?php if($this->session->flashdata('error')) { ?>
				<div class="alert alert-error">
					<button class="close" data-dismiss="alert">×</button>
					<strong>Error!</strong> <?php echo $this->session->flashdata('error'); ?>
				</div>
				<?php } ?>
			</div>
		</div>
		<!-- END Alert widget-->
		<?php } ?>
		<!-- BEGIN ADVANCED TABLE widget-->
		<div class="row-fluid">
			<div class="span12">
				<!-- BEGIN EXAMPLE TABLE widget-->
				<div class="widget blue">
					<div class="widget-title">
						<h4><i class="icon-reorder"></i> List All </h4>
						<span class="tools">
							<a href="javascript:;" class="icon-chevron-down"></a>
						</span>
					</div>
					<div class="widget-body">
						<table class="table table-striped table-bordered" id="sample_1">
							<thead>
								<tr>
									<th>Code of A/C</th>
									<th>Name of A/C</th>
									<th class="center">A/C Class</th>
									<th class="center">Created</th>
									<th class="center">Status</th>
									<?php if($this->session->userdata['user_type'] == 'Admin'){?>		<th class="center span3">Action</th><?php }?>
								</tr>
							</thead>   
							<tbody>
								<?php foreach($charts as $chart): ?>
									<tr>
										<td><?php echo $chart['code']; ?></td>
										<td><?php echo $chart['name']; ?></td>
										<td><span class="label label-info"><?php echo $chart['type']; ?></span></td>
										<td class="right"><?php echo date('jS M, Y ', strtotime($chart['created_at'])); ?></td>
										<td class="center"><?php if($chart['status']== 'Active'){ ?><span class="label label-success">Active</span><?php }else{ ?><span class="label label-important">Inactive</span><?php } ?></td>
										<?php if($this->session->userdata['user_type'] == 'Admin'){?>
										<td class="center">
											<a class="btn btn-edit" href="settings/chart_save/<?php echo $chart['id']; ?>"><i class="icon-edit icon-white"></i> Edit</a>
											<a class="btn del btn-danger" href="settings/chart_delete/<?php echo $chart['id']; ?>"><i class="icon-trash icon-white"></i> Delete</a>
										</td>
										<?php }?>
									</tr>
								<?php endforeach; ?>
							</tbody>
						</table>
					</div>
				</div>
				<!-- END EXAMPLE TABLE widget-->
			</div>
		</div>

		<!-- END ADVANCED TABLE widget-->
	</div>
	<!-- END PAGE CONTAINER-->
</div>