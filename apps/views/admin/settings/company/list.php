<div id="main-content">
	<!-- BEGIN PAGE CONTAINER-->
	<div class="container-fluid">
		<!-- BEGIN PAGE HEADER-->   
		<div class="row-fluid">
			<div class="span12">
				<!-- BEGIN PAGE TITLE & BREADCRUMB-->
				<h3 class="page-title">
					Company
				</h3>
				<ul class="breadcrumb">
					<li>
						<a href="dashboard">Dashboard</a>
						<span class="divider">/</span>
					</li>
					<li>
						<a href="settings">Settings</a>
						<span class="divider">/</span>
					</li>
					<li class="active">
						List All
					</li>
				</ul>
				<!-- END PAGE TITLE & BREADCRUMB-->
			</div>
		</div>
		<!-- END PAGE HEADER-->
		<?php if($this->session->flashdata('success') || $this->session->flashdata('info') || $this->session->flashdata('error')) { ?>
		<!-- BEGIN Alert widget-->
		<div class="row-fluid">
			<div class="span12">
				<?php if($this->session->flashdata('success')) { ?>
				<div class="alert alert-success">
					<button class="close" data-dismiss="alert">×</button>
					<strong>Success!</strong> <?php echo $this->session->flashdata('success'); ?>
				</div>
				<?php } ?>
				<?php if($this->session->flashdata('info')) { ?>
				<div class="alert alert-info">
					<button class="close" data-dismiss="alert">×</button>
					<strong>Info!</strong> <?php echo $this->session->flashdata('info'); ?>
				</div>
				<?php } ?>
				<?php if($this->session->flashdata('error')) { ?>
				<div class="alert alert-error">
					<button class="close" data-dismiss="alert">×</button>
					<strong>Error!</strong> <?php echo $this->session->flashdata('error'); ?>
				</div>
				<?php } ?>
			</div>
		</div>
		<!-- END Alert widget-->
		<?php } ?>
		<!-- BEGIN ADVANCED TABLE widget-->
		<div class="row-fluid">
			<div class="span12">
				<!-- BEGIN EXAMPLE TABLE widget-->
				<div class="widget blue">
					<div class="widget-title">
						<h4><i class="icon-reorder"></i> Company List</h4>
						<span class="tools">
							<a href="javascript:;" class="icon-chevron-down"></a>
						</span>
					</div>
					<div class="widget-body">
						<table class="table table-striped table-bordered" id="sample_1">
							<thead>
								<tr>
									<th>Name</th>
									<th>Address</th>
									<th>Contact Person</th>
									<th>Contact Number</th>
									<th>Email Address</th>
									<?php if($this->session->userdata['user_type'] == 'Admin'){?>		<th class="center span3">Action</th><?php }?>
								</tr>
							</thead>
							<tbody>
								<?php
								foreach ($companies as $company) {
							

									if($this->session->userdata('user_company') == 1){
									?>
									<tr>
										<td align="left"><?php echo $company['name']; ?></td>
										<td align="left"><?php echo $company['address']; ?></td>
										<td align="left"><?php echo $company['contact_person']; ?></td>
										<td align="left"><?php echo $company['mobile_no']; ?></td>
										<td align="left"><?php echo $company['email']; ?></td>
										<?php if($this->session->userdata['user_type'] == 'Admin'){?>
										<td class="actions center">
											<a class="btn btn-primary" href="settings/cmp_set/<?php echo $company['id']; ?>"><i class="icon-map-marker icon-white"></i> Set As</a>
											<a class="btn btn-edit" href="settings/cmp_save/<?php echo $company['id']; ?>"><i class="icon-edit icon-white"></i> Edit</a>
											<a class="btn btn-danger del" href="settings/cmp_delete/<?php echo $company['id']; ?>"><i class="icon-trash icon-white"></i> Delete</a>
										</td>
										<?php }?>
									</tr>
									<?php
								}elseif($company['id'] != 1){ ?>
  
								<tr>
										<td align="left"><?php echo $company['name']; ?></td>
										<td align="left"><?php echo $company['address']; ?></td>
										<td align="left"><?php echo $company['contact_person']; ?></td>
										<td align="left"><?php echo $company['mobile_no']; ?></td>
										<td align="left"><?php echo $company['email']; ?></td>
										<?php if($this->session->userdata['user_type'] == 'Admin'){?>
										<td class="actions center">
											<a class="btn btn-primary" href="settings/cmp_set/<?php echo $company['id']; ?>"><i class="icon-map-marker icon-white"></i> Set As</a>
											<a class="btn btn-edit" href="settings/cmp_save/<?php echo $company['id']; ?>"><i class="icon-edit icon-white"></i> Edit</a>
											<a class="btn btn-danger del" href="settings/cmp_delete/<?php echo $company['id']; ?>"><i class="icon-trash icon-white"></i> Delete</a>
										</td>
										<?php }?>
									</tr>
						<?php	}
						}
								?>
							</tbody>
						</table>
					</div>
				</div>
				<!-- END EXAMPLE TABLE widget-->
			</div>
		</div>

		<!-- END ADVANCED TABLE widget-->
	</div>
	<!-- END PAGE CONTAINER-->
</div>