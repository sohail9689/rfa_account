<div id="main-content">
	<!-- BEGIN PAGE CONTAINER-->
	<div class="container-fluid">
		<!-- BEGIN PAGE HEADER-->
		<div class="row-fluid">
			<div class="span12">
				<!-- BEGIN PAGE TITLE & BREADCRUMB-->
				<h3 class="page-title">
					Company
				</h3>
				<ul class="breadcrumb">
					<li>
						<a href="dashboard">Dashboard</a>
						<span class="divider">/</span>
					</li>
					<li>
						<a href="settings">Settings</a>
						<span class="divider">/</span>
					</li>
					<li class="active">
						<?php if(count($company) > 0){ ?>Edit <?php }else{ ?>Add New<?php } ?> Company
					</li>
				</ul>
				<!-- END PAGE TITLE & BREADCRUMB-->
			</div>
		</div>
		<!-- END PAGE HEADER-->

		<!-- BEGIN PAGE CONTENT-->
		<div class="row-fluid">
			<div class="span12">
				<!-- BEGIN SAMPLE FORM PORTLET-->
				<div class="widget blue">
					<div class="widget-title">
						<h4><i class="icon-reorder"></i> Entry Form </h4>
						<span class="tools">
							<a href="javascript:;" class="icon-chevron-down"></a>
						</span>
					</div>
					<div class="widget-body">
						<!-- BEGIN FORM-->
						<form action="settings/cmp_save" method="post" enctype="multipart/form-data" class="form-horizontal form-bordered form-validate" id="frm1">
                            <div class="control-group">
                                <label for="name" class="control-label">Company Name</label>
                                <div class="controls">
                                    <input type="text" name="name" value="<?php if(count($company) > 0){ echo $company['name']; } ?>" id="name" class="input-xlarge" data-rule-required="true" data-rule-required="true" />
                                </div>
                            </div>
                            <div class="control-group">
                                <label for="currency_id" class="control-label">Select Currency</label>
                                <div class="controls">
                                    <select name="currency_id" id="currency_id" data-rule-required="true">
                                        <?php foreach($currencies as $currency) { ?>
                                        <option value="<?php echo $currency['id']; ?>" <?php if (count($company) > 0 && $company['currency_id'] == $currency['id']) { echo 'selected'; }?>><?php echo $currency['fullname'] . ' - ( ' . $currency['symbol'] . ' )'; ?></option>
                                        <?php } ?>
                                    </select>
                                </div>
                            </div>
                            <div class="control-group">
                                <label for="currency_symbol_position" class="control-label">Symbol Position</label>
                                <div class="controls">
                                    <select name="currency_symbol_position" id="currency_symbol_position" data-rule-required="true">
                                        <option value="Before" <?php if (count($company) > 0 && $company['currency_symbol_position'] == 'Before') { echo 'selected'; }?>>Before</option>
                                        <option value="After" <?php if (count($company) > 0 && $company['currency_symbol_position'] == 'After') { echo 'selected'; }?>>After</option>
                                    </select>
                                </div>
                            </div>
                            <?php if(count($company) > 0 && $company['logo'] != '') { ?>
                            <div class="control-group">
                                <label for="address" class="control-label">Current Logo</label>
                                <div class="controls">
                                    <img src="uploads/companies/<?php echo $company['logo']; ?>">
                                </div>
                            </div>
                            <?php } ?>
                            <div class="control-group">
                                <label for="address" class="control-label">Company Logo</label>
                                <div class="controls">
                                    <input type="file" name="logo">
                                </div>
                            </div>
                            <div class="control-group">
                                <label for="address" class="control-label">Address</label>
                                <div class="controls">
                                    <textarea name="address" id="description" class="input-xlarge"><?php if(count($company) > 0){ echo $company['address']; } ?></textarea>
                                </div>
                            </div>
                            <div class="control-group">
                                <label for="area" class="control-label">Area</label>
                                <div class="controls">
                                    <input type="text" name="area" value="<?php if(count($company) > 0){ echo $company['area']; } ?>" id="re_orderfield" class="input-xlarge" data-rule-required="true" data-rule-required="true" />
                                </div>
                            </div>
                            <div class="control-group">
                                <label for="city" class="control-label">City</label>
                                <div class="controls">
                                    <input type="text" name="city" value="<?php if(count($company) > 0){ echo $company['city']; } ?>" id="re_orderfield" class="input-xlarge" data-rule-required="true" data-rule-required="true" />
                                </div>
                            </div>
                            <div class="control-group">
                                <label for="zip" class="control-label">Zip</label>
                                <div class="controls">
                                    <input type="text" name="zip" value="<?php if(count($company) > 0){ echo $company['zip']; } ?>" id="re_orderfield" class="input-xlarge" data-rule-required="true" data-rule-required="true" />
                                </div>
                            </div>
                            <div class="control-group">
                                <label for="country" class="control-label">Country</label>
                                <div class="controls">
                                    <input type="text" name="country" value="<?php if(count($company) > 0){ echo $company['country']; } ?>" id="re_orderfield" class="input-xlarge" data-rule-required="true" data-rule-required="true" />
                                </div>
                            </div>
                            <div class="control-group">
                                <label for="phone" class="control-label">Phone Number</label>
                                <div class="controls">
                                    <input type="text" name="phone" value="<?php if(count($company) > 0){ echo $company['phone']; } ?>" id="re_orderfield" class="input-xlarge" data-rule-required="true" data-rule-required="true" />
                                </div>
                            </div>
                            <div class="control-group">
                                <label for="contact_person" class="control-label">Contact Person</label>
                                <div class="controls">
                                    <input type="text" name="contact_person" value="<?php if(count($company) > 0){ echo $company['contact_person']; } ?>" id="purchasefield" class="input-xlarge" data-rule-required="true" data-rule-required="true" />
                                </div>
                            </div>
                            <div class="control-group">
                                <label for="mobile_no" class="control-label">Contact Number</label>
                                <div class="controls">
                                    <input type="text" name="mobile_no" value="<?php if(count($company) > 0){ echo $company['mobile_no']; } ?>" id="purchasefield" class="input-xlarge" data-rule-required="true" data-rule-required="true" />
                                </div>
                            </div>
                            <div class="control-group">
                                <label for="email" class="control-label">Email</label>
                                <div class="controls">
                                    <input type="text" name="email" value="<?php if(count($company) > 0){ echo $company['email']; } ?>" id="salefield" class="input-xlarge" data-rule-required="true" data-rule-required="true" />
                                </div>
                            </div>
                            <?php if( ! $company) { ?>
                            <div class="control-group">
                                <label for="password" class="control-label">Password</label>
                                <div class="controls">
                                    <input type="password" name="password" value="" id="salefield" class="input-xlarge" data-rule-required="true" data-rule-required="true" />
                                </div>
                            </div>
                            <?php } ?>
                            <div class="control-group">
                                <label for="status" class="control-label">Select Status</label>
                                <div class="controls">
                                    <select name="status" id="status" data-rule-required="true">
                                        <option value="Active" <?php if (count($company) > 0 && $company['status'] == 'Active') { echo 'selected'; }?>>Active</option>
                                        <option value="Inactive" <?php if (count($company) > 0 && $company['status'] == 'Inactive') { echo 'selected'; }?>>Inactive</option>
                                    </select>
                                </div>
                            </div>
                            <input type="hidden" name="<?php echo $this->security->get_csrf_token_name(); ?>" value="<?php echo $this->security->get_csrf_hash(); ?>" />
                            <?php if(count($company) > 0){ ?>
                            <input type="hidden" name="id" value="<?php echo $company['id']; ?>" />
                            <?php } ?>
                            <div class="form-actions">
                                <input type="submit" class="btn btn-success" value="Save Changes">
                                <button type="reset" class="btn">Cancel</button>
                            </div>
                        </form>
                        <!-- END FORM-->
                    </div>
                </div>
                <!-- END SAMPLE FORM PORTLET-->
            </div>
        </div>

        <!-- END PAGE CONTAINER-->
    </div>
    <!-- END PAGE -->
</div>

<script>
  $('#name').focus();
</script>