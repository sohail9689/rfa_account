<div id="main-content">
	<!-- BEGIN PAGE CONTAINER-->
	<div class="container-fluid">
		<!-- BEGIN PAGE HEADER-->
		<div class="row-fluid">
			<div class="span12">
				<!-- BEGIN PAGE TITLE & BREADCRUMB-->
				<h3 class="page-title">
					Currency
				</h3>
				<ul class="breadcrumb">
					<li>
						<a href="dashboard">Dashboard</a>
						<span class="divider">/</span>
					</li>
					<li>
						<a href="settings">Settings</a>
						<span class="divider">/</span>
					</li>
					<li class="active">
						<?php if(count($currency) > 0){ ?>Edit <?php }else{ ?>Add New<?php } ?> Currency
					</li>
				</ul>
				<!-- END PAGE TITLE & BREADCRUMB-->
			</div>
		</div>
		<!-- END PAGE HEADER-->

		<!-- BEGIN PAGE CONTENT-->
		<div class="row-fluid">
			<div class="span12">
				<!-- BEGIN SAMPLE FORM PORTLET-->
				<div class="widget blue">
					<div class="widget-title">
						<h4><i class="icon-reorder"></i> Entry Form </h4>
						<span class="tools">
							<a href="javascript:;" class="icon-chevron-down"></a>
						</span>
					</div>
					<div class="widget-body">
						<!-- BEGIN FORM-->
						<form action="settings/currency_save" method="post" class="form-horizontal form-bordered form-validate" id="frm1">
                            <div class="control-group">
                                <label for="fullname" class="control-label">Country Name</label>
                                <div class="controls">
                                    <input type="text" name="fullname" value="<?php if(count($currency) > 0){ echo $currency['fullname']; } ?>" id="fullname" class="input-xlarge" data-rule-required="true" data-rule-required="true" />
                                </div>
                            </div>
                            <div class="control-group">
                                <label for="shortname" class="control-label">Short Form</label>
                                <div class="controls">
                                    <input type="text" name="shortname" value="<?php if(count($currency) > 0){ echo $currency['shortname']; } ?>" id="re_orderfield" class="input-xlarge" data-rule-required="true" data-rule-required="true" />
                                </div>
                            </div>
                            <div class="control-group">
                                <label for="symbol" class="control-label">Symbol</label>
                                <div class="controls">
                                    <input type="text" name="symbol" value="<?php if(count($currency) > 0){ echo $currency['symbol']; } ?>" id="re_orderfield" class="input-xlarge" data-rule-required="true" data-rule-required="true" />
                                </div>
                            </div>
                            <div class="control-group">
                                <label for="status" class="control-label">Select Status</label>
                                <div class="controls">
                                    <select name="status" class="status_" id="status" data-rule-required="true">
                                        <option value="active" <?php if (count($currency) > 0 && $currency['status'] == 'Active') { echo 'selected'; }?>>Active</option>
                                        <option value="inactive" <?php if (count($currency) > 0 && $currency['status'] == 'Inactive') { echo 'selected'; }?>>Inactive</option>
                                    </select>
                                </div>
                            </div>
                            <input type="hidden" name="<?php echo $this->security->get_csrf_token_name(); ?>" value="<?php echo $this->security->get_csrf_hash(); ?>" />
                            <?php if(count($currency) > 0) { ?>
                                <input type="hidden" name="id" value="<?php echo $currency['id']; ?>">
                            <?php } ?>
                            <div class="form-actions">
                                <input type="submit" class="btn btn-success" value="Save Changes">
                                <button type="reset" class="btn">Cancel</button>
                            </div>
                        </form>
                        <!-- END FORM-->
                    </div>
                </div>
                <!-- END SAMPLE FORM PORTLET-->
            </div>
        </div>

        <!-- END PAGE CONTAINER-->
    </div>
    <!-- END PAGE -->
</div>

<script>
    $('#fullname').focus();
</script>