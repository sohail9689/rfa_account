<div id="main-content">
	<!-- BEGIN PAGE CONTAINER-->
	<div class="container-fluid">
		<!-- BEGIN PAGE HEADER-->
		<div class="row-fluid">
			<div class="span12">
				<!-- BEGIN PAGE TITLE & BREADCRUMB-->
				<h3 class="page-title">
					User
				</h3>
				<ul class="breadcrumb">
					<li>
						<a href="dashboard">Dashboard</a>
						<span class="divider">/</span>
					</li>
					<li>
						<a href="user">User</a>
						<span class="divider">/</span>
					</li>
					<li class="active">
						List All
					</li>
				</ul>
				<!-- END PAGE TITLE & BREADCRUMB-->
			</div>
		</div>
		<!-- END PAGE HEADER-->

		<!-- BEGIN Alert widget-->
		<?php if($this->session->flashdata('success') || $this->session->flashdata('info') || $this->session->flashdata('error')) { ?>
		<div class="row-fluid">
			<div class="span12">
				<?php if($this->session->flashdata('success')) { ?>
				<div class="alert alert-success">
					<button class="close" data-dismiss="alert">×</button>
					<strong>Success!</strong> <?php echo $this->session->flashdata('success'); ?>
				</div>
				<?php } ?>
				<?php if($this->session->flashdata('info')) { ?>
				<div class="alert alert-info">
					<button class="close" data-dismiss="alert">×</button>
					<strong>Info!</strong> <?php echo $this->session->flashdata('info'); ?>
				</div>
				<?php } ?>
				<?php if($this->session->flashdata('error')) { ?>
				<div class="alert alert-error">
					<button class="close" data-dismiss="alert">×</button>
					<strong>Error!</strong> <?php echo $this->session->flashdata('error'); ?>
				</div>
				<?php } ?>
			</div>
		</div>
		<?php } ?>
		<!-- END Alert widget-->

		<!-- BEGIN ADVANCED TABLE widget-->
		<div class="row-fluid">
			<div class="span12">
				<!-- BEGIN EXAMPLE TABLE widget-->
				<div class="widget blue">
					<div class="widget-title">
						<h4><i class="icon-reorder"></i> User List</h4>
						<span class="tools">
							<a href="javascript:;" class="icon-chevron-down"></a>
						</span>
					</div>
					<div class="widget-body">
						<table class="table table-striped table-bordered" id="sample_1">
							<thead>
								<tr>
									<th>Name</th>
									<th>Email</th>
									<th>Company</th>
									<th>Created</th>
									<th class="center">Status</th>
									<th class="center">User Type</th>
									<th class="span5 center">Action</th>
								</tr>
							</thead>
							<tbody>
								<?php foreach ($users as $user) { 
									
									if($this->session->userdata('user_id') == 1){
									        
									?>
									<tr>

									<td><?php echo $user['name']; ?></td>
									<td><?php echo $user['email']; ?></td>
									<td><?php echo $user['c_name']; ?></td>
									<td><?php echo $user['created']; ?></td>
									<td class="center"><?php echo $user['status']; ?></td>
									<td class="center"><?php echo $user['type']; ?></td>
									<td class="center">
										<?php if ($this->session->userdata('user_type') != 'User'): ?>
											<a class="btn btn-warning" href="user/privileges/<?php echo $user['id']; ?>"><i class="icon-lock icon-white"></i> Permission</a>
										<?php endif; ?>
										<a class="btn btn-edit" href="user/save/<?php echo $user['id']; ?>"><i class="icon-edit icon-white"></i> Edit</a>
										<a class="btn btn-danger del" href="user/delete/<?php echo $user['id']; ?>"><i class="icon-trash icon-white"></i> Delete</a>
									</td>
									</tr>
									<?php } elseif($user['id'] !=1 ){
										?>
										<tr>

									<td><?php echo $user['name']; ?></td>
									<td><?php echo $user['email']; ?></td>
									<td><?php echo $user['c_name']; ?></td>
									<td><?php echo $user['created']; ?></td>
									<td class="center"><?php echo $user['status']; ?></td>
									<td class="center"><?php echo $user['type']; ?></td>
									<td class="center">
										<?php if ($this->session->userdata('user_type') != 'User'): ?>
											<a class="btn btn-warning" href="user/privileges/<?php echo $user['id']; ?>"><i class="icon-lock icon-white"></i> Permission</a>
										<?php endif; ?>
										<a class="btn btn-edit" href="user/save/<?php echo $user['id']; ?>"><i class="icon-edit icon-white"></i> Edit</a>
										<a class="btn btn-danger del" href="user/delete/<?php echo $user['id']; ?>"><i class="icon-trash icon-white"></i> Delete</a>
									</td>
									</tr>
									<?php 
									} 
									}?>

							</tbody>
						</table>
					</div>
				</div>
				<!-- END EXAMPLE TABLE widget-->
			</div>
		</div>

		<!-- END ADVANCED TABLE widget-->
	</div>
	<!-- END PAGE CONTAINER-->
</div> 