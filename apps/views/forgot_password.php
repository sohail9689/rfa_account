<!DOCTYPE html>
<!--[if IE 8]> <html lang="en" class="ie8"> <![endif]-->
<!--[if IE 9]> <html lang="en" class="ie9"> <![endif]-->
<!--[if !IE]><!--> <html lang="en"> <!--<![endif]-->
<!-- BEGIN HEAD -->
<head>
    <meta charset="utf-8" />
    <title><?php echo $title; ?></title>
    <base href="<?php echo base_url(); ?>" />
    <meta content="width=device-width, initial-scale=1.0" name="viewport" />
    <meta content="" name="description" />
    <meta content="" name="author" />
    <link rel="shortcut icon" href="uploads/companies/2.jpg" type="image/x-icon" />
    <link href="assets/backend/assets/bootstrap/css/bootstrap.min.css" rel="stylesheet" />
    <link href="assets/backend/assets/bootstrap/css/bootstrap-responsive.min.css" rel="stylesheet" />
    <link href="assets/backend/assets/font-awesome/css/font-awesome.css" rel="stylesheet" />
        <link href="assets/backend/css/stylles.css" rel="stylesheet" /> 
    <link href="assets/backend/css/style-responsive.css" rel="stylesheet" />
    <link href="assets/backend/css/style-default.css" rel="stylesheet" id="style_color" />




    <style type="text/css">
       body{
  background-color:#fff;

}

</style>
</head>
<!-- END HEAD -->
<!-- BEGIN BODY -->
<body class="lock">
    <!-- <div class="lock-header"> -->
        <!-- BEGIN LOGO -->
       <!--  <a class="center" id="logo" href="">
            <img class="center" alt="logo" src="assets/backend/img/logo.png" width="250"> 
        </a>
    
    </div> -->
 
    <div class="forget-wrap">
        <?php if($this->session->flashdata('error')) { ?>
        <div class="alert alert-error" style="margin: 0px 10px 10px 0px;">
            <strong>Error!</strong> <?php echo $this->session->flashdata('error'); ?>
        </div>
        <?php } ?>

   <div class="row" style="margin-top: 5%;">
                                <div class="span6"></div>
                                <div class="span4 text-center">
                                   <a  id="logo" href="">
                                      <img  alt="logo"  src="uploads/companies/2.jpg" width="280" height="250"  > 
                                  </a>
                                </div>
                            </div>
                             <br> 
        <div class="row">
        <div class="span6"></div>
        <div class="span4">  
        <div class="forget_pass">
     <div>
     <br><br>
        <b style="font-size: 22px; color:white;  ">Forgot Password</b></div>
        <br><br>
        <form action="login/forgot-password" method="post">
           
                    <input type="text" name="email" value="" tabindex="1" autofocus placeholder="User Email">
         <
            <input type="hidden" name="<?php echo $this->security->get_csrf_token_name(); ?>" value="<?php echo $this->security->get_csrf_hash(); ?>" />
        
                <input type="submit" name="submit" tabindex="3" value="Send Reset Link" class="ppaging">
                    
                    
            <div id="llink">
            
                <a id="forget-password"   href="" style="color:black;">Go Back.</a>
            </div>
        </form>
        </div>
        </div></div>
        </div>
    </div>
</body>
<!-- END BODY -->
</html>