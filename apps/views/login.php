<!DOCTYPE html>
<!--[if IE 8]> <html lang="en" class="ie8"> <![endif]-->
<!--[if IE 9]> <html lang="en" class="ie9"> <![endif]-->
<!--[if !IE]><!--> <html lang="en"> <!--<![endif]-->
<!-- BEGIN HEAD -->
<head>
    <meta charset="utf-8" />
    <title><?php echo $title; ?>info login</title>
    


    <base href="<?php echo base_url(); ?>" />
    <meta content="width=device-width, initial-scale=1.0" name="viewport" />
    <meta content="" name="description" />
    <meta content="" name="author" />
    <link rel="shortcut icon" href="uploads/companies/2.png" type="image/x-icon" />
    <link href="assets/backend/assets/bootstrap/css/bootstrap.min.css" rel="stylesheet" />
    <link href="assets/backend/assets/bootstrap/css/bootstrap-responsive.min.css" rel="stylesheet" />
    <link href="assets/backend/assets/font-awesome/css/font-awesome.css" rel="stylesheet" />
     
    <link href="assets/backend/css/style-responsive.css" rel="stylesheet" />
    <link href="assets/backend/css/style-default.css" rel="stylesheet" id="style_color" />
    <link href="assets/backend/css/styles.css" rel="stylesheet" />
    <link href="assets/backend/assets/fullcalendar/fullcalendar/bootstrap-fullcalendar.css" rel="stylesheet" />
    <link href="assets/backend/assets/datepicker/datepicker.css" rel="stylesheet">

       <style type="text/css">
       body{
  background-color: #fff;

}

</style>
 <script src="assets/js/jquery-1.11.1.min.js"></script>
  
</head>
<!-- END HEAD -->
<!-- BEGIN BODY -->
<body>


     <div >
        <!-- BEGIN LOGO -->
       
            <!--  -->
        <!-- </a></div>
      <!-- END LOGO   -->
  
    <div class="login-wrap">
        <?php if($this->session->flashdata('success')) { ?>
        <div class="alert alert-success">
            <button class="close" data-dismiss="alert">×</button>
            <strong>Success!</strong> <?php echo $this->session->flashdata('success'); ?>
        </div>
        <?php } ?>
        <?php if($this->session->flashdata('info')) { ?>
        <div class="alert alert-info" style="margin: 0px 10px 10px 0px;">
            <button class="close" data-dismiss="alert">×</button>
            <strong>Info!</strong> <?php echo $this->session->flashdata('info'); ?>
        </div>
        <?php } ?>
        <?php if($this->session->flashdata('error')) { ?>
        <div class="alert alert-error" style="margin: 0px 10px 10px 0px;">
            <strong>Error!</strong> <?php echo $this->session->flashdata('error'); ?>
        </div>
        <?php } ?>
            <div class="row" style="margin-top: 5%;">
                            
                                <div class="span6"></div>
                                <div class="span4 text-center">
                                   <a  id="logo" href="">
                                      <img  alt="logo" src="uploads/companies/2.png" width="50" height="50"  > 
                                  </a>
                                </div>
                            
                        </div> 
                        <br>  
        <div class="row">
          
        <div class="span6"></div>
        <div class="span4">              
        <div class="aa">
            
        <br><br>

          <b>Login</b>
          <br><br>
        <form action="login" method="post">
            <?php 
                $current_date = strtotime(date('d-m-Y'));
                $june_date = strtotime(date('02-07-Y'));
                $current_july_date = date('01/07/Y');
                $prev_july_date = date('01/07/Y', strtotime("-1 year")); 
                $current_june = strtotime(date('02-07-Y'));
                $next_june_date = date('30/06/Y', strtotime("+1 year"));
                $currnt_june_date = date('30/06/Y'); 
             ?>
                      <div class="col-sm-12">
                      <input type="text" name="email" value=""  class=" form-control"  style="" autofocus placeholder="Email" ><br>
                   
                        <input type="password" name="password" value=""  placeholder="Password" class="form-control"><br>
                    <input name="start_date" readonly="readonly" class="form-control" id="start_date" data-form="datepicker" placeholder="Start Date" type="text" value="<?php 
                    if($june_date <= $current_date){
                        
                        echo $current_july_date;
                    }elseif($june_date >= $current_date){
                        echo  $prev_july_date;
                    }
                     ?>" ><br>
                    <input name="end_date" id="end_date" readonly="readonly" data-form="datepicker" placeholder="End Date" type="text" class="form-control" value="<?php 
                    if($current_june <= $current_date){
                        
                        echo $next_june_date;
                    }elseif($current_june >= $current_date){
                        echo  $currnt_june_date;
                    }
                     ?>">  
             <input type="hidden" name="<?php echo $this->security->get_csrf_token_name(); ?>" value="<?php echo $this->security->get_csrf_hash(); ?>" /> <br>
            
                <input type="submit" name="submit" class="paging"  ><br><br>
                <div style="color: #000;text-align:center;">
                    <input type="checkbox"  />Remember Me
                </div>
               <a   href="login/forgot-password" class="link" >Forgot Password?</a>
               <br> 
                     
                
            

            
            
        </form>
        </div>
        </div></div>
       </div>
         </div>
        <div class="login-footer">
         <div class="remember-hint pull-left">
                           </div> 
           
        </div>
  
  </div>
  <?php $this->load->view('admin/js'); ?>
  <script>
  (function (){
  var rep = /.*\?.*/,
      links = document.getElementsByTagName('link'),
      scripts = document.getElementsByTagName('script'),
      process_scripts = false;
  for (var i=0;i<links.length;i++){
    var link = links[i],
        href = link.href;
    if(rep.test(href)){
      link.href = href+'&'+Date.now();
    }
    else{
      link.href = href+'?'+Date.now();
    }

  }
  if(process_scripts){
    for (var i=0;i<scripts.length;i++){
      var script = scripts[i],
          src = script.src;
      if(rep.test(src)){
        script.src = src+'&'+Date.now();
      }
      else{
        script.src = src+'?'+Date.now();
      }

    }
  }
})();
</script>
</div>

</body>
<!-- END BODY -->
</html>
